
import Foundation
import Alamofire


enum ErrorCode:Int {
    case Caneled = -999
    case NoInternet = -1009
    case UnKnown = 000
    case Parsing = -10008

}
//MARK:- MESSAGES
let CONNECTION_ERROR = "Connection Error"
let EMPTY_DATA = _EmptyData
let TURN_LOCATION = "Turn on location services"
let SETTING = "Setting"
let SEND_SUCCESSFULLY = "Send Successfully"
let ENTER_VALID_EMAIL = "Enter a valid Email"
let NO_NOTIFICATIONS = "No Notifications at the moment."
let NOT_ALLOW_NUMBER = "This app is not allowed to Call Numbers"
let CHAT = "Chat"
let WRONG_NUMBER = "Phone Number Not Valid"
let CODE_SEND = "Code Send Successfully"
let WRONG_CODE = "Not Valid Code"


typealias errorType = (AFError?) -> ()
typealias errorCompletionType = ((ErrorCode?)->())


class APIManager {
    
    
    func
        
        requestWith(upload data: Data,to url:String,withkey name:String ,andExtension extn:(type:String,ext:String),forbody parameters: [String : Any], success: @escaping  (Any) -> (), errorHandler:  @escaping errorType){
        
        let headers: HTTPHeaders = [
            "lang": DEF.language,
            "jwt": "\(DEF.jwt)",
            "Content-type": "multipart/form-data"
        ]
        print("para ==>>",parameters)
        print("#URL",url.encodeUrl())
        print("#header",headers)
        print("#body",parameters )
        AF.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters { // this for loop for appendding (parameters Dictionary) to (multipartFormData Array)
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            multipartFormData.append(data, withName: name, fileName: "\(Date().timeString()).\(extn.ext)", mimeType: "\(extn.type)/\(extn.ext)")
            
            }, to: url, usingThreshold: UInt64.init(), method: .post, headers: headers).responseJSON { (response) in
            if response.error != nil {
                print("@PIManger\( response.error)")
                if let errorCodeValue = response.error?._code,
                    let errorCode = ErrorCode(rawValue: errorCodeValue){
                    errorHandler(response.error)
                } else {
                    errorHandler(response.error)
                }
                return
            }
            
            if response.data?.count == 0 {
                let noData = AFError.responseValidationFailed(reason: .dataFileNil)
                errorHandler(noData)
                return
            }
            if let responseValue = response.value {
                success(responseValue)
            }
        }
    }
    
    
    func requestWith(upload data: Data,capture:Data,to url:String,withkey name:String ,andExtension extn:(type:String,ext:String),forbody parameters: [String : Any], success: @escaping  (Any) -> (), errorHandler:  @ escaping errorType){
        
        let headers: HTTPHeaders = [
            "lang": DEF.language,
            "jwt": "\(DEF.jwt)",
            "Content-type": "multipart/form-data"
        ]
        print("para ==>>",parameters)
        print("#URL",url.encodeUrl())
        print("#header",headers)
        print("#body",parameters)
        AF.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters { // this for loop for appendding (parameters Dictionary) to (multipartFormData Array)
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            multipartFormData.append(data, withName: name, fileName: "\(Date().timeString()).\(extn.ext)", mimeType: "\(extn.type)/\(extn.ext)")
            multipartFormData.append(capture, withName: "capture", fileName: "\(Date().timeString()).jpeg", mimeType: "image/jpeg")
            
            
            }, to: url, usingThreshold: UInt64.init(), method: .post, headers: headers).responseJSON { (response) in
   
            if response.error != nil {
                print("@PIManger\( response.error)")
                if let errorCodeValue = response.error?._code,
                    let errorCode = ErrorCode(rawValue: errorCodeValue){
                    errorHandler(response.error)
                } else {
                    errorHandler(response.error)
                }
                return
            }
            
            if response.data?.count == 0 {
                let noData = AFError.responseValidationFailed(reason: .dataFileNil)
                errorHandler(noData)
                return
            }
            if let responseValue = response.value {
                success(responseValue)
            }

        }
    }
    
    func requestWith(upload datas: [Data?],key:String,to url:String ,mimeType mime:String,forbody parameters: [String : Any], success: @escaping  (Any) -> (), errorHandler:  @ escaping errorType){
        
        let headers: HTTPHeaders = [
            "lang": DEF.language,
            "jwt": "\(DEF.jwt)",
            "Content-type": "multipart/form-data"
        ]
        print("para ==>>",parameters)
        print("#URL",url.encodeUrl())
        print("#header",headers)
        print("#body",parameters )
        AF.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters { // this for loop for appendding (parameters Dictionary) to (multipartFormData Array)
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            for i in 0..<datas.count {
                guard let data = datas[i] else { return  }
                print("#dt = \(data),#key = \(key)")
                
                multipartFormData.append(data, withName: key + "[]", fileName: "\(Date().timeString()).jpeg", mimeType: "\(mime)/jpeg")
            }
            
            }, to: url.encodeUrl(), usingThreshold: UInt64.init(), method: .post, headers: headers).responseJSON { (response) in
           if response.error != nil {
               print("@PIManger\( response.error)")
               if let errorCodeValue = response.error?._code,
                   let errorCode = ErrorCode(rawValue: errorCodeValue){
                   errorHandler(response.error)
               } else {
                   errorHandler(response.error)
               }
               return
           }
           
           if response.data?.count == 0 {
            let noData = AFError.responseValidationFailed(reason: .dataFileNil)
               errorHandler(noData)
               return
           }
           if let responseValue = response.value {
               success(responseValue)
           }

        }
    }
    
    
    func requestWith(upload dataArray: [(data:Data,key:String,ext:String)],to url:String ,forbody parameters: [String : Any], success: @escaping  (Any) -> (), errorHandler:  @ escaping errorType){
        
        let headers: HTTPHeaders = [
            "lang": DEF.language,
            "jwt": "\(DEF.jwt)",
            "Content-type": "multipart/form-data"
        ]
        print("#toUrl ==>>",url.encodeUrl())
        
        print("#header ==>>",headers)
        print("#para ==>>",parameters)
        print("#URL",url)
        print("#header",headers)
        print("#body",parameters)
        AF.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters { // this for loop for appendding (parameters Dictionary) to (multipartFormData Array)
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            for item in dataArray {
                print("#dt = \(item.data),#key = \(item.key)")
                multipartFormData.append(item.data, withName: item.key, fileName: "\(Date().timeString()).\(item.ext)", mimeType: "image/\(item.ext)")
            }
            
            }, to: url.encodeUrl(), usingThreshold: UInt64.init(), method: .post, headers: headers).responseJSON { (response) in
            if response.error != nil {
                print("@PIManger\( response.error)")
                if let errorCodeValue = response.error?._code,
                    let errorCode = ErrorCode(rawValue: errorCodeValue){
                    errorHandler(response.error)
                } else {
                    errorHandler(response.error)
                }
                return
            }
            
            if response.data?.count == 0 {
                let noData = AFError.responseValidationFailed(reason: .dataFileNil)
                errorHandler(noData)
                return
            }
            if let responseValue = response.value {
                success(responseValue)
            }
        }
    }
    
    
    func requestWith(upload data_array: [Data],array_key:String ,parent_data :Data,parent_key:String,to url:String ,mimeType mime:(String,String),forbody parameters: [String : Any], success: @escaping  (Any) -> (), errorHandler:  @ escaping errorType){
        
        let headers: HTTPHeaders = [
            "lang": DEF.language,
            "jwt": "\(DEF.jwt)",
            "Content-type": "multipart/form-data"
        ]
        print("para ==>>",parameters)
        print("#URL",url.encodeUrl())
        print("#header",headers)
        print("#body",parameters )
        AF.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters { // this for loop for appendding (parameters Dictionary) to (multipartFormData Array)
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            for data in data_array {
                multipartFormData.append(data, withName: array_key + "[]", fileName: "\(Date().timeString()).\(mime.1)", mimeType: "\(mime.0)/\(mime.1)")
            }
            multipartFormData.append(parent_data, withName: parent_key, fileName: "\(Date().timeString()).\(mime.1)", mimeType: "\(mime.0)/\(mime.1)")
            
            
        }, to: url.encodeUrl(), usingThreshold: UInt64.init(), method: .post, headers: headers).responseJSON{ (response) in
            if response.error != nil {
                print("@PIManger\( response.error)")
                if let errorCodeValue = response.error?._code,
                    let errorCode = ErrorCode(rawValue: errorCodeValue){
                    errorHandler(response.error)
                } else {
                    errorHandler(response.error)
                }
                return
            }
            
            if response.data?.count == 0 {
                let noData = AFError.responseValidationFailed(reason: .dataFileNil)
                errorHandler(noData)
                return
            }
            if let responseValue = response.value {
                success(responseValue)
            }
        }
    }
    
    
    func contectToApiWith(url : String , methodType : HTTPMethod ,params : [String:Any]?, success: @escaping (Any) -> (), errorHandler: @ escaping errorType ) {
        let HEADER: HTTPHeaders = ["lang": DEF.language,
                                   "jwt": DEF.jwt
        ]
        // 1GpdLGBEdajoJKxyjhT7TTEYh
        print("#URL",url.encodeUrl())
        print("#header",HEADER)
        print("#body",params ?? ["NOBODY":"NOBODY"])
        AF.request(url,
                   method: methodType,
                   parameters: params,
                   encoding: URLEncoding.default, headers: HEADER).responseJSON{ response in
                    print("response ==>",response)
                    if response.error != nil {
                        print("@PIManger\( response.error)")
                        if let errorCodeValue = response.error?._code,
                            let errorCode = ErrorCode(rawValue: errorCodeValue){
                            errorHandler(response.error)
                        } else {
                            errorHandler(response.error)
                        }
                        return
                    }
                    
                    if response.data?.count == 0 {
                        let noData = AFError.responseValidationFailed(reason: .dataFileNil)
                        errorHandler(noData)
                        return
                    }
                    if let responseValue = response.value {
                        success(responseValue)
                    }
        }
        
        
    }
    func contectToApiWith(jsonRequest:Bool ,url : String , methodType : HTTPMethod ,params : [String:Any]?, success: @escaping (Any) -> (), errorHandler: @ escaping errorType ) {
        let HEADER: HTTPHeaders = ["lang": DEF.language,
                                   "jwt": "\(DEF.jwt)",
            "Content-Type": "application/json"
        ]
        
        print("#URL",url.encodeUrl())
        print("#header",HEADER)
        print("#body",params ?? ["NOBODY":"NOBODY"])
        AF.request(url,
                   method: methodType,
                   parameters: params,
                   encoding: JSONEncoding.default, headers: HEADER).validate().responseJSON{ response in
                    print("response ==>",response)
                    if response.error != nil {
                        print("@PIManger\( response.error!)")
                        if let errorCodeValue = response.error?._code,
                            let errorCode = ErrorCode(rawValue: errorCodeValue){
                            errorHandler(response.error)
                        } else {
                            errorHandler(response.error)
                        }
                        return
                    }
                    
                    if response.data?.count == 0 {
                        let noData = AFError.responseValidationFailed(reason: .dataFileNil)
                        errorHandler(noData)
                        return
                    }
                    if let responseValue = response.value {
                        success(responseValue)
                    }
        }
        
        
    }
    
    
}
