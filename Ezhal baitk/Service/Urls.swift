

import Foundation

enum API_status: Int {
    case Success                = 200
    case Failed                 = 401
    case JwtExpired             = 405
    case NotActive             = 303
}


enum MyURLs {
    //TODO:- make in-accissible properties private
    private static let USER_URL = "https://ezhel.my-staff.net/api/user/"
    private static let BASE_URL = "https://ezhel.my-staff.net/api/"
    
    // MARK:- Auth Cases
    case Login
    case add_phone
    case delete_phone
    case Register
    case complete
    case send_phone
    case get_phone
    case update_profile
    case countries
    case cities
    case package
    case change_password
    case firebase
    case get_companies_categories
    case notification
    case categories
    case price_filter
    case shop_offers
    case reviews
    case add_review
    case Following
    case terms
    case about_us
    case follow
    case block
    case code_check
    case forget_password
    case search_filter
    case units
    case issues
    case history
    case contacts
    case active_jobs
    case old_sub
    case sub
    case next
    case progress
    case completed
    case reserve
    case filter
    case order_details
    case vist_details
    case rate
    case cancel
    case reschedule
    case order
    case mylocations
    case removelocation
    case addLocation
    case myfatoorah_live
    case make_subscription
    case myfatoorah_test
    case send_message
    case messages
    case message_history
    
    
    
    var url: String {
        
        switch self {
        // MARK:- Auth URLS
        case .Login: return MyURLs.USER_URL + "Authentication/Login"
        case .Register: return MyURLs.USER_URL + "Authentication/Registration"
        case .complete: return MyURLs.USER_URL + "register/complete"
        case .send_phone: return MyURLs.USER_URL + "checkPhone"
        case .get_phone: return MyURLs.USER_URL + "checkPhone"
        case .countries: return MyURLs.USER_URL + "countries"
        case .update_profile: return MyURLs.USER_URL + "Profile/Update"
        case .package: return MyURLs.USER_URL + "Packages"
        case .change_password : return MyURLs.USER_URL + "Authentication/ChangePassword"
        case .firebase : return MyURLs.USER_URL + "firebase"
        case .get_companies_categories: return MyURLs.USER_URL + "companies/services_filter?service_id="
        case .notification: return MyURLs.USER_URL + "Notifications"
        case .categories: return MyURLs.USER_URL + "Categories"
        case .price_filter: return MyURLs.USER_URL + "filter_data"
        case .shop_offers: return MyURLs.USER_URL + "shops_details?shop_id="
        case .reviews: return MyURLs.USER_URL + "reviews_details?shop_id="
        case .Following:  return MyURLs.USER_URL + "following_list"
        case .terms : return MyURLs.USER_URL + "Authentication/GetTerms"
        case .about_us : return MyURLs.USER_URL + "About"
        case .follow: return MyURLs.USER_URL + "follow"
        case .code_check: return MyURLs.USER_URL + "Authentication/VerificationCode"
        case .forget_password : return MyURLs.USER_URL + "Authentication/GetVerificationCode"
        case .cities: return MyURLs.USER_URL + "Cities"
        case .search_filter: return MyURLs.USER_URL + "search?sortby="
        case .block: return MyURLs.USER_URL + "block"
        case .add_review: return MyURLs.USER_URL + "add_review"
        case .add_phone:  return MyURLs.USER_URL + "add_phone"
        case .delete_phone:  return MyURLs.USER_URL + "delete_phone?phone_id="
        case .units: return MyURLs.USER_URL + "Units?category_id="
        case .issues: return MyURLs.USER_URL + "Subcategories?unit_id="
        case .history: return MyURLs.USER_URL + "History"
        case .contacts: return MyURLs.USER_URL + "Contacts"
        case .active_jobs: return MyURLs.USER_URL + "History/ActiveJobs"
        case .old_sub: return MyURLs.USER_URL + "Packages/subscription-details"
        case .sub: return MyURLs.USER_URL + "Packages/offers?package_id="
        case .next:  return MyURLs.USER_URL + "Packages/get-visit-history?type=0&category_type="
        case .progress: return MyURLs.USER_URL + "Packages/get-visit-history?type=1&category_type="
        case .completed: return MyURLs.USER_URL + "Packages/get-visit-history?type=2&category_type="
        case .reserve: return MyURLs.USER_URL + "Subscriptions/Reserve"
        case .filter: return MyURLs.USER_URL + "Filter"
        case .order_details:  return MyURLs.USER_URL + "Order/JobDetails?order_id="
        case .vist_details:  return MyURLs.USER_URL + "Packages/get-visit-details?visit_id="
            
        case .rate:  return MyURLs.USER_URL + "Rate/Technician"
        case .cancel: return MyURLs.USER_URL + "Order/Cancel"
        case .reschedule: return MyURLs.USER_URL + "Order/Reschedule"
        case .order: return MyURLs.USER_URL + "Order"
        case .mylocations: return MyURLs.USER_URL + "Profile/my_locations"
        case .removelocation :  return MyURLs.USER_URL + "Profile/delete_location"
        case .addLocation :  return MyURLs.USER_URL + "Profile/add_location"
        case .myfatoorah_live : return MyURLs.BASE_URL + "go-to-payment?full_name=\(DEF.name)&invoice_value="
        case .myfatoorah_test : return MyURLs.BASE_URL + "test-payment?full_name=\(DEF.name)&invoice_value="
            
        case .make_subscription: return MyURLs.USER_URL + "Packages/make-subscription"
            
        case .send_message: return MyURLs.USER_URL + "Messages/send-message"
        case .messages: return MyURLs.USER_URL + "Messages/messages?"
            
        case .message_history:  return MyURLs.USER_URL + "Messages/message_history"
            
        }
    }
}
