//
//  AppDelegate+UI.swift
//  Jaz-User
//
//  Created by iMac on 10/27/19.
//  Copyright © 2019 Grand. All rights reserved.
//

import UIKit
import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import MOLH
import AVFoundation

extension AppDelegate:MOLHResetable{
    func reset() {
                exit(0)
        print("reset called")
//        let storyboard = UIStoryboard(name: AppStoryboard.Main.rawValue, bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "SplashVC") as! SplashVC
//        UIApplication.shared.keyWindow?.rootViewController = vc
        
    }
    
    
    func setUp_UI()   {
        //        let pre = Locale.preferredLanguages
        //        for lang in pre{
        //            print("$$pre ==>",lang)
        //        }
        //        print("$$region ==>>",Locale.current.regionCode!)
        //        let xlang = Locale.current.identifier
        //         print("$$Xlang ==>",xlang)
        //        print("$$lang ==>",Locale.current.languageCode ?? "ar")
        //        MOLHFont.shared.arabic = UIFont(name: "Courier", size: 13)!
        //        MOLHLanguage.setDefaultLanguage("ar")
        //        MOLH.shared.activate(true)
        //        MOLH.shared.specialKeyWords = ["Cancel","Done"]
        print("@@",DEF.language)
        MOLH.setLanguageTo(DEF.language)
        MOLH.shared.activate(false)
        IQKeyboardManager.shared.enable = true
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().barTintColor = .white
        UINavigationBar.appearance().tintColor = FontColor
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor:  FontColor]
        UINavigationBar.appearance().shadowImage = UIImage()
        UITabBar.appearance().tintColor = .MainColor
        UIFont.overrideInitialize()
        
        
//        
//        UINavigationBar.appearance().barTintColor = mainColor
//              UINavigationBar.appearance().tintColor = .white
//              UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor:  UIColor.white]
        
    }
    
    func getPreferredLocale() -> Locale {
        guard let preferredIdentifier = Locale.preferredLanguages.first else {
            return Locale.current
        }
        return Locale(identifier: preferredIdentifier)
    }
    func setUp_Google(){
        GMSServices.provideAPIKey("AIzaSyBQNQyTLfZJEoHYVxlY_AmUJ-oYQrh3edU") // For Services
        //        GMSPlacesClient.provideAPIKey("AIzaSyBQNQyTLfZJEoHYVxlY_AmUJ-oYQrh3edU")  // For Places
    }
    func SetUptheAudioSession()  {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(.playback, mode: .moviePlayback)
        }
        catch {
            print("Setting category to AVAudioSessionCategoryPlayback failed.")
        }
        
    }
    
}
