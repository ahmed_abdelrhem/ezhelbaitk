//
//  Protocols.swift
//  ByDeals
//
//  Created by Islamic on 2/12/19.
//  Copyright © 2019 2grand. All rights reserved.
//

import Foundation


//Service Location Protocol
protocol LocationDelegate: class {
    func RetriveLocation(lat: Double, lng: Double, add: String,country: String)
}

//Time Protocol
protocol TimeDelegate: class {
    func RetriveTime(time: String)
}


//Date Protocol
protocol DateDelegate: class {
    func RetriveDate(date: String)
}

//Terms and Conditions Protocol
protocol TermsDelegate: class {
    func RetriveSelection(isSelected: Bool)
}
// Presenter protocal
protocol LoaderDelegate:LoaderView {
    
    func onFailure<T>(_ msg: T)
    func onSuccess<T>(_ msg: T)
    func onEmpty()
    func onConnection()
}
protocol LoaderView:class {
    func loader_start()
    func loader_stop()
}
protocol PresentDelegate:class {
    func PresentVC<T>(_ model:T)
    func onConnection()
    func onFailure<T>(_ msg: T)
    
    
}
protocol ButtonDelegate:class {
    func button_start()
    func button_stop()
    
}

protocol ValidateDelegate:class {
    func validate(completion: @escaping CompletionHandler)
}
protocol JwtDelegate:class {
    // Present log in view Controller
    func jwtExpired<T>(_ model:T)
}
protocol DidSelectDelegate:class {
    func didselect<T>(_ data:T)
}

typealias LoaderJwtDelegate =  (LoaderDelegate & JwtDelegate)
