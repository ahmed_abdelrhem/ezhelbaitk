//
//  NotificationCenter+Extentions.swift
//  Aman
//
//  Created by iMac on 7/8/19.
//  Copyright © 2019 apple. All rights reserved.
//
import Foundation
let _NC = NotificationCenter.default
extension Notification.Name {
    static let exchangeBackgroundColor = Notification.Name("exchangeBackgroundColor")
    static let didReceiveShipping = Notification.Name("didReceiveShipping")
    static let didReceivePay = Notification.Name("didReceivePay")
    static let didCompleteTask = Notification.Name("didCompleteTask")
    static let completedLengthyDownload = Notification.Name("completedLengthyDownload")
    static let userType = Notification.Name("userType")
    static let getCategries = Notification.Name("getCategries")
    static let change_fav = Notification.Name("change_fav")
    static let on_chat = Notification.Name("on_chat")
    static let on_order_chat = Notification.Name("on_order_chat")
    static let on_Order = Notification.Name("on_Order")
    static let on_Vist = Notification.Name("on_Vist")
    static let on_Beauty = Notification.Name("on_Beauty")
    static let chat_open = Notification.Name("chat_open")
    static let selected_list = Notification.Name("selected_list")
    static let make_subscription = Notification.Name("make_subscription")

    static let make_order = Notification.Name("make_order")


    static let pushFromHomeVC = Notification.Name("pushFromHomeVC")
    static let pushRateVC = Notification.Name("pushRateVC")

    static let getInstitutionBranches = Notification.Name("getInstitutionBranches")
    static let filterSelected = Notification.Name("filterSelected")
    static let delete = Notification.Name("delete")
    static let  chatNewMessage = Notification.Name("chatNewMessage")
       static let  counterObs = Notification.Name("counterObs")
    
    
    
    static let pushViewControllerFromMenu = Notification.Name("pushViewControllerFromMenu")
    
}
