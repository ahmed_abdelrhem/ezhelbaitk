//
//  CornerNavBar.swift
//  Moon Shop
//
//  Created by apple on 2/26/20.
//  Copyright © 2020 2grand. All rights reserved.
//

import UIKit
@IBDesignable
class CornerNavBar: UINavigationBar {
    
    
    @IBInspectable public var topLeftCorner: CGFloat = 0 {
        didSet {
            layer.cornerRadius = topLeftCorner
            layer.maskedCorners = [.layerMinXMinYCorner]
        }
    }
    @IBInspectable public var bottomRightCorner: CGFloat = 0 {
        didSet {
           
        }
    }
    @IBInspectable public var bottomleftCorner: CGFloat = 0 {
        didSet {
            layer.cornerRadius = bottomleftCorner
            layer.maskedCorners = [.layerMinXMaxYCorner]
        }
    }
    @IBInspectable public var bottom2Sides: CGFloat = 0 {
        didSet {
            layer.cornerRadius = bottom2Sides
            layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        }
    }
    @IBInspectable public var top2Sides: CGFloat = 0 {
        didSet {
            layer.cornerRadius = top2Sides
            layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
            clipsToBounds = true
        }
    }
    @IBInspectable public var toprightCorner: CGFloat = 0 {
        didSet {
            layer.cornerRadius = toprightCorner
            layer.maskedCorners = [.layerMaxXMinYCorner]
        }
    }
}
