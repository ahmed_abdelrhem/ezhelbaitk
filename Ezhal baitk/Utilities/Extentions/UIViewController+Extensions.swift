import UIKit
import Lottie
import AVKit
let ANIMATION_VIEW = AnimationView(name: "loader")

class BaseViewController:UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(">>",self.description)
        
        
        _NC.addObserver(self, selector: #selector(onOrder), name: .on_Order , object: nil)
        
        _NC.addObserver(self, selector: #selector(onVist), name: .on_Vist , object: nil)
        
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print(">> BaseViewController >>> viewDidDisappear")
        _NC.removeObserver(self, name: .on_Order, object: .none)
        _NC.removeObserver(self, name: .on_Vist, object: .none)

    }
    
    deinit {
        print(" >> BaseViewController >>> deinit")
        
        _NC.removeObserver(self, name: .on_Order, object: .none)
        _NC.removeObserver(self, name: .on_Vist, object: .none)
        
    }
    
    
    @objc private func onOrder(_ notification: Notification)
    {
        print("#on_order")
        
        if let data = notification.userInfo as? [String: Any]
        {
            let  fb = (data["fb"] as? Firebase_Base)
            guard let order_idStr = fb?.order_id else {
                return
            }
            guard let order_id = Int(order_idStr) else {
                return
            }
            guard let statusStr = fb?.status else {
                return
            }
            guard let status = Int(statusStr) else {
                return
            }
            guard let tec = fb?.tech_name else {
                return
            }
            guard let cat  = fb?.cat_name else {
                return
            }
            guard let img  = fb?.tech_image else {
                return
            }
            let vc = UIStoryboard(name: AppStoryboard.Shared.rawValue, bundle: nil).instantiateViewController(withIdentifier: "StatusVC") as! StatusVC
            vc.order = (status:status,order_id: order_id,tec,cat,img)
            if navigationController != nil {
                navigationController?.pushViewController(vc, animated: true)
            }else{
                vc.WithMenu = true
                let nav = UINavigationController(rootViewController: vc)
                let menu =  MenuVC()
                let Rvc : SWRevealViewController =  SWRevealViewController(rearViewController: menu, frontViewController: nav)
                Rvc.rightViewController = menu
                UIApplication.shared.keyWindow?.rootViewController = Rvc
            }
            
        }
        return
    }
    
    
    @objc private func onVist(_ notification: Notification)
    {
        print("#onVist")
        
        if let data = notification.userInfo as? [String: Any]
        {
            let  fb = (data["fb"] as? Firebase_Base)
          
           
            guard let cat_idStr = fb?.category_id else {
                return
            }
            guard let cat_id = Int(cat_idStr) else {
                return
            }
            
            let vc = UIStoryboard(name: AppStoryboard.Shared.rawValue, bundle: nil).instantiateViewController(withIdentifier: "SubStatusVC") as! SubStatusVC
            vc.cat_id = cat_id
            if navigationController != nil {
                navigationController?.pushViewController(vc, animated: true)
            }else{
                let nav = UINavigationController(rootViewController: vc)
                let menu = MenuVC()
                let Rvc : SWRevealViewController =  SWRevealViewController(rearViewController: menu, frontViewController: nav)
                Rvc.rightViewController = menu
                UIApplication.shared.keyWindow?.rootViewController = Rvc
            }
        }
        return
        
    }
    
    
    
    
}


extension UIViewController {
    
    
    var alertController: UIAlertController? {
        guard let alert = UIApplication.topViewController() as? UIAlertController else { return nil }
        return alert
    }
}
extension UIViewController{
    func alertDialoga2Login(){
        
        
        
        
        var  title = "Warning".localized
        var  message =  "please,log in to continue .".localized
        var    BtnTitle = "Ok".localized
        
        
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: BtnTitle, style: UIAlertAction.Style.default, handler: { (action) in
            let vc = UIStoryboard(name: AppStoryboard.Main.rawValue, bundle: nil).instantiateViewController(withIdentifier: "AuthNav")
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: true, completion: nil)
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
        
        
    }
    func alert_login(_ msg:String) {
        let alert = UIAlertController(title: "", message: msg, preferredStyle: UIAlertController.Style.alert)
        // add an action (button)
        alert.addAction(UIAlertAction(title: _Ok, style: UIAlertAction.Style.default, handler: { (action) in
            DEF.logout()
            guard let vc = UIStoryboard(name: AppStoryboard.Main.rawValue, bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginVC") as? LoginVC else{return}
            let nav = UINavigationController(rootViewController: vc)
            UIApplication.shared.keyWindow?.rootViewController = nav
            UIApplication.shared.keyWindow?.makeKeyAndVisible()
        }))
        
        alert.addAction(UIAlertAction(title: _Cancel, style: .cancel, handler: .none))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    func uploadingAlert()  {
        let alert: UIAlertController = UIAlertController(style: .alert)
        
        
        let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 50, y: 10, width: 37, height: 37)) as UIActivityIndicatorView
        loadingIndicator.center = self.view.center
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();
        
        alert.setValue(loadingIndicator, forKey: "accessoryView")
        loadingIndicator.startAnimating()
        
        alert.show();
    }
    
    func transparent_nav()  {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
    }
    func alertDialogawithSegueLogout(_ sourceViewController: UIViewController,_ destinationViewController: UIViewController){
        
        
        
        
        let  title = "Warning".localized
        let message = DEF.isLogin ? "Do you want to log out ?" : "Do you want to log in ?".localized
        let BtnTitle = _Ok
        let BtnTitle2 = _Cancel
        
        
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: BtnTitle, style: UIAlertAction.Style.default, handler: { (action) in
            DEF.logout()
            sourceViewController.present(destinationViewController, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: BtnTitle2, style: UIAlertAction.Style.default, handler: { (action) in
            
            alert.dismiss(animated: true, completion: nil)
        }))
        
        // show the alert
        sourceViewController.present(alert, animated: true, completion: nil)
        
        
    }
    func PresentHomeVC(msg:String? = .none)  {
        let menu =  MenuVC()
        let tabs =  TabBarVC()
        let swRevealVC =  SWRevealViewController(rearViewController: menu, frontViewController: tabs)
        swRevealVC?.rightViewController = menu
        if msg == nil{
            UIApplication.shared.keyWindow!.rootViewController = swRevealVC
            UIApplication.shared.keyWindow?.makeKeyAndVisible()

        }else{
            UIApplication.shared.keyWindow!.rootViewController = swRevealVC
            UIApplication.shared.keyWindow?.makeKeyAndVisible()
            Messages.instance.showMessage(title: "", body: msg ?? "", state: .success, layout: .messageView)
            
        }
        
    }


    
    func playVideo(_ strUrl: String) {
        guard let url = URL(string: strUrl) else {
            return
        }
        // Create an AVPlayer, passing it the HTTP Live Streaming URL.
        let player = AVPlayer(url: url)
        
        // Create a new AVPlayerViewController and pass it a reference to the player.
        let controller = AVPlayerViewController()
        controller.player = player
        
        // Modally present the player and call the player's play() method when complete.
        present(controller, animated: true) {
            player.play()
        }
    }
    
    func createLabelNoResult(_ txt: String?,_ viewController : UIViewController){
        // CGRectMake has been deprecated - and should be let, not var
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width , height: 80 ))
        label.numberOfLines = 0
        
        // you will probably want to set the font (remember to use Dynamic Type!)
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        
        // and set the text color too - remember good contrast
        label.textColor = .black
        
        // may not be necessary (e.g., if the width & height match the superview)
        // if you do need to center, CGPointMake has been deprecated, so use this
        label.center = CGPoint(x: self.view.frame.width / 2.0, y: self.view.frame.height / 2.0)
        
        // this changed in Swift 3 (much better, no?)
        label.textAlignment = .center
        label.font = label.font.withSize(18)
        label.text = txt ?? "لا يوجد بيانات ."
        
        
        viewController.view.addSubview(label)
    }
    func sideMenu()
    {
        let sideButton = UIBarButtonItem(image: UIImage(named: "menu"),  style: .plain , target: self, action: Selector(("didTapEditButton:")))
        if self.revealViewController() != nil
        {
            
            //            MOLHLanguage.isRTL()
            print("#language ==>",UIApplication.shared.getLanguage())
            
            if UIApplication.shared.isRTL {
                // right setting
                sideButton.target = self.revealViewController()
                sideButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
                self.revealViewController().rightViewRevealWidth = UIScreen.main.bounds.width - 60.0
                
            }else {
                // left setting
                sideButton.target = self.revealViewController()
                sideButton.action = #selector(SWRevealViewController.revealToggle(_:))
                self.revealViewController().rearViewRevealWidth = UIScreen.main.bounds.width - 60.0
                
            }
            self.navigationItem.leftBarButtonItem = sideButton
            //           self.revealViewController().frontViewController.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
            //            let pan = self.revealViewController().panGestureRecognizer()!
            //            self.view.addGestureRecognizer(pan)
            
        }
    }
    
    //Indicator
    
    
    func startAnimating(ponit:(x:CGFloat,y:CGFloat)?) {
        ANIMATION_VIEW.frame = CGRect(x: 0, y: 0, width: 250, height: 250)
        ANIMATION_VIEW.center = CGPoint(x: ponit?.x ?? self.view.center.x, y: ponit?.y ?? self.view.center.y)
        ANIMATION_VIEW.contentMode = .scaleAspectFit
        ANIMATION_VIEW.loopMode = .loop
        self.view.addSubview(ANIMATION_VIEW)
        ANIMATION_VIEW.play()
    }
    
    func stopAnimating() {
        ANIMATION_VIEW.stop()
        ANIMATION_VIEW.removeFromSuperview()
    }
    
    func hideBackButtonTitle() {
        if let topItem = self.navigationController?.navigationBar.topItem {
            topItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        }
    }
    
    
}
