import UIKit
import MOLH
import AVFoundation

extension UIApplication {
    var isRTL :Bool  {
        return  MOLHLanguage.isRTLLanguage()
    }
    func generateThumbnail(path: URL) -> UIImage? {
           do {
               let asset = AVURLAsset(url: path, options: nil)
               let imgGenerator = AVAssetImageGenerator(asset: asset)
               imgGenerator.appliesPreferredTrackTransform = true
               let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
               let thumbnail = UIImage(cgImage: cgImage)
               return thumbnail
           } catch let error {
               print("*** Error generating thumbnail: \(error.localizedDescription)")
               return nil
           }
       }

    func getLanguage() -> String{
     
           if MOLHLanguage.currentAppleLanguage() == "en"{
               return "en"
           }else{
               return "ar"
           }
       }
    
    
    class func topViewController(_ viewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = viewController as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = viewController as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = viewController?.presentedViewController {
            return topViewController(presented)
        }
        
        return viewController
    }
}
