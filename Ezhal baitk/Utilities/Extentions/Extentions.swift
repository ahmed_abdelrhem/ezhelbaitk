//
//  extentions.swift
//  Green Arrow
//
//  Created by OSX on 7/4/18.
//  Copyright © 2018 2Grand. All rights reserved.
//


import UIKit


extension UIViewController : IndicatorView {
    
    
    
  
    func addBarBtn(){
         var button1 = UIBarButtonItem()
        button1 = UIBarButtonItem(title: "أضف", style: .plain, target: self, action: #selector(go2Notification))
                // action:#selector(Class.MethodName) for swift 3
        self.navigationItem.rightBarButtonItem = button1
    }
    @objc func go2Notification() {
        print("notification UIBarButtonItem Pressed ")


    }
    //Hide Keyboard
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    //Remove ViewController
    func removeViewControllerWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.removeViewController))
        view.addGestureRecognizer(tap)
    }
    
    @objc func removeViewController() {
        popUpDismissVC()
    }
    
    
    // POPUP PRESENT
    func popUpPresent(_ uivewController: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.fade
        transition.subtype = CATransitionSubtype.fromTop
        self.view.window?.layer.add(transition, forKey: kCATransition)
        present(uivewController, animated: false, completion: nil)
    }
    
    // CUSTOM PRESENT
    func CustomPresent(_ uivewController: UIViewController) {
        let transition = CATransition()
      transition.duration = 0.3
        transition.type = CATransitionType.fade
        transition.subtype = CATransitionSubtype.fromBottom
        self.view.window?.layer.add(transition, forKey: kCATransition)
        present(uivewController, animated: false, completion: nil)
    }
    
    func CustomPresentFromRight(_ uivewController: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type     = CATransitionType.push
        transition.subtype  = CATransitionSubtype.fromRight
        self.view.window?.layer.add(transition, forKey: kCATransition)
        present(uivewController, animated: false, completion: nil)
    }

    
    // POPUP DISMISS
    func popUpDismissVC() {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.fade
        transition.subtype = CATransitionSubtype.fromRight
        self.view.window?.layer.add(transition, forKey: kCATransition)
        dismiss(animated: false, completion: nil)
    }
    
    func popUpDismissFromLeft() {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.fade
        transition.subtype = CATransitionSubtype.fromLeft
        self.view.window?.layer.add(transition, forKey: kCATransition)
        dismiss(animated: false, completion: nil)
    }

    
    //View Controller Animation
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25) {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    
    func removeAnimation() {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3 , y: 1.3)
            self.view.alpha = 0.0;
        }) { (finished : Bool) in
            if finished {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    func add(_ child: UIViewController) {
        addChild(child)
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    func remove() {
        guard parent != nil else {
            return
        }
        willMove(toParent: nil)
        removeFromParent()
        view.removeFromSuperview()
    }
    
}
extension Date {
    var mediumDateString: String {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter.string(from: self)
    }
}
extension UILabel {
    @IBInspectable var localizedString: String {
        get {
            return ""
        }
        set(value) {
            self.text = NSLocalizedString(value, comment: "")
        }
    }
}

extension UITextField {
    
    @IBInspectable var localizedString: String {
        get {
            return ""
        }
        set(value) {
            self.text = NSLocalizedString(value, comment: "")
        }
    }
    
    @IBInspectable var PlaceholderLocalized: String {
        get {
            return ""
        }
        set(value) {
            self.placeholder = NSLocalizedString(value, comment: "")
        }
    }
    
  
}

extension CALayer {
    
    func addBorder(edge: UIRectEdge?, color: UIColor, thickness: CGFloat) {
        
        let border = CALayer()
        
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x: 0, y: 0, width: self.bounds.width , height: thickness)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect(x:0, y: self.bounds.height - 1, width: self.bounds.width, height: thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect(x: 0, y: 0, width: thickness, height: self.bounds.height)
            break
        case UIRectEdge.right:
            border.frame = CGRect(x: self.bounds.width - 1, y: 0, width: thickness, height: self.bounds.height)
            break
        default:
            //For Center Line
            border.frame = CGRect(x: self.bounds.width/2 - thickness, y: 0, width: thickness, height: self.bounds.height)
            break

        }
        
        border.backgroundColor = color.cgColor;
        self.addSublayer(border)
    }
}

extension  UITextField {
    
    static func isValidEmail(testStr: UITextField) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr.text)
    }
    
    
    static func compare(testStr: String , confirm: String) -> Bool{
        if testStr == confirm {
            return true
        }
        return false
    }
    
    static func isValidPassword(testStr: UITextField) -> Bool {
        if (testStr.text?.count)! >= 6 {
            return true
        }
        return false
    }

    
}


extension String {
    var localized: String {
        // ar.lpro
    
        let path = Bundle.main.path(forResource: DEF.language, ofType: "lproj")
        let bundle = Bundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
      }
    public func isPhone() -> Bool {
            let phoneRegex = "966[0-9]{9}"
            let predicate = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
            return  predicate.evaluate(with: self)
        
    }
    
    public func isNum() -> Bool {
        if self.isAllDigits() {
            return true
        } else {
            return false
        }
    }
    
    private func isAllDigits() -> Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = self.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  self == filtered
    }
    
    func encodeUrl() -> String
    {
        return self.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
    }
    
    var fixedArabicURL: String?  {
        return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics
            .union(CharacterSet.urlPathAllowed)
            .union(CharacterSet.urlHostAllowed))
    }
    
}


extension Int {
    init(_ range: Range<Int> ) {
        let delta = range.lowerBound < 0 ? abs(range.lowerBound) : 0
        let min = UInt32(range.lowerBound + delta)
        let max = UInt32(range.upperBound   + delta)
        self.init(Int(min + arc4random_uniform(max - min)) - delta)
    }
}


//function that receive the number of digits requested, calculates the range of the random number
func random(_ digits:Int) -> Int {
    let min = Int(pow(Double(10), Double(digits-1))) - 1
    let max = Int(pow(Double(10), Double(digits))) - 1
    return Int(Range(uncheckedBounds: (min, max)))
}



//extension String {
//    func extractURLs() -> [NSURL] {
//        var urls : [NSURL] = []
//        do {
//            let detector = try NSDataDetector(types: NSTextCheckingType.Link.rawValue)
//            detector.enumerateMatchesInString(self,
//                                              options: [],
//                                              range: NSMakeRange(0, text.characters.count),
//                                              usingBlock: { (result, _, _) in
//                                                if let match = result, url = match.URL {
//                                                    urls.append(url)
//                                                }
//            })
//        } catch let error as NSError {
//            print(error.localizedDescription)
//        }
//        return urls
//    }
//}

///----------------

//let input = "This is a test with the URL https://www.hackingwithswift.com to be detected."
//let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
//let matches = detector.matches(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count))
//
//for match in matches {
//    guard let range = Range(match.range, in: input) else { continue }
//    let url = input[range]
//    print(url)
//}
///----------

//
//let encodedLink = requesturl.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
//let encodedURL = NSURL(string: encodedLink!)! as URL
 
