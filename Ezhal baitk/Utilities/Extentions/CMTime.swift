//
//  CMTime.swift
//  Moon Shop
//
//  Created by a7med on 3/31/20.
//  Copyright © 2020 2grand. All rights reserved.
//

import AVKit
extension CMTime{
    
    func toDisplayString( ) -> String {
             let totalSecond = Int(CMTimeGetSeconds(self))
                   let seconds = totalSecond % 60
                   let minutes = totalSecond / 60
                   let timeformatString =  String(format: "%02d:%02d",minutes, seconds)
        return timeformatString
    }
}
