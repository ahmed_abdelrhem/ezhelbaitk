//
//  UserDEF.swift
//
//  Copyright © 2018 2Grand. All rights reserved.
//

import UIKit
import MOLH
let DEF = UserDefaults.standard
//let APPversion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
//let buildNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
//let systemVersion = UIDevice.current.systemVersion
//let DeviceModel = UIDevice.current.model
//let DEVICE_ID = UIDevice.current.identifierForVendor!.uuidString

extension UserDefaults {
    private(set) var DeviceID: String {
        get {
            return DEF.object(forKey: "DeviceID") as? String ?? ""
        } set {
            DEF.set(UIDevice.current.identifierForVendor!.uuidString, forKey: "DeviceID")
            DEF.synchronize()
        }
    }
    private(set) var Device_locale: Locale {
        get {
            return DEF.object(forKey: "Device_locale") as? Locale ?? Locale.autoupdatingCurrent
        } set {
            DEF.set(Locale.autoupdatingCurrent, forKey: "Device_locale")
            DEF.synchronize()
        }
    }
    
    private(set) var MoLH_locale: String {
           get {
               return DEF.object(forKey: "MoLH_locale") as? String ?? "en_GB"
           } set {
               DEF.set(MOLHLanguage.currentLocaleIdentifier(), forKey: "MoLH_locale")
               DEF.synchronize()
           }
       }
    
    
    private(set) var DeviceModel: String {
        get {
            return DEF.object(forKey: "DeviceModel") as? String ?? ""
        } set {
            DEF.set(UIDevice.current.model, forKey: "DeviceModel")
            DEF.synchronize()
        }
    }
    private(set) var systemVersion: String {
        get {
            return DEF.object(forKey: "buildNumber") as? String ?? ""
        } set {
            DEF.set(UIDevice.current.systemVersion, forKey: "buildNumber")
            DEF.synchronize()
        }
    }
    private(set) var buildNumber: String {
        get {
            return DEF.object(forKey: "buildNumber") as? String ?? ""
        } set {
            DEF.set(Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String, forKey: "buildNumber")
            DEF.synchronize()
        }
    }
    private(set) var APPversion: String {
        get {
            return DEF.object(forKey: "APPversion") as? String ?? ""
        } set {
            DEF.set(Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String, forKey: "APPversion")
            DEF.synchronize()
        }
    }
    
    
    
    var FireBaseToken: String {
        get {
            return DEF.object(forKey: "FireBaseToken") as? String ?? "Not Configured yet"
        } set {
            DEF.set(newValue, forKey: "FireBaseToken")
            DEF.synchronize()
        }
    }
    
    var isLogin: Bool {
        get {
            return DEF.object(forKey: "isLogin") as? Bool ?? false
        } set {
            DEF.set(newValue, forKey: "isLogin")
            DEF.synchronize()
        }
    }


    var isFristRun: Bool {
        get {
            return DEF.object(forKey: "isFristRun") as? Bool ?? true
        } set {
            DEF.set(newValue, forKey: "isFristRun")
            DEF.synchronize()
        }
    }
    
    
    var userID: Int {
        get {
            return DEF.object(forKey: "UserID") as? Int ?? 0
        } set {
            DEF.set(newValue, forKey: "UserID")
            DEF.synchronize()
        }
    }


    
    
    var name: String {
        get {
            return DEF.object(forKey: "Name") as? String ?? "guest"
        } set {
            DEF.set(newValue, forKey: "Name")
            DEF.synchronize()
        }
    }
    
    var language: String {
        get {
            return DEF.object(forKey: "lang") as? String ?? "en"
        } set {
            DEF.set(newValue, forKey: "lang")
            DEF.synchronize()
        }
    }
    var backEndLanguage: String {
         get {
             return DEF.object(forKey: "lang") as? String ?? "en"
         } set {
             DEF.set(newValue, forKey: "lang")
             DEF.synchronize()
         }
     }

    
    var email: String {
        get {
            return DEF.object(forKey: "email") as? String ?? ""
        } set {
            DEF.set(newValue, forKey: "email")
            DEF.synchronize()
        }
    }
    
    var phone: String {
        get {
            return DEF.object(forKey: "phone") as? String ?? ""
        } set {
            DEF.set(newValue, forKey: "phone")
            DEF.synchronize()
        }
    }
    
    var jwt: String {
        get {
            return DEF.object(forKey: "jwt") as? String ?? ""
        } set {
            DEF.set(newValue, forKey: "jwt")
            DEF.synchronize()
        }
    }
    
    
    var image: String {
        get {
            return DEF.object(forKey: "UserImage") as? String ?? ""
        } set {
            DEF.set(newValue, forKey: "UserImage")
            DEF.synchronize()
        }
    }
    
    var address: String {
        get {
            return DEF.object(forKey: "Address") as? String ?? ""
        } set {
            DEF.set(newValue, forKey: "Address")
            DEF.synchronize()
        }
    }
    var lat: String {
        get {
            return DEF.object(forKey: "lat") as? String ?? ""
        } set {
            DEF.set(newValue, forKey: "lat")
            DEF.synchronize()
        }
    }
    var lng: String {
        get {
            return DEF.object(forKey: "lng") as? String ?? ""
        } set {
            DEF.set(newValue, forKey: "lng")
            DEF.synchronize()
        }
    }
    



    var currency: String {
        get {
            return DEF.object(forKey: "currency") as? String ?? ""
        } set {
            DEF.set(newValue, forKey: "currency")
            DEF.synchronize()
        }
    }
    var subscriptions_price: String {
        get {
            return DEF.object(forKey: "subscriptions_price") as? String ?? ""
        } set {
            DEF.set(newValue, forKey: "subscriptions_price")
            DEF.synchronize()
        }
    }
    var subscriptions_id: Int {
        get {
            return DEF.object(forKey: "subscriptions_id") as? Int ?? 0
        } set {
            DEF.set(newValue, forKey: "subscriptions_id")
            DEF.synchronize()
        }
    }
    var category_id: Int {
        get {
            return DEF.object(forKey: "category_id") as? Int ?? 0
        } set {
            DEF.set(newValue, forKey: "category_id")
            DEF.synchronize()
        }
    }

    
    var user_subscriptions: [User_subscriptions] {
        get {
            if let def = DEF.data(forKey: "User_subscriptions") {
                let user_subscriptions = try! JSONDecoder().decode([User_subscriptions].self, from: def)
                return user_subscriptions
            }
            return []
        } set {
            let user_subscriptions = try! JSONEncoder().encode(newValue)
            DEF.set(user_subscriptions, forKey: "User_subscriptions")
        }
    }
    
    
    func saveUserDefault(_ user: UserData)  {
        //        DEF
        userID = user.id ?? 0
        name = user.name ?? ""
        email = user.email ?? ""
        phone = user.phone ?? ""
        image = user.image ?? ""
        lat = user.lat ?? ""
        lng = user.lng ?? ""
        //        days = user.days ?? []
        isLogin = true
        jwt = user.jwt_token ?? ""
        address = user.address ?? ""
        user_subscriptions = user.user_subscriptions ?? []
        if (user.user_subscriptions ?? [] ).count > 0 {
            subscriptions_id = user.user_subscriptions?[0].id ?? 0
            subscriptions_price = user.user_subscriptions?[0].price ?? "0"
            category_id = user.user_subscriptions?[0].category_id ?? 0
            currency = user.user_subscriptions?[0].currency ?? "QR".localized
        }
        
        print("save user default")
        
    }
    func logout()  {
        userID =  0
        jwt =  ""
        name =  ""
        email = ""
        phone =  ""
        image =  ""
        address =  ""
        lat =  ""
        lng =  ""
        subscriptions_id =  0
        subscriptions_price = "0"
        category_id =  0
        currency =  "QR".localized
        user_subscriptions = []
        isLogin = false
    }
    
    
    
}
