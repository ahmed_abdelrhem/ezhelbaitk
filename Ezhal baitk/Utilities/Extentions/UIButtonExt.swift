//
//  UIButtonExt.swift
//
//  Copyright © 2019 apple. All rights reserved.
//

import Foundation
import UIKit


extension UIButton {
    @IBInspectable var localizedString: String {
        get {
            return ""
        }
        set(value) {
            self.setTitle(NSLocalizedString(value, comment: ""), for: .normal)
        }
    }
}



