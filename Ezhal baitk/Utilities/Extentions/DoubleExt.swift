//
//  DoubleExt.swift
//  QApp User
//
//  Created by Islamic on 9/22/19.
//  Copyright © 2019 Organization. All rights reserved.
//

import Foundation

extension Double {
    
    static func twoFraction(num: Double) -> Double {
        return Double(Darwin.round((1000*num)/1000))
    }
    
}
