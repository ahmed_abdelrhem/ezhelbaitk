//
//  MyIndicator.swift
//  Tabadul
//
//  Created by Islamic on 3/3/19.
//  Copyright © 2019 2grand. All rights reserved.
//

import UIKit

//Constant Variables
var myIndicatorImg = UIImage(named: "loader")?.withColor(HexStringToUIColor.hexStringToUIColor(hex: "#493824"))


class MyIndicator: UIView {
    
    //create imagView
    let imageView = UIImageView()
    
    init(frame: CGRect, image: UIImage) {
        super.init(frame: frame)
        imageView.frame = bounds
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        imageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(imageView)
    }
    
    required init(coder: NSCoder) {
        fatalError()
    }
    
    func startAnimating() {
        isHidden = false
        rotate()
    }
    
    func stopAnimating() {
        isHidden = true
        removeRotation()
    }
    
    private func rotate() {
        let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.toValue = NSNumber(value: Double.pi * 2)
        rotation.duration = 1.0
        rotation.isCumulative = true
        rotation.repeatCount = Float.greatestFiniteMagnitude
        self.imageView.layer.add(rotation, forKey: "rotationAnimation")
    }
    
    private func removeRotation() {
        self.imageView.layer.removeAnimation(forKey: "rotationAnimation")
    }
}



//MARK:- Protocols
public protocol IndicatorView { }
public protocol IndicatorBlockUI { }


//MARK:- IndicatorViewable with UIViewController Extension
public extension IndicatorView where Self: UIViewController {
    
    //start indicator animation
    func startIndicator() {
        
        //set Indicator
        let ind = MyIndicator(frame: CGRect(x: UIScreen.main.bounds.width/2-30, y: UIScreen.main.bounds.height/2-60, width: 60, height: 60), image: myIndicatorImg!)
        ind.tag = 999
        self.view.addSubview(ind)
        ind.startAnimating()
    }
    
    
    //stop indicator animation
    func stopIndicator() {
        if let view = self.view.viewWithTag(999) {
            view.removeFromSuperview()
        }
    }
    
}

//MARK:- IndicatorBlockUI with UIViewController Extension
public extension IndicatorBlockUI where Self: UIViewController {
    
    //start indicator animation
    func startIndicator() {
        
        //set Indicator
        let ind = MyIndicator(frame: CGRect(x: UIScreen.main.bounds.width/2-30, y: UIScreen.main.bounds.height/2-60, width: 60, height: 60), image: myIndicatorImg!)
        ind.tag = 999
        self.view.addSubview(ind)
        ind.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    
    //stop indicator animation
    func stopIndicator() {
        if let view = self.view.viewWithTag(999) {
            view.removeFromSuperview()
            UIApplication.shared.endIgnoringInteractionEvents()
        }
    }
    
}

class HexStringToUIColor {
    
    
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
}
extension UIImage {
    
    func withColor(_ color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        guard let ctx = UIGraphicsGetCurrentContext(), let cgImage = cgImage else { return self }
        color.setFill()
        ctx.translateBy(x: 0, y: size.height)
        ctx.scaleBy(x: 1.0, y: -1.0)
        ctx.clip(to: CGRect(x: 0, y: 0, width: size.width, height: size.height), mask: cgImage)
        ctx.fill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
        guard let colored = UIGraphicsGetImageFromCurrentImageContext() else { return self }
        UIGraphicsEndImageContext()
        return colored
    }
    
}
