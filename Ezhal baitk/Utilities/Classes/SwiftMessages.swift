//
//  SwiftMessages.swift
//  DrWash
//
//  Created by Islamic on 1/29/19.
//  Copyright © 2019 2grand. All rights reserved.
//

import UIKit
import SwiftMessages
import MOLH

class Messages {
    
    static let instance = Messages()
    
    func showMessage(title: String, body: String, state: Theme, layout: MessageView.Layout) {
        // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
        // files in the main bundle first, so you can easily copy them into your project and make changes.
        let view = MessageView.viewFromNib(layout: layout)
        view.button?.isHidden = true

        switch state {
        case .warning:
            view.configureTheme(backgroundColor: .white, foregroundColor:  mainColor, iconImage: UIImage(named: "warningIcon"))
            
        case .success:
            view.configureTheme(backgroundColor: .white, foregroundColor: mainColor, iconImage: UIImage(named: "successIcon"))
            
        case .error:
            view.configureTheme(backgroundColor: .white, foregroundColor: mainColor, iconImage: UIImage(named: "errorIcon"))

        case .info:
            view.configureTheme(backgroundColor: .white, foregroundColor: mainColor, iconImage: UIImage(named: "infoIcon"))
        }
        
        // Theme message elements with the warning style.
        view.configureTheme(state)

        // Add a drop shadow.
        view.configureDropShadow()

        // Set message title, body, and icon. Here, we're overriding the default warning
        view.configureContent(title: title, body: body)

        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)

        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        let xView = view.backgroundView as? CornerRoundingView
        xView?.layer.cornerRadius = 10

        // Show the message.
        SwiftMessages.show(view: view)
    }
    
    func actionsConfigMessage(title: String, body: String, buttonTitle: String, completion: @escaping CompletionHandler) {
        // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
        // files in the main bundle first, so you can easily copy them into your project and make changes.
        let view = MessageView.viewFromNib(layout: .cardView)
    
        view.button?.isHidden = false
        
        // Theme message elements with the warning style.
        view.configureTheme(.warning)
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        // Set message title, body, and icon. Here, we're overriding the default warning
        view.configureContent(title: title, body: body)
        view.button?.setTitle(buttonTitle, for: .normal)
        view.buttonTapHandler = { _ in
            completion(true)
            SwiftMessages.hide()
        }
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.layer.cornerRadius = 10

        //show Configure message
        var config = SwiftMessages.Config()
        
        // Slide up from the top or bottom.
        config.presentationStyle = .center
        
        // Display in a window at the specified window level: UIWindow.Level.statusBar
        // displays over the status bar while UIWindow.Level.normal displays under.
        config.presentationContext = .window(windowLevel: .statusBar)
        
        // Disable the default auto-hiding behavior.
        config.duration = .forever
        
        // Dim the background like a popover view. Hide when the background is tapped.
        config.dimMode = .gray(interactive: true)
        
        // Disable the interactive pan-to-hide gesture.
        config.interactiveHide = true
        
        // Specify a status bar style to if the message is displayed directly under the status bar.
        config.preferredStatusBarStyle = .lightContent
        
        // Specify one or more event listeners to respond to show and hide events.
        config.eventListeners.append() { _ in
            //Do nothing
        }
        
        SwiftMessages.show(config: config, view: view)

    }
    func demoCentered(title: String, body: String, buttonTitle: String, completion: @escaping CompletionHandler) {
          let messageView: MessageView = MessageView.viewFromNib(layout: .centeredView)
          messageView.configureBackgroundView(width: 320)
          messageView.configureContent(title: title, body: body, iconImage: UIImage(named: "question")!, iconText: nil, buttonImage: nil, buttonTitle: buttonTitle) { _ in
              SwiftMessages.hide()
              completion(true)
          }
          messageView.backgroundView.backgroundColor = UIColor.init(white: 0.97, alpha: 1)
          messageView.backgroundView.layer.cornerRadius = 10
          var config = SwiftMessages.defaultConfig
          config.presentationStyle = .center
          config.duration = .forever
          config.dimMode = .blur(style: .dark, alpha: 1, interactive: true)
          config.presentationContext  = .window(windowLevel: UIWindow.Level.statusBar)
          SwiftMessages.show(config: config, view: messageView)
      }
    
    func showConfigMessage(title: String, body: String, state: Theme, layout: MessageView.Layout, style: SwiftMessages.PresentationStyle) {
        // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
        // files in the main bundle first, so you can easily copy them into your project and make changes.
        let view = MessageView.viewFromNib(layout: layout)
        view.button?.isHidden = true
        
        // Theme message elements with the warning style.
        view.configureTheme(state)
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        // Set message title, body, and icon. Here, we're overriding the default warning
        view.configureContent(title: title, body: body)
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.layer.cornerRadius = 10
        
        //show Configure message
        var config = SwiftMessages.Config()
        
        switch state {
        case .warning:
            view.configureTheme(backgroundColor: .white, foregroundColor:  mainColor, iconImage: UIImage(named: "warningIcon"))
            
        case .success:
            view.configureTheme(backgroundColor: .white, foregroundColor: mainColor, iconImage: UIImage(named: "successIcon"))
            
        case .error:
            view.configureTheme(backgroundColor: .white, foregroundColor: mainColor, iconImage: UIImage(named: "errorIcon"))

        case .info:
            view.configureTheme(backgroundColor: .white, foregroundColor: mainColor, iconImage: UIImage(named: "infoIcon"))
        }
        
        // Slide up from the top or bottom.
        config.presentationStyle = style
        
        // Display in a window at the specified window level: UIWindow.Level.statusBar
        // displays over the status bar while UIWindow.Level.normal displays under.
        config.presentationContext = .window(windowLevel: .statusBar)
        
        // Disable the default auto-hiding behavior.
        config.duration = .automatic
        
        // Dim the background like a popover view. Hide when the background is tapped.
        config.dimMode = .gray(interactive: true)
        
        // Disable the interactive pan-to-hide gesture.
        config.interactiveHide = true
        
        // Specify a status bar style to if the message is displayed directly under the status bar.
        config.preferredStatusBarStyle = .lightContent
        
        // Specify one or more event listeners to respond to show and hide events.
        config.eventListeners.append() { _ in }
        
        SwiftMessages.show(config: config, view: view)
    }
    
    func showActionMessage(body: String, buttonTitle: String, completion: @escaping CompletionHandler) {
        // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
        // files in the main bundle first, so you can easily copy them into your project and make changes.
        let view = MessageView.viewFromNib(layout: .messageView)
        view.button?.isHidden = false

        // Theme message elements with the warning style.
        view.configureTheme(.warning)
        
        view.configureTheme(backgroundColor: .white, foregroundColor:  mainColor, iconImage: UIImage(named: "warningIcon"))

        // Add a drop shadow.
        view.configureDropShadow()
        
        // Set message title, body, and icon. Here, we're overriding the default warning
        view.configureContent(title: "", body: body)

        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.layer.cornerRadius = 10
        
        
        view.button?.setTitle(buttonTitle, for: .normal)
        
        //show Configure message
        var config = SwiftMessages.Config()
                
        // Slide up from the top or bottom.
        config.presentationStyle = .top
        
        // Disable the default auto-hiding behavior.
        config.duration = .forever
        
        view.buttonTapHandler = { _
            in SwiftMessages.hide()
            completion(true)
        }
        
        SwiftMessages.show(config: config, view: view)
    }
}

