//
//  AttachmentHandler+Delegate.swift
//  Ezhal baitk
//
//  Created by a7med on 5/3/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
import Photos


//MARK: - IMAGE PICKER DELEGATE
// This is responsible for image picker interface to access image, video and then responsibel for canceling the picker
extension AttachmentHandler: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    //MARK: - Add image to Library
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer  ) {
        if let error = error {
            // we got back an error!
            showAlertWith(title: "Save error", message: error.localizedDescription)
        } else {
            
            let fileManager = FileManager.default
            
            let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("camera.jpg")
            
            print("camera image path ==>>\(paths)")
            
            let imageData = image.jpegData(compressionQuality: 0.33)
            self.singleImagePickedBlock?(NSURL(fileURLWithPath: paths),image, imageData! as NSData)
            showAlertWith(title: "Saved!", message: "Your image has been saved to your photos.")
        }
    }
    
    func showAlertWith(title: String, message: String){
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        currentVC?.present(ac, animated: true)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        currentVC?.dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {

            print("correct img")
            if picker.sourceType == UIImagePickerController.SourceType.camera {


                let imgData = image.jpegData(compressionQuality: 0.35)
                self.imagesPickedBlock?([image], [info[UIImagePickerController.InfoKey.imageURL] as? URL], [imgData! as NSData])
                self.singleImagePickedBlock?(info[UIImagePickerController.InfoKey.imageURL] as? NSURL,image, imgData! as NSData)

                
            } else {
                print("photo lib")
                
                print("imageurl == > \(String(describing: info[UIImagePickerController.InfoKey.imageURL]))")
                let data = NSData(contentsOf: info[UIImagePickerController.InfoKey.imageURL] as! URL)!
                self.singleImagePickedBlock?(info[UIImagePickerController.InfoKey.imageURL] as? NSURL,image, data)
                
            }
            currentVC?.dismiss(animated: true, completion: nil)
        } else{
            print("Something went wrong in  image")
        }
        
        if let videoUrl = info[UIImagePickerController.InfoKey.mediaURL] as? NSURL{
            print("videourl: ", videoUrl)
            //trying compression of video
            let data = NSData(contentsOf: videoUrl as URL)!
            print("File size before compression: \(Double(data.length / 1048576)) mb")
            let duration = AVURLAsset(url: videoUrl as URL).duration.seconds
            print(duration)
            compressWithSessionStatusFunc(videoUrl, "\(duration)")
            currentVC?.dismiss(animated: true, completion: nil)
        }
        else{
            print("Something went wrong in  video")
        }
        
    }
    
    //MARK: Video Compressing technique
    fileprivate func compressWithSessionStatusFunc(_ videoUrl: NSURL,_ duration:String) {
        let capture = UIApplication.shared.generateThumbnail(path: videoUrl as URL)
        let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".MOV")
        compressVideo(inputURL: videoUrl as URL, outputURL: compressedURL) { (exportSession) in
            guard let session = exportSession else {
                return
            }
            
            switch session.status {
            case .unknown:
                break
            case .waiting:
                break
            case .exporting:
                break
            case .completed:
                guard let compressedData = NSData(contentsOf: compressedURL) else {
                    return
                }
                print("File size after compression: \(Double(compressedData.length / 1048576)) mb")
                
                DispatchQueue.main.async {
                    self.videoPickedBlock?(compressedURL as NSURL, compressedData,capture, duration)
                }
                
            case .failed:
                break
            case .cancelled:
                break
            }
        }
    }
    
    // Now compression is happening with medium quality, we can change when ever it is needed
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPreset1280x720) else {
            handler(nil)
            
            return
        }
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mov
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
    
    
}
