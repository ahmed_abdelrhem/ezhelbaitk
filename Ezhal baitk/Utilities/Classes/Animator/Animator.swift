//
//  Animator.swift
//  UITableViewCellAnimation-Article
//
//  Created by Vadym Bulavin on 9/4/18.
//  Copyright © 2018 Vadim Bulavin. All rights reserved.
//

import UIKit

final class Animator {
	private var hasAnimatedAllCells = false
	private let animationCell: AnimationCell?
    private let animationView: AnimatedView?
    private let animationConstrain : AnimationConstrain?
    private let animationAlpha : AnimationAlpha?
    

	init(animationCell: @escaping AnimationCell) {
		self.animationCell = animationCell
        self.animationView = nil
        self.animationConstrain = nil
        self.animationAlpha = nil
	}
    init(animationView: @escaping AnimatedView) {
        self.animationView = animationView
        self.animationCell = nil
        self.animationConstrain = nil
        self.animationAlpha = nil
    }
    init(animationView: @escaping AnimationAlpha) {
        self.animationView = nil
        self.animationCell = nil
        self.animationConstrain = nil
        self.animationAlpha = animationView
    }
    init(animationView: @escaping AnimationConstrain) {
        self.animationView = nil
        self.animationCell = nil
        self.animationConstrain = animationView
        self.animationAlpha = nil
    }

	func animate(cell: UITableViewCell, at indexPath: IndexPath, in tableView: UITableView) {
		guard !hasAnimatedAllCells else {
			return
		}

        animationCell!(cell, indexPath, tableView)

		hasAnimatedAllCells = tableView.isLastVisibleCell(at: indexPath)
	}
    
    func animate(child: UIView, in parent: UIView) {
       
//        animationCe(cell, indexPath, tableView)
        animationView!(child, parent)
        
        
    }
    func animate(constrain: NSLayoutConstraint, of view: UIView) {
        
        //        animationCe(cell, indexPath, tableView)
        animationConstrain!(constrain,view)
        
        
    }
    func animate( view: UIView) {
        
        //        animationCe(cell, indexPath, tableView)
        animationAlpha!(view)
        
        
    }

    func animate(cell: UICollectionViewCell, at indexPath: IndexPath, in tableView: UICollectionView) {
        guard !hasAnimatedAllCells else {
            return
        }
        
        animationCell!(cell, indexPath, tableView)
        
        hasAnimatedAllCells = tableView.isLastVisibleCell(at: indexPath)
    }
    
}
