//
//  MoveUpWithBounceTableCellAnimator.swift
//  UITableViewCellAnimation-Article
//
//  Created by Vadym Bulavin on 9/4/18.
//  Copyright © 2018 Vadim Bulavin. All rights reserved.
//

import UIKit

typealias AnimationCell = (UIView, IndexPath, UIView) -> Void
typealias AnimatedView = (UIView,UIView) -> Void
typealias AnimationConstrain = (NSLayoutConstraint,UIView) -> Void
typealias AnimationAlpha = (UIView) -> Void


enum AnimationFactory {

	static func makeFade(duration: TimeInterval, delayFactor: Double) -> AnimationCell {
		return { cell, indexPath, _ in
			cell.alpha = 0

			UIView.animate(
				withDuration: duration,
				delay: delayFactor * Double(indexPath.row),
				animations: {
					cell.alpha = 1
			})
		}
	}

	static func makeMoveUpWithBounce(rowHeight: CGFloat, duration: TimeInterval, delayFactor: Double) -> AnimationCell {
		return { cell, indexPath, tableView in
			cell.transform = CGAffineTransform(translationX: 0, y: rowHeight)

			UIView.animate(
				withDuration: duration,
				delay: delayFactor * Double(indexPath.row),
				usingSpringWithDamping: 0.4,
				initialSpringVelocity: 0.1,
				options: [.curveEaseInOut],
				animations: {
					cell.transform = CGAffineTransform(translationX: 0, y: 0)
			})
		}
	}

	static func makeSlideIn(duration: TimeInterval, delayFactor: Double) -> AnimationCell {
		return { cell, indexPath, tableView in
			cell.transform = CGAffineTransform(translationX: tableView.bounds.width, y: 0)

			UIView.animate(
				withDuration: duration,
				delay: delayFactor * Double(indexPath.row),
				options: [.curveEaseInOut],
				animations: {
					cell.transform = CGAffineTransform(translationX: 0, y: 0)
			})
		}
	}

	static func makeMoveUpWithFade(rowHeight: CGFloat, duration: TimeInterval, delayFactor: Double) -> AnimationCell {
		return { cell, indexPath, tableView in
			cell.transform = CGAffineTransform(translationX: 0, y: rowHeight / 2)
			cell.alpha = 0

			UIView.animate(
				withDuration: duration,
				delay: delayFactor * Double(indexPath.row),
				options: [.curveEaseInOut],
				animations: {
					cell.transform = CGAffineTransform(translationX: 0, y: 0)
					cell.alpha = 1
			})
		}
	}
    
    
    static func insertinAtTopAndBottom() -> AnimationCell {
        // static value 
        return { cell, indexPath, tableView in
            if indexPath.row != 0{
                cell.alpha = 0
                let transform = CATransform3DTranslate(CATransform3DIdentity, -250, 20, 0)
                cell.layer.transform = transform
                UIView.animate(withDuration: 1.0, animations: {
                    cell.alpha = 1.0
                    cell.layer.transform = CATransform3DIdentity
                })
            }
        }
    }
    
   static func insertinView(withDuration: TimeInterval,delay: TimeInterval) -> AnimatedView  {
        
        return { child,parent in
            
                    UIView.animate(withDuration: withDuration, delay: delay, options: [],
                                   animations: {
                                   child.center.x += parent.bounds.width
                    },
                                   completion: nil
                    )
        }
        
    }
    
    static func sheetOfpaper(duration: TimeInterval? ) -> AnimationCell {
        return { cell, indexPath, tableView in
            UIView.transition(with: tableView, duration: duration ?? 0.35,
                                                            options: [.curveEaseOut, .transitionCurlDown],
                                          animations: {
                                            tableView.isHidden = false
                        },
                                          completion: {_ in
                                            //transition completion
                        } )
        }
    }
    

    static func animationWithConstrain(duration: TimeInterval?,delay: TimeInterval? ) -> AnimationConstrain {
        return { constrain,superView in
            UIView.animate(withDuration: duration ?? 0.5, delay: delay ?? 0.3, options: [],
                           animations: {
                            constrain.constant += superView.bounds.width
                            superView.layoutIfNeeded()
            },
                           completion: nil
            )
        }
    }
    
    
    static func animationWithAlpha(duration: TimeInterval?,delay: TimeInterval? ) -> AnimationAlpha {
        return { xView in
            UIView.animate(withDuration: duration ?? 0.5, delay: delay ?? 0.3, options: [],
                           animations: {
                                                        xView.alpha = 1
            },
                           completion: nil
            )
        }
    }
    static func animationAlpha(Views: [UIView], duration: TimeInterval? ) {
        var delay: Double = 0.0
        let delayplus = Double(duration ?? 1.0) / (Double(exactly: Views.count) ?? 10.0 )
        for v in Views {
            UIView.animate(withDuration: duration ?? 0.5, delay: delay , options: [],
                           animations: {
                            v.alpha = 1
            },
                           completion: nil
            )
        print("interval ==>\(duration ?? 0)delay==>\(delay)")
            delay += delayplus
            

        }
            
        
            }
    
    static func animationConstrains(constrains: [NSLayoutConstraint],superView: UIView,duration: TimeInterval? ) {
        var delay: Double = 0.0
        let delayplus = Double(duration ?? 1.0) / (Double(exactly: constrains.count) ?? 10.0 )
        for con in constrains {
            UIView.animate(withDuration: duration ?? 0.5, delay: delay , options: [],
                           animations: {
                            con.constant += superView.bounds.width
                            superView.layoutIfNeeded()
            },
                           completion: nil
            )
                print("interval ==>\(duration ?? 0)delay==>\(delay)")
            delay += delayplus
            
        }
        
    }
    static func animationConstrainsSpring(constrains: [NSLayoutConstraint],superView: UIView,duration: TimeInterval? ) {
        var delay: Double = 0.0
        let delayplus = Double(duration ?? 1.0) / (Double(exactly: constrains.count) ?? 10.0 )
        for con in constrains {
            UIView.animate(withDuration: duration ?? 0.5, delay: delay,
                           usingSpringWithDamping: 0.4, initialSpringVelocity: 0.1, options: [],
                           animations: {
                            con.constant += superView.bounds.width
                            superView.layoutIfNeeded()
                            
            }, completion: nil)
            print("interval ==>\(duration ?? 0)delay==>\(delay)")
            delay += delayplus
            
        }
        
    }

  

}
