import MOLH
class SharedHandler{
    //MARK:- UISegmentedControl
    private let buttonBar = UIView()
    private var segmentControl: UISegmentedControl?
    private var view: UIView?
    private var numberOfSegments: CGFloat?
    static let shared = SharedHandler()
    
    func setupSegment(view: UIView, segmentControl: UISegmentedControl, numberOfSegments: CGFloat) {
        
        self.view = view
        self.segmentControl = segmentControl
        self.numberOfSegments = numberOfSegments
        
        segmentControl.backgroundColor = .white
        segmentControl.tintColor = .white
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkText], for: .normal)
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: mainColor], for: .selected)
        
        segmentControl.addTarget(self, action: #selector(self.segmentedControlValueChanged(_:)), for: UIControl.Event.valueChanged)
        segmentControl.removeBorders()
        
        
        // This needs to be false since we are using auto layout constraints
        buttonBar.translatesAutoresizingMaskIntoConstraints = false
        buttonBar.backgroundColor = mainColor
        
        view.addSubview(buttonBar)
        if MOLHLanguage.isRTLLanguage() {
            buttonBar.topAnchor.constraint(equalTo: segmentControl.bottomAnchor).isActive = true
            buttonBar.heightAnchor.constraint(equalToConstant: 2.5).isActive = true
            // Constrain the button bar to the left side of the segmented control
            buttonBar.rightAnchor.constraint(equalTo: segmentControl.rightAnchor).isActive = true
            // Constrain the button bar to the width of the segmented control divided by the number of segments
            buttonBar.widthAnchor.constraint(equalTo: segmentControl.widthAnchor, multiplier: CGFloat(1 / numberOfSegments)).isActive = true
        }else{
            buttonBar.topAnchor.constraint(equalTo: segmentControl.bottomAnchor).isActive = true
            buttonBar.heightAnchor.constraint(equalToConstant: 2.5).isActive = true
            // Constrain the button bar to the left side of the segmented control
            buttonBar.leftAnchor.constraint(equalTo: segmentControl.leftAnchor).isActive = true
            // Constrain the button bar to the width of the segmented control divided by the number of segments
            buttonBar.widthAnchor.constraint(equalTo: segmentControl.widthAnchor, multiplier: CGFloat(1 / numberOfSegments)).isActive = true
        }
        

        
    }
    
    @objc func segmentedControlValueChanged(_ sender: UISegmentedControl) {

        UIView.animate(withDuration: 0.3) {
            DispatchQueue.main.async { [unowned self] in
                if MOLHLanguage.isRTLLanguage() {
                    self.buttonBar.frame.origin.x = self.view!.width - (self.view!.width / self.numberOfSegments!)  - ( (self.view!.width / self.numberOfSegments!) * CGFloat(self.segmentControl!.selectedSegmentIndex))

                }else{
                    self.buttonBar.frame.origin.x = (self.view!.width / self.numberOfSegments!) * CGFloat(self.segmentControl!.selectedSegmentIndex)

                }
            }
        }
    }
    
    
    
    
    
    func setupSegmentHeader(view: UIView, segmentControl: UISegmentedControl, numberOfSegments: CGFloat) {
        
        self.view = view
        self.segmentControl = segmentControl
        self.numberOfSegments = numberOfSegments
        
        segmentControl.backgroundColor = .clear
        segmentControl.tintColor = .clear
        
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkText,NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18, weight: .medium)], for: .normal)
        segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: IconColor,NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18, weight: .medium)], for: .selected)
        
        segmentControl.addTarget(self, action: #selector(self.segmentedControlValueChanged(_:)), for: UIControl.Event.valueChanged)
        segmentControl.removeBorders()
        
        
        // This needs to be false since we are using auto layout constraints
        buttonBar.translatesAutoresizingMaskIntoConstraints = false
        buttonBar.backgroundColor = .white
        
        view.addSubview(buttonBar)
        
        if MOLHLanguage.isRTLLanguage() {
                buttonBar.topAnchor.constraint(equalTo: segmentControl.bottomAnchor).isActive = true
                buttonBar.heightAnchor.constraint(equalToConstant: 2.5).isActive = true
                // Constrain the button bar to the left side of the segmented control
                buttonBar.rightAnchor.constraint(equalTo: segmentControl.rightAnchor).isActive = true
                // Constrain the button bar to the width of the segmented control divided by the number of segments
                buttonBar.widthAnchor.constraint(equalTo: segmentControl.widthAnchor, multiplier: CGFloat(1 / numberOfSegments)).isActive = true
            }else{
                buttonBar.topAnchor.constraint(equalTo: segmentControl.bottomAnchor).isActive = true
                buttonBar.heightAnchor.constraint(equalToConstant: 2.5).isActive = true
                // Constrain the button bar to the left side of the segmented control
                buttonBar.leftAnchor.constraint(equalTo: segmentControl.leftAnchor).isActive = true
                // Constrain the button bar to the width of the segmented control divided by the number of segments
                buttonBar.widthAnchor.constraint(equalTo: segmentControl.widthAnchor, multiplier: CGFloat(1 / numberOfSegments)).isActive = true
            }
            
    }
    
    
    
}
