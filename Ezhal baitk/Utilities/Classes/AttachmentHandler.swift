//
//  AttachmentHandler.swift
//  Tabadul
//
//  Created by Islamic on 3/5/19.
//  Copyright © 2019 2grand. All rights reserved.
//

import Foundation
import UIKit
import MobileCoreServices
import AVFoundation
import Photos
import BSImagePicker


class AttachmentHandler: NSObject{
    
    static let shared = AttachmentHandler()
    internal var currentVC: UIViewController?
    private  var maxNumberOfSelections = 1
    
    //MARK: - Internal Properties
    var singleImagePickedBlock: ((NSURL?,UIImage,NSData) -> Void)?
    var singleFilePickedBlock: ((URL) -> Void)?
    var imagesPickedBlock: (([UIImage],[URL?],[NSData]) -> Void)?
    var videoPickedBlock: ((NSURL?,NSData,UIImage?,String) -> Void)?
    
    
    enum AttachmentType: String{
        case camera, video, photoLibrary, Gallary, File,video_camera
    }
    
    
    //MARK: - Constants
    struct Constants {
        static let actionFileTypeHeading = "Add a File"
        static let actionFileTypeDescription = "Choose a filetype to add..."
        static let camera = "Camera"
        static let gallary = "Gallary"
        static let photoLibrary = "photo Library"
        static let video = "Video"
        static let file = "File"
        static let videoCamera = "Capture Video"
        
        static let alertForCameraAccessMessage = "App does not have access to your camera. To enable access, tap settings and turn on Camera."
        static let alertForPhotoLibraryMessage = "App does not have access to your photos. To enable access, tap settings and turn on Photo Library Access."
        static let alertForVideoLibraryMessage = "App does not have access to your video. To enable access, tap settings and turn on Video Library Access."
        
        static let settingsBtnTitle = "Settings"
        static let cancelBtnTitle = "Cancel"
    }
    
    //MARK: - showAttachmentActionSheet
    // This function is used to show the attachment sheet for image, video, photo and file.
    func showAttachmentActionSheetPhotoLibarary(_ vc: UIViewController,_ sender: Any ) {
        currentVC = vc
        let actionSheet = UIAlertController(title: Constants.actionFileTypeHeading, message: Constants.actionFileTypeDescription, preferredStyle: .actionSheet)
        actionSheet.popoverPresentationController?.sourceView = sender as? UIView
        actionSheet.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
        
        actionSheet.addAction(UIAlertAction(title: Constants.photoLibrary, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .photoLibrary, vc: self.currentVC!)
            
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: Constants.camera, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .camera, vc: self.currentVC!)
            
        }))
        actionSheet.addAction(UIAlertAction(title: Constants.video, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .video, vc: self.currentVC!)
            
        }))
        actionSheet.addAction(UIAlertAction(title: Constants.cancelBtnTitle, style: .cancel, handler: nil))
        vc.present(actionSheet, animated: true, completion: nil)
    }
    func showAttachmentActionSheetVideo(_ vc: UIViewController,_ sender: Any ) {
        currentVC = vc
        let actionSheet = UIAlertController(title: Constants.actionFileTypeHeading, message: Constants.actionFileTypeDescription, preferredStyle: .actionSheet)
        actionSheet.popoverPresentationController?.sourceView = sender as? UIView
        actionSheet.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
        
        actionSheet.addAction(UIAlertAction(title: Constants.video, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .video, vc: self.currentVC!)
            
        }))
        actionSheet.addAction(UIAlertAction(title: Constants.cancelBtnTitle, style: .cancel, handler: nil))
        vc.present(actionSheet, animated: true, completion: nil)
    }
    
    
    func showAttachmentActionSheetGallery(_ vc: UIViewController,_ sender: Any ) {
        currentVC = vc
        let actionSheet = UIAlertController(title: Constants.actionFileTypeHeading, message: Constants.actionFileTypeDescription, preferredStyle: .actionSheet)
        actionSheet.popoverPresentationController?.sourceView = sender as? UIView
        actionSheet.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
        
        actionSheet.addAction(UIAlertAction(title: Constants.photoLibrary, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .Gallary, vc: self.currentVC!)
            
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: Constants.camera, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .camera, vc: self.currentVC!)
            
        }))
        actionSheet.addAction(UIAlertAction(title: Constants.video, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .video, vc: self.currentVC!)
            
        }))
        actionSheet.addAction(UIAlertAction(title: Constants.videoCamera, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .video_camera, vc: self.currentVC!)
            
        }))
        actionSheet.addAction(UIAlertAction(title: Constants.cancelBtnTitle, style: .cancel, handler: nil))
        vc.present(actionSheet, animated: true, completion: nil)
    }
    
    func showAttachmentActionSheetGalleryWithoutVedio(_ vc: UIViewController,_ sender: Any ) {
        currentVC = vc
        let actionSheet = UIAlertController(title: Constants.actionFileTypeHeading, message: Constants.actionFileTypeDescription, preferredStyle: .actionSheet)
        actionSheet.popoverPresentationController?.sourceView = sender as? UIView
        actionSheet.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
        
        actionSheet.addAction(UIAlertAction(title: Constants.photoLibrary, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .Gallary, vc: self.currentVC!)
            
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: Constants.camera, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .camera, vc: self.currentVC!)
            
        }))
        actionSheet.addAction(UIAlertAction(title: Constants.cancelBtnTitle, style: .cancel, handler: nil))
        vc.present(actionSheet, animated: true, completion: nil)
    }
    
    func showAttachmentActionSheetPhotosWithoutVedio(_ vc: UIViewController,_ sender: Any ,_ max_images: Int ) {
        currentVC = vc
        maxNumberOfSelections = max_images
        let actionSheet = UIAlertController(title: Constants.actionFileTypeHeading, message: Constants.actionFileTypeDescription, preferredStyle: .actionSheet)
        actionSheet.popoverPresentationController?.sourceView = sender as? UIView
        actionSheet.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
        
        actionSheet.addAction(UIAlertAction(title: Constants.photoLibrary, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .photoLibrary, vc: self.currentVC!)
            
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: Constants.camera, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .camera, vc: self.currentVC!)
            
        }))
        actionSheet.addAction(UIAlertAction(title: Constants.cancelBtnTitle, style: .cancel, handler: nil))
        vc.present(actionSheet, animated: true, completion: nil)
    }
    
    func showAttachmentImage(_ vc: UIViewController,_ sender: AnyObject ) {
        currentVC = vc
        let actionSheet = UIAlertController(title: Constants.actionFileTypeHeading, message: Constants.actionFileTypeDescription, preferredStyle: .actionSheet)
        actionSheet.popoverPresentationController?.sourceView = sender as? UIView
        actionSheet.popoverPresentationController?.sourceRect = sender.bounds
        
        actionSheet.addAction(UIAlertAction(title: Constants.camera, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .camera, vc: self.currentVC!)
        }))
        
        actionSheet.addAction(UIAlertAction(title: Constants.gallary, style: .default, handler: { (action) -> Void in
            self.authorisationStatus(attachmentTypeEnum: .Gallary, vc: self.currentVC!)
        }))
        
        actionSheet.addAction(UIAlertAction(title: Constants.cancelBtnTitle, style: .cancel, handler: nil))
        
        vc.present(actionSheet, animated: true, completion: nil)
    }
    func showAttachmentFile(_ vc: UIViewController,_ sender: AnyObject ) {
        currentVC = vc
        let actionSheet = UIAlertController(title: Constants.actionFileTypeHeading, message: Constants.actionFileTypeDescription, preferredStyle: .actionSheet)
        actionSheet.popoverPresentationController?.sourceView = sender as? UIView
        actionSheet.popoverPresentationController?.sourceRect = sender.bounds
        
        
        actionSheet.addAction(UIAlertAction(title: Constants.file, style: .default, handler: { (action) -> Void in
            self.pickFile()
        }))
        
        actionSheet.addAction(UIAlertAction(title: Constants.cancelBtnTitle, style: .cancel, handler: nil))
        
        vc.present(actionSheet, animated: true, completion: nil)
    }
    
    //MARK: - Authorisation Status
    // This is used to check the authorisation status whether user gives access to import the image, photo library, video.
    // if the user gives access, then we can import the data safely
    // if not show them alert to access from settings.
    func authorisationStatus(attachmentTypeEnum: AttachmentType, vc: UIViewController){
        currentVC = vc
        
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            if attachmentTypeEnum == AttachmentType.camera{
                DispatchQueue.main.async { [weak self] in
                    self?.openCamera()
                    
                }
            }
            if attachmentTypeEnum == AttachmentType.Gallary{
                DispatchQueue.main.async { [weak self] in
                    self?.photoLibrary()
                    
                }
                
            }
            
            if attachmentTypeEnum == AttachmentType.photoLibrary{
                DispatchQueue.main.async { [weak self] in
                    self?.imagesLibrary()
                    
                }
                
            }
            if attachmentTypeEnum == AttachmentType.video{
                videoLibrary()
            }
            if attachmentTypeEnum == AttachmentType.video_camera{
                videoCameara()
            }
        case .denied:
            print("permission denied")
            self.addAlertForSettings(attachmentTypeEnum)
        case .notDetermined:
            print("Permission Not Determined")
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == PHAuthorizationStatus.authorized{
                    // photo library access given
                    print("access given")
                    if attachmentTypeEnum == AttachmentType.photoLibrary{
                        self.imagesLibrary()
                    }
                    if attachmentTypeEnum == AttachmentType.video{
                        self.videoLibrary()
                    }
                    if attachmentTypeEnum == AttachmentType.video_camera{
                        self.videoCameara()
                    }
                }else{
                    print("restriced manually")
                    self.addAlertForSettings(attachmentTypeEnum)
                }
            })
        case .restricted:
            print("permission restricted")
            self.addAlertForSettings(attachmentTypeEnum)
        default:
            break
        }
    }
    
    //MARK: - PHOTO PICKER
    //This function is used to open camera from the iphone and
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .camera
            myPickerController.cameraCaptureMode  = .photo
            
            currentVC?.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    
    //MARK: - PHOTO PICKER
    func photoLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .photoLibrary
            currentVC?.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    
    //MARK:- Multi Images Picker
    func imagesLibrary(){
        
        // create an instance
        let vc = ImagePickerController()
        vc.navigationBar.tintColor = FontColor
        vc.navigationBar.barTintColor = .white
        //display picture gallery
        currentVC?.presentImagePicker(vc, animated: true, select: { (asset: Asset) -> Void in
            
        }, deselect: { (asset: Asset) -> Void in
            // User deselected an assets.
        }, cancel: { (assets: [Asset]) -> Void in
            // User cancelled. And this where the assets currently selected.
        }, finish: { (assets: [Asset]) -> Void in
            // User finished with these assets
            
            self.convertAssetToImages(assets)
        }, completion: nil)
        
    }
    
    func convertAssetToImages(_ SelectedAssets: [Asset]) -> Void {
        var images : [UIImage] = []
        var datas : [Data] = []
        var urls : [URL] = []

            for asset in SelectedAssets{
                
                guard let data = asset.image?.jpegData(compressionQuality: 1) else{continue}
                datas.append(data)
                guard let image = asset.image else { return  }
                images.append(image)
                guard let url = asset.url else { return  }
                urls.append(url)


            }
            
            self.imagesPickedBlock?(images, urls, datas as [NSData])
            
    }
    
    
    //MARK: - VIDEO PICKER
    func videoLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .photoLibrary
            myPickerController.mediaTypes = [kUTTypeMovie as String, kUTTypeVideo as String,kUTTypeMPEG4 as String,kUTTypeMPEG as String]
            myPickerController.videoMaximumDuration = TimeInterval(30.0)
            
            currentVC?.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    func videoCameara(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .camera
            myPickerController.mediaTypes = [kUTTypeMovie as String, kUTTypeVideo as String,kUTTypeMPEG4 as String,kUTTypeMPEG as String]
            myPickerController.cameraCaptureMode = .video
            myPickerController.videoMaximumDuration = TimeInterval(30.0)
            
            currentVC?.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    
    //MARK: - SETTINGS ALERT
    func addAlertForSettings(_ attachmentTypeEnum: AttachmentType){
        var alertTitle: String = ""
        
        if attachmentTypeEnum == AttachmentType.camera{
            alertTitle = Constants.alertForCameraAccessMessage
        }
        
        if attachmentTypeEnum == AttachmentType.Gallary{
            alertTitle = Constants.alertForPhotoLibraryMessage
        }
        
        if attachmentTypeEnum == AttachmentType.photoLibrary{
            alertTitle = Constants.alertForPhotoLibraryMessage
        }
        if attachmentTypeEnum == AttachmentType.video{
            alertTitle = Constants.alertForVideoLibraryMessage
        }
        if attachmentTypeEnum == AttachmentType.video_camera{
            alertTitle = Constants.alertForCameraAccessMessage
        }
        
        let cameraUnavailableAlertController = UIAlertController (title: alertTitle , message: nil, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: Constants.settingsBtnTitle, style: .destructive) { (_) -> Void in
            let settingsUrl = NSURL(string:UIApplication.openSettingsURLString)
            if let url = settingsUrl {
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            }
        }
        let cancelAction = UIAlertAction(title: Constants.cancelBtnTitle, style: .default, handler: nil)
        cameraUnavailableAlertController .addAction(cancelAction)
        cameraUnavailableAlertController .addAction(settingsAction)
        currentVC?.present(cameraUnavailableAlertController , animated: true, completion: nil)
    }
    
}



extension AttachmentHandler{
    
    func saveImageDocumentDirectory(_ image: UIImage?){
        
        //            let documentDirectory = FileManager.SearchPathDirectory.documentDirectory
        //            let userDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        //            let paths             = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)
        let fileManager = FileManager.default
        
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("camera.jpg")
        
        print("camera image path ==>>\(paths)")
        
        let imageData = image?.jpegData(compressionQuality: 1)
        self.singleImagePickedBlock?(NSURL(fileURLWithPath: paths),image!, imageData as! NSData)
        
        
        fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
        
    }
    
    func getDirectoryPath() -> String {
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        
        let documentsDirectory = paths[0]
        return documentsDirectory
        
    }
    
    func getImage(){
        
        let fileManager = FileManager.default
        
        let imagePAth = (self.getDirectoryPath() as NSString).appendingPathComponent("camera.jpg")
        //        print("imagePAth ==>> getImage ==>> \(imagePAth) ")
        
        if fileManager.fileExists(atPath: imagePAth){
            
            _ = UIImage(contentsOfFile: imagePAth)
            
        }else{
            
            print("No Image")
            
        }
        
    }
    
    func createDirectory(){
        
        let fileManager = FileManager.default
        
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("customDirectory")
        
        if !fileManager.fileExists(atPath: paths){
            
            try! fileManager.createDirectory(atPath: paths, withIntermediateDirectories: true, attributes: nil)
            
        }else{
            
            print("Already dictionary created.")
            
        }
        
    }
    
    func deleteDirectory(){
        
        let fileManager = FileManager.default
        
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("customDirectory")
        
        if fileManager.fileExists(atPath: paths){
            
            try! fileManager.removeItem(atPath: paths)
            
        }else{
            
            print("Something wronge.")
            
        }
        
    }
    
    
}
extension AttachmentHandler : UIDocumentMenuDelegate,UIDocumentPickerDelegate{
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        print("#f",myURL)
        singleFilePickedBlock!(myURL)
        
    }
    
    
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        
        currentVC?.present(documentPicker, animated: true, completion: nil)
    }
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        currentVC?.dismiss(animated: true, completion: nil)
    }
    func pickFile(){
        
        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        currentVC?.present(importMenu, animated: true, completion: nil)
    }
    
    
}



