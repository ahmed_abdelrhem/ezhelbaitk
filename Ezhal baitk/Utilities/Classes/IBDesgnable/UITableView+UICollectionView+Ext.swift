//
//  UITableView+Ext.swift
//  UITableViewCellAnimation-Article
//
//  Created by Vadym Bulavin on 9/4/18.
//  Copyright © 2018 Vadim Bulavin. All rights reserved.
//

import UIKit
import MOLH
extension UITableView {
	func isLastVisibleCell(at indexPath: IndexPath) -> Bool {
		guard let lastIndexPath = indexPathsForVisibleRows?.last else {
			return false
		}

		return lastIndexPath == indexPath
	}
}


extension UICollectionView {
    func isLastVisibleCell(at indexPath: IndexPath) -> Bool {
        guard let lastIndexPath = indexPathsForVisibleItems.last else {
            return false
        }
        
        return lastIndexPath == indexPath
    }
}


extension  UICollectionViewFlowLayout{
    open override var flipsHorizontallyInOppositeLayoutDirection: Bool {
        return true
    }

//    open override var developmentLayoutDirection: UIUserInterfaceLayoutDirection {
//        return UIUserInterfaceLayoutDirection.rightToLeft
//    }
}
