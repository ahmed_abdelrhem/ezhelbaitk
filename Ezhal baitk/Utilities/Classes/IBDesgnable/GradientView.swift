//
//  GradientView.swift
//  قصص  و عبر
//
//  Created by a7med on 5/10/18.
//  Copyright © 2018 a7med. All rights reserved.
//

import UIKit



@IBDesignable
class GradientView: UIView {
    override class var layerClass: AnyClass {
        get {
            return CAGradientLayer.self
        }
    }
    
    @IBInspectable var isHorizontal: Bool = true {
        didSet {
            updateView()
        }
    }
    @IBInspectable var firstColor: UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    @IBInspectable var secondColor: UIColor = UIColor.clear {
        didSet {
            updateView()
        }
    }
    
  
    func updateView() {
        let layer = self.layer as! CAGradientLayer
        layer.colors = [firstColor, secondColor].map{$0.cgColor}
        if (self.isHorizontal) {
            layer.startPoint = CGPoint(x: 0.8, y: 1)
            layer.endPoint = CGPoint (x: 0, y: 0)
            
           
        } else {
            layer.startPoint = CGPoint(x: 0, y: 0)
            layer.endPoint = CGPoint (x: 0.8, y: 1)
          
           
        }
    }
}



