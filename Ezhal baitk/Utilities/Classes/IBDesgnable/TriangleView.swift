import UIKit
class TriangleView: UIView {

    override func draw(_ rect: CGRect) {
        let path = UIBezierPath()
        let point = CGPoint(x: rect.center.x, y: 0)
        path.move(to: point)
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        path.addLine(to: CGPoint(x: 0, y: rect.maxY))
        path.close()

        UIColor.white.setFill()
        path.fill()
    }

}
