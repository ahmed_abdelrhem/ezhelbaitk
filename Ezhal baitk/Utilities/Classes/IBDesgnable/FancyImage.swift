//
//  CircleImage.swift
//  SocialApp
//
//  Created by OSX on 5/27/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit

class FancyImage: UIImageView {

    override func awakeFromNib() {
        super.awakeFromNib()
        layer.shadowColor = UIColor(red: 120.0 / 255.0, green: 120.0 / 255.0, blue: 120.0 / 255.0, alpha: 120.0 / 255.0).cgColor
        layer.shadowOpacity = 8.8
        layer.shadowRadius = 5.0
        layer.borderWidth = 0.5
        layer.borderColor = #colorLiteral(red: 0.2588235294, green: 0.2039215686, blue: 0.6745098039, alpha: 0.5)
        layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.size.width / 2
        clipsToBounds = true
    }

    
}
