//
//  Enums.swift
//  Moon Shop
//
//  Created by apple on 1/13/20.
//  Copyright © 2020 2grand. All rights reserved.
//

import UIKit
//Mark : -  shop types
enum AppType: Int,CaseIterable {
    case consumer_market = 1
    case market_service = 2
    case industries = 3
    case famous = 4
    case  photographer = 5
    case companies = 6
    case institutions = 8
    case clinic = 9
    case beauty = 10
    
}



//Mark : -  Storyboard

enum AppStoryboard : String {
    
    case Main = "Main"
    case Shop = "Shop"
    case Company = "Company"
    case Shared = "Shared"
    case FamousCycle = "FamousCycle"
    case Institution = "Institution"
    case FamousApp = "FamousApp"
    case Clinic = "Clinic"
    case Beauty = "Beauty"
    case Business = "Business"
    
    
    
    
}

//Mark : -  language

enum AppLaguage : String {
    
    case en = "en"
    case ar = "ar"
    
}

enum AppHome : String {
    
    case shop = "SWRevealViewController_Shop"
    case company = "SWRevealViewController_Company"
    case Institution = "SWRevealViewController_Institution"
    case famous_app = "SWRevealViewController_FamousApp"
    case ar = "ar"
    case swrevealviewcontroller = "SWRevealViewController"
    case MainNav = "MainNav"

    
}


