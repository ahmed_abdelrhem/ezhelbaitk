

import Foundation
import Firebase
import FirebaseMessaging
import FirebaseInstanceID
import UserNotifications
import SwiftyJSON
import Alamofire
extension AppDelegate{
    func configureFB(_ application: UIApplication)  {
        //        MARK:- Firebase
        FirebaseApp.configure()
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (success, error) in
            if error == nil {
                print("Successful Authorization")
            }
        }
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        /////
        
        
        application.registerForRemoteNotifications()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshToken(notification:)), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        
        ///////
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                DEF.FireBaseToken = result.token
            }
        }
        //                //////
        //
    }
    
    @objc func refreshToken (notification : NSNotification){
        
        //update device token method can be call here
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                DEF.FireBaseToken = result.token
            }
        }
        FBHandler()
    }
    
    func FBHandler(){
        Messaging.messaging().shouldEstablishDirectChannel = true
    }
    //
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // print("Unable to register for remote notifications: \(error.localizedDescription)")
        print("APNs registration failed: \(error)")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        print("APNs device token: \(deviceTokenString)")
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("FCM token: \(result.token)")
                DEF.FireBaseToken = result.token
            }
        }
    }
    
}


@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("willPresent")
        let userInfo = notification.request.content.userInfo as! [String : Any]
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        print("user info:",userInfo)
        parsing(userInfo)
        
        let myJson = JSON(userInfo)
        
        completionHandler([.alert, .badge, .sound])
        
        print("myJson: ",myJson)
        print("RECIVE")
        _ = UIApplication.shared.applicationState
        if let order_id = fb_msg?.order_id  {
//            _NC.post(name: .on_Order, object: nil, userInfo: ["fb":fb_msg!])
            
        }
        
        
        
        
        
        
        
        
    }
    
    
    // Push notification received
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
        print("DidReceiveRemoteNotification")
        
    }
    
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("didReceive")
        let userInfo = response.notification.request.content.userInfo
        print(userInfo)
        parsing(userInfo)
        
        let myJson = JSON(userInfo)
        
        print("user info2:",userInfo)
        print("myJson: ",myJson)
        print("TAPPED")
        
        
        if let order_id = fb_msg?.order_id  {
            if order_id == "" {
                onVist()
            }else{
                onOrder()

            }
        }
        
        
        
        
        
        
    }
    
    func onVist() {
        let state = UIApplication.shared.applicationState
        
        if state == .active {
            _NC.post(name: .on_Vist, object: nil, userInfo: ["fb":fb_msg!])
            
        }else{
            
            guard let cat_idStr = fb_msg?.category_id else {
                return
            }
            guard let cat_id = Int(cat_idStr) else {
                return
            }
            let vc = UIStoryboard(name: AppStoryboard.Shared.rawValue, bundle: nil).instantiateViewController(withIdentifier: "SubStatusVC") as! SubStatusVC
            vc.cat_id = cat_id

            let nav = UINavigationController(rootViewController: vc)
            let menu =  MenuVC()
            let Rvc : SWRevealViewController =  SWRevealViewController(rearViewController: menu, frontViewController: nav)
            Rvc.rightViewController = menu
            UIApplication.shared.keyWindow?.rootViewController = Rvc
            
        }
        
    }
    
    
    func onOrder() {
        let state = UIApplication.shared.applicationState
        
        if state == .active {
            _NC.post(name: .on_Order, object: nil, userInfo: ["fb":fb_msg!])
            
        }else{
            
            guard let order_idStr = fb_msg?.order_id else {
                return
            }
            guard let order_id = Int(order_idStr) else {
                return
            }
            guard let statusStr = fb_msg?.status else {
                return
            }
            guard let status = Int(statusStr) else {
                return
            }
            guard let cat = fb_msg?.cat_name else {
                         return
                     }
            guard let tech = fb_msg?.tech_name else {
                                    return
                                }
            
            guard let img  = fb_msg?.tech_image else {
                           return
                       }
            let vc = UIStoryboard(name: AppStoryboard.Shared.rawValue, bundle: nil).instantiateViewController(withIdentifier: "StatusVC") as! StatusVC
            vc.order = (status:status,order_id: order_id,tech,cat,img)
            vc.WithMenu = true
            let nav = UINavigationController(rootViewController: vc)
            let menu =  MenuVC()
            let Rvc : SWRevealViewController =  SWRevealViewController(rearViewController: menu, frontViewController: nav)
            Rvc.rightViewController = menu
            UIApplication.shared.keyWindow?.rootViewController = Rvc
            
        }
        
    }
    
}

// [END ios_10_message_handling]

extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
        UserDefaults.standard.setValue(fcmToken, forKey: "device_token")
        UserDefaults.standard.synchronize()
        
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    
    
}


extension AppDelegate {
    
    func parsing(_ userInfo: [AnyHashable : Any])  {
        
        let decoder = JSONDecoder()
        if let data = try? JSONSerialization.data(withJSONObject: userInfo) {
            
            do {
                fb_msg = try decoder.decode(Firebase_Base.self, from: data)
            }catch{
                 print("##error\(error)")
                        let error = AFError.responseSerializationFailed(reason: .decodingFailed(error: error))
                
            }
            
        }
        
    }
    
}
