//
//  AppMessages.swift
//  Moon Shop
//
//  Created by apple on 3/10/20.
//  Copyright © 2020 2grand. All rights reserved.
//

import Foundation


let _Addservice = "you must enter service ".localized
let _Name = "name".localized
let _And = "and".localized
let _Price = "price".localized
let _Service = "Service".localized
let _Services = "Services".localized

let _NewService =  "New Service".localized
let _DeleteItem =  "are you sure to delete this Item.".localized
let _Delete =  "Delete".localized
let _Active = "Active".localized
let _Confirmed = "Confirmed".localized
let _Finished = "Finished".localized
let _Cancelled = "Cancelled".localized
let _Years = "year".localized
let _OFFLine = "The Internet connection appears to be offline".localized

let _AddServices = "Add Services".localized
let _AtLeastOne = "You must Enter one half at least.".localized
let _SelectTime = "Select Time".localized
let _Ok = "OK".localized
let _ConnectionError = " Connection Error ".localized
let _EmptyData = "Empty Data".localized
let _Description = "Description".localized
let _Done = "Done".localized
let _Info = "Info".localized
let _Please = "Please, you should enter".localized
let _Fees = "fees".localized
let _Experience = "experience years".localized
let _Specialist = "specialist".localized
let _AcceptReservation = "are you sure to Accept this Reservation.".localized
let _CancelReservtion = "are you sure to Cancel this Reservation.".localized
let _ProfilePhoto = "Profile Photo".localized
let _SearchName = "Search By Name".localized
let _AlbumCover = " Album Cover is Selected ".localized
let _Album = "album".localized
let _Cover = "cover photo".localized
let _AtLeastPhoto = "Please, you should add one photo to album at least.".localized
let _Edit = "Edit".localized
let _Yes = "YES".localized
let _AddTOAlbum = "Are You Sure to Add This Photo To ALBUM ?".localized
let _Photo = "photo".localized
let _Video = "Video".localized
let _AddService = "Add Service".localized
let _Added = "Added".localized
let _Location = "location".localized
let _Hotel = "Hotel".localized
let _Address = "address".localized
let _Map = "map location".localized
let _Email = "E-mail".localized
let _Phone = "Phone Number".localized
let _Mobile = "mobile".localized
let _Problem = "Problem".localized

let _MaxImages = "Maximum number of images for product is 6".localized
let _All = "All".localized
let _Woman = "Woman".localized
let _Man = "man".localized
let _Cancel = "Cancel".localized
let _SwitchTo = "Your account Switched To .".localized
let _Shop = "Shop".localized
let _Famous = "Famous".localized
let _Photographer = "Photographer".localized
let _Logout =  "are you sure to Log Out.".localized
let _RestLanguage = "Application will be shut down to configure your language setting.".localized
let  _ProductDetails = "Product Details".localized
let _MiniQuantity = "Please, you should enter minimum quantity less than quantity of product.".localized
let _Product = "Product".localized
let _Add = "Add".localized
let _Size = "Size".localized
let _Color = "Color".localized
let _Quantity = "quantity".localized
let _Minimum = "minimum".localized
let _WholeSale = "whole sale".localized
let _Less = "less than".localized
let _Or = "or".localized
let _Department = "Department".localized
let _En = "en".localized
let _Password = "Password".localized
let _PasswordCount = "Password lenght more than 5 digits or charachers".localized

let _AccountType = "Acount Type".localized
let _PleaseLanguage = "Please Choose Your Language".localized
let _Code = "Code".localized
let _Selected = "You selected ".localized
let _Sectoral = "Sectoral".localized
let _Category = "category".localized
let _Categories = "categories".localized
let _Nick = "nick".localized
let _Your = "your".localized
let _MatchPassword = "Please, confirm password must match your password.".localized

let _City = "City".localized
let _Area = "Area".localized
let _AreaNum = "Area Number".localized

let _Block = "block".localized
let _Street = "street".localized
let _House = "house".localized
let _Floor = "floor".localized

let _Regsion = "regsion".localized
let _AcceptTerms = "Please, you should accept our  terms  and policies.".localized
let _Type = "type".localized
let _CommercialImages = "commercial registration image (back nd front)".localized
let _TaxesImages = "taxes image (back nd front)".localized
let _Ar = "ar".localized
let _Consumer = "consumer".localized
let _Market = "market".localized
let _Chat = "Chat".localized
let _Home = "Home".localized
let _MyProfile = "My Profile".localized
let _ActiveJobs = "Active Jobs".localized
let _Reviews = "Reviews".localized
let _Subscription = "Subscription".localized
let _SubscriptionStatus = "Subscription status".localized
let _History = "History".localized
let _Addresses = "addresses".localized
let _Login = "You must login first to complete process.".localized
let _ChatHistory = "Chat History".localized
let _Credit = "About App".localized
let _Notifications = "Notifications".localized
let _ContactUs = "Contact Us".localized
let _PhotoGraphers = "PhotoGraphers".localized
let _AboutUs = "About App".localized
let _PrivacyPolicy = "Privacy Policy".localized
let _Language = "Language".localized
let _ShareApp = "Share App".localized
let _RateApp = "Rate App".localized
let _BeShop = "Be Shop".localized
let _LogOut = "Logout".localized
let _Package = "Package".localized
let _Back = "Back".localized
let _ConsumerMarket = "consumer_market".localized
let _MarketService = "market_service".localized
let _Industries = "industries".localized
let _Companies = "companies".localized
let _Institutions = "institutions".localized
let _Clinic = "clinic".localized
let _Beauty = "beauty".localized
let _Doctor = "Doctor".localized
let _Find = "Find".localized
let _Result =  "Results".localized
let _Following =  "Following".localized
let _Follower =  "Followers".localized
let _Order = "Order".localized
let _PickCategory =  "Pick Category".localized
let _PickUnit = "Pick Category".localized
let _PickService = "Pick Service".localized


