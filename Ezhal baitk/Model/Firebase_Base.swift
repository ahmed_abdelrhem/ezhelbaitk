/* 
 Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 
 For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar
 
 */

import Foundation
struct Firebase_Base : Codable {
    let gcm_notification_status : String?
    let message : String?
    let title : String?
    let tech_name : String?
    let tech_image : String?
    let cat_name : String?
    let category_id : String?

    
    let google_c_sender_id : String?
    let order_number : String?
    let gcm_message_id : String?
    let aps : Aps?
    let gcm_notification_message : String?
    let gcm_notification_type : String?
    let body : String?
    let status : String?
    let type : String?
    let sound : String?
    let gcm_notification_order_id : String?
    let order_id : String?
    let gcm_notification_order_number : String?
    let google_c_a_e : String?

    enum CodingKeys: String, CodingKey {

        case gcm_notification_status = "gcm.notification.status"
        case message = "message"
        case title = "title"
        case google_c_sender_id = "google.c.sender.id"
        case order_number = "order_number"
        case gcm_message_id = "gcm.message_id"
        case aps = "aps"
        case gcm_notification_message = "gcm.notification.message"
        case gcm_notification_type = "gcm.notification.type"
        case body = "body"
        case status = "status"
        case type = "type"
        case sound = "sound"
        case gcm_notification_order_id = "gcm.notification.order_id"
        case order_id = "order_id"
        case gcm_notification_order_number = "gcm.notification.order_number"
        case google_c_a_e = "google.c.a.e"
        case tech_name = "tech_name"
        case cat_name = "cat_name"
        case tech_image = "tech_image"
        case category_id = "category_id"



    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        gcm_notification_status = try values.decodeIfPresent(String.self, forKey: .gcm_notification_status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        tech_name = try values.decodeIfPresent(String.self, forKey: .tech_name)
        cat_name = try values.decodeIfPresent(String.self, forKey: .cat_name)
        tech_image = try values.decodeIfPresent(String.self, forKey: .tech_image)


        google_c_sender_id = try values.decodeIfPresent(String.self, forKey: .google_c_sender_id)
        order_number = try values.decodeIfPresent(String.self, forKey: .order_number)
        gcm_message_id = try values.decodeIfPresent(String.self, forKey: .gcm_message_id)
        aps = try values.decodeIfPresent(Aps.self, forKey: .aps)
        gcm_notification_message = try values.decodeIfPresent(String.self, forKey: .gcm_notification_message)
        gcm_notification_type = try values.decodeIfPresent(String.self, forKey: .gcm_notification_type)
        body = try values.decodeIfPresent(String.self, forKey: .body)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        sound = try values.decodeIfPresent(String.self, forKey: .sound)
        gcm_notification_order_id = try values.decodeIfPresent(String.self, forKey: .gcm_notification_order_id)
        order_id = try values.decodeIfPresent(String.self, forKey: .order_id)
        gcm_notification_order_number = try values.decodeIfPresent(String.self, forKey: .gcm_notification_order_number)
        google_c_a_e = try values.decodeIfPresent(String.self, forKey: .google_c_a_e)
        category_id = try values.decodeIfPresent(String.self, forKey: .category_id)

    }

}
