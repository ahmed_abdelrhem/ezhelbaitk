//
//  CircularProgress.swift
//  Ezhal baitk
//
//  Created by a7med on 5/3/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

class CircularProgress: UIView {
    let shapeLayer = CAShapeLayer()
    var pulsatingLayer = CAShapeLayer()
    var radius:CGFloat = 15
    var toValue = 0.5
    var textColor = UIColor(hexString: "#FF7314")
    var trackColor = UIColor(hexString: "#F2F2F2")
    var shapeColor = UIColor(hexString: "#FF7314")
    var filledColor = UIColor.white
    
    private var percentageLbl: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont(name: "Roboto-Bold", size: 8)
        label.textColor = .black
        return label
    }()
    
    
    
    
    override func draw(_ rect: CGRect) {
        setUpProgress(rect)
        animateCircle()
        
    }
    
    fileprivate func setUpProgress(_ rect: CGRect) {
        // set tracklayer
        let trackLayer = CAShapeLayer()
        let circularPath = UIBezierPath(arcCenter: .zero, radius: radius, startAngle: 0, endAngle: (2 * CGFloat.pi), clockwise: true)
        // set pulsatingLayer
             pulsatingLayer = CAShapeLayer()
             pulsatingLayer.path = circularPath.cgPath
             pulsatingLayer.strokeColor = UIColor.clear.cgColor
             pulsatingLayer.lineWidth = 4
        pulsatingLayer.fillColor = shapeColor.withAlphaComponent(0.7).cgColor
             pulsatingLayer.lineCap = CAShapeLayerLineCap.round
             pulsatingLayer.position = rect.center
             layer.addSublayer(pulsatingLayer)
        // set trackLayer
        trackLayer.path = circularPath.cgPath
        trackLayer.strokeColor = trackColor.cgColor
        trackLayer.lineWidth = 4
        trackLayer.fillColor = filledColor.cgColor
        trackLayer.lineCap = CAShapeLayerLineCap.round
        trackLayer.position = rect.center
        layer.addSublayer(trackLayer)
     
//        animatePulsating()
        // set shapelayer
        shapeLayer.path = circularPath.cgPath
        shapeLayer.strokeColor = shapeColor.cgColor
        shapeLayer.lineWidth = 3.5
        shapeLayer.fillColor = filledColor.cgColor
        shapeLayer.lineCap = CAShapeLayerLineCap.round
        shapeLayer.position = rect.center
        shapeLayer.transform = CATransform3DMakeRotation(-CGFloat.pi / 2, 0, 0, 1)
        shapeLayer.strokeEnd = 0
        layer.addSublayer(shapeLayer)
        setUpLabel(rect)
        
    }
    
    
    fileprivate func setUpLabel(_ rect: CGRect) {
        // Drawing code
        percentageLbl.text = "\(Int(toValue * 100))%"
        percentageLbl.textColor = textColor
        self.addSubview(percentageLbl)
        percentageLbl.frame = CGRect(x: 0, y: 0, width: rect.width - 10, height: rect.height - 10)
        percentageLbl.center = rect.center
        
    }
    fileprivate func animatePulsating(){
        let basicAnimation = CABasicAnimation(keyPath: "transform.scale")
        basicAnimation.toValue = 1.3
        basicAnimation.duration = 0.8
        basicAnimation.timingFunction = CAMediaTimingFunction(name: .easeOut)
        basicAnimation.autoreverses = true
        basicAnimation.repeatCount = Float.infinity
         pulsatingLayer.add(basicAnimation, forKey: "pulsing")
    }
    
    fileprivate func animateCircle() {
        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        basicAnimation.toValue = toValue
        basicAnimation.duration = 2
        basicAnimation.fillMode = CAMediaTimingFillMode.forwards
        basicAnimation.isRemovedOnCompletion = false
        shapeLayer.add(basicAnimation, forKey: "urSoBasic")
    }
    
    
}
