//
//  CustomPickerView.swift
//  Moon Shop
//
//  Created by apple on 2/3/20.
//  Copyright © 2020 2grand. All rights reserved.
//

import UIKit

class CustomPickerView: UIView {
    
    @IBOutlet weak var colorView: UIViewX!
    
    @IBOutlet weak var colorValue: UILabel!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    func configure(_ color: String)  {
        colorValue.text = color
        colorView.backgroundColor = HexStringToUIColor.hexStringToUIColor(hex: color)
    }
}
