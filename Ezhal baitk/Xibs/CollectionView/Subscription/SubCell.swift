//
//  SubCell.swift
//  Ezhal baitk
//
//  Created by a7med on 6/5/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
protocol SubCellView:class {
    func setMonth(_ mon:String)
    func setPrice(_ price:String,_ currency:String)
    func setCategory(_ category:String)
    func didSelect()
    func didUnselect()
    
    
}

class SubCell: UICollectionViewCell,SubCellView {
    
    
    
    
    
    
    @IBOutlet weak var containterView: UIViewX!
    @IBOutlet weak var pkLbl: UILabel!
    
    @IBOutlet weak var monthlyLbl: UILabel!
    @IBOutlet weak var monLbl: UILabel!
    @IBOutlet weak var PriceLbl: UILabel!
    @IBOutlet weak var categoryLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setMonth(_ mon: String) {
        monLbl.text = mon + "Months".localized
    }
    
    func setPrice(_ price:String,_ currency:String) {
        let prc = price + " " + currency 
        PriceLbl.text = prc 
    }
    
    func setCategory(_ category: String) {
        categoryLbl.text = category
    }
    func didSelect() {
        containterView.backgroundColor = mainColor
        pkLbl.textColor = .white
        monthlyLbl.textColor = .white
        monLbl.textColor = .white
        PriceLbl.textColor = .white
        categoryLbl.textColor = .white
    }
    
    func didUnselect() {
        containterView.backgroundColor = .white
        pkLbl.textColor = mainColor
        monthlyLbl.textColor = .darkText
        monLbl.textColor = mainColor
        PriceLbl.textColor = .darkText
        categoryLbl.textColor = .darkText
    }
    
}
