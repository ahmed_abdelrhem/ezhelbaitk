//
//  UnitCell.swift
//  Ezhal baitk
//
//  Created by a7med on 5/6/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
import SDWebImage
protocol UnitCellView {
    func setName(name: String)
     func setPhoto(with Url: String)
}
class UnitCell: UICollectionViewCell,UnitCellView {
   
 
    
    @IBOutlet weak var photo: UIImageViewX!
    
    @IBOutlet weak var titleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setName(name: String) {
        titleLbl.text = name
       }
    func setPhoto(with Url: String) {
                let logo_url = URL(string: Url)
                photo.sd_imageIndicator = SDWebImageActivityIndicator.gray
                photo.sd_setImage(with: logo_url, placeholderImage: placeHolder_image)
           
       }
}
