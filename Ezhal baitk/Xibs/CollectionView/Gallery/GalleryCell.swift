//
//  GalleryCell.swift
//  Moon Shop
//
//  Created by apple on 1/23/20.
//  Copyright © 2020 2grand. All rights reserved.
//

import UIKit
import SDWebImage
import AVKit
import CoreMedia

protocol GalleryCellView:AnyObject {
    func setPhoto(with Url: String)
    func setPhotoTag(with tag: Int)
    func setPhoto(with Url: String,  didDataReady: @escaping (Data)->())
    func setPhoto(with data: Data)
    func setPhoto(with image: UIImage?)

}
protocol GalleryCellDelegate:AnyObject {
    func deleteBtnPressed(_ sender: UICollectionViewCell)
}

class GalleryCell: UICollectionViewCell,GalleryCellView {

   
    
    weak var delegate: GalleryCellDelegate!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var photo: UIImageViewX!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        photo.fullScreenWhenTapped()
        
    }
    

    func setPhotoTag(with tag: Int) {
        photo.tag = tag
    }
    
  
    @IBAction func deleteBtnPressed(_ sender: UIButton) {
        delegate.deleteBtnPressed(self)
    }
    
   
    func setPhoto(with Url: String) {
        let logo_url = URL(string: Url)
        photo.sd_imageIndicator = SDWebImageActivityIndicator.gray
        photo.sd_setImage(with: logo_url, placeholderImage: placeHolder_image,options: [.refreshCached,.continueInBackground,.progressiveLoad])
    }
    
    
    func setPhoto(with Url: String, didDataReady: @escaping (Data) -> ()) {
        let logo_url = URL(string: Url)
        photo.sd_imageIndicator = SDWebImageActivityIndicator.gray
        photo.sd_setImage(with: logo_url, placeholderImage: placeHolder_image,options: [.refreshCached,.continueInBackground,.progressiveLoad])
        guard let dataOfImage = photo.image?.jpegData(compressionQuality: 1) else{
            return
        }
        didDataReady(dataOfImage)
        
    }
    
    func setPhoto(with data: Data) {
        photo.image =  UIImage(data: data)
    }
    
    func setPhoto(with image: UIImage?) {
        photo.image = image
    }
    

}
