//
//  CategoryCell.swift
//  Ezhal baitk
//
//  Created by apple on 19/12/2021.
//  Copyright © 2021 a7med. All rights reserved.
//

import UIKit
import SDWebImage
class CategoryCell: UICollectionViewCell,SeviceCellView {

    
    @IBOutlet weak var progress: CircularProgress!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var photo: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }
    
    func setPhoto(with Url: String) {
        let logo_url = URL(string: Url)
        photo.sd_imageIndicator = SDWebImageActivityIndicator.gray
        photo.sd_setImage(with: logo_url, placeholderImage: placeHolder_image)
        
    }
    
    func setTitle(_ title: String) {
        titleLbl.text = title
     }
     
     func setProgessValue(_ value: Double) {
        progress.toValue = value / 100
        
     }
    
}
