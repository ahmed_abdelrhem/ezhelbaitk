//
//  StatusFooter.swift
//  Ezhal baitk
//
//  Created by a7med on 6/3/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
protocol StatusFooterDelegate:class {
    func reschaduleBtnPressed()
    func viewJobBtnPressed(_ sender: UIButton)
     func cancelBtnPressed()
}
class StatusFooter: UITableViewHeaderFooterView {
    weak var delegate : StatusFooterDelegate?
    @IBOutlet weak var reschaduleBtn: UIButtonX!
    
    @IBOutlet weak var cancelBtn: UIButtonX!
    
    @IBOutlet weak var viewJobBtn: UIButtonX!
    
    func setBtnTitle(_ title:String,_ tag:Int)  {
        viewJobBtn.setTitle(title, for: .normal)
        viewJobBtn.tag = tag
      
    }
    func hideBtns()  {
          cancelBtn.isHidden = true
              reschaduleBtn.isHidden = true
    }
    
    @IBAction func reschaduleBtnPressed(_ sender: UIButton) {
        delegate?.reschaduleBtnPressed()
    }
    
    
    @IBAction func viewJobBtnPressed(_ sender: UIButton) {
        delegate?.viewJobBtnPressed(sender)

    }
    
    @IBAction func cancelBtnPressed(_ sender: UIButton) {
        delegate?.cancelBtnPressed()

    }
    
}
