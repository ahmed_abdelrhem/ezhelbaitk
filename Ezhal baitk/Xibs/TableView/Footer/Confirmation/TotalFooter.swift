//
//  TotalFooter.swift
//  Ezhal baitk
//
//  Created by a7med on 6/1/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

class TotalFooter: UITableViewHeaderFooterView {

    @IBOutlet weak var totaLbl: UILabel!

    func setTotal(_ total:String)  {
        totaLbl.text = total
    }
    
}
