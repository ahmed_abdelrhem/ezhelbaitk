//
//  SummeryFooter.swift
//  Ezhal baitk
//
//  Created by a7med on 5/31/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
protocol SummeryFooterDelegate:class {
    func addNotesBtnPressed()
    func addMoreBtnPressed()
     func continueBtnPressed()
}

class SummeryFooter: UITableViewHeaderFooterView {
    weak var delegate:SummeryFooterDelegate?
    @IBOutlet weak var continueBtn: UIButtonX!
    @IBOutlet weak var addMoreBtn: UIButtonX!
    @IBOutlet weak var addNotesBtn: UIButtonX!
    @IBOutlet weak var priceLbl: UILabel!
    
    @IBOutlet weak var methodLbl: UILabel!
    
    
    @IBAction func addNotesBtnPressed(_ sender: UIButton) {
        delegate?.addNotesBtnPressed()
    }
    
    @IBAction func addMoreBtnPressed(_ sender: UIButton) {
        delegate?.addMoreBtnPressed()
    }
    
    
    @IBAction func continueBtnPressed(_ sender: UIButton) {
        delegate?.continueBtnPressed()
    }
    
    
     
     func setPrice(_ price: String) {
          priceLbl.text = price
        }
    func setMethod(_ method: String) {
             methodLbl.text = method
           }
}
