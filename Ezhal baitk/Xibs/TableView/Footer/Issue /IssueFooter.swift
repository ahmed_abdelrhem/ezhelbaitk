//
//  IssueFooter.swift
//  Ezhal baitk
//
//  Created by a7med on 5/5/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
protocol IssueFooterView:class {
    func setwarranty(_ warranty:String)
    func settotal(_ total:String)
    func setdetails(_ details:String)
    func setContinueBtn(_ enable: Bool)
}
protocol IssueFooterDelegate:class {
    func continueBtnPressed(_ sender: UIButton)
}

class IssueFooter: UITableViewHeaderFooterView,IssueFooterView {
   
    
   
    weak var delegate:IssueFooterDelegate?
    @IBOutlet weak var continueBtn: UIButtonX!
    @IBOutlet weak var warrantyLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var detailsLbl: UILabel!
    
    
    @IBAction func continueBtnPressed(_ sender: UIButton) {
        guard delegate != nil else {
            fatalError("Error: Issue Footer Delegate is nil")
        }
        delegate?.continueBtnPressed(sender)
    }
    func setwarranty(_ warranty: String) {
        warrantyLbl.text = warranty + "warranty".localized
       }
       
       func settotal(_ total: String) {
        totalLbl.text = total
       }
       
       func setdetails(_ details: String) {
        detailsLbl.text = details
       }
    func setContinueBtn(_ enable: Bool) {
        continueBtn.isEnabled = enable
       }
       
    
}
