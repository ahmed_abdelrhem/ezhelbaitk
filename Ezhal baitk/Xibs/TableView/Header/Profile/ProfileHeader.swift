//
//  ProfileHeader.swift
//  Moon Shop
//
//  Created by apple on 2/11/20.
//  Copyright © 2020 2grand. All rights reserved.
//

import UIKit
import SDWebImage
protocol ProfileHeaderView:class {
    func setPhoto(with Url: String)
    func setName(_ name:String)
}
protocol ProfileHeaderDelegate:class {
    func photoTapped(_ sender : UIImageViewX)
}
class ProfileHeader: UITableViewHeaderFooterView,ProfileHeaderView {
    
    weak var delegate: ProfileHeaderDelegate!
    @IBOutlet weak var photo: UIImageViewX!
   
    @IBOutlet weak var name: UILabel!
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    func setPhoto(with Url: String) {
        let tap = UITapGestureRecognizer(target: self, action: #selector(photoTapped))
        tap.numberOfTapsRequired = 1
        photo.isUserInteractionEnabled = true
        photo.addGestureRecognizer(tap)
        let logo_url = URL(string: Url)
        photo.sd_imageIndicator = SDWebImageActivityIndicator.gray
        photo.sd_setImage(with: logo_url, placeholderImage: placeHolder_image)
    }
    
    func setName(_ name: String) {
        self.name.isHidden = true
        self.name.text = name
    }
    
    @IBAction func camerBtnPressed(_ sender: UIButton) {
        delegate.photoTapped(photo)
    }
    
    @objc func photoTapped(_ sender:UITapGestureRecognizer ){
        delegate.photoTapped(photo)
    }
}
