//
//  MenuHeader.swift
//  ClinicApp
//
//  Created by a7med on 10/16/19.
//  Copyright © 2019 a7med. All rights reserved.
//

import UIKit
import SDWebImage

protocol MenuHeaderView:class  {
    func setName(name: String)
    func setPhoto(with imageUrl: String)
   func setDelegate(with delegateView: MenuHeaderDelegate)
}

protocol MenuHeaderDelegate:class  {
    func presentProfile()
    
}
class MenuHeader: UITableViewHeaderFooterView,MenuHeaderView{

    
    
    
    
    
    
    
    weak var delegate:MenuHeaderDelegate?
    
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var namelbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer(target: self, action: #selector(fetch_profile))
        tap.numberOfTapsRequired = 1
        photo.addGestureRecognizer(tap)
        
        
    }
    @objc func fetch_profile(_ sender:UITapGestureRecognizer){
        delegate?.presentProfile()
    }
   
 func setDelegate(with delegateView: MenuHeaderDelegate) {
     delegate = delegateView
 }
 
 
    
    func setName(name: String) {
        namelbl.text = name
    }
    
    
    func setPhoto(with imageUrl: String) {
        let logo_url = URL(string: imageUrl)
        photo.sd_imageIndicator = SDWebImageActivityIndicator.gray
        photo.sd_setImage(with: logo_url, placeholderImage: placeHolder_image)
    }
    
    
}
