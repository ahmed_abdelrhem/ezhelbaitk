//
//  SummeryHeader.swift
//  Ezhal baitk
//
//  Created by a7med on 5/31/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
import SDWebImage
protocol SummeryHeaderView:class {
     func setName(name: String)
      func setPhoto(with imageUrl: String)
    func setDelegate(with delegateView: SummeryHeaderDelegate)

}

protocol SummeryHeaderDelegate:class {
     func deleteBtnPressed(_ header: UITableViewHeaderFooterView)
      func editBtnPressed(_ header: UITableViewHeaderFooterView)
}

class SummeryHeader: UITableViewHeaderFooterView,SummeryHeaderView {
   
    
  
    weak private var delegate:SummeryHeaderDelegate?

    @IBOutlet weak var nameLbl: UILabel!
    
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    func setDelegate(with delegateView: SummeryHeaderDelegate) {
           delegate = delegateView
       }
    @IBAction func deleteBtnPressed(_ sender: UIButton) {
        delegate?.deleteBtnPressed(self)
    }
    
    @IBAction func editBtnPressed(_ sender: UIButton) {
        delegate?.editBtnPressed(self)

    }
     func setName(name: String) {
        nameLbl.text = name
    }
    
    
    func setPhoto(with imageUrl: String) {
        let logo_url = URL(string: imageUrl)
        photo.sd_imageIndicator = SDWebImageActivityIndicator.gray
        photo.sd_setImage(with: logo_url, placeholderImage: placeHolder_image)
    }
      
    
}
