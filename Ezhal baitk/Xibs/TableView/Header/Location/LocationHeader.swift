//
//  LocationHeader.swift
//  Ezhal baitk
//
//  Created by a7med on 5/31/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
protocol LocationHeaderDelegate:class  {
    func viewAction()
    
}
protocol LocationHeaderView:class {
     func setDetails(_ details:String)
     func setTitle(_ title:String)
}

class LocationHeader: UITableViewHeaderFooterView ,LocationHeaderView{
    weak var delegate: LocationHeaderDelegate?

    @IBOutlet weak var headerTitle: UILabel!
    
    @IBOutlet weak var headerDetails: UILabel!
    
    func setTitle(_ title:String)  {
        headerTitle.text = title
    }
    func setDetails(_ details:String)  {
        if DEF.language == "ar"{
            headerDetails.textAlignment = .left
        }
        headerDetails.text = details
    }
    func addAction()  {
        headerDetails.numberOfLines = 1
        headerDetails.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(viewAction))
               tap.numberOfTapsRequired = 1
               headerDetails.addGestureRecognizer(tap)
    }
    @objc func viewAction(_ sender:UITapGestureRecognizer){
           delegate?.viewAction()
       }
    
    func hideDetails()  {
        headerDetails.isHidden = true
    }
}
