//
//  ConfirmationHeader.swift
//  Ezhal baitk
//
//  Created by a7med on 5/31/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
import SDWebImage
protocol ConfirmationHeaderView:class {
       func setName(name: String)
     func setPhoto(with Url: String)
    func setTimeDate(_ date:String,_ time:String)
    func setRate(_ rate: Double)
    func setPhone(phone: String)
     func setStack(_ hide: Bool)

}
protocol ConfirmationHeaderDelegate:class {
    func callBtnPressed(_ cell: ConfirmationHeader)
    func chatBtnPressed(_ cell: ConfirmationHeader)
}

class ConfirmationHeader: UITableViewHeaderFooterView,ConfirmationHeaderView {
  
    weak var delegate : ConfirmationHeaderDelegate?
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var callBtn: UIButtonX!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var cosmosView: CosmosView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var datelbl: UILabel!
    @IBOutlet weak var timelbl: UILabel!
    @IBOutlet weak var photo: UIImageViewX!
    func setPhoto(with Url: String) {
                   let logo_url = URL(string: Url)
                   photo.sd_imageIndicator = SDWebImageActivityIndicator.gray
                   photo.sd_setImage(with: logo_url, placeholderImage: placeHolder_image)
              
          }
    
    @IBAction func callBtnPressed(_ sender: UIButton) {
        guard let delegate = delegate else {
            return
        }
        delegate.callBtnPressed(self)
    }
    
    
    @IBAction func chatBtnPressed(_ sender: UIButton) {
        guard let delegate = delegate else {
            return
        }
        delegate.chatBtnPressed(self)
    }
    
    
    func setTimeDate(_ date:String,_ time:String) {
        datelbl.text = date
        timelbl.text = time
    }
    func setStack(_ hide: Bool)  {
        stackView.isHidden = hide
    }
    
    func setName(name: String) {
           nameLbl.text = name
          }
          func setRate(_ rate: Double) {
            cosmosView.rating = rate
            }
            
            func setPhone(phone: String) {
                phoneLbl.text = phone
            }
     
       
  
          
    
}
