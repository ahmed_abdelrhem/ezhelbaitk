//
//  PickerCell.swift
//  Ezhal baitk
//
//  Created by a7med on 6/2/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
protocol PickerCellDelegate:class {
    func catBtnPressed(_ sender: UIButton,_ cell:UITableViewCell )
}

class PickerCell: UITableViewCell {
    weak var delegate: PickerCellDelegate?
    @IBOutlet weak var catBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none

    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setCat(_ cat: String)  {
        catBtn.setTitle(cat, for: .normal)
    }
    @IBAction func catBtnPressed(_ sender: UIButton) {
        guard delegate != nil else {
            return
        }
        delegate?.catBtnPressed(catBtn, self)
    }
    
}
