//
//  LocationCell.swift
//  Ezhal baitk
//
//  Created by a7med on 5/31/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
import GoogleMaps
protocol LocationCellView:class {
      func setLocation(_ lat:Double,_ lng:Double)
}

class LocationCell: UITableViewCell,LocationCellView {
    @IBOutlet weak var myMapView: UIView!
  private  var mapView: GMSMapView!
   private    var lat: Double?
     private  var lng: Double?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setLocation(_ lat:Double,_ lng:Double)  {
        self.lat = lat
        self.lng = lng
        locationAuth()

    }
    
}


extension LocationCell: CLLocationManagerDelegate, GMSMapViewDelegate {
    
    func locationAuth(){
        mapView = GMSMapView()
        self.MapSetup(lat:"\(self.lat!)", long: "\(self.lng!)")
    }
    
    func MapSetup(lat: String , long: String) {
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(lat)!, longitude: CLLocationDegrees(long)!, zoom: 16)
        let f: CGRect = myMapView.frame
        let mapFrame = CGRect(x: 0, y: 0, width: self.width, height: self.height)
        mapView = GMSMapView.map(withFrame: mapFrame, camera: camera)
        mapView.settings.compassButton = true
        mapView.delegate = self
        self.myMapView.addSubview(mapView)
        MapMarkerSetup()
    }
    
    func MapMarkerSetup() {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: self.lat!, longitude: self.lng!)
        marker.icon =  GMSMarker.markerImage(with: HexStringToUIColor.hexStringToUIColor(hex: "#FF7314"))
        marker.map = mapView
        mapView.selectedMarker = marker
    }
    
    
}
