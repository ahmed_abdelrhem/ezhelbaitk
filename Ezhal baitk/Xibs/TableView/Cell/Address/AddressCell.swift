//
//  AddressCell.swift
//  Ezhal baitk
//
//  Created by a7med on 5/31/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
protocol AddressCellView:class {
     func setArea(_ area:String)
       func setBlock(_ block:String)
       func setStreet(_ street:String)
       func setHouse(_ house:String)
        
}

class AddressCell: UITableViewCell,AddressCellView {
    
    
    @IBOutlet weak var blockLbl: UILabel!
    @IBOutlet weak var areaLbl: UILabel!
    @IBOutlet weak var streetLbl: UILabel!
    @IBOutlet weak var houseLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setArea(_ area:String)  {
        areaLbl.text = area
    }
    func setBlock(_ block:String)  {
        blockLbl.text = block
    }
    func setStreet(_ street:String)  {
        streetLbl.text = street
    }
    func setHouse(_ house:String)  {
        houseLbl.text = house
    }
    
}
