//
//  TextFieldCell.swift
//  Moon Shop
//
//  Created by apple on 2/11/20.
//  Copyright © 2020 2grand. All rights reserved.
//

import UIKit
protocol TextFieldCellView:class {
    func settxt(txt: String,tag:Int,placeholder:String)
}
class TextFieldCell: UITableViewCell,TextFieldCellView {

    weak var  delegate: UITextFieldDelegate!
    @IBOutlet weak var txtFeild: UITextFieldX!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        txtFeild.forceSwitchingRegardlessOfTag = true


    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func settxt(txt: String,tag:Int,placeholder:String) {
        txtFeild.text = txt
        txtFeild.placeholder = placeholder
        txtFeild.tag = tag
    }
    
   
    
    @IBAction func endEditingTxt(_ sender: UITextField) {
        delegate.textFieldDidEndEditing?(sender)
        
    }
    
    @IBAction func beginEditingTxt(_ sender: UITextField) {
        delegate.textFieldDidBeginEditing?(sender)
    }
    
}
