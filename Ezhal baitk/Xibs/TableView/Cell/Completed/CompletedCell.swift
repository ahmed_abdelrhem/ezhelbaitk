//
//  CompletedCell.swift
//  Ezhal baitk
//
//  Created by a7med on 6/3/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
protocol CompletedCellView:class {
    func setSubPk(_ subPk:String)
       func setId(_ id:String)
       func setDate(_ date:String)
}
protocol CompletedCellDelegate:class {
    func rateBtnPressed(_ sender: UITableViewCell)
}

class CompletedCell: UITableViewCell,CompletedCellView {
    weak var delegate: CompletedCellDelegate?
    @IBOutlet weak var idLbl: UILabel!
    @IBOutlet weak var rateBtn: UIButtonX!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var monLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func rateBtnPressed(_ sender: UIButton) {
        guard delegate != nil else {
            return
        }
        delegate!.rateBtnPressed(self)
    }
    func setSubPk(_ subPk: String) {
        monLbl.text = subPk + " Months package"
     }
     
     func setId(_ id: String) {
        idLbl.text = "#" + id
     }
     
     func setDate(_ date: String) {
        dateLbl.text = date
     }
     
   
    
}
