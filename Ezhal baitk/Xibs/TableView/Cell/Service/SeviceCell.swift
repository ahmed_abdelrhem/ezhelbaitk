//
//  SeviceCell.swift
//  Ezhal baitk
//
//  Created by a7med on 5/5/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
import SDWebImage
protocol SeviceCellView  {
    func setPhoto(with Url: String)
    func setTitle(_ title:String)
    func setProgessValue(_ value:Double)
}


class SeviceCell: UITableViewCell,SeviceCellView {
 
    
    @IBOutlet weak var progress: CircularProgress!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var photo: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func setPhoto(with Url: String) {
        let logo_url = URL(string: Url)
        photo.sd_imageIndicator = SDWebImageActivityIndicator.gray
        photo.sd_setImage(with: logo_url, placeholderImage: placeHolder_image)
        
    }
    
    func setTitle(_ title: String) {
        titleLbl.text = title
     }
     
     func setProgessValue(_ value: Double) {
        progress.toValue = value / 100
        
     }
    
}
