//
//  NotificationCell.swift
//  Jaz-User
//
//  Created by iMac on 10/23/19.
//  Copyright © 2019 Grand. All rights reserved.
//

import UIKit
protocol NotificationCellDelegate {
    func setTitle(title: String)
    func setBody(body: String)
    func setTime(time:String)
}

class NotificationCell: UITableViewCell,NotificationCellDelegate {
  
    
    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet weak var bodylbl: UILabel!
    @IBOutlet weak var timelbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setTitle(title: String) {
        titlelbl.text = title
      }
      
      func setBody(body: String) {
        bodylbl.text = body
      }
      
      func setTime(time: String) {
        timelbl.text = time
      }
      
}
