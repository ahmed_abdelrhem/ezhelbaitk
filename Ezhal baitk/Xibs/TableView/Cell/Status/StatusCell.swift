//
//  StatusCell.swift
//  Ezhal baitk
//
//  Created by a7med on 6/3/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

class StatusCell: UITableViewCell {
    
    
    @IBOutlet weak var detailsLbl: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var selectedBtn: UIButtonX!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        topView.isHidden = false
        bottomView.isHidden = false
        selectedBtn.isSelected = false
        titleLbl.textColor = .lightGray
        detailsLbl.textColor = .lightGray
        topView.backgroundColor = .lightGray
        bottomView.backgroundColor = .lightGray

    }
    func hideTopView() {
        topView.isHidden = true
    }
    func hideBottomView() {
        bottomView.isHidden = true
    }
    func selectedStatus()  {
        titleLbl.textColor = .darkText
        detailsLbl.textColor = mainColor
        selectedBtn.isSelected = true
        topView.backgroundColor = mainColor
        
    }
    func setBottomView()  {
        bottomView.backgroundColor = mainColor
    }
    func setTitle(_ title:String)  {
        titleLbl.text = title
    }
    func setDetails(_ details:String)  {
         detailsLbl.text = details
     }

}
