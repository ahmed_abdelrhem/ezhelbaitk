//
//  SubTBCell.swift
//  Ezhal baitk
//
//  Created by apple on 03/08/2021.
//  Copyright © 2021 a7med. All rights reserved.
//

import UIKit

class SubTBCell: UITableViewCell,SubCellView {
    @IBOutlet weak var containterView: UIViewX!
    @IBOutlet weak var pkLbl: UILabel!
    
    @IBOutlet weak var monthlyLbl: UILabel!
    @IBOutlet weak var monLbl: UILabel!
    @IBOutlet weak var PriceLbl: UILabel!
    @IBOutlet weak var categoryLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setMonth(_ mon: String) {
        monLbl.text = mon + "Months"
    }
    
    func setPrice(_ price:String,_ currency:String) {
        let prc = price + currency + "/"
        PriceLbl.text = prc + "package"
    }
    
    func setCategory(_ category: String) {
        categoryLbl.text = category
    }
    func didSelect() {
        containterView.backgroundColor = mainColor
        pkLbl.textColor = .white
        monthlyLbl.textColor = .white
        monLbl.textColor = .white
        PriceLbl.textColor = .white
        categoryLbl.textColor = .white
    }
    
    func didUnselect() {
        containterView.backgroundColor = .white
        pkLbl.textColor = mainColor
        monthlyLbl.textColor = .darkText
        monLbl.textColor = mainColor
        PriceLbl.textColor = .darkText
        categoryLbl.textColor = .darkText
    }
    
}
