//
//  OfferCell.swift
//  Ezhal baitk
//
//  Created by a7med on 6/5/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
protocol OfferCellView:class {
    func setTitle(_ title:String)
    func setBtn(_ image:String)

}
protocol OfferCellDelegate:class {
    func offerBtnPressed(_ sender: UITableViewCell)
}

class OfferCell: UITableViewCell,OfferCellView {
   
    weak var delegate: OfferCellDelegate?
    @IBOutlet weak var btn: UIButton!
    
    @IBOutlet weak var titleBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setTitle(_ title: String) {
        titleBtn.setTitle(title, for: .normal)
       }
    
    func setBtn(_ image: String) {
        btn.setImage(UIImage(named: image), for: .normal)
       }
       
    @IBAction func offerBtnPressed(_ sender: UIButton) {
        guard delegate != nil else {
            return
        }
        delegate?.offerBtnPressed(self)
    }
    
    
}
