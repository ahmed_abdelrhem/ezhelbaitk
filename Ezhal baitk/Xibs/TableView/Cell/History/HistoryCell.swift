//
//  HistoryCell.swift
//  Ezhal baitk
//
//  Created by a7med on 6/2/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
protocol HistoryCellView{
    func setDate(_ date:String)
    func setPrice(_ price:String)
    func setStatus(_ status:String)

}

class HistoryCell: UITableViewCell,HistoryCellView {
 
    
    @IBOutlet weak var dateLbl: UILabel!
    
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDate(_ date: String) {
        dateLbl.text = date
     }
     
     func setPrice(_ price: String) {
        priceLbl.text = price
     }
     
     func setStatus(_ status: String) {
        statusLbl.text = status
     }
    
}
