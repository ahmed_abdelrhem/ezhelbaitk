//
//  PackageCell.swift
//  Ezhal baitk
//
//  Created by a7med on 6/3/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
protocol PackageCellView:class {
    func setSubPk(_ subPk:String)
    func setId(_ id:String)
    func setDate(_ date:String)
    func setTime(_ time:String)
}
class PackageCell: UITableViewCell,PackageCellView {
 
    
    @IBOutlet weak var subPkLbl: UILabel!
    
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var idLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setSubPk(_ subPk: String) {
        subPkLbl.text = subPk + " Months package"
     }
     
     func setId(_ id: String) {
        idLbl.text = "#" + id
     }
     
     func setDate(_ date: String) {
        dateLbl.text = date
     }
     
     func setTime(_ time: String) {
        timeLbl.text = time
     }
    
}
