//
//  PackageDetailsCell.swift
//  Ezhal baitk
//
//  Created by a7med on 6/5/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
protocol  PackageDetailsCellView:class {
    func setFacility(_ facility:String)
    func setPrice(_ price:String)
    func setName(_ name:String)
    func setAddress(_ address:String)
    
    func setPhone(_ phone:String)
    func setEmail(_ email:String)
    func setDate(_ date:String)
    func setRest(_ rest:String)

    func setTime(_ time:String)
}
protocol PackageDetailsCellDelegate:class {
    func updateBtnPressed(_ cell: UITableViewCell)
}

class PackageDetailsCell: UITableViewCell,PackageDetailsCellView {
   
    
    weak var delegate: PackageDetailsCellDelegate?
    @IBOutlet weak var updateBtn: UIButtonX!
    @IBOutlet weak var facilityLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var datelbl: UILabel!
    @IBOutlet weak var mailLbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    
    @IBOutlet weak var restLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    @IBAction func updateBtnPressed(_ sender: UIButton) {
        guard delegate != nil else {
            return
        }
        delegate?.updateBtnPressed(self)
    }
    
    func setRest(_ rest: String) {
        restLbl.text = rest
    }
    func setFacility(_ facility: String) {
        facilityLbl.text = facility
    }
    
    func setPrice(_ price: String) {
        priceLbl.text = price
    }
    
    func setName(_ name: String) {
        nameLbl.text = name
    }
    
    func setAddress(_ address: String) {
        addressLbl.text = address
    }
    
    func setPhone(_ phone: String) {
        phoneLbl.text = phone
    }
    
    func setEmail(_ email: String) {
        mailLbl.text = email
    }
    
    func setDate(_ date: String) {
        datelbl.text = date
    }
    
    func setTime(_ time: String) {
        timeLbl.text = time
    }
    
}
