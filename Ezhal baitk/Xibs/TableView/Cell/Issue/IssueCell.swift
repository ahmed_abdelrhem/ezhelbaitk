//
//  IssueCell.swift
//  Ezhal baitk
//
//  Created by a7med on 5/5/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
protocol IssueCellView:class {
    func setName(_ name:String)
    func setDetails(_ details:String)
    func setPrice(_ price:String ,_ currency:String)
    func setSelect(_ select: Bool)
    func setFreeCell(_ price:String,_ currency:String)
    func setCounter(_ count:String)
    func hideCounter(_ hide: Bool)

}
protocol IssueCellDelegate:class {
    func selectedBtnPressed(_ sender: UIButton,_ cell:UITableViewCell)
    func minusBtnPressed(_ sender: UIButton,_ cell:UITableViewCell)
    func plusBtnPressed(_ sender: UIButton,_ cell:UITableViewCell)
}

class IssueCell: UITableViewCell,IssueCellView {
   
    
    
    weak var delegate:IssueCellDelegate?
    var sectionIsExpanded: Bool = true {
        didSet {
            UIView.animate(withDuration: 0.25) {
                if self.sectionIsExpanded {
                    self.expandBtn.transform = CGAffineTransform.identity
                } else {
                    self.expandBtn.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2.0)
                }
            }
        }
    }
    @IBOutlet weak var countLbl: UILabel!
    @IBOutlet weak var countView: UIViewX!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var expandBtn: UIButton!
    @IBOutlet weak var detailsLbl: UILabel!
    @IBOutlet weak var selectedBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setCounter(_ count: String) {
        countLbl.text = count
    }
    func hideCounter(_ hide: Bool) {
        countView.isHidden = hide
       }
       
    
    func setName(_ name: String) {
        nameLbl.text = name
    }
    
    func setDetails(_ details: String) {
        detailsLbl.text = details
    }
    
    func setPrice(_ price:String ,_ currency:String){
        priceLbl.text =  price + currency 
    }
    
    func setSelect(_ select: Bool) {
        selectedBtn.isSelected = select
        minusBtn.isEnabled = select
        plusBtn.isEnabled = select
        
    }
    
    func setFreeCell(_ price:String,_ currency:String) {
        expandBtn.isHidden = true
        countView.isHidden = true
        priceLbl.textColor = mainColor
        priceLbl.text =  price
        
    }
    
    @IBAction func selectedBtnPressed(_ sender: UIButton) {
        guard delegate != nil else {
            fatalError("Error: Issue Cell Delegate is nil")
        }
        delegate?.selectedBtnPressed(sender, self)
    }
    
    @IBAction func expandBtnPressed(_ sender: UIButton) {
        sectionIsExpanded = !sectionIsExpanded
        detailsLbl.numberOfLines = sectionIsExpanded ? 1 : 2
        
    }
    
    @IBAction func minusBtnPressed(_ sender: UIButton) {
        guard delegate != nil else {
            fatalError("Error: Issue Cell Delegate is nil")
        }
        delegate?.minusBtnPressed(sender, self)
    }
    
    @IBAction func plusBtnPressed(_ sender: UIButton) {
        guard delegate != nil else {
            fatalError("Error: Issue Cell Delegate is nil")
        }
        delegate?.plusBtnPressed(sender, self)
    }
    
}
