//
//  RangeCell.swift
//  a3ln
//
//  Created by a7med on 3/22/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
protocol RangeCellView:class {
    func set(max:CGFloat,min:CGFloat)
}
protocol RangeCellDelegate:class {
    func sliderChanged(_ cell:UITableViewCell,_ sender: RangeSeekSlider)
}
class RangeCell: UITableViewCell ,RangeCellView{
    
    weak var delegate : RangeCellDelegate?
    @IBOutlet weak var slider: RangeSeekSlider!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none

    }
    func set(max: CGFloat, min: CGFloat) {
        slider.minValue = min
        slider.maxValue = max
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func sliderChanged(_ sender: RangeSeekSlider) {
        delegate?.sliderChanged(self,sender)
    }
    
}
