//
//  JobCell.swift
//  Ezhal baitk
//
//  Created by a7med on 6/3/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
import SDWebImage
protocol JobCellView:class {
    func setTotal(_ total:String)
    func setTech(_ tech:String)
    func setArea(_ area:String)
    func setStatus(_ status:String)
      func setCategory(_ category:String)
    func setPhoto(_ url:String)



}

class JobCell: UITableViewCell,JobCellView {
   
    
    @IBOutlet weak var totalLbl: UILabel!
    
    @IBOutlet weak var areaLbl: UILabel!
    @IBOutlet weak var techLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var categoryLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setTotal(_ total: String) {
        totalLbl.text = total
       }
       
       func setTech(_ tech: String) {
        techLbl.text = tech
       }
       
       func setArea(_ area: String) {
        areaLbl.text = area
       }
       
       func setStatus(_ status: String) {
        statusLbl.text = status
       }
       
       func setCategory(_ category: String) {
        categoryLbl.text = category
       }
       
       func setPhoto(_ url: String) {
           let logo_url = URL(string: url)
                       photo.sd_imageIndicator = SDWebImageActivityIndicator.gray
                       photo.sd_setImage(with: logo_url, placeholderImage: placeHolder_image)
       }
}
