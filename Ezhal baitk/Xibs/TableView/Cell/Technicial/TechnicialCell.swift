//
//  TechnicialCell.swift
//  Ezhal baitk
//
//  Created by a7med on 6/7/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
import SDWebImage
protocol TechnicialCellView:class {
    func setName(name: String)
    func setPhoto(with Url: String)
    func setRate(_ rate: Double)
    func setCat(cat: String)

    
    
}

class TechnicialCell: UITableViewCell,TechnicialCellView {
  
    
    
    
    @IBOutlet weak var photo: UIImageViewX!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var catLbl: UILabel!
    @IBOutlet weak var rateView: CosmosView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setName(name: String) {
        nameLbl.text = name
    }
    func setCat(cat: String) {
        catLbl.text = cat
      }
      
    
    func setPhoto(with Url: String) {
        let logo_url = URL(string: Url)
        photo.sd_imageIndicator = SDWebImageActivityIndicator.gray
        photo.sd_setImage(with: logo_url, placeholderImage: placeHolder_image)
        
    }
    
    func setRate(_ rate: Double) {
        rateView.rating = rate
    }
    
}
