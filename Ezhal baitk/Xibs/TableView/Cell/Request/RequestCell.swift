//
//  RequestCell.swift
//  Ezhal baitk
//
//  Created by a7med on 5/31/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
protocol RequestCellView:class {
    func setPrice(_ price:String)
    func setTotal(_ total:String)
    func setname(_ name:String)
    func setPayMethod()


}

class RequestCell: UITableViewCell,RequestCellView {
   
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var issueTotalLbl: UILabel!
    
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setPrice(_ price: String) {
        priceLbl.text = price
      }
      
      func setTotal(_ total: String) {
        totalLbl.text = total
      }
    func setPayMethod(){
        issueTotalLbl.text = "Payment method".localized
    }
      
    func setname(_ name: String) {
        nameLbl.text = name
       }
       
}
