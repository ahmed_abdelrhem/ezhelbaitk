//
//  PackageCategoryCell.swift
//  Ezhal baitk
//
//  Created by apple on 02/08/2021.
//  Copyright © 2021 a7med. All rights reserved.
//
import SDWebImage
import UIKit
protocol PackageCategoryCellV:class {
    func configure(_ name: String,_ image: UIImage?)
}
protocol PackageCategoryCellD:class {
    func nameBtnPressed(_ cell: UITableViewCell)
    
}
class PackageCategoryCell: UITableViewCell,PackageCategoryCellV {
    weak var delegate : PackageCategoryCellD?

    @IBOutlet weak var nameBtn: UIButton!
    @IBOutlet weak var photo: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(_ name: String,_ image: UIImage?)  {
        guard let newImage = image?.sd_resizedImage(with: photo.frame.size, scaleMode: .aspectFill) else {return}
        photo.image = newImage
        nameBtn.setTitle(name, for: .normal)
    }
    
    
    @IBAction func nameBtnPressed(_ sender: UIButton) {
        guard let delegate = delegate else { return  }
        delegate.nameBtnPressed(self)
    }
    
}
