//
//  MenuCell.swift
//  Cham
//
//  Created by iMac on 5/12/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
import SDWebImage
 protocol MenuCellView {
      func setName(name: String)
      func setPhoto(with icon: UIImage?)
    func setPhoto(with Url: String)
    func setTextColor(with color: UIColor)
}
class MenuCell: UITableViewCell,MenuCellView {
   
   
    private var right2Sides: CGFloat = 0 {
       didSet {
           layer.cornerRadius = right2Sides
           layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
       }
   }
    private var left2Sides: CGFloat = 0 {
       didSet {
           layer.cornerRadius = left2Sides
           layer.maskedCorners = [.layerMinXMinYCorner,.layerMinXMaxYCorner]
       }
   }
   
    
    
    @IBOutlet weak var menuIcon: UIImageView!
    @IBOutlet weak var menuTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none


    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        if selected {
            menuTitle.textColor =   mainColor
            self.left2Sides = 20
            self.backgroundColor = .white

        }else{
            menuTitle.textColor =   .white
            self.left2Sides = 0
            self.backgroundColor = mainColor
        }
    }
   func setTextColor(with color: UIColor) {
    menuTitle.textColor = color
      }
      
    func setName(name: String) {
        menuTitle.text = name
       }
       
    func setPhoto(with icon: UIImage?) {
        menuIcon.image = icon
       }
   
    
    func setPhoto(with Url: String) {
                let logo_url = URL(string: Url)
                menuIcon.sd_imageIndicator = SDWebImageActivityIndicator.gray
                menuIcon.sd_setImage(with: logo_url, placeholderImage: placeHolder_image)
           
       }
       
    
}
