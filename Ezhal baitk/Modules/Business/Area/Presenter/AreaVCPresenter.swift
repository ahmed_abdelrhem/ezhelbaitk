//
//  AreaVCPresenter.swift
//  Ezhal baitk
//
//  Created by a7med on 6/1/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
class AreaVCPresenter {
    private let interactor = AreaVCInteractor()
    private var list = [CityData]()
    private var filter_list = [CityData]()

    weak var view: (LoaderJwtDelegate)?
    init(view:LoaderJwtDelegate) {
        self.view = view
    }
    
    func viewdidload()  {
        view?.loader_start()
        interactor.getCities(didDataReady: { [weak self](model) in
            guard self != nil else { return }
            if model.status == API_status.Success.rawValue{
                self?.list = model.data ?? []
                if self!.list.count == 0{
                    self?.view?.onEmpty()
                }else{
                    self!.view?.onSuccess("")
                }
            }else if model.status == API_status.JwtExpired.rawValue{
                self!.view?.jwtExpired(model.message ?? "")
            }else{
                
                self?.view?.onFailure(model.message ?? "")
            }
            self?.view?.loader_stop()
            
        }) { [weak self](error) in
            
            guard self != nil else { return }
            self?.view?.loader_stop()
            self?.view?.onConnection()
            self?.view?.onFailure(error?.errorDescription)
        }
    }
    func getlistCount() ->Int {
        return list.count
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        filter_list = list.filter({( item : CityData) -> Bool in
            let name = item.name ?? ""
            return name.lowercased().contains(searchText.lowercased())
        })

        view?.onSuccess("")
    }
    func getFilterListCount() ->Int {
        return filter_list.count
    }
    
    func configureCell(cell:MenuCellView,at row: Int ,isFilter: Bool)  {
         let item = !isFilter ? list[row] : filter_list[row]
         cell.setName(name: item.name ?? "")
         cell.setPhoto(with: nil)
        cell.setTextColor(with: .darkText)
         
        
     }
   
    func didSelect(_ row:Int,isFilter: Bool) -> CityData  {
        return !isFilter ? list[row]  : filter_list[row] 
    }
    
    
    
    
}
