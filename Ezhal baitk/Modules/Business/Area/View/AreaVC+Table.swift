//
//  AreaVC+Table.swift
//  Ezhal baitk
//
//  Created by a7med on 6/1/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
extension AreaVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering(){
         return   presenter.getFilterListCount()
        }
        return presenter.getlistCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath)  as! MenuCell
        presenter.configureCell(cell: cell, at: indexPath.row, isFilter: isFiltering())
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.areaRetrive(presenter.didSelect(indexPath.row, isFilter: isFiltering()))
        navigationController?.popViewController(animated: true)
    }
    
}
