//
//  AreaVC+Search.swift
//  Ezhal baitk
//
//  Created by a7med on 6/1/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
extension AreaVC: UITextFieldDelegate{
    
    // MARK: - Private instance methods
    func textFieldDidEndEditing(_ textField: UITextField) {
                presenter.filterContentForSearchText(searchTxtf.text!)
    }
//
    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchTxtf.text?.isEmpty ?? true
    }
    
   

    func isFiltering() -> Bool {
        return  !searchBarIsEmpty()
    }
}
