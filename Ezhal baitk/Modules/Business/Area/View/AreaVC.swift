//
//  AreaVC.swift
//  Ezhal baitk
//
//  Created by a7med on 6/1/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
protocol AreaVCdelegate:class {
    func areaRetrive(_ city: CityData)
}
class AreaVC:BaseViewController{
    weak var delegate : AreaVCdelegate?
    internal var presenter: AreaVCPresenter!

    @IBOutlet weak var searchTxtf: UITextField!
     @IBOutlet weak var tableView: UITableView!{
         didSet{
             tableView.tableFooterView = UIView(frame: .zero)
             tableView.separatorStyle = .none
             tableView.rowHeight = 60
             tableView.register(UINib(nibName: "MenuCell", bundle: nil), forCellReuseIdentifier: "MenuCell")

         }
     }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter = AreaVCPresenter(view: self)
               searchTxtf.delegate = self
               presenter.viewdidload()
    }
    

 @IBAction func seachBtnPressed(_ sender: UIButton) {
        presenter.filterContentForSearchText(searchTxtf.text!)
    }

}
