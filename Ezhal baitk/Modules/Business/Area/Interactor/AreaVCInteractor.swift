//
//  AddressVCInteractor.swift
//  Ezhal baitk
//
//  Created by a7med on 6/1/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
class AreaVCInteractor{
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias model = (City_Base)->()
    
    
    func getCities(didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorType) {
       
        apiManager.contectToApiWith(url: MyURLs.cities.url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
                                        print(json)
                                        if let data = try? JSONSerialization.data(withJSONObject: json) {
                                            
                                            do {
                                                let result = try self.decoder.decode(City_Base.self, from: data)
                                                didDataReady(result)

                                            }catch{
                                                print("model parse error\(error)")

                                            }
                                          
                                        }
        }) { (error) in
           print(error)
            errorCompletion(error)
        }
    }

}
