//
//  SummeryVC.swift
//  Ezhal baitk
//
//  Created by a7med on 5/31/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

protocol UnitsVCDelegate: class {
    func appendIssues(issues:[IssueData],tag:Int)
}



class SummeryVC:BaseViewController,UnitsVCDelegate{
    
    
    
    
    
    
    
    var issues : [IssueData]?
    var method : String?
    var payment: Int?
    var total: Int {
        var total = 0
        for item in issues ?? [] {
            if item.selected && item.price != "FREE" {
                let price = Int(item.price ?? "0") ?? 0
                let count = item.count
                total += (price*count)
            }
        }
        return total
    }
    var currency : String {
        if issues?.count ?? 0 > 0{
            return issues?[0].currency ?? "QR".localized
        }else{
            return "QR".localized
        }
    }
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
            tableView.rowHeight = UITableView.automaticDimension
            tableView.sectionHeaderHeight = 60
            tableView.separatorStyle = .none
            tableView.register(UINib(nibName: "RequestCell", bundle: nil), forCellReuseIdentifier: "RequestCell")
            tableView.register(UINib(nibName: "SummeryHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "SummeryHeader")
            let footer = (Bundle.main.loadNibNamed("SummeryFooter", owner: self, options: nil)![0] as? SummeryFooter)
            tableView.tableFooterView = footer
            tableView.tableFooterView?.height = 500
            guard let method = method else {
                return
            }
            footer?.setMethod(method)
            footer?.setPrice("\(total) \(currency)")
            footer?.delegate = self
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        title = "Summary".localized
    }
    
    
    
    
    func appendIssues(issues: [IssueData], tag: Int) {
        if tag == 0 {
            self.issues?.append(issues)
            guard let footer = tableView.tableFooterView as? SummeryFooter else {
                return
            }
            footer.setPrice("\(total) \(currency)")
            tableView.reloadData()
            
        }else{
            
            guard let oldIssues = self.issues  else { return }
            
            
            for newIssue in issues {
                
                for i in 0..<oldIssues.count {
                    if newIssue.id == oldIssues[i].id {
                        self.issues?.remove(at: i)
                        break
                    }
                }
                
            }
            
            self.issues?.append(issues)
            guard let footer = tableView.tableFooterView as? SummeryFooter else {
                return
            }
            footer.setPrice("\(total) \(currency)")
            tableView.reloadData()
            
        }
    }
    
    
}
