//
//  SummeryVC+Table.swift
//  Ezhal baitk
//
//  Created by a7med on 5/31/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
extension SummeryVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RequestCell", for: indexPath) as! RequestCell
        cell.setname(issues?[indexPath.section].name ?? "")
        cell.setPrice("\(issues?[indexPath.section].count ?? 0) x \(issues?[indexPath.section].price ?? "0") \(issues?[indexPath.section].currency ?? "")")
        let count = issues?[indexPath.section].count ?? 0
        let price = Int(issues?[indexPath.section].price ?? "0") ?? 0
        let total = count * price
        cell.setTotal("\(total) \(issues?[indexPath.section].currency ?? "")")
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "SummeryHeader") as! SummeryHeader
        header.tag = section
        header.setName(name: issues?[section].unit_name ?? "")
        header.setPhoto(with: issues?[section].unit_image ?? "")
        header.setDelegate(with: self)
        return header
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return issues?.count ?? 0
    }
}
extension SummeryVC:SummeryFooterDelegate,SummeryHeaderDelegate{
    func deleteBtnPressed(_ header: UITableViewHeaderFooterView) {
        let section = header.tag
        
        issues?.remove(at: section)
        let indexSet = IndexSet(integer: section)
        self.tableView.deleteSections(indexSet, with: .none)
        guard let footer = tableView.tableFooterView as? SummeryFooter else {
            return
        }
        footer.setPrice("\(total) \(currency)")
        self.tableView.reloadData()
        
    }
    
    func editBtnPressed(_ header: UITableViewHeaderFooterView) {
        let section = header.tag
        guard let issue = issues?[section] else { return  }
        let unit = (issue.unit_id ?? 0,issue.unit_name ?? "",issue.unit_image ?? "")
        let cat_id = issues?[section].category_id
        _NC.post(name: .selected_list, object: nil, userInfo: ["selected_list":issues ?? []])
        let vc = UIStoryboard(name: AppStoryboard.Business.rawValue, bundle: .main).instantiateViewController(withIdentifier: "IssuesVC") as! IssuesVC
        vc.unit = unit
        vc.cat_id = cat_id
        vc.summeryDelegate_tag = 1
        vc.summeryDelegate = self
        vc.selected_list = issues ?? []
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func addNotesBtnPressed() {
        let vc = UIStoryboard(name: AppStoryboard.Business.rawValue, bundle: nil).instantiateViewController(withIdentifier: "NotesVC") as! NotesVC
        vc.issues = issues
        vc.payment = payment
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func addMoreBtnPressed() {
        let vc = UIStoryboard(name: AppStoryboard.Business.rawValue, bundle: .main).instantiateViewController(withIdentifier: "UnitsVC") as! UnitsVC
        vc.summeryDelegate = self
        vc.cat_id = issues?.first?.category_id
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func continueBtnPressed() {
        let vc = UIStoryboard(name: AppStoryboard.Business.rawValue, bundle: nil).instantiateViewController(withIdentifier: "DateVC") as! DateVC
        vc.issues = issues
        vc.pay4 = .issues
        vc.payment = payment
        vc.cat_id = 1 // 
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
}
