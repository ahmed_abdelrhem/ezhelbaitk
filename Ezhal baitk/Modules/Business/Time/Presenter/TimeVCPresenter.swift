//
//  TimeVCPresenter.swift
//  Ezhal baitk
//
//  Created by a7med on 6/6/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
protocol TimeVCDelegate:class {
    func PresentHomeVC<T>(_ model:T)
}
class TimeVCPresenter {
    private let interactor = TimeVCInteractor()
    weak var view: (LoginVCView&JwtDelegate&TimeVCDelegate)?
    init(view:LoginVCView&JwtDelegate&TimeVCDelegate) {
        self.view = view
    }
    
    func reserve(subscription_id: Int,date: String,time: String)  {
        view?.loader_start()
        interactor.reserve(subscription_id: subscription_id,date: date,time: time, didDataReady: { [weak self](model) in
            guard self != nil else { return }
            if model.status == API_status.Success.rawValue{
                DEF.saveUserDefault(model.data!)
                self?.view?.PresentVC(model.message  ?? "")

            }
            else if model.status == API_status.JwtExpired.rawValue{
                self!.view?.jwtExpired(model.message ?? "")

            }
            else{
                
                self?.view?.onFailure(model.message  ?? "")
                self?.view?.loader_stop()

            }
            
        }) { [weak self](error) in
            
            guard self != nil else { return }
            self?.view?.loader_stop()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }
    
    
    func reschedule(id:Int,date:String,time:String)  {
        view?.loader_start()
        interactor.reschedule(id:id,date:date,time:time, didDataReady: { [weak self](model) in
            guard self != nil else { return }
            if model.status == API_status.Success.rawValue{
                self?.view?.PresentHomeVC(model.message  ?? "")

            }else{
                
                self?.view?.onFailure(model.message  ?? "")
                self?.view?.loader_stop()

            }
            
        }) { [weak self](error) in
            
            guard self != nil else { return }
            self?.view?.loader_stop()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }

    
}
