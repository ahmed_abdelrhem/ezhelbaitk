//
//  TimeVCInteractor.swift
//  Ezhal baitk
//
//  Created by a7med on 6/6/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
import Alamofire
class TimeVCInteractor {
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias user_model = (User_Base)->()

    func reserve(subscription_id: Int,date: String,time: String, didDataReady: @escaping user_model, andErrorCompletion errorCompletion: @escaping errorType) {
        let dateObj = Date(iso8601String: date)
          let sender_date = dateObj.dateString(with: "dd/MM/yyyy")
        let param = [
            "subscription_id": subscription_id,
            "date": sender_date,
            "time": time
            ] as [String : Any]
        print("param",param)
        apiManager.contectToApiWith(url: MyURLs.reserve.url,
                                    methodType: .post,
                                    params: param,
                                    success: { (json) in
                                        print("==>jsn",json)
                                        if let data = try? JSONSerialization.data(withJSONObject: json) {
                                            
                                            do {
                                                let result = try self.decoder.decode(User_Base.self, from: data)
                                                didDataReady(result)

                                            }catch{
                                                 print("##error\(error)")
                        let error = AFError.responseSerializationFailed(reason: .decodingFailed(error: error))
                        errorCompletion(error)

                                            }
                                          
                                        }
        }) { (error) in
           print(error)
            errorCompletion(error)
        }
    }
    
    func reschedule(id:Int,date:String,time:String, didDataReady: @escaping user_model, andErrorCompletion errorCompletion: @escaping errorType) {
           
           let param = [
               "id": id,
               "date":date,
               "time":time
               ] as [String : Any]
           print("param",param)
           apiManager.contectToApiWith(url: MyURLs.reschedule.url,
                                       methodType: .post,
                                       params: param,
                                       success: { (json) in
                                           print("==>jsn",json)
                                           if let data = try? JSONSerialization.data(withJSONObject: json) {
                                               
                                               do {
                                                   let result = try self.decoder.decode(User_Base.self, from: data)
                                                   didDataReady(result)

                                               }catch{
                                                    print("##error\(error)")
                        let error = AFError.responseSerializationFailed(reason: .decodingFailed(error: error))
                        errorCompletion(error)

                                               }
                                             
                                           }
           }) { (error) in
              print(error)
               errorCompletion(error)
           }
       }
    
    
 
}
