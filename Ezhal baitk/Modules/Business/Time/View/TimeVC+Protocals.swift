//
//  TimeVC+Protocals.swift
//  Ezhal baitk
//
//  Created by a7med on 6/6/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
extension TimeVC:LoginVCView,JwtDelegate,TimeVCDelegate{
    func PresentHomeVC<T>(_ model: T) {
                  PresentHomeVC()
    }
    
    func jwtExpired<T>(_ model: T) {
           alert_login(_Login)
      }
    func activatYourAcount(_ phone: String) {
      print("no activatYourAcount state")
    }
    
    func onConnection() {
        Messages.instance.showMessage(title: "", body: CONNECTION_ERROR, state: .error, layout: .messageView)
    }
    
 
    func PresentVC<T>(_ model: T) {
        dateSelectedBtn.stopAnimation(animationStyle: .normal, revertAfterDelay: 0.5) {[weak self] in
            let vc = UIStoryboard(name: AppStoryboard.Shared.rawValue, bundle: nil).instantiateViewController(withIdentifier: "JobSubmitVC") as! JobSubmitVC
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .coverVertical
            self!.present(vc, animated: true, completion: nil)
        }
    }
    
    func onFailure<T>(_ msg: T) {
        Messages.instance.showMessage(title: "", body: msg as! String, state: .error, layout: .messageView)
    }
    
    
    
    
    func loader_start() {
        dateSelectedBtn.startAnimation()
        
    }
    
    func loader_stop() {
        dateSelectedBtn.stopAnimation()
        
    }
    
    
    
}
