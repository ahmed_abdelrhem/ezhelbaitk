//
//  TimeVC.swift
//  Ezhal baitk
//
//  Created by a7med on 6/1/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

class TimeVC:BaseViewController{
    var issues : [IssueData]?
    var pay4 : Pay4?

    var package : NewSubscriptionData?
    var note : (String?,[Data?]?)
    var order_id: Int!
    var payment: Int?
    var selectionDates : [String] = []
    var fix_time : String?
    internal var presenter : TimeVCPresenter!
    
    @IBOutlet weak var dateSelectedBtn: TransitionButton!
    private var time = Date().dateString(with: "h:mm a")
    @IBOutlet weak var timePicker: UIDatePicker!
    @IBOutlet weak var dayLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        title = "Schedule".localized
        presenter = TimeVCPresenter(view: self)
        
    }
    
    @IBAction func selectTimeBtnPressed(_ sender: UIButton) {
        switch pay4 ?? .issues {
        case .issues:
            let vc = MyLocationsVC()
            vc.payment = payment
            vc.issues = issues
            vc.schedule = (selectionDates.first ?? "",time)
            vc.note = note
            vc.pay4 = pay4
            navigationController?.pushViewController(vc, animated: true)
        default:
            let vc = MyLocationsVC()
            vc.payment = payment
            vc.package = package
            vc.selectionDates = selectionDates
            vc.pay4 = pay4
            vc.note = note
            vc.fix_time = time
            navigationController?.pushViewController(vc, animated: true)
      
        }
        
    }
    
    
    @IBAction func timePickerChangedValue(_ sender: UIDatePicker) {
        time = sender.date.dateString(with: "h:mm a")
        
    }
    
}
