//
//  IssuesVCInteractor.swift
//  Ezhal baitk
//
//  Created by a7med on 5/20/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
class IssuesVCInteractor{
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias model = (Issue_Base)->()
    
    
    func getIssues(unit_id:Int,didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorType) {
       
        apiManager.contectToApiWith(url: MyURLs.issues.url + "\(unit_id)",
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
                                        print(json)
                                        if let data = try? JSONSerialization.data(withJSONObject: json) {
                                            
                                            do {
                                                let result = try self.decoder.decode(Issue_Base.self, from: data)
                                                didDataReady(result)

                                            }catch{
                                                print("model parse error\(error)")

                                            }
                                          
                                        }
        }) { (error) in
           print(error)
            errorCompletion(error)
        }
    }

}
