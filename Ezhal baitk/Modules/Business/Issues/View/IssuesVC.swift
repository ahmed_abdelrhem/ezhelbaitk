//
//  IssuesVC.swift
//  Ezhal baitk
//
//  Created by a7med on 5/20/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

class IssuesVC:BaseViewController{
    var cat_id : Int?
    var payment: Int?
    weak var summeryDelegate : UnitsVCDelegate?
    var summeryDelegate_tag : Int = 0
var selected_list : [IssueData] = []
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
            tableView.rowHeight = 170
            tableView.sectionFooterHeight = 300
            tableView.separatorStyle = .none
            tableView.register(UINib(nibName: "IssueCell", bundle: nil), forCellReuseIdentifier: "IssueCell")
            tableView.register(UINib(nibName: "IssueFooter", bundle: nil), forHeaderFooterViewReuseIdentifier: "IssueFooter")
        }
    }
    var unit : (id:Int,name:String,image:String)?
    internal var presenter:IssuesVCPresenter!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        title = "Issue".localized
        guard let cat_id = cat_id else { return  }
        presenter = IssuesVCPresenter(view: self, cat_id, selected_list: selected_list)
              guard unit != nil else {
                  fatalError("Error: Category id = Null")
              }
        presenter.viewdidload(unit!.id)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
