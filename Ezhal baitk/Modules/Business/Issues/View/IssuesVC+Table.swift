//
//  IssuesVC+Table.swift
//  Ezhal baitk
//
//  Created by a7med on 5/30/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
extension IssuesVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getlistCount()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IssueCell", for: indexPath) as! IssueCell
        cell.delegate = self as IssueCellDelegate
        presenter.configureCell(cell: cell, at: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: "IssueFooter") as! IssueFooter
        presenter.configureFooter(footer)
        footer.delegate = self
        return footer
    }
    
    
}
extension IssuesVC:IssueCellDelegate,IssueFooterDelegate{
    func continueBtnPressed(_ sender: UIButton) {
        let selectedIssues = presenter.getSelectedIssues()
        guard  cat_id != nil else {
            return
        }
        for sub in DEF.user_subscriptions {
            guard let  category_id = sub.category_id else {
                return
            }
            if cat_id == category_id{
                guard let subPrice = Int(sub.price ?? "") else {
                    break
                }
                if presenter.total <= subPrice{
                    // payment = 2 // old cycle of subscriptions
                    payment = 1
                }else{
                    break
                }
                print("DEF.user_subscriptions",sub)
                
                
                if summeryDelegate != nil {
                    summeryDelegate?.appendIssues(issues: selectedIssues, tag:summeryDelegate_tag)
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: SummeryVC.self) {
                            self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                }else{
                    let vc = UIStoryboard(name: AppStoryboard.Business.rawValue, bundle: nil).instantiateViewController(withIdentifier: "SummeryVC") as! SummeryVC
                    vc.issues = selectedIssues
                    vc.method = "Package".localized
                    vc.payment = payment
                    navigationController?.pushViewController(vc, animated: true)
                }
                
                
                return
            }
        }
        
        
        
        if summeryDelegate != nil {
            summeryDelegate?.appendIssues(issues: selectedIssues,tag: summeryDelegate_tag)
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: SummeryVC.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }else{
            
            let vc = UIStoryboard(name: AppStoryboard.Business.rawValue, bundle: nil).instantiateViewController(withIdentifier: "PayVC") as! PayVC
            vc.issues = selectedIssues
            vc.pay4 = .issues
            navigationController?.pushViewController(vc, animated: true)
            
        }
        
        
    }
    
    func selectedBtnPressed(_ sender: UIButton, _ cell: UITableViewCell) {
        let indexp = tableView.indexPath(for: cell)!
        presenter.selected(indexp.row)
        tableView.reloadData()
        
        
    }
    
    func minusBtnPressed(_ sender: UIButton, _ cell: UITableViewCell) {
        let indexp = tableView.indexPath(for: cell)!
        presenter.minus(indexp.row)
        tableView.reloadData()
    }
    
    func plusBtnPressed(_ sender: UIButton, _ cell: UITableViewCell) {
        let indexp = tableView.indexPath(for: cell)!
        presenter.Plus(indexp.row)
        tableView.reloadData()
        
    }
    
    
}
