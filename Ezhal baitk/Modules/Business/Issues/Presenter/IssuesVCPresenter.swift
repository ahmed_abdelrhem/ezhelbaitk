//
//  IssuesVCPresenter.swift
//  Ezhal baitk
//
//  Created by a7med on 5/20/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation

class IssuesVCPresenter {
    private let interactor = IssuesVCInteractor()
    private var list = [IssueData]()
    private var warranty = ""
    private var details = ""
    private (set) var total = 0
    private var selectedFreeRow = false
    private var cat_id : Int?
    private var selected_list : [IssueData] = []
    weak var view: (LoaderJwtDelegate)?
    init(view:LoaderJwtDelegate,_ cat_id:Int,selected_list : [IssueData]) {
        self.view = view
        self.cat_id = cat_id
        self.selected_list = selected_list
    
        
    }
    
    deinit {
        _NC.removeObserver(self, name: .selected_list, object: nil)
    }
    
    func viewdidload(_ unit_id:Int)  {
        view?.loader_start()
        interactor.getIssues(unit_id: unit_id,didDataReady: { [weak self](model) in
            guard self != nil else { return }
            if model.status == API_status.Success.rawValue{
                self?.list = model.data ?? []
                self?.warranty = model.warranty ?? ""
                self?.details = model.details ?? ""
                if self!.list.count == 0{
                    self?.view?.onEmpty()
                }else{
                    self!.view?.onSuccess("")
                }
            }else if model.status == API_status.JwtExpired.rawValue{
                self!.view?.jwtExpired(model.message ?? "")
            }else{
                
                self?.view?.onFailure(model.message ?? "")
            }
            self?.view?.loader_stop()
            
        }) { [weak self](error) in
            
            guard self != nil else { return }
            self?.view?.loader_stop()
            self?.view?.onConnection()
            self?.view?.onFailure(error?.errorDescription)
        }
    }
    func getlistCount() ->Int {
        return list.count
    }
    
    func configureCell(cell:IssueCellView,at row: Int)  {
        // set category id to issue
        list[row].category_id = cat_id
        for selected_item in selected_list {
            print("compare>>>",selected_item,list[row])
            if (selected_item.id! == list[row].id!) && (selected_item.unit_id! == list[row].unit_id!) {
               list[row].selected = true
            }
        }
        
        let item = list[row]
        if (item.price ?? "") == "FREE" {
            cell.setFreeCell("FREE".localized, item.currency ?? "")
        }else{
            cell.setPrice(item.price ?? "", item.currency ?? "")
        }
        cell.setName(item.name ?? "")
        cell.hideCounter(row == 0)
        cell.setDetails(item.details ?? "")
        cell.setCounter("\(item.count)")
        cell.setSelect(item.selected)
    }
    
    
    func configureFooter(_ footer: IssueFooterView){
        footer.setdetails(details)
        footer.setwarranty(warranty)
        footer.settotal(" \(total) QR")
        let enb = total > 0 || selectedFreeRow
        footer.setContinueBtn(enb)
        
        
    }
    func Plus(_ row: Int){
        list[row].count += 1
        calculateTotal()
        
    }
    func selected(_ row: Int){
        
        if 0 == row {
            list[row].selected = !list[row].selected
            selectedFreeRow = list[row].selected
            for i in 0..<list.count {
                if i != row {
                    list[i].selected = false
                }
            }
        }else{
            list[row].selected = !list[row].selected
            list[0].selected = false
            selectedFreeRow = false
            
            
        }
        
        calculateTotal()
        
    }
    
    func minus(_ row: Int){
        if  list[row].count > 1{
            list[row].count -= 1
        }
        calculateTotal()
    }
    private func calculateTotal(){
        total = 0
        for item in list {
            if item.selected && item.price != "FREE" {
                let price = Int(item.price ?? "0") ?? 0
                let count = item.count
                total += (price*count)
            }
        }
    }
    
    func getSelectedIssues() -> [IssueData] {
        return list.filter { (item) -> Bool in
            return item.selected
        }
    }
    
}
