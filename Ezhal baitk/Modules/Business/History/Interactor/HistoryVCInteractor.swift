//
//  HistoryVCInteractor.swift
//  Ezhal baitk
//
//  Created by a7med on 6/2/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
class HistoryVCInteractor{
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias model = (History_Base)->()
    
    
    func getHistory(didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorType) {
       
        apiManager.contectToApiWith(url: MyURLs.history.url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
                                        print(json)
                                        if let data = try? JSONSerialization.data(withJSONObject: json) {
                                            
                                            do {
                                                let result = try self.decoder.decode(History_Base.self, from: data)
                                                didDataReady(result)

                                            }catch{
                                                print("model parse error\(error)")

                                            }
                                          
                                        }
        }) { (error) in
           print(error)
            errorCompletion(error)
        }
    }

}
