//
//  HistoryVCPresenter.swift
//  Ezhal baitk
//
//  Created by a7med on 6/2/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
class HistoryVCPresenter {
    private let interactor = HistoryVCInteractor()
    private var list = [HistoryData]()
    weak var view: (LoaderJwtDelegate)?
    init(view:LoaderJwtDelegate) {
        self.view = view
    }
    
    func viewdidload()  {
        view?.loader_start()
        interactor.getHistory(didDataReady: { [weak self](model) in
            guard self != nil else { return }
            if model.status == API_status.Success.rawValue{
                self?.list = model.data ?? []
                if self!.list.count == 0{
                    self?.view?.onEmpty()
                }else{
                    self!.view?.onSuccess("")
                }
            }else if model.status == API_status.JwtExpired.rawValue{
                self!.view?.jwtExpired(model.message ?? "")
            }else{
                
                self?.view?.onFailure(model.message ?? "")
            }
            self?.view?.loader_stop()
            
        }) { [weak self](error) in
            
            guard self != nil else { return }
            self?.view?.loader_stop()
            self?.view?.onConnection()
            self?.view?.onFailure(error?.errorDescription)
        }
    }
    func getlistCount() ->Int {
        return list.count
    }
    
    func configureCell(cell:HistoryCellView,at row: Int)  {
        let dateString = list[row].created_at ?? ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateObj = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = "MMM d, yyyy"
        let date = dateObj?.dateString() ?? ""
        switch list[row].status ?? 0 {
        case 0:
            cell.setStatus("Job has been submitted".localized)
        case 1:
            cell.setStatus("Technician assigned".localized)
        case 2:
            cell.setStatus("Technician on the road".localized)
        case 3:
            cell.setStatus("Technician on Site".localized)
        case 4:
            cell.setStatus("Job in progress".localized)
        case 5:
            cell.setStatus("Job completed".localized)
        case 6:
            cell.setStatus("Job Refused".localized)
            
        default:
            break
        }
        
        cell.setDate(date)
        cell.setPrice("\(list[row].total ?? "") \(list[row].currency ?? "")")
        
    }
    func didselect(_ row:Int) -> Int? {
        return list[row].id
    }
    
    
    
}
