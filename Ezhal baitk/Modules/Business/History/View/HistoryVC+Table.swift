//
//  HistoryVC+Table.swift
//  Ezhal baitk
//
//  Created by a7med on 6/2/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
extension HistoryVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getlistCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell", for: indexPath)  as! HistoryCell
        presenter.configureCell(cell: cell, at: indexPath.row)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let  order_id = presenter.didselect(indexPath.row) else{return}
           let vc = JobDetailsVC()
                 vc.order_id = order_id
                 navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
}
