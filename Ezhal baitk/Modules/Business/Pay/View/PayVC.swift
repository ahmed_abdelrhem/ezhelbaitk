//
//  PayVC.swift
//  Ezhal baitk
//
//  Created by a7med on 5/30/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
enum Pay4:Int {
    case issues = 0
    case package = 1
    case reschadule = 2
}

class PayVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var issues : [IssueData]?
    var package:NewSubscriptionData?
    var cat_id:Int?
    var pay4 : Pay4?
    var payment : Int?
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.dataSource = self
            tableView.delegate = self
            tableView.rowHeight = UITableView.automaticDimension
            tableView.tableFooterView = UIView(frame: .zero)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
            let cell = tableView.dequeueReusableCell(withIdentifier: "CreditCell", for: indexPath)
            return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !DEF.isLogin {
            alert_login("Please,You should Login first .".localized)

            return
        }
        switch pay4 ?? .issues {
        case .issues:
                let vc = UIStoryboard(name: AppStoryboard.Business.rawValue, bundle: nil).instantiateViewController(withIdentifier: "SummeryVC") as! SummeryVC
                vc.issues = issues
                vc.method = "Credit Card".localized
                vc.payment = 1
                navigationController?.pushViewController(vc, animated: true)
       
            
        case .package :
                let vc = UIStoryboard(name: AppStoryboard.Business.rawValue, bundle: nil).instantiateViewController(withIdentifier: "DateVC") as! DateVC
                vc.issues = issues
                vc.pay4 = pay4
                vc.package = package
                vc.payment = 1
                vc.cat_id = cat_id
                navigationController?.pushViewController(vc, animated: true)
         
        default:
            break
        }
    }
    
    
    
    
    
}
