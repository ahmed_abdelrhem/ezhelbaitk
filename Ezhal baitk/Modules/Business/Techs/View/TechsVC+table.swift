//
//  TechsVC+table.swift
//  Ezhal baitk
//
//  Created by a7med on 6/7/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
extension TechsVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getlistCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TechnicialCell", for: indexPath)  as! TechnicialCell
        presenter.configureCell(cell: cell, at: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let unit = presenter.didSelect(indexPath.row)
        let vc = UIStoryboard(name: AppStoryboard.Business.rawValue, bundle: .main).instantiateViewController(withIdentifier: "IssuesVC") as! IssuesVC
        vc.unit = unit
        vc.cat_id = cat_id
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
}
