//
//  TechsVC.swift
//  Ezhal baitk
//
//  Created by a7med on 6/7/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

class TechsVC:BaseViewController{
    internal var presenter : TechsVCPresenter!
    var cat_id : Int?
    var location : (lat:String,lng:String)?
    var rate: CGFloat?
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.tableFooterView = UIView(frame: .zero)
         
            tableView.separatorStyle = .singleLine
            tableView.rowHeight = 100
            tableView.register(UINib(nibName: "TechnicialCell", bundle: nil), forCellReuseIdentifier: "TechnicialCell") 
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    
        presenter = TechsVCPresenter(view: self)
        presenter.viewdidload(cat_id: cat_id ?? 0, location: location, rate: rate)
        title = "Filter Result".localized
    }
    
  
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
