//
//  TechsVCPresenter.swift
//  Ezhal baitk
//
//  Created by a7med on 6/7/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
class TechsVCPresenter {
    private let interactor = TechsVCInteractor()
    private var list : [Technicians_list] = []
    weak var view: (LoaderJwtDelegate)?
    init(view:LoaderJwtDelegate) {
        self.view = view
    }
    
    func viewdidload(cat_id:Int,location : (lat:String,lng:String)?,rate: CGFloat?)  {
        view?.loader_start()
        interactor.filter(cat_id:cat_id,location : location,rate: rate,didDataReady: { [weak self](model) in
            guard self != nil else { return }
            if model.status == API_status.Success.rawValue{
                self?.list = model.data?.technicians_list ?? []
                if self!.list.count == 0{
                    self?.view?.onEmpty()
                }else{
                    self!.view?.onSuccess("")
                }
            }else if model.status == API_status.JwtExpired.rawValue{
                self!.view?.jwtExpired(model.message ?? "")
            }else{
                
                self?.view?.onFailure(model.message ?? "")
            }
            self?.view?.loader_stop()
            
        }) { [weak self](error) in
            
            guard self != nil else { return }
            self?.view?.loader_stop()
            self?.view?.onConnection()
            self?.view?.onFailure(error?.errorDescription)
        }
    }
    func getlistCount() ->Int {
        return list.count
    }
    
    func configureCell(cell:TechnicialCellView,at row: Int)  {
        let tech = list[row]
        cell.setPhoto(with: tech.technician_image ?? "")
        cell.setRate(tech.rate ?? 0)
        cell.setName(name: tech.technician_name ?? "")
        cell.setCat(cat:tech.subcategory_name ?? "")
        

        
    }
    func didSelect(_ row:Int) ->  (id:Int,name:String,image:String) {
        let tech = list[row]
        print(">>",tech)
        let unit = (tech.subcategory_id ?? 0,tech.subcategory_name ?? "",tech.technician_image ?? "")
        return unit
    }
    
    
    
}
