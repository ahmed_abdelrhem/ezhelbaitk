//
//  TechsVCInteractor.swift
//  Ezhal baitk
//
//  Created by a7med on 6/7/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
import Alamofire
class TechsVCInteractor {
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias user_model = (Technicians_Base)->()

    func filter(cat_id:Int,location : (lat:String,lng:String)?,rate: CGFloat?, didDataReady: @escaping user_model, andErrorCompletion errorCompletion: @escaping errorType) {
        
        var param = [
            "cat_id": cat_id
            ] as [String : Any]
        if location != nil {
            param["lat"] = location!.lat
            param["lng"] = location!.lng


        }
        if rate != nil {
            param["rate"] = rate!
        }
       
        print("param",param)
        apiManager.contectToApiWith(url: MyURLs.filter.url,
                                    methodType: .post,
                                    params: param,
                                    success: { (json) in
                                        print("==>jsn",json)
                                        if let data = try? JSONSerialization.data(withJSONObject: json) {
                                            
                                            do {
                                                let result = try self.decoder.decode(Technicians_Base.self, from: data)
                                                didDataReady(result)

                                            }catch{
                                                 print("##error\(error)")
                        let error = AFError.responseSerializationFailed(reason: .decodingFailed(error: error))
                        errorCompletion(error)

                                            }
                                          
                                        }
        }) { (error) in
           print(error)
            errorCompletion(error)
        }
    }
 
}
