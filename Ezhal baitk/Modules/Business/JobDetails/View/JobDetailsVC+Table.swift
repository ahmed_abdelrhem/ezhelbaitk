//
//  JobDetailsVC+Table.swift
//  Ezhal baitk
//
//  Created by a7med on 6/7/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
extension JobDetailsVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0,1:
            return 0
        case 2,3:
            return 1
        case 4 :
            print("")
            return  presenter.getSub()
        default:
            return 0
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LocationCell", for: indexPath) as! LocationCell
            presenter.configureLocation(cell)
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddressCell", for: indexPath) as! AddressCell
            presenter.configureAddress(cell)
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "RequestCell", for: indexPath) as! RequestCell
            presenter.configureRequest(cell, indexPath.row)
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0:
            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ConfirmationHeader") as! ConfirmationHeader
            header.delegate = self
            presenter.configureHeader(header)
            return header
        case 1:
            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "LocationHeader") as! LocationHeader
            presenter.ConfigurePackageHeader(header)
            return header
        case 2:
            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "LocationHeader") as! LocationHeader
            header.setTitle("Location:".localized)
            header.setDetails("View")
            header.addAction()
            header.delegate = self
            return header
        case 3:
            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "LocationHeader") as! LocationHeader
            header.setTitle("Address:".localized)
            header.hideDetails()
            return header
        default:
            return UIView(frame: .zero)
        }
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 330
        case 1:
            return 100
        case 2,3 :
            return 20
        default:
            return .zero
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 2:
            return 150
        case 3:
            return 100
        case 4:
            return UITableView.automaticDimension
        default:
            return UITableView.automaticDimension
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2 {
            let vc = ShowLocationVC()
            vc.lat = presenter.getLocation().lat
            vc.lng = presenter.getLocation().lng
            vc.modalPresentationStyle = .overFullScreen
            present(vc, animated: true, completion: nil)
        }
    }
    
    
    
    
}
extension JobDetailsVC:LocationHeaderDelegate,ConfirmationHeaderDelegate{
    func chatBtnPressed(_ cell: ConfirmationHeader) {
        print("chatBtnPressed")
        guard let id = order_id else { return  }
        let vc = ChatVC(id: id, receiver_id: presenter.data?.technician?.id, is_vist: is_vist)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func callBtnPressed(_ cell: ConfirmationHeader) {
        if is_vist{
            guard let number = presenter.data?.tech?.phone else {
                return
            }
             if let url = URL(string: "tel://\(number)") {
                UIApplication.shared.open(url)
            }
        }else{
            guard let number = presenter.data?.technician?.phone else {
                return
            }
             if let url = URL(string: "tel://\(number)") {
                UIApplication.shared.open(url)
            }
        }
     
    }
    
    func viewAction() {
        let vc = ShowLocationVC()
        vc.lat = presenter.getLocation().lat
        vc.lng = presenter.getLocation().lng
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: true, completion: nil)
    }
    
    
}
