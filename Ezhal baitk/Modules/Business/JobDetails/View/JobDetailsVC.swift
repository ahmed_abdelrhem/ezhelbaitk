//
//  JobDetailsVC.swift
//  Ezhal baitk
//
//  Created by a7med on 6/7/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

class JobDetailsVC:BaseViewController{
    var order_id : Int?
    var is_vist = false
    var presenter : JobDetailsVCPresenter!
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            
            tableView.delegate = self
            tableView.dataSource = self
            tableView.separatorStyle = .none
            tableView.register(UINib(nibName: "RequestCell", bundle: nil), forCellReuseIdentifier: "RequestCell")
            tableView.register(UINib(nibName: "AddressCell", bundle: nil), forCellReuseIdentifier: "AddressCell")
            tableView.register(UINib(nibName: "LocationCell", bundle: nil), forCellReuseIdentifier: "LocationCell")
            tableView.register(UINib(nibName: "LocationHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "LocationHeader")
            tableView.register(UINib(nibName: "ConfirmationHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "ConfirmationHeader")

        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        title = "Job Details".localized
        presenter = JobDetailsVCPresenter(view: self,is_vist:is_vist)
        presenter.viewdidload(id: order_id ?? 0)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
