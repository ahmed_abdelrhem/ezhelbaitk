//
//  JobDetailsVCInteractor.swift
//  Ezhal baitk
//
//  Created by a7med on 6/7/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
class JobDetailsVCInteractor{
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias model = (Order_Base)->()
    
    
    func getDetails(id:Int,is_vist:Bool,didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorType) {
        let url = is_vist ? MyURLs.vist_details.url : MyURLs.order_details.url
       
        apiManager.contectToApiWith(url: url + "\(id)",
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
                                        print(json)
                                        if let data = try? JSONSerialization.data(withJSONObject: json) {
                                            
                                            do {
                                                let result = try self.decoder.decode(Order_Base.self, from: data)
                                                didDataReady(result)

                                            }catch{
                                                print("model parse error\(error)")

                                            }
                                          
                                        }
        }) { (error) in
           print(error)
            errorCompletion(error)
        }
    }

}
