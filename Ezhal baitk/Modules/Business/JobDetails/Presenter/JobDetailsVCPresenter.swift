//
//  JobDetailsVCPresenter.swift
//  Ezhal baitk
//
//  Created by a7med on 6/7/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
class JobDetailsVCPresenter {
    private var is_vist = false

    private let interactor = JobDetailsVCInteractor()
    private (set) var data : OrderData?
    weak var view: (LoaderJwtDelegate)?
    init(view:LoaderJwtDelegate,is_vist:Bool) {
        self.view = view
        self.is_vist = is_vist
    }
    
    func viewdidload(id:Int)  {
        view?.loader_start()
        interactor.getDetails(id:id, is_vist: is_vist,didDataReady: { [weak self](model) in
            guard self != nil else { return }
            if model.status == API_status.Success.rawValue{
                self?.data = model.data
                if self!.data == nil{
                    self?.view?.onEmpty()
                }else{
                    self!.view?.onSuccess("")
                }
            }else if model.status == API_status.JwtExpired.rawValue{
                self!.view?.jwtExpired(model.message ?? "")
            }else{
                
                self?.view?.onFailure(model.message ?? "")
            }
            self?.view?.loader_stop()
            
        }) { [weak self](error) in
            
            guard self != nil else { return }
            self?.view?.loader_stop()
            self?.view?.onConnection()
            self?.view?.onFailure(error?.errorDescription)
        }
    }
    
    
    func configureHeader(_ header: ConfirmationHeaderView )  {
        header.setStack(false)
        if is_vist{
            header.setName(name: data?.tech?.name ?? "")
            let rate = Double(data?.tech_rate ?? "0") ?? 0
            header.setRate(rate)
            let dateString = data?.fix_date ?? ""
            header.setTimeDate(dateString, data?.fix_time ?? "")
            header.setPhoto(with: data?.tech?.image ?? "")
            header.setPhone(phone: data?.tech?.phone ?? "")
        }else{
            header.setName(name: data?.technician?.name ?? "")
            let rate = Double(data?.technicianRate ?? "0") ?? 0
            header.setRate(rate)
            let dateString = data?.date ?? ""
            header.setTimeDate(dateString, data?.time ?? "")
            header.setPhoto(with: data?.technician?.image ?? "")
            header.setPhone(phone: data?.technician?.phone ?? "")
        }
     
        
        
    }
    func configureLocation(_ cell: LocationCellView ) {
        if is_vist{
            let lat = Double(data?.user_package?.user_location?.lat ?? "0.0") ?? 0.0
            let lng = Double(data?.user_package?.user_location?.lng ?? "0.0") ?? 0.0
            cell.setLocation(lat, lng)
        }else{
            let lat = Double(data?.order_details?.lat ?? "0.0") ?? 0.0
            let lng = Double(data?.order_details?.lng ?? "0.0") ?? 0.0
            cell.setLocation(lat, lng)
        }
 
    }
    func configureAddress(_ cell: AddressCellView) {
        if is_vist{
            cell.setArea(data?.user_package?.user_location?.area ?? "")
            cell.setBlock(data?.user_package?.user_location?.block ?? "")
            cell.setStreet(data?.user_package?.user_location?.street ?? "")
            cell.setHouse(data?.user_package?.user_location?.house ?? "")
        }else{
            cell.setArea(data?.order_details?.area ?? "")
            cell.setBlock(data?.order_details?.block ?? "")
            cell.setStreet(data?.order_details?.street ?? "")
            cell.setHouse(data?.order_details?.house ?? "")
        }
  
        
        
    }
    func ConfigurePackageHeader(_ header: LocationHeaderView)  {
        if is_vist{
            
            header.setTitle("Package:".localized)
            header.setDetails("\(data?.category_name ?? "") \(data?.package_name ?? "") / \(data?.user_package?.package_category?.package?.name ?? "")")
        }else{
            var issuesNames:String {
                var names = ""
                for issue in data?.subcategory ?? []{
                    names += "\(issue.name ?? "") "
                }
                return names
            }
            header.setTitle("Tasks:".localized)
            header.setDetails(issuesNames)
        }
       
    }
    func configureRequest(_ cell: RequestCellView,_ row:Int)  {
        if is_vist {
            let pay = "From Package".localized
            cell.setTotal(pay)
            cell.setPrice("")
            cell.setname("\(data?.category_name ?? "") \(data?.package_name ?? "") / \(data?.user_package?.package_category?.package?.name ?? "")")

        }else{
            let item = data?.subcategory?[row]
            cell.setname("\(item?.name ?? "")")
            cell.setPrice("\(item?.price ?? "0") \(data?.currency ?? "")")
            let pay = (data?.payment_type ?? 0) == 0 ? "Cash".localized : "Online".localized
            cell.setTotal(pay)
        }
        cell.setPayMethod()
    }
    func getSub()-> Int  {
        if is_vist {
          return 1
        }else{
            return data?.subcategory?.count ?? 0

        }
    }
    
    func getLocation() -> (lat:Double,lng:Double) {
        let lat = Double(data?.order_details?.lat ?? "0.0") ?? 0.0
        let lng = Double(data?.order_details?.lng ?? "0.0") ?? 0.0
        print(">>",lat,">>",lng)
        return(lat,lng)
        
    }
    
}
