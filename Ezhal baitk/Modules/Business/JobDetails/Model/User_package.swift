/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct User_package : Codable {
	let id : Int?
	let subscription_num : String?
	let status : Int?
	let user_id : Int?
	let location_id : Int?
	let package_category_id : Int?
	let fix_time : String?
	let expire_at : String?
	let price : String?
	let created_at : String?
	let package_name : String?
	let user_location : User_location?
	let package_category : Package_category?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case subscription_num = "subscription_num"
		case status = "status"
		case user_id = "user_id"
		case location_id = "location_id"
		case package_category_id = "package_category_id"
		case fix_time = "fix_time"
		case expire_at = "expire_at"
		case price = "price"
		case created_at = "created_at"
		case package_name = "package_name"
		case user_location = "user_location"
		case package_category = "package_category"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		subscription_num = try values.decodeIfPresent(String.self, forKey: .subscription_num)
		status = try values.decodeIfPresent(Int.self, forKey: .status)
		user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
		location_id = try values.decodeIfPresent(Int.self, forKey: .location_id)
		package_category_id = try values.decodeIfPresent(Int.self, forKey: .package_category_id)
		fix_time = try values.decodeIfPresent(String.self, forKey: .fix_time)
		expire_at = try values.decodeIfPresent(String.self, forKey: .expire_at)
		price = try values.decodeIfPresent(String.self, forKey: .price)
		created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
		package_name = try values.decodeIfPresent(String.self, forKey: .package_name)
		user_location = try values.decodeIfPresent(User_location.self, forKey: .user_location)
		package_category = try values.decodeIfPresent(Package_category.self, forKey: .package_category)
	}

}