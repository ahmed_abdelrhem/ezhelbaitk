/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct OrderData : Codable {
	let id : Int?
	let order_id : Int?
    var technician : UserData?
	let subcategory : [Subcategory]?
	let order_details : Order_details?
	let payment_type : Int?
	let currency : String?
	let date : String?
	let time : String?
	let technicianRate : String?
	let total : Int?
	let subscription_Data : SubscriptionData?
    let visit_num : String?
    let user_id : Int?
    let tech_id : Int?
    let user_package_id : Int?
    let items : Int?
    let send_items : String?
    let confirm_items : String?
    let status : Int?
    let fix_date : String?
    let fix_time : String?
    let user_rate : String?
    let user_comment : String?
    let tech_rate : String?
    let tech_comment : String?
    let created_at : String?
    let months : Int?
    let package_name : String?
    let category_name : String?
    let user_name : String?
    let category_id : Int?
    let user_package : User_package?
    var tech : UserData?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case order_id = "order_id"
		case technician = "technician"
		case subcategory = "subcategory"
		case order_details = "order_details"
		case payment_type = "payment_type"
		case currency = "currency"
		case date = "date"
		case time = "time"
		case technicianRate = "technicianRate"
		case total = "total"
		case subscription_Data = "subscription_Data"
        case visit_num = "visit_num"
        case user_id = "user_id"
        case tech_id = "tech_id"
        case user_package_id = "user_package_id"
        case items = "items"
        case send_items = "send_items"
        case confirm_items = "confirm_items"
        case status = "status"
        case fix_date = "fix_date"
        case fix_time = "fix_time"
        case user_rate = "user_rate"
        case user_comment = "user_comment"
        case tech_rate = "tech_rate"
        case tech_comment = "tech_comment"
        case created_at = "created_at"
        case months = "months"
        case package_name = "package_name"
        case category_name = "category_name"
        case user_name = "user_name"
        case category_id = "category_id"
        case user_package = "user_package"
        case tech = "tech"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		order_id = try values.decodeIfPresent(Int.self, forKey: .order_id)
		technician = try values.decodeIfPresent(UserData.self, forKey: .technician)
		subcategory = try values.decodeIfPresent([Subcategory].self, forKey: .subcategory)
		order_details = try values.decodeIfPresent(Order_details.self, forKey: .order_details)
		payment_type = try values.decodeIfPresent(Int.self, forKey: .payment_type)
		currency = try values.decodeIfPresent(String.self, forKey: .currency)
		date = try values.decodeIfPresent(String.self, forKey: .date)
		time = try values.decodeIfPresent(String.self, forKey: .time)
		technicianRate = try values.decodeIfPresent(String.self, forKey: .technicianRate)
        do {
		subscription_Data = try values.decodeIfPresent(SubscriptionData.self, forKey: .subscription_Data)
        }catch{
            subscription_Data = nil

        }
        do {
            total = try values.decodeIfPresent(Int.self, forKey: .total)

        } catch  {
            total = Int (try values.decodeIfPresent(String.self, forKey: .total) ?? "0") ?? 0

        }
        
        
        visit_num = try values.decodeIfPresent(String.self, forKey: .visit_num)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        do {
            tech_id = try values.decodeIfPresent(Int.self, forKey: .tech_id)
        } catch  {
            let id = try values.decodeIfPresent(String.self, forKey: .tech_id)
            tech_id = Int(id ?? "")
        }
        user_package_id = try values.decodeIfPresent(Int.self, forKey: .user_package_id)
        items = try values.decodeIfPresent(Int.self, forKey: .items)
        send_items = try values.decodeIfPresent(String.self, forKey: .send_items)
        confirm_items = try values.decodeIfPresent(String.self, forKey: .confirm_items)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        fix_date = try values.decodeIfPresent(String.self, forKey: .fix_date)
        fix_time = try values.decodeIfPresent(String.self, forKey: .fix_time)
        user_rate = try values.decodeIfPresent(String.self, forKey: .user_rate)
        user_comment = try values.decodeIfPresent(String.self, forKey: .user_comment)
        tech_rate = try values.decodeIfPresent(String.self, forKey: .tech_rate)
        tech_comment = try values.decodeIfPresent(String.self, forKey: .tech_comment)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        months = try values.decodeIfPresent(Int.self, forKey: .months)
        package_name = try values.decodeIfPresent(String.self, forKey: .package_name)
        category_name = try values.decodeIfPresent(String.self, forKey: .category_name)
        user_name = try values.decodeIfPresent(String.self, forKey: .user_name)
        category_id = try values.decodeIfPresent(Int.self, forKey: .category_id)
        user_package = try values.decodeIfPresent(User_package.self, forKey: .user_package)
        tech = try values.decodeIfPresent(UserData.self, forKey: .tech)
        if technician == nil {
            technician = tech
        }
        if tech == nil {
            tech = technician
        }
        

	}

}
