/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct User_location : Codable {
	let id : Int?
	let user_id : Int?
	let city_id : Int?
	let area_num : String?
	let area : String?
	let block : String?
	let street : String?
	let house : String?
	let floor : String?
	let appartment : String?
	let directions : String?
	let lat : String?
	let lng : String?
	let deleted : Int?
	let created_at : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case user_id = "user_id"
		case city_id = "city_id"
		case area_num = "area_num"
		case area = "area"
		case block = "block"
		case street = "street"
		case house = "house"
		case floor = "floor"
		case appartment = "appartment"
		case directions = "directions"
		case lat = "lat"
		case lng = "lng"
		case deleted = "deleted"
		case created_at = "created_at"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
		city_id = try values.decodeIfPresent(Int.self, forKey: .city_id)
		area_num = try values.decodeIfPresent(String.self, forKey: .area_num)
		area = try values.decodeIfPresent(String.self, forKey: .area)
		block = try values.decodeIfPresent(String.self, forKey: .block)
		street = try values.decodeIfPresent(String.self, forKey: .street)
		house = try values.decodeIfPresent(String.self, forKey: .house)
		floor = try values.decodeIfPresent(String.self, forKey: .floor)
		appartment = try values.decodeIfPresent(String.self, forKey: .appartment)
		directions = try values.decodeIfPresent(String.self, forKey: .directions)
		lat = try values.decodeIfPresent(String.self, forKey: .lat)
		lng = try values.decodeIfPresent(String.self, forKey: .lng)
		deleted = try values.decodeIfPresent(Int.self, forKey: .deleted)
		created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
	}

}