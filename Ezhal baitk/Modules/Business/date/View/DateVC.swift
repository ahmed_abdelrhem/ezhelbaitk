//
//  DateVC.swift
//  Ezhal baitk
//
//  Created by a7med on 6/1/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
import SwiftUI
class DateVC:BaseViewController{
    var issues : [IssueData]?
    var pay4 : Pay4?
    var package : NewSubscriptionData?
    var cat_id : Int?
    
    var note : (String?,[Data?]?)
    var order_id: Int?
    var payment: Int?
    
    
    var invalidPeriodLength = 4
    @IBOutlet weak var collectionView: Koyomi!{
        didSet {
            collectionView.circularViewDiameter = 0.2
            collectionView.calendarDelegate = self
            collectionView.currentDateFormat = "dd/MM/yyyy"
            
            collectionView.inset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            collectionView.weeks = ("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat")
            collectionView.style = .standard
            collectionView.dayPosition = .center
            collectionView.selectedStyleColor = .MainColor
            collectionView.setDayFont(size: 16)
            collectionView.setWeekFont(size: 14)
        }
    }
    
    
    var selectionDates : [String] = []
    
    
    @IBOutlet weak var currentMonLbl: UILabel!
    @IBOutlet weak var previousBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        title = "Date".localized
        guard let cat_id = cat_id else { return  }
        guard let pay4 = pay4 else { return  }
        switch pay4 {
        case .package:
            if cat_id == 1 {
                collectionView.selectionMode = .single(style: .background)
                invalidPeriodLength = 1
            }else{
                collectionView.selectionMode = .multiple(style: .background)
                invalidPeriodLength = 4
                
            }
        default:
            collectionView.selectionMode = .single(style: .background)
            invalidPeriodLength = 1
        }
           
        
        previousBtn.isHidden = true
        currentMonLbl.text = collectionView.currentDateString(withFormat: "MMMM yyyy")
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        switch pay4 {
        case .package:
        let msg = cat_id == 1 ? "The specialist will visit you once a month. Please select a visit date.".localized : "The specialist will visit you four times a month. Please select the date of the visit.".localized
        let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok".localized, style: .destructive, handler: .none)
        alert.addAction(action)
        present(alert, animated: true, completion: .none)
        default:
            break
        }
           
      
    }
    
    @IBAction func selectdateBtnPressed(_ sender: UIButtonX) {
        if selectionDates.isEmpty {
            Messages.instance.showMessage(title: "", body: "please, select a date.".localized, state: .error, layout: .messageView)
            return
        }
        switch pay4 ?? .issues {
        case .issues:
            let vc = UIStoryboard(name: AppStoryboard.Business.rawValue, bundle: nil).instantiateViewController(withIdentifier: "TimeVC") as! TimeVC
            vc.issues = issues
            vc.pay4 = pay4
            vc.note = note
            vc.order_id = order_id
            vc.payment = payment
            vc.selectionDates = selectionDates
            navigationController?.pushViewController(vc, animated: true)

        default:
            let vc = UIStoryboard(name: AppStoryboard.Business.rawValue, bundle: nil).instantiateViewController(withIdentifier: "TimeVC") as! TimeVC
            vc.pay4 = pay4
            vc.package = package
            vc.payment = payment
            vc.selectionDates = selectionDates
            navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    
    
    
    @IBAction func nextBtnPressed(_ sender: UIButton) {
        collectionView.display(in: .next)
        
    }
    
    
    @IBAction func previousBtnPressed(_ sender: UIButton) {
        collectionView.display(in: .previous)
        
    }
    
}


extension DateVC: KoyomiDelegate {
    
    func koyomi(_ koyomi: Koyomi, didSelect date: Date?, forItemAt indexPath: IndexPath) {
        guard let date = date else { return  }
        print("You Selected: \(date)")
        for i in 0..<selectionDates.count {
            if date.dateString(with: "dd-MM-yyyy") == selectionDates[i]{
                selectionDates.remove(at: i)
                return
            }
        }
        
        if selectionDates.count < invalidPeriodLength {
            
            selectionDates.append(date.dateString(with: "dd-MM-yyyy"))
        }
       

        
    }
    
    func koyomi(_ koyomi: Koyomi, currentDateString dateString: String) {
        
        
        currentMonLbl.text = koyomi.currentDateString(withFormat: "MMMM yyyy")
        
    }
    
    
    @objc(koyomi:shouldSelectDates:to:withPeriodLength:)
    func koyomi(_ koyomi: Koyomi, shouldSelectDates date: Date?, to toDate: Date?, withPeriodLength length: Int) -> Bool {
        //        guard let date = date else { return  false}
        //        guard let toDate = toDate else { return  false}
//        guard let date = date?.adding(.day, value: 1) else { return false  }
        guard let date = date else { return false }
        
        print("You shouldSelectDates: \(date)")
        
        for i in 0..<selectionDates.count {
            if date.dateString(with: "dd-MM-yyyy") == selectionDates[i]{
                return true
            }
        }
        
        if selectionDates.count >= invalidPeriodLength {
            print("More than \(invalidPeriodLength) days are invalid period.")
            return false
        }
        
        let current_day = date.adding(.day, value: 1)
        if current_day.isInPast{
                
            print("you selected day in past.")

            return false
        }
            
        
        
        return true
    }
    
    
}
