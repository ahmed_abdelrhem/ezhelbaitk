//
//  CreditVC.swift
//  Ezhal baitk
//
//  Created by a7med on 5/31/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

class CreditVC:BaseViewController{
    var issues : [IssueData]?
    var package : NewSubscriptionData?
    var pay4: Pay4?
    var payment: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    @IBAction func addCardBtnPressed(_ sender: UIButton) {
        switch pay4 {
        case .issues:
            let vc = UIStoryboard(name: AppStoryboard.Business.rawValue, bundle: nil).instantiateViewController(withIdentifier: "SummeryVC") as! SummeryVC
            vc.issues = issues
            vc.method = "Online".localized
            vc.payment = payment
            navigationController?.pushViewController(vc, animated: true)
        case .package:
            let vc = UIStoryboard(name: AppStoryboard.Business.rawValue, bundle: nil).instantiateViewController(withIdentifier: "DateVC") as! DateVC
            vc.issues = issues
            vc.pay4 = pay4
            vc.package = package
            vc.payment = payment
            navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
        
    }
    
}
