//
//  HomeVCPresenter.swift
//  Ezhal baitk
//
//  Created by a7med on 5/19/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
class HomeVCPresenter {
    private let interactor = HomeVCInteractor()
    private var list = [Categories]()
    private var filter_list = [Categories]()
    weak var view: (LoaderJwtDelegate)?
    init(view:LoaderJwtDelegate) {
        self.view = view
    }
    
    func viewdidload()  {
        view?.loader_start()
        interactor.getCategories(didDataReady: { [weak self](model) in
            guard self != nil else { return }
            if model.status == API_status.Success.rawValue{
                self?.list = model.data?.categories ?? []
                if self!.list.count == 0{
                    self?.view?.onEmpty()
                }else{
                    self!.view?.onSuccess("")
                }
            }else if model.status == API_status.JwtExpired.rawValue{
                self!.view?.jwtExpired(model.message ?? "")
            }else{
                
                self?.view?.onFailure(model.message ?? "")
            }
            self?.view?.loader_stop()
            
        }) { [weak self](error) in
            
            guard self != nil else { return }
            self?.view?.loader_stop()
            self?.view?.onConnection()
            self?.view?.onFailure(error?.errorDescription)
        }
    }
    func getlistCount() ->Int {
        return list.count
    }
    func getlist() ->[Categories] {
           return list
       }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        filter_list = list.filter({( item : Categories) -> Bool in
            let name = item.name ?? ""
            return name.lowercased().contains(searchText.lowercased())
        })

        view?.onSuccess("")
    }
    
    func getFilterListCount() ->Int {
        return filter_list.count
    }
    
    
    func configureCell(cell:SeviceCellView,at row: Int ,isFilter: Bool)  {
        let item = !isFilter ? list[row] : filter_list[row]
        cell.setPhoto(with: item.icon ?? "")
        cell.setTitle(item.name ?? "")
        cell.setProgessValue(Double(item.orders_percent ?? 0))
        
       
    }
    func didSelect(_ row:Int,isFilter: Bool) -> Int  {
        return !isFilter ? list[row].id ?? 0 : filter_list[row].id ?? 0
    }
    
    
    
    
}
