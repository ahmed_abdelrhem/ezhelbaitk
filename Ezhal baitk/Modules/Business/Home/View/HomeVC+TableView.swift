//
//  HomeVC+TableView.swift
//  Ezhal baitk
//
//  Created by a7med on 5/19/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
extension HomeVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isFiltering(){
         return   presenter.getFilterListCount()
        }
        return presenter.getlistCount()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath)  as! CategoryCell
        presenter.configureCell(cell: cell, at: indexPath.row, isFilter: isFiltering())
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cat_id = presenter.didSelect(indexPath.row, isFilter: isFiltering())
        let vc = UIStoryboard(name: AppStoryboard.Business.rawValue, bundle: .main).instantiateViewController(withIdentifier: "UnitsVC") as! UnitsVC
        vc.cat_id = cat_id
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let w = (collectionView.frame.width / 3)
        
        return CGSize(width: w, height: 110)
    }
    
}
