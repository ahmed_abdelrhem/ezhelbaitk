//
//  HomeVC.swift
//  Ezhal baitk
//
//  Created by a7med on 5/10/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

class HomeVC:BaseViewController{
    internal var presenter: HomeVCPresenter!
    
    @IBOutlet weak var searchTxtf: UITextFieldX!
    
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
            collectionView.register(UINib(nibName: "CategoryCell", bundle: nil), forCellWithReuseIdentifier: "CategoryCell")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //        navigationController?.navigationBar.barTintColor = .white
        //        navigationController?.navigationBar.tintColor = .darkText
        //        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:  UIColor.darkText]
        searchTxtf.forceSwitchingRegardlessOfTag = true
        presenter = HomeVCPresenter(view: self)
        searchTxtf.delegate = self
        presenter.viewdidload()
        sideMenu()
    }
    
 
  
    @IBAction func seachBtnPressed(_ sender: UIButton) {
        presenter.filterContentForSearchText(searchTxtf.text!)
    }
    
    @IBAction func FilterBtnPressed(_ sender: UIButton) {
        let vc = UIStoryboard(name: AppStoryboard.Business.rawValue, bundle: nil).instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        vc.list = presenter.getlist()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func notificationBtnPressed(_ sender: UIBarButtonItem) {
        let vc = UIStoryboard(name: AppStoryboard.Shared.rawValue, bundle: nil).instantiateViewController(withIdentifier: "NotificationsVC") as! NotificationsVC
        navigationController?.pushViewController(vc, animated: true)
        
        
    }
}
