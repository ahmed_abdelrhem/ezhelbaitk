//
//  TabBarVC.swift
//  Ezhal baitk
//
//  Created by apple on 03/01/2022.
//  Copyright © 2022 a7med. All rights reserved.
//

import UIKit

class TabBarVC: UITabBarController {
    
    lazy private var homeVC: UINavigationController = {
        let item = UITabBarItem()
        item.title = _Home
        item.image = .home
        let vc =  UIStoryboard(name: AppStoryboard.Business.rawValue, bundle: .main).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let nav = UINavigationController(rootViewController: vc)
        nav.title = _Home
        nav.tabBarItem = item
        return nav
    }()
    
    lazy private var subscriptionVC: UINavigationController = {
        let item = UITabBarItem()
        item.title = _Subscription
        item.image = .subscription
        let vc =  UIStoryboard(name: AppStoryboard.Shared.rawValue, bundle: .main).instantiateViewController(withIdentifier: "SubscriptionVC") as! SubscriptionVC
        let nav = UINavigationController(rootViewController: vc)
        nav.title = _Subscription
        nav.tabBarItem = item
        return nav
    }()
    
    lazy private var jobVC: UINavigationController = {
        let item = UITabBarItem()
        item.title = _ActiveJobs
        item.image = .activeJobs
        let vc =  UIStoryboard(name: AppStoryboard.Shared.rawValue, bundle: .main).instantiateViewController(withIdentifier: "JobVC") as! JobVC
        let nav = UINavigationController(rootViewController: vc)
        nav.title = _ActiveJobs
        nav.tabBarItem = item
        return nav
    }()
    
    lazy private var profileVC: UINavigationController = {
        let item = UITabBarItem()
        item.title = _MyProfile
        item.image = .profile
        let vc =  UIStoryboard(name: AppStoryboard.Shared.rawValue, bundle: .main).instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        let nav = UINavigationController(rootViewController: vc)
        nav.title = _MyProfile
        nav.tabBarItem = item
        return nav
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    
    private func animate(_ imageView: UIImageView) {
           UIView.animate(withDuration: 0.1, animations: {
               imageView.transform = CGAffineTransform(scaleX: 1.25, y: 1.25)
           }) { _ in
               UIView.animate(withDuration: 0.25, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 3.0, options: .curveEaseInOut, animations: {
                   imageView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
               }, completion: nil)
           }
       }


    init(with selectedIndex:Int = 0) {
        super.init(nibName: "TabBarVC", bundle: .main)
        viewControllers = [homeVC,subscriptionVC,jobVC,profileVC]
        if selectedIndex < (viewControllers?.count ?? 0) {
            selectedViewController = viewControllers?[selectedIndex]
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        print("didSelect item \(selectedIndex)>>",item.title ?? "")

     
    }


}
