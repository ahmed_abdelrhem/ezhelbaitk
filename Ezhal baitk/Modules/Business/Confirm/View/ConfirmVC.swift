//
//  ConfirmVC.swift
//  Ezhal baitk
//
//  Created by a7med on 6/1/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

class ConfirmVC:BaseViewController{
    var issues : [IssueData]?
    var payment : Int?
    var info : AddressInfo?
    var note : (String?,[Data?]?)
    var isPaymentSuccess = false
    internal var issuesNames:String {
        var names = ""
        for issue in issues ?? []{
            names += "\(issue.name ?? "") "
        }
        return names
    }
    internal var total: Int {
        var total = 0
        for item in issues ?? [] {
            if item.selected && item.price != "FREE" {
                let price = Int(item.price ?? "0") ?? 0
                let count = item.count
                total += (price*count)
            }
        }
        return total
    }
    internal var currency : String {
        if issues?.count ?? 0 > 0{
            return issues?[0].currency ?? "QR".localized
        }else{
            return "QR".localized
        }
    }
    
    internal var presenter: ConfirmVCPresenter!
    @IBOutlet weak var submitBtn: TransitionButton!
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
            tableView.separatorStyle = .none
            tableView.register(UINib(nibName: "RequestCell", bundle: nil), forCellReuseIdentifier: "RequestCell")
            tableView.register(UINib(nibName: "AddressCell", bundle: nil), forCellReuseIdentifier: "AddressCell")
            tableView.register(UINib(nibName: "LocationCell", bundle: nil), forCellReuseIdentifier: "LocationCell")
            tableView.register(UINib(nibName: "LocationHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "LocationHeader")
            tableView.register(UINib(nibName: "TotalFooter", bundle: nil), forHeaderFooterViewReuseIdentifier: "TotalFooter")
            tableView.register(UINib(nibName: "ConfirmationHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "ConfirmationHeader")
            
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        submitBtn.isHidden = true
        // Do any additional setup after loading the view.
        presenter = ConfirmVCPresenter(view: self)
        if isPaymentSuccess {
            presenter.submit(issues: issues ?? [], info: info!, payment: payment ?? 1, note: note)
        }else{
            let alert = UIAlertController(title: "Sorry!".localized, message: "Payment Failed, Please Try Again.".localized, preferredStyle: .alert)
            let action = UIAlertAction(title: "Done".localized, style: .default){_ in
                
                alert.dismiss(animated: true) { [weak self] in
                    guard let self = self else{return}
                    self.PresentHomeVC()
                }
            }
            
            alert.addAction(action)
            present(alert, animated: true, completion: .none)
            
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    @IBAction func submirBtnPressed(_ sender: TransitionButton) {
        
        PresentHomeVC()
    }
    
    
}
