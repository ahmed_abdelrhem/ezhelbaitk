//
//  ConfirmVC+Protocals.swift
//  Ezhal baitk
//
//  Created by a7med on 6/1/20.
//  Copyright © 2020 a7med. All rights reserved.
//

extension ConfirmVC:LoginVCView{
    func activatYourAcount(_ phone: String) {
        
        let vc = UIStoryboard(name: AppStoryboard.Main.rawValue, bundle: .main).instantiateViewController(withIdentifier: "CodeVC") as! CodeVC
        vc.is_activate = true
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    func onConnection() {
        Messages.instance.showMessage(title: "", body: CONNECTION_ERROR, state: .error, layout: .messageView)
    }
    
    
    func PresentVC<T>(_ model: T) {
        
        let alert = UIAlertController(title: "Congratulations!".localized, message: "Order Created Sucessfully.".localized, preferredStyle: .alert)
        let action = UIAlertAction(title: "Done".localized, style: .default) { _ in
            alert.dismiss(animated: true) { [weak self] in
                guard let self = self else{return}
                let menu =  MenuVC()
                let tabs =  TabBarVC(with: 2)
                let swRevealVC =  SWRevealViewController(rearViewController: menu, frontViewController: tabs)
                swRevealVC?.rightViewController = menu
                UIApplication.shared.keyWindow?.rootViewController = swRevealVC
                UIApplication.shared.keyWindow?.makeKeyAndVisible()
            }
        }
            alert.addAction(action)
            self.present(alert, animated: true, completion: .none)
            
        }
    
    
    func onFailure<T>(_ msg: T) {
        Messages.instance.showMessage(title: "", body: msg as! String, state: .error, layout: .messageView)
    }
    
    
    
    
    func loader_start() {
        startAnimating(ponit: nil)
        
        
    }
    
    func loader_stop() {
//        submitBtn.isHidden = false
        stopAnimating()
        
    }
    
    
    
}

