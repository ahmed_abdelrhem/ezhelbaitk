//
//  ConfirmVC+Table.swift
//  Ezhal baitk
//
//  Created by a7med on 6/1/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

extension ConfirmVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0,1:
            return 0
        case 2,3:
            return 1
        case 4 :
            return issues?.count ?? 0
        default:
            return 0
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LocationCell", for: indexPath) as! LocationCell
            cell.setLocation(info?.lat ?? 0, info?.lng ?? 0)
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddressCell", for: indexPath) as! AddressCell
            cell.setArea(info?.area?.name ?? "")
            cell.setBlock(info?.block ?? "")
            cell.setStreet(info?.street ?? "")
            cell.setHouse(info?.house ?? "")
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "RequestCell", for: indexPath) as! RequestCell
            cell.setname(issues?[indexPath.row].name ?? "")
            cell.setPrice("\(issues?[indexPath.row].count ?? 0) x \(issues?[indexPath.row].price ?? "0") \(issues?[indexPath.row].currency ?? "")")
            let count = issues?[indexPath.row].count ?? 0
            let price = Int(issues?[indexPath.row].price ?? "0") ?? 0
            let total = count * price
            cell.setTotal("\(total) \(issues?[indexPath.row].currency ?? "")")
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0:
            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ConfirmationHeader") as! ConfirmationHeader
            header.setStack(true)
            header.setTimeDate(info?.schedule.date ?? "", info?.schedule.time ?? "")
            return header
        case 1:
            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "LocationHeader") as! LocationHeader
            header.setTitle("Issues:".localized)
            header.setDetails(issuesNames)
            return header
        case 2:
            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "LocationHeader") as! LocationHeader
            header.setTitle("Location:".localized)
            header.setDetails("View")
            header.addAction()
            header.delegate = self
            return header
        case 3:
            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "LocationHeader") as! LocationHeader
            header.setTitle("Address:".localized)
            header.hideDetails()
            return header
        default:
            return UIView(frame: .zero)
        }
        
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 4 {
            return 50
        }
        return .zero
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 4 {
            let footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: "TotalFooter") as! TotalFooter
            footer.setTotal("\(total) \(currency)")
                    return footer
        }
        return UIView(frame: .zero)
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 260
        case 1:
            return UITableView.automaticDimension
        case 2,3 :
            return 20
        default:
            return .zero
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 2:
            return 150
        case 3:
            return 100
        case 4:
            return UITableView.automaticDimension
        default:
            return UITableView.automaticDimension
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2 {
            let vc = ShowLocationVC()
            vc.lat = info?.lat ?? 0
            vc.lng = info?.lng
            vc.modalPresentationStyle = .overFullScreen
            present(vc, animated: true, completion: nil)
        }
    }
    
    
    
    
}
extension ConfirmVC:LocationHeaderDelegate{
    func viewAction() {
           let vc = ShowLocationVC()
                 vc.lat = info?.lat ?? 0
                 vc.lng = info?.lng
                 vc.modalPresentationStyle = .overFullScreen
                 present(vc, animated: true, completion: nil)
    }

    
}
