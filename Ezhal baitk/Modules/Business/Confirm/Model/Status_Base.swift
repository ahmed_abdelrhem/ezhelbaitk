//
//  Status_Base.swift
//  Ezhal baitk
//
//  Created by a7med on 7/22/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
struct Status_Base : Codable {
    let status : Int?
    let message : String?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case message = "message"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }

}
