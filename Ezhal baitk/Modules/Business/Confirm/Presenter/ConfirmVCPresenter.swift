//
//  ConfirmVCPresenter.swift
//  Ezhal baitk
//
//  Created by a7med on 7/22/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
class ConfirmVCPresenter {
    private let interactor = ConfirmVCInteractor()
    weak var view: (LoginVCView)?
    init(view:LoginVCView) {
        self.view = view
    }
    
    func submit(issues: [IssueData],info: AddressInfo,payment:Int,note:(String?,[Data?]?)) {
        view?.loader_start()
        interactor.submit(issues: issues,info: info,payment:payment,note:note, didDataReady: { [weak self](model) in
            guard self != nil else { return }
            self?.view?.loader_stop()

            if model.status == API_status.Success.rawValue{
                self?.view?.PresentVC(model.message  ?? "")

            }
            else{
                
                self?.view?.onFailure(model.message  ?? "")

            }
            
        }) { [weak self](error) in
            
            guard self != nil else { return }
            self?.view?.loader_stop()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }
    
}
