//
//  ConfirmVCInteractor.swift
//  Ezhal baitk
//
//  Created by a7med on 7/22/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
import Alamofire
class ConfirmVCInteractor {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias model = (Status_Base)->()
    
    func submit(issues: [IssueData],info: AddressInfo,payment:Int,note:(String?,[Data?]?), didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorType) {
        
        print("info>>",info)
        
        var param : [String : Any] = [
            "address": info.address ?? "",
            "lat":info.lat ?? 0.0,
            "lng":info.lng ?? 0.0,
            "area":info.area?.name ?? "",
            "city_id":info.area?.id ?? 0,
            "block": info.block ?? "",
            "house": info.house ?? "",
            "street":info.street ?? "",
            "floor": info.floor ?? "",
            "appartment": info.apartment ?? "",
            "directions": info.extra ?? "",
            "date": info.schedule.date,
            "time":info.schedule.time,
            "payment_type": payment,
            "area_num":info.area?.id ?? 0
        ]
        if note.0 != nil {
            param["note"] = note.0 ?? ""
        }
        
        
        let subcategory_idsAndCount = issues.map({ (issue) -> (id:Int,count:Int) in
            return (issue.id ?? 0,issue.count)
        })
        
        for i in 0..<subcategory_idsAndCount.count {
            param ["subcategory_id[\(i)]"] = subcategory_idsAndCount[i].id
            param ["quantity[\(i)]"]  = subcategory_idsAndCount[i].count
        }
        
        
        
        
        print("##param",param)
        if note.1 == nil {
            print("XXx")
            apiManager.contectToApiWith(url: MyURLs.order.url,
                                        methodType: .post,
                                        params: param,
                                        success: { (json) in
                                            print("#==>jsn",json)
                                            if let data = try? JSONSerialization.data(withJSONObject: json) {
                                                
                                                do {
                                                    let result = try self.decoder.decode(Status_Base.self, from: data)
                                                    didDataReady(result)
                                                    
                                                }catch{
                                                    print("##error\(error)")
                                                    let error = AFError.responseSerializationFailed(reason: .decodingFailed(error: error))
                                                    errorCompletion(error)
                                                    
                                                }
                                                
                                            }
                                        }) { (error) in
                print(error)
                errorCompletion(error)
            }
        }else{
            
            
            print("yyy")
            guard let data = note.1 else { return  }
            
            apiManager.requestWith(upload: data, key: "image", to: MyURLs.order.url, mimeType: "image", forbody: param, success: { (json) in
                if let data = try? JSONSerialization.data(withJSONObject: json) {
                    print("jsn",json)
                    
                    do {
                        let result = try self.decoder.decode(Status_Base.self, from: data)
                        didDataReady(result)
                    }catch{
                        print("##error\(error)")
                        let error = AFError.responseSerializationFailed(reason: .decodingFailed(error: error))
                        errorCompletion(error)
                        
                    }
                    
                }
            })  { (error) in
                print(error)
                errorCompletion(error)
            }
            
            
            
            
        }
        
    }
    
    
    
}
