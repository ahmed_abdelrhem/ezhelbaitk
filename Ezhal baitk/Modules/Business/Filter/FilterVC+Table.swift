//
//  FilterVC+Table.swift
//  Ezhal baitk
//
//  Created by a7med on 6/2/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

extension FilterVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return 1
        }else{
            return rows.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PickerCell", for: indexPath) as! PickerCell
            switch indexPath.row {
            case 0:
                cell.setCat(_PickCategory)
                
            case 1:
                cell.setCat(_PickUnit)
            case 2:
                cell.setCat(_PickService)
            default:
                break
            }
            cell.delegate = self
            return cell
            //        case 1:
            //            let cell = tableView.dequeueReusableCell(withIdentifier: "RangeCell", for: indexPath) as! RangeCell
            //            cell.set(max: 5, min: 0)
        //            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LocationCell", for: indexPath) as! LocationCell
            cell.setLocation(Double(DEF.lat) ?? 0, Double(DEF.lng) ?? 0)
            return cell
        default:
            return UITableViewCell()
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 1:
            return 25
        default:
            return .zero
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 60
            //        case 1:
        //            return 60
        case 1:
            return 250
        default:
            return UITableView.automaticDimension
            
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
            
            //        case 1:
            //            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "LocationHeader") as! LocationHeader
            //            header.setTitle("Technician Rate".localized)
            //            header.hideDetails()
        //            return header
        case 1:
            let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "LocationHeader") as! LocationHeader
            header.setTitle("Location:".localized)
            header.setDetails("View".localized)
            header.addAction()
            header.delegate = self
            return header
        default:
            return UIView(frame: .zero)
        }
        
    }
    
    
    
}


extension FilterVC:LocationHeaderDelegate,LocationDelegate,PickerCellDelegate,RangeCellDelegate{
    func sliderChanged(_ cell: UITableViewCell, _ sender: RangeSeekSlider) {
        print("rate",sender.selectedMaxValue)
        rate = sender.selectedMaxValue
    }
    
    func catBtnPressed(_ sender: UIButton, _ cell: UITableViewCell) {
        guard let indexpath = tableView.indexPath(for: cell) else { return  }
        var selectIndex = 0
        let alert = UIAlertController(style: .actionSheet, title: "", message: "")
        var pickerViewValues: [[String]] = [list.map { $0.name ?? "" }]
        switch indexpath.row {
        case 0:
            pickerViewValues = [list.map { $0.name ?? "" }]
            
        case 1:
            pickerViewValues = [presenter.list.map { $0.name ?? "" }]
        case 2:
            pickerViewValues = [presenter.issues.map { $0.name ?? "" }]
        default:
            break
        }
        let pickerViewSelectedValue: PickerViewViewController.Index = (column: 0, row: selectIndex)
        
        alert.addPickerView(values: pickerViewValues, initialSelection: pickerViewSelectedValue) {[unowned self] vc, picker, index, values in
            DispatchQueue.main.async {
                switch indexpath.row {
                case 0:
                    sender.setTitle(self.list[index.row].name ?? "", for: .normal)
                    self.cat = self.list[index.row]
                    
                case 1:
                    sender.setTitle(self.presenter.list[index.row].name ?? "", for: .normal)
                    self.sub = self.presenter.list[index.row]
                case 2:
                    sender.setTitle(self.presenter.issues[index.row].name ?? "", for: .normal)
                    self.issue = self.presenter.issues[index.row]
                default:
                    break
                }
                selectIndex = index.row
                
            }
        }
        alert.addAction(UIAlertAction(title: _Ok, style: .default, handler: { [weak self] (action) in
            guard let self = self else{return}
            switch indexpath.row {
            case 0:
                self.presenter.getUnits(self.cat?.id ?? 0)
                
            case 1:
                self.presenter.getIssues(self.sub?.id ?? 0)
                
            default:
                break
            }
            if indexpath.row < 2  && self.rows.count < 3{
                self.addRow()
            }
        }))
        alert.addAction(UIAlertAction(title: _Cancel, style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func RetriveLocation(lat: Double, lng: Double, add: String, country: String) {
        location = ("\(lat)","\(lng)")
    }
    
    func viewAction() {
        let vc = MapVC()
        vc.delegate = self
        vc.modalPresentationStyle = .overFullScreen
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func addRow() {
        self.rows.insert(rows.count, at: rows.count)
        
        self.tableView.performBatchUpdates({
            self.tableView.insertRows(at: [IndexPath(row: rows.count - 1,
                                                     section: 0)],
                                      with: .automatic)
        }, completion: nil)
    }
    
    
}
