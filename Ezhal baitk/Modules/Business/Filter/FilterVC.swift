//
//  FilterVC.swift
//  Ezhal baitk
//
//  Created by a7med on 6/2/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

class FilterVC:BaseViewController{
    var rows = [0]
     var list = [Categories]()
    internal var cat : Categories?
    internal var sub : Categories?
    internal var issue : IssueData?
    internal var location : (lat:String,lng:String)?
    internal var rate: CGFloat?
    private (set) var presenter : UnitsVCPresenter!

    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
            tableView.separatorStyle = .none
            tableView.register(UINib(nibName: "LocationCell", bundle: nil), forCellReuseIdentifier: "LocationCell")
            tableView.register(UINib(nibName: "PickerCell", bundle: nil), forCellReuseIdentifier: "PickerCell")
//            tableView.register(UINib(nibName: "RangeCell", bundle: nil), forCellReuseIdentifier: "RangeCell")
            tableView.register(UINib(nibName: "LocationHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "LocationHeader")
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter = UnitsVCPresenter()
        if list.count > 0 {
            cat  = list[0]
        }
    }
    

    @IBAction func restBtnPressed(_ sender: UIButton) {
        rows = [0]
        tableView.reloadData()
    }
    
    
    @IBAction func applyBtnPressed(_ sender: UIButton) {
        let vc = TechsVC()
        vc.cat_id = cat?.id
        vc.location = location
//        vc.rate = rate
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
