//
//  AddressVCPresenter.swift
//  Ezhal baitk
//
//  Created by iMac on 9/28/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
//protocol AddressVCDelegate: class {
//    func loader_start()
//    func loader_stop()
//    func onFailure<T>(_ msg: T)
//    func onSuccess<T>(_ msg: T)
//}
class AddressVCPresenter {
    private let interactor = AddressVCInteractor()
    weak var view: (LoaderDelegate)?
    init(view:LoaderDelegate) {
        self.view = view
    }
    
    func addLocation(_ address:AddressInfo)  {
        view?.loader_start()
        interactor.addLocation(address ,didDataReady: { [weak self](model) in
            guard self != nil else { return }
            if model.status == API_status.Success.rawValue{
                self?.view?.onSuccess(model.message ?? "")
            }else{
                self?.view?.onFailure(model.message  ?? "")
            }
            self?.view?.loader_stop()
            
        }) { [weak self](error) in
            
            guard self != nil else { return }
            self?.view?.onFailure(CONNECTION_ERROR)

            self?.view?.loader_stop()
        }
    }
    
    
    
    
    
    
}
