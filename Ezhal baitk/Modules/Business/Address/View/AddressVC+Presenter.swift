//
//  AddressVC+Presenter.swift
//  Ezhal baitk
//
//  Created by iMac on 9/28/20.
//  Copyright © 2020 a7med. All rights reserved.
//



extension AddressVC: LoaderDelegate{
    func onEmpty() {
                Messages.instance.showMessage(title: "", body: "Error", state: .error, layout: .messageView)

    }
    
    func jwtExpired<T>(_ model: T) {
         alert_login(_Login)
    }
    func onConnection() {
        Messages.instance.showMessage(title: "", body: "Error", state: .error, layout: .messageView)
    }
    
 
    func onFailure<T>(_ msg: T) {
        Messages.instance.showMessage(title: "", body: msg as! String, state: .error, layout: .messageView)
        
    }
    
    func onSuccess<T>(_ msg: T) {
        guard let info = info else { return  }
        delegate?.addLocation(location: info)
            navigationController?.popViewController(animated: true)
    }
    
    
    func loader_start() {
        startAnimating(ponit: nil)
    }
    
    func loader_stop() {
        stopAnimating()
    }
    
    
    
    
}
