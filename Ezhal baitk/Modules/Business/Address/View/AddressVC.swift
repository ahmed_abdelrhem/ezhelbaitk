//
//  AddressVC.swift
//  Ezhal baitk
//
//  Created by a7med on 6/1/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

protocol AddLocationDelegate:class {
    func addLocation(location:AddressInfo)
    
}

class AddressVC:BaseViewController{
    weak var delegate : AddLocationDelegate?
    var issues : [IssueData]?
    var schedule = (date:"",time:"")
    var note : (String?,[Data?]?)
    private (set) var info : AddressInfo?
    private var lat:Double?
    private var lng:Double?
    private var city: CityData?
    private (set) var presenter : AddressVCPresenter!
    @IBOutlet weak var areaNumTxtF: UITextField!
    @IBOutlet weak var addressTxtF: UITextField!
    @IBOutlet weak var areaTxtF: UITextField!
    @IBOutlet weak var blockTxtF: UITextField!
    @IBOutlet weak var extraTxtF: UITextField!
    @IBOutlet weak var officeTxtF: UITextField!
    @IBOutlet weak var floorTxtF: UITextField!
    @IBOutlet weak var houseTxtF: UITextField!
    @IBOutlet weak var streetTxtF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        presenter = AddressVCPresenter(view: self)
    }
    
    
    @IBAction func mapBtnPressed(_ sender: UIButton) {
        let vc = MapVC()
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func areaBtnPressed(_ sender: UIButton) {
        let vc = UIStoryboard(name: AppStoryboard.Business.rawValue, bundle: nil).instantiateViewController(withIdentifier: "AreaVC") as! AreaVC
        vc.delegate = self
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func continueBtnPessed(_ sender: UIButton) {
        guard  addressTxtF.text != "" else {
            Messages.instance.showMessage(title: _Info, body: "\(_Please) \(_Map).", state: .info, layout: .messageView)
            return
        }
        
        guard  areaTxtF.text != "" else {
            Messages.instance.showMessage(title: _Info, body: "\(_Please) \(_Area).", state: .info, layout: .messageView)
            return
        }
        guard  areaNumTxtF.text != "" else {
                Messages.instance.showMessage(title: _Info, body: "\(_Please) \(_AreaNum).", state: .info, layout: .messageView)
                return
            }
            
        
//        guard  blockTxtF.text != "" else {
//            Messages.instance.showMessage(title: _Info, body: "\(_Please) \(_Block).", state: .info, layout: .messageView)
//            return
//        }
//        
        guard  streetTxtF.text != "" else {
            Messages.instance.showMessage(title: _Info, body: "\(_Please) \(_Street).", state: .info, layout: .messageView)
            return
        }
        guard  houseTxtF.text != "" else {
            Messages.instance.showMessage(title: _Info, body: "\(_Please) \(_House).", state: .info, layout: .messageView)
            return
        }
        
        guard let areaNum = Int(areaNumTxtF.text ?? "") else{
            Messages.instance.showMessage(title: _Info, body: "\(_Please) \(_AreaNum).", state: .info, layout: .messageView)
                         return
        }
        
        guard let delegate = delegate  else {
            return
        }
    
         info = AddressInfo(address: addressTxtF.text!, lat: lat!, lng: lng!, area: city!,area_num:areaNum, block: blockTxtF.text!, street: streetTxtF.text!, house: houseTxtF.text!, floor: floorTxtF.text ?? "", apartment: officeTxtF.text ?? "", extra: extraTxtF.text ?? "",schedule:schedule)
        guard let info = info else { return  }
        presenter.addLocation(info)
     
    }
}

extension AddressVC:LocationDelegate,AreaVCdelegate{
    func areaRetrive(_ city: CityData) {
        print("#Area",city)
        self.city = city
        areaTxtF.text = city.name ?? ""
        
    }
    
    func RetriveLocation(lat: Double, lng: Double, add: String,country: String){
        print("#MyAddress",add)
        self.lat = lat
        self.lng = lng
        addressTxtF.text = add
    }
    
    
}
