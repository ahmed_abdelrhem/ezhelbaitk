//
//  AddressVCInteractor.swift
//  Ezhal baitk
//
//  Created by iMac on 9/28/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
import Alamofire
class AddressVCInteractor {
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias user_model = (Location_Base)->()

    func addLocation(_ address:AddressInfo, didDataReady: @escaping user_model, andErrorCompletion errorCompletion: @escaping errorType) {
        let para = [
            "city_id": address.area?.id ?? 0,
            "lat": "\(address.lat ?? 0.0)",
            "lng": "\(address.lng ?? 0.0)",
            "area_num": address.area_num ?? 0,
            "area": address.area?.name ?? "",
            "block": address.block ?? "",
            "street": address.street ?? "",
            "house": address.house ?? "",
            "floor": address.floor ?? "",
            "appartment": address.apartment ?? "",
            "directions": address.extra ?? ""
            
            ] as [String : Any]
    
        apiManager.contectToApiWith(url: MyURLs.addLocation.url,
                                    methodType: .post,
                                    params: para,
                                    success: { (json) in
                                        if let data = try? JSONSerialization.data(withJSONObject: json) {
                                            
                                            do {
                                                let result = try self.decoder.decode(Location_Base.self, from: data)
                                                didDataReady(result)

                                            }catch{
                                                 print("##error\(error)")
                        let error = AFError.responseSerializationFailed(reason: .decodingFailed(error: error))
                        errorCompletion(error)

                                            }
                                          
                                        }
        }) { (error) in
           print(error)
            errorCompletion(error)
            
        }
    }
    
 
}
