//
//  AddressInfo.swift
//  Ezhal baitk
//
//  Created by a7med on 6/1/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
struct AddressInfo {
    let address : String?
    let lat : Double?
    let lng: Double?
    let area: CityData?
    let area_num: Int?
    let block : String?
    let street : String?
    let house : String?
    let floor: String?
    let apartment:String?
    let extra:String?
    let schedule: (date:String,time:String)
    init(address:String,lat:Double,lng:Double,area:CityData,area_num:Int,block:String,street:String,house:String,floor:String,apartment:String,extra:String,schedule: (date:String,time:String)) {
        self.address = address
        self.lat = lat
        self.lng = lng
        self.area = area
        self.block = block
        self.street = street
        self.house = house
        self.floor = floor
        self.apartment = apartment
        self.extra =  extra
        self.schedule = schedule
        self.area_num = area_num
    }


    
}
