//
//  MyLocationVCInteractor.swift
//  Ezhal baitk
//
//  Created by iMac on 9/27/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
import Alamofire
class MyLocationVCInteractor {
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias user_model = (Location_Base)->()
    typealias invoice_model = (Invoice_Base)->()

    func getLocations( didDataReady: @escaping user_model, andErrorCompletion errorCompletion: @escaping errorType) {
    
        apiManager.contectToApiWith(url: MyURLs.mylocations.url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
                                        if let data = try? JSONSerialization.data(withJSONObject: json) {
                                            
                                            do {
                                                let result = try self.decoder.decode(Location_Base.self, from: data)
                                                didDataReady(result)

                                            }catch{
                                                 print("##error\(error)")
                        let error = AFError.responseSerializationFailed(reason: .decodingFailed(error: error))
                        errorCompletion(error)

                                            }
                                          
                                        }
        }) { (error) in
           print(error)
            errorCompletion(error)
        }
    }
    
    
    func getPaymentLink(price: String , didDataReady: @escaping invoice_model, andErrorCompletion errorCompletion: @escaping errorType) {
        let url = MyURLs.myfatoorah_live.url + price
        apiManager.contectToApiWith(url: url.encodeUrl(),
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
                                        if let data = try? JSONSerialization.data(withJSONObject: json) {
                                            
                                            do {
                                                let result = try self.decoder.decode(Invoice_Base.self, from: data)
                                                didDataReady(result)

                                            }catch{
                                                 print("##error\(error)")
                        let error = AFError.responseSerializationFailed(reason: .decodingFailed(error: error))
                        errorCompletion(error)

                                            }
                                          
                                        }
        }) { (error) in
           print(error)
            errorCompletion(error)
        }
    }
    

    func removeLocation(_ id:Int, didDataReady: @escaping user_model, andErrorCompletion errorCompletion: @escaping errorType) {
        let param = ["location_id":id]
    
        apiManager.contectToApiWith(url: MyURLs.removelocation.url,
                                    methodType: .post,
                                    params: param,
                                    success: { (json) in
                                        if let data = try? JSONSerialization.data(withJSONObject: json) {
                                            
                                            do {
                                                let result = try self.decoder.decode(Location_Base.self, from: data)
                                                didDataReady(result)

                                            }catch{
                                                 print("##error\(error)")
                        let error = AFError.responseSerializationFailed(reason: .decodingFailed(error: error))
                        errorCompletion(error)

                                            }
                                          
                                        }
        }) { (error) in
           print(error)
            errorCompletion(error)
        }
    }
 
}

