//
//  MyLocationsVC.swift
//  Ezhal baitk
//
//  Created by iMac on 9/27/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

class MyLocationsVC:BaseViewController{
    var is_menu = false

    var schedule = (date:"",time:"")
    var note : (String?,[Data?]?)
    var payment : Int?
    var pay4 : Pay4?
    var cat_id : Int?
    

    var issues : [IssueData]?
    

    var package : NewSubscriptionData?
    
    
    var selectionDates : [String] = []
    var fix_time : String?

    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.allowsMultipleSelection = false
            tableView.allowsSelection = true
            tableView.register(UINib(nibName: "MyLocationCell", bundle: .main), forCellReuseIdentifier: "MyLocationCell")
            tableView.sectionFooterHeight = 0
            tableView.sectionHeaderHeight = 0
            tableView.separatorStyle = .singleLine
            tableView.rowHeight = 100
            tableView.tableFooterView = UIView(frame: .zero)
            tableView.tableHeaderView = UIView(frame: .zero)
        }
    }
    private (set) var presenter : MyLocationVCPresenter!
    override func viewDidLoad() {
        super.viewDidLoad()
        print("issues>>",issues)
        // Do any additional setup after loading the view. MyLocationCell
        if is_menu {
            sideMenu()
        }
        presenter = MyLocationVCPresenter(view: self)
        presenter.getLocations()
    }
    
    

    
    @IBAction func addAddressBtnPressed(_ sender: TransitionButton) {
        
        let vc = UIStoryboard(name: AppStoryboard.Business.rawValue, bundle: .main).instantiateViewController(withIdentifier: "AddressVC") as! AddressVC
        vc.issues = issues
        vc.schedule = schedule
        vc.note = note
        vc.delegate = self

        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
}


extension MyLocationsVC:AddLocationDelegate{
    func addLocation(location: AddressInfo) {
        presenter.getLocations()
        tableView.reloadData()
    }
    
    
    
}
