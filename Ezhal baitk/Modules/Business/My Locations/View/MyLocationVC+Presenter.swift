//
//  MyLocationVC+Presenter.swift
//  Ezhal baitk
//
//  Created by iMac on 9/27/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

extension MyLocationsVC: MyLocationVCDelegate{
    func presentPayment(_ msg: String) {

        guard let invoice = presenter.invoice else { return  }
        guard let InvoiceURL = invoice.invoiceURL else { return  }
     
        
        Messages.instance.showMessage(title: "", body: msg , state: .success, layout: .centeredView)

        switch pay4 ?? .issues {
        case .issues:
            guard let info = presenter.info else {
                return
            }
            print("info>>",info)
            print("issues>>",issues)
            print("note>>",note)
            print("pay4>>",pay4)

            let vc = WebVC(url: InvoiceURL,issues: issues, payment: payment, info: info, note: note, pay4: pay4)
            navigationController?.pushViewController(vc, animated: true)
        default:
            var param : [String:Any] = [:]
            guard let location_id = presenter.selectedLocation?.id else { return  }
            param["location_id"] = location_id
            guard let fix_time = self.fix_time else { return  }
            param["fix_time"] = fix_time
            guard let price = self.package?.price else { return  }
            param["price"] = price

            for i in 0..<selectionDates.count {
                param["fix_date[\(i)]"] = selectionDates[i]
            }
            guard let package_id = self.package?.id else { return  }
            param["package_id"] = package_id
            print("param >>",param)
            let vc = WebVC(url: InvoiceURL, parameters: param, pay4: pay4)
            navigationController?.pushViewController(vc, animated: true)
        }


       
        

    }
    
    func reload() {
        tableView.backgroundView = nil
        tableView.reloadData()
    }
    func jwtExpired<T>(_ model: T) {
        alert_login(_Login)
    }
    
    func loader_start() {
        startAnimating(ponit: nil)
        
    }
    
    func loader_stop() {
        stopAnimating()
        
    }
    
    func didselect<T>(_ data: T) {
        if !is_menu {
            switch pay4 ?? .issues {
            case .issues:
                var total: Int {
                    var total = 0
                    for item in issues ?? [] {
                        if item.price != "FREE" {
                            let price = Int(item.price ?? "0") ?? 0
                            let count = item.count
                            total += (price*count)
                        }
                    }
                    return total
                }
                presenter.getPaymentLink("\(total)")
            default:
                guard let price = package?.price else { return  }
                presenter.getPaymentLink(price)
            }
        
        }
        
    }
   
    
    func onFailure<T>(_ msg: T) {
        Messages.instance.showMessage(title: "", body: msg as! String, state: .error, layout: .messageView)
        
    }
    
    func onSuccess<T>(_ msg: T) {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundView = nil
        tableView.reloadData()
    }
    
    func onEmpty() {
        tableView.setEmptyView()
        tableView.reloadData()
    }
    
    func onConnection() {
        tableView.setConnectionView()
        tableView.reloadData()
    }
    
    
}


