//
//  MyLocationCell.swift
//  Ezhal baitk
//
//  Created by iMac on 9/27/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
protocol MyLocationCellDelegate:class {
    func setLocation(_ location: String)
}

class MyLocationCell: UITableViewCell,MyLocationCellDelegate {

    @IBOutlet weak var locationLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setLocation(_ location: String) {
        locationLbl.text = location
    }
    

    
}
