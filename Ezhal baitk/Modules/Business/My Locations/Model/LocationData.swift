/* 
 Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 
 For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar
 
 */

import Foundation
struct LocationData : Codable {
    let id : Int?
    let user_id : Int?
    let city_id : Int?
    let area_num : String?
    let area : String?
    let block : String?
    let street : String?
    let house : String?
    let floor : String?
    let appartment : String?
    let directions : String?
    let lat : String?
    let lng : String?
    let deleted : Int?
    let created_at : String?
    let updated_at : String?
    let city_name : String?
    var address  = ""
    var city : CityData?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case user_id = "user_id"
        case city_id = "city_id"
        case area_num = "area_num"
        case area = "area"
        case block = "block"
        case street = "street"
        case house = "house"
        case floor = "floor"
        case appartment = "appartment"
        case directions = "directions"
        case lat = "lat"
        case lng = "lng"
        case deleted = "deleted"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case city_name = "city_name"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
        city_id = try values.decodeIfPresent(Int.self, forKey: .city_id)
        area_num = try values.decodeIfPresent(String.self, forKey: .area_num)
        area = try values.decodeIfPresent(String.self, forKey: .area)
        block = try values.decodeIfPresent(String.self, forKey: .block)
        street = try values.decodeIfPresent(String.self, forKey: .street)
        house = try values.decodeIfPresent(String.self, forKey: .house)
        floor = try values.decodeIfPresent(String.self, forKey: .floor)
        appartment = try values.decodeIfPresent(String.self, forKey: .appartment)
        directions = try values.decodeIfPresent(String.self, forKey: .directions)
        lat = try values.decodeIfPresent(String.self, forKey: .lat)
        lng = try values.decodeIfPresent(String.self, forKey: .lng)
        deleted = try values.decodeIfPresent(Int.self, forKey: .deleted)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
        city_name = try values.decodeIfPresent(String.self, forKey: .city_name)
        city = CityData(city_id, city_name)
    
    }
    init(address: String,_ city_id: Int?,_ area_num:String?,area:String?,block:String?,street:String?,house:String?,floor:String?,appartment:String?,directions:String?,lat:String?,lng:String?,deleted:Int?,city_name:String?) {
        id = nil
        user_id = DEF.userID
        self.city_id = city_id
        self.area_num = area_num
        self.block = block
        self.street = street
        self.house = house
        self.floor = floor
        self.appartment = appartment
        self.directions = directions
        self.lat = lat
        self.lng = lng
        self.deleted = deleted
        self.city_name = city_name
        created_at = Date().dateString()
        updated_at = Date().dateString()
        self.area = area
        self.address = address
        self.city = CityData(city_id, city_name)
        
        
        
        
    }
    
}
