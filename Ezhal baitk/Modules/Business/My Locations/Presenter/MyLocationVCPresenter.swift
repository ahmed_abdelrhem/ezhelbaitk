//
//  MyLocationVCPresenter.swift
//  Ezhal baitk
//
//  Created by iMac on 9/27/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
import  GoogleMaps

protocol MyLocationVCDelegate: class {
    func loader_start()
    func loader_stop()
    func didselect<T>(_ data:T)
    func onFailure<T>(_ msg: T)
    func onSuccess<T>(_ msg: T)
    func onEmpty()
    func onConnection()
    func reload()
    func presentPayment(_ msg:String)
    func jwtExpired<T>(_ model:T)

}
class MyLocationVCPresenter {
    private let interactor = MyLocationVCInteractor()
    private (set) var locations: [LocationData] = []
    private (set) var selectedLocation: LocationData?
    private (set) var info : AddressInfo?
    weak var view: (MyLocationVCDelegate)?
    private (set) var invoice: Invoice?

    init(view:MyLocationVCDelegate) {
        self.view = view
    }
    
    
    func getLocations()  {
        view?.loader_start()
        interactor.getLocations( didDataReady: { [weak self](model) in
            guard self != nil else { return }
            if model.status == API_status.Success.rawValue{
                self?.locations = model.data ?? []
                if self?.locations.count == 0{
                    self?.view?.onEmpty()
                    
                }else{
                    self?.view?.onSuccess(model.message  ?? "")
                    
                }
                
            }else if  model.status == API_status.JwtExpired.rawValue{
                self!.view?.jwtExpired(model.message ?? "")

            }else{
                
                self?.view?.onFailure(model.message  ?? "")
                
            }
            self?.view?.loader_stop()
            
        }) { [weak self](error) in
            
            guard self != nil else { return }
            self?.view?.loader_stop()
            self?.view?.onConnection()
            self?.view?.onFailure(error?.errorDescription)
        }
    }
    
    func getPaymentLink(_ price:String)  {
        view?.loader_start()
        interactor.getPaymentLink( price: price,didDataReady: { [weak self](model) in
            guard self != nil else { return }
            if model.isSuccess ?? false {
                self?.invoice  = model.invoice
                self?.view?.presentPayment(model.message  ?? "")

            }else{
                
                self?.view?.onFailure(model.message  ?? "")
                
            }
            self?.view?.loader_stop()
            
        }) { [weak self](error) in
            
            guard self != nil else { return }
            self?.view?.loader_stop()
            self?.view?.onConnection()
            self?.view?.onFailure(error?.errorDescription)
        }
    }
    
    func removeLocation(with id:Int)  {
        interactor.removeLocation(id, didDataReady: { [weak self](model) in
            guard self != nil else { return }
        }) { [weak self](error) in
            guard self != nil else { return }
        }
    }
    
    
    func configure(_ cell: MyLocationCellDelegate,_ row:Int)  {
        let geocoder = GMSGeocoder()
        guard  let lat = Double(locations[row].lat ?? "") else{return }
        guard let lng = Double(locations[row].lng ?? "") else{return }
        
        let position = CLLocationCoordinate2DMake(lat, lng)
        geocoder.reverseGeocodeCoordinate(position) { [weak self] response , error in
            guard let self = self else{return}
            if error != nil {
                print("Error: \(String(describing: error?.localizedDescription))")
            } else {
                guard let response = response else { return  }
                print("#google location",response.results())
                let result = response.results()?.first
                let location = result?.lines?.reduce("") { $0 == "" ? $1 : $0 + ", " + $1 }
                let country = result?.country ?? ""
                self.locations[row].address = location ?? ""
                cell.setLocation(self.locations[row].address)
            }
        }
        
    }
    
    
    func append(_ address: AddressInfo)  {
        
        let location = LocationData(address:address.address ?? "",address.area?.id, "\(address.area_num ?? 0)", area: address.area?.name,block: address.block, street: address.street, house: address.house, floor: address.floor, appartment: address.apartment, directions: address.extra, lat: "\(address.lat ?? 0.0)", lng: "\(address.lng ?? 0.0)", deleted: 0, city_name:
            address.area?.name)
        locations.append(location)
        
        view?.reload()
        
        
    }
    func remove(at row:Int)  {
        guard let id = locations[row].id else {
            locations.remove(at: row)
            return
        }
        locations.remove(at: row)
        removeLocation(with: id)
    }
    
    func didSelect(at row: Int,schedule: (date:String,time:String)) {
        let location = locations[row]
        selectedLocation = location
         info = AddressInfo(address: location.address, lat: Double(location.lat ?? "") ?? 0.0, lng: Double(location.lng ?? "") ?? 0.0, area: location.city!, area_num: Int(location.area_num ?? "") ?? 0, block: location.block ?? "", street: location.street ?? "", house: location.house ?? "", floor: location.floor ?? "", apartment: location.appartment ?? "", extra: location.directions ?? "", schedule: schedule)
        view?.didselect(info)
    }
    
}
