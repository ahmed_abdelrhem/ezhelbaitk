//
//  WebVC.swift
//  Ezhal baitk
//
//  Created by apple on 03/08/2021.
//  Copyright © 2021 a7med. All rights reserved.
//

import UIKit
import WebKit
class WebVC: UIViewController {
    private var parameters:[String:Any]?
    private var pay4 : Pay4?
    
    private var issues : [IssueData]?
    private var payment : Int?
    private var info : AddressInfo?
    private var note : (String?,[Data?]?)
    
    @IBOutlet weak var webV: WKWebView!{
        didSet{
            webV.uiDelegate = self
            webV.navigationDelegate = self
            
        }
    }
    
    private var url : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let url = url else { return  }
        if let url = URL(string: url) {
            let request = URLRequest(url: url)
            webV.load(request)
        }
        webV.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
        webV.addObserver(self, forKeyPath: #keyPath(WKWebView.title), options: .new, context: nil)
        
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            print(Float(webV.estimatedProgress))
            
            if Float(webV.estimatedProgress) == 1.0 {
                webV.evaluateJavaScript("document.documentElement.outerHTML.toString()",
                                        completionHandler: { (html: Any?, error: Error?) in
                                            print(html)
                                        })
            }
        }
        
        if keyPath == "title" {
            if let title = webV.title {
                print(title)
            }
        }
        
        for page in webV.backForwardList.backList {
            print("User visited \(page.url.absoluteString)")
        }
    }
    
    
    init(url:String,parameters:[String:Any]?,pay4 : Pay4?) {
        super.init(nibName: "WebVC", bundle: .main)
        self.url = url
        self.parameters = parameters
        self.pay4  = pay4
    }
    
    init(url:String,issues : [IssueData]?, payment: Int?, info : AddressInfo?,note : (String?,[Data?]?),pay4 : Pay4?) {
                
        super.init(nibName: "WebVC", bundle: .main)
        self.url = url
        self.issues = issues
        self.payment = payment
        self.info = info
        self.note = note

        self.pay4  = pay4
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
}


extension WebVC: WKNavigationDelegate,WKUIDelegate{
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        print("decidePolicyFor Action",navigationAction.request.url)
        decisionHandler(.allow)
        
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        
        decisionHandler(.allow)
        
    }
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        print("didReceiveServerRedirectForProvisionalNavigation",webView.url?.absoluteURL)
        guard let urlStr = webView.url?.absoluteString else { return  }
        guard let url = URLComponents(string: urlStr) else { return  }
        guard let queryItems = url.queryItems else { return  }
        for item in queryItems {
            print("item>>",item.value)
        }
        //        ezhel.my-staff.net
        if urlStr.contains("ezhel.my-staff.net"){
            let success = urlStr.contains("payment-success")
            let msg = success ? "payment success" : "payment fail"
            Messages.instance.showMessage(title: "", body: msg, state: .info, layout: .messageView)
            switch pay4 ?? .issues {
            
            case .issues:
                print(">>",info)
                print(">>",issues)
                print(">>",note)

                let vc = UIStoryboard(name: AppStoryboard.Business.rawValue, bundle: nil).instantiateViewController(withIdentifier: "ConfirmVC") as! ConfirmVC
                vc.info = info
                vc.issues = issues
                vc.note = note
                vc.payment = payment
                vc.isPaymentSuccess = success
                navigationController?.pushViewController(vc, animated: true)
                
            default:
                print("parameters>>",parameters)
                navigationController?.popToRootViewController(animated: true)

                _NC.post(name: .make_subscription, object: .none, userInfo: success ? parameters : .none)
            }
            
            
            

            
        }
        
    }
    
    
}
