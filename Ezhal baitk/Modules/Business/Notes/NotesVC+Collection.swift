//
//  NotesVC+Collection.swift
//  Ezhal baitk
//
//  Created by apple on 04/01/2022.
//  Copyright © 2022 a7med. All rights reserved.
//

import UIKit


extension NotesVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return assets.count + 1 // cell for add camera
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row  == assets.count{
            // ==> if last item in list configured add camera cell
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CameraCell", for: indexPath) as! CameraCell
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCell", for: indexPath) as! GalleryCell
            cell.delegate = self
            cell.setPhoto(with: assets[indexPath.row].image)
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = CGFloat(collectionView.width / 3.0)
        return CGSize(width: w , height: w)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row  == assets.count{
            self.cameraTapped()
        }
    }
    
    
}
