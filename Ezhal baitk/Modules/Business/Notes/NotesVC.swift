//
//  NotesVC.swift
//  Ezhal baitk
//
//  Created by a7med on 5/31/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
import BSImagePicker
import Photos
class NotesVC: UIViewController,UITextViewDelegate {
    var issues : [IssueData]?
    private  var note : (String?,[Data?]?)
    var payment : Int?
    fileprivate (set) var assets: [Asset] = [] {
        didSet{
            note.1 = assets.map({ asset in
                return asset.data
            })
        }
    }
    
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
            collectionView.register(UINib(nibName: "\(CameraCell.self)", bundle: Bundle.main), forCellWithReuseIdentifier: "\(CameraCell.self)")
            collectionView.register(UINib(nibName: "\(GalleryCell.self)", bundle: Bundle.main), forCellWithReuseIdentifier: "\(GalleryCell.self)")

            collectionView.dataSource = self
            collectionView.delegate = self
            collectionView.allowsMultipleSelection = false
        }
    }
    fileprivate var observer: NSKeyValueObservation!

    
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    @IBOutlet weak var txtV: UITextViewX!
    @IBOutlet weak var continueBtn: TransitionButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        txtV.delegate = self
        title = "Add note".localized
        let camera = UIBarButtonItem(barButtonSystemItem: .camera, target: self, action: #selector(cameraTapped))
        camera.tintColor = .darkGray
        navigationItem.rightBarButtonItem = camera
        observer = collectionView.observe(\.contentSize) { (collection, _) in
            self.collectionHeight.constant = self.collectionView.contentSize.height
        }
        
    }
    @objc func cameraTapped(){
        
        let imagePicker = ImagePickerController()
        imagePicker.settings.selection.max = 6
//        imagePicker.settings.selection.unselectOnReachingMax = true
        imagePicker.settings.theme.selectionStyle = .numbered
        imagePicker.settings.fetch.assets.supportedMediaTypes = [.image]
        imagePicker.albumButton.tintColor = UIColor.MainColor
        imagePicker.cancelButton.tintColor = UIColor.systemRed
        imagePicker.doneButton.tintColor = UIColor.MainColor
//
        self.presentImagePicker(imagePicker, select: { (asset) in
            print("Selected: \(asset)")
        }, deselect: { (asset) in
            print("Deselected: \(asset)")
        }, cancel: { (assets) in
            print("Canceled with selections: \(assets)")
        }, finish: { [weak self] (assets) in
            print("Finished with selections: \(assets)")
            // Request the maximum size. If you only need a smaller size make sure to request that instead.
            guard let self = self else{return}
            self.assets = assets
            
            self.collectionView.reloadData()
          
        }, completion: {
            
        })
        
    }
    
    @IBAction func ContinueBtnPressed(_ sender: TransitionButton) {
        if note.0 == nil && note.1 == nil{
            Messages.instance.showMessage(title: "", body: "your must write a note or select note image at least".localized, state: .error, layout: .messageView)
            return
        }
        let vc = UIStoryboard(name: AppStoryboard.Business.rawValue, bundle: nil).instantiateViewController(withIdentifier: "DateVC") as! DateVC
        vc.payment = payment
        vc.issues = issues
        vc.pay4 = .issues
        vc.note = note
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.textColor = .black
        textView.text = ""
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" || textView.text == "write notes..."{
            textView.textColor = .lightGray
            textView.text = "write notes..."
        }else{
            note.0 = textView.text
            
        }
    }
}



extension NotesVC : GalleryCellDelegate{
    
    func deleteBtnPressed(_ sender: UICollectionViewCell) {
        Messages.instance.actionsConfigMessage(title: "", body: _DeleteItem, buttonTitle: "\(_Delete)") { [weak self](action) in
            guard let self = self else{return}
            guard let indexPath = self.collectionView.indexPath(for: sender) else{return}
            self.assets.remove(at: indexPath.row)
            self.collectionView.deleteItems(at: [indexPath])
        }
        
    }
    
    
}
