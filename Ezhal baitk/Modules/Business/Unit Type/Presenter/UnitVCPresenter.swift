//
//  UnitVCPresenter.swift
//  Ezhal baitk
//
//  Created by a7med on 5/19/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
class UnitsVCPresenter {
    private let interactor = UnitsVCInteractor()
    private (set) var list = [Categories]()
    private (set) var issues = [IssueData]()

    weak var view: (LoaderJwtDelegate)?
    init(view:LoaderJwtDelegate) {
        self.view = view
    }
    init() {
        
    }
    
    func viewdidload(_ cat_id:Int)  {
        view?.loader_start()
        interactor.getUnits(cat_id: cat_id,didDataReady: { [weak self](model) in
            guard self != nil else { return }
            if model.status == API_status.Success.rawValue{
                self?.list = model.data?.units ?? []
                if self!.list.count == 0{
                    self?.view?.onEmpty()
                }else{
                    self!.view?.onSuccess("")
                }
            }else if model.status == API_status.JwtExpired.rawValue{
                self!.view?.jwtExpired(model.message ?? "")
            }else{
                
                self?.view?.onFailure(model.message ?? "")
            }
            self?.view?.loader_stop()
            
        }) { [weak self](error) in
            
            guard self != nil else { return }
            self?.view?.loader_stop()
            self?.view?.onConnection()
            self?.view?.onFailure(error?.errorDescription)
        }
    }
    
    func getUnits(_ cat_id:Int)  {
           interactor.getUnits(cat_id: cat_id,didDataReady: { [weak self](model) in
               guard self != nil else { return }
                   self?.list = model.data?.units ?? []
              
           }) { [weak self](error) in
               
               guard self != nil else { return }
           }
       }
    
    func getIssues(_ unit_id:Int)  {
        interactor.getIssues(unit_id: unit_id,didDataReady: { [weak self](model) in
            guard self != nil else { return }
            self?.issues = model.data ?? []
            
        }) { [weak self](error) in
            
            guard self != nil else { return }
  
        }
    }
    func getlistCount() ->Int {
        return list.count
    }
    
    func configureCell(cell:UnitCellView,at row: Int)  {
        let item = list[row]
        cell.setPhoto(with: item.image ?? "")
        cell.setName(name: item.name ?? "")
    }
    
    func didSelect(_ row:Int) -> (id:Int,name:String,image:String) {
           return (list[row].id ?? 0,list[row].name ?? "",list[row].image ?? "")
       }
    
    
}
