//
//  UnitsVC+Protocal.swift
//  Ezhal baitk
//
//  Created by a7med on 5/19/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
extension UnitsVC:LoaderJwtDelegate {
    func jwtExpired<T>(_ model: T) {
         alert_login(_Login)
    }
    func onConnection() {
        collectionView.setConnectionView()
        collectionView.reloadData()
    }
    
    func onEmpty() {
        collectionView.setEmptyView()
        collectionView.reloadData()
    }
    
    func onFailure<T>(_ msg: T) {
        Messages.instance.showMessage(title: "", body: msg as! String, state: .error, layout: .messageView)
        
    }
    
    func onSuccess<T>(_ msg: T) {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundView = nil
        collectionView.reloadData()
    }
    
    
    func loader_start() {
        startAnimating(ponit: nil)
    }
    
    func loader_stop() {
        stopAnimating()
    }
    
}
