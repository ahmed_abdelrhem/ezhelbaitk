
//
//  UnitsVC+Collection.swift
//  Ezhal baitk
//
//  Created by a7med on 5/19/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

extension UnitsVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.getlistCount()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UnitCell", for: indexPath) as! UnitCell
        presenter.configureCell(cell: cell, at: indexPath.row)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let unit = presenter.didSelect(indexPath.row)
        let vc = UIStoryboard(name: AppStoryboard.Business.rawValue, bundle: .main).instantiateViewController(withIdentifier: "IssuesVC") as! IssuesVC
        vc.unit = unit
        vc.cat_id = cat_id
        vc.summeryDelegate = summeryDelegate
        vc.summeryDelegate_tag = 0
        navigationController?.pushViewController(vc, animated: true)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionFlowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        collectionFlowLayout.minimumInteritemSpacing = 0
        collectionFlowLayout.minimumLineSpacing = 0
        let width = collectionView.width / 2
        let height = collectionView.height / 3
        return CGSize(width: width, height: height)
    }
}
