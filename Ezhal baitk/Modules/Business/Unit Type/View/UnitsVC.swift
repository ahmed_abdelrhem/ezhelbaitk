//
//  UnitsVC.swift
//  Ezhal baitk
//
//  Created by a7med on 5/19/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

class UnitsVC:BaseViewController{
    weak var summeryDelegate : UnitsVCDelegate?
    var cat_id : Int?
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
              collectionView.register(UINib(nibName: "UnitCell", bundle: Bundle.main), forCellWithReuseIdentifier: "UnitCell")
              collectionView.allowsMultipleSelection = false
        }
    }
    internal var presenter: UnitsVCPresenter!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        title = "Unit Type".localized
        presenter = UnitsVCPresenter(view: self)
        guard cat_id != nil else {
            fatalError("Erroor: Category id = Null")
        }
        presenter.viewdidload(cat_id!)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
