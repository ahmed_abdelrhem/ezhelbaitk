//
//  UnitVCInteractor.swift
//  Ezhal baitk
//
//  Created by a7med on 5/19/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
class UnitsVCInteractor{
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias model = (Category_Base)->()
    typealias issue_model = (Issue_Base)->()

    func getIssues(unit_id:Int,didDataReady: @escaping issue_model, andErrorCompletion errorCompletion: @escaping errorType) {
         
          apiManager.contectToApiWith(url: MyURLs.issues.url + "\(unit_id)",
                                      methodType: .get,
                                      params: nil,
                                      success: { (json) in
                                          print(json)
                                          if let data = try? JSONSerialization.data(withJSONObject: json) {
                                              
                                              do {
                                                  let result = try self.decoder.decode(Issue_Base.self, from: data)
                                                  didDataReady(result)

                                              }catch{
                                                  print("model parse error\(error)")

                                              }
                                            
                                          }
          }) { (error) in
             print(error)
              errorCompletion(error)
          }
      }
    
    func getUnits(cat_id:Int,didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorType) {
       
        apiManager.contectToApiWith(url: MyURLs.units.url + "\(cat_id)",
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
                                        print(json)
                                        if let data = try? JSONSerialization.data(withJSONObject: json) {
                                            
                                            do {
                                                let result = try self.decoder.decode(Category_Base.self, from: data)
                                                didDataReady(result)

                                            }catch{
                                                print("model parse error\(error)")

                                            }
                                          
                                        }
        }) { (error) in
           print(error)
            errorCompletion(error)
        }
    }

}
