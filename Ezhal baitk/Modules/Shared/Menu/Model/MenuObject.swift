//
//  MenuObject.swift
//  Wuzfone
//
//  Created by apple on 10/18/18.
//  Copyright © 2018 grand. All rights reserved.
//

import UIKit

class MenuObject{
    var id : Int?
    var image : UIImage?
    var titleEn :String?
    var show : Bool?
    
    init(id : Int?,image: UIImage?,titleEn: String?,show:Bool?) {
        self.id = id
        self.image = image
        self.titleEn = titleEn
        self.show = show
    }
}
