//
//  MenuVC+ProtocalsAndDelegates.swift
//  Aman
//
//  Created by iMac on 7/1/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
extension MenuVC: MenuVCView{
    func loader_start() {
        print("")
    }
    
    func loader_stop() {
        print("")
        
    }
    
    func onFailure<T>(_ msg: T) {
        print("")
        
    }
    
    func onEmpty() {
        print("")
        
    }
    
    func onConnection() {
        print("")
        
    }
    
    
    
    
    
    func onSuccess<T>(_ msg: T) {
        tableView.backgroundView = nil
        
        tableView.reloadData()
        
    }
    
    
    
    func PresentVC<T>(_ model: T) {
        let id  = model as! Int
        switch id  {
        case 0 :
            
            self.PresentHomeVC()

        case 1:
            print("1")
            if DEF.isLogin{
                pushFrontViewController("JobVC")
            }else{
                alert_login("Please,You should Login first .".localized)
            }
        case 2:
            print("2")
            if DEF.isLogin{
                pushFrontViewController("SubscriptionVC")
            }else{
                alert_login("Please,You should Login first .".localized)
            }
        case 3:
            print("3")
            if DEF.isLogin{
                let vc = PackageCategoryVC()
                vc.is_menu = true
                let nav = UINavigationController(rootViewController: vc)
                let Rvc : SWRevealViewController = self.revealViewController() as SWRevealViewController
                Rvc.pushFrontViewController(nav, animated: true)
                
            }else{
                alert_login("Please,You should Login first .".localized)
            }
            
            
        case 4 :
            if DEF.isLogin{
                pushFrontViewController("HistoryVC")
            }else{
                alert_login("Please,You should Login first .".localized)
            }
        case 5:
            if DEF.isLogin {
            let vc = MyLocationsVC()
            vc.is_menu = true
            let nav = UINavigationController(rootViewController: vc)
            let Rvc : SWRevealViewController = self.revealViewController() as SWRevealViewController
            Rvc.pushFrontViewController(nav, animated: true)
            }else{
                alert_login("Please,You should Login first .".localized)

            }
        case 6:
            pushFrontViewController("ConditionsVC")

        case 7 :
            pushFrontViewController("ContactVC")
  
        case 8:
            guard let appStoreLink = URL(string: "https://apps.apple.com/eg/app/ezhalbaitk-user/id1462066579") else{return}
            UIApplication.shared.open( appStoreLink, options: [:], completionHandler: nil)
            
            
        //
        case 9:
            let vc = UIStoryboard(name: AppStoryboard.Main.rawValue, bundle: nil).instantiateViewController(withIdentifier: "LanguageVC") as! LanguageVC
            vc.WithMenu = true
            let nav = UINavigationController(rootViewController: vc)
            let Rvc : SWRevealViewController = self.revealViewController() as SWRevealViewController
            Rvc.pushFrontViewController(nav, animated: true)
            
        case 10 :
            Messages.instance.actionsConfigMessage(title: "", body: _Logout, buttonTitle: _Ok) { [weak self](action) in
                self?.presenter.updateFB()
                let storyboard = UIStoryboard(name: AppStoryboard.Main.rawValue, bundle: nil)
                guard let vc = storyboard.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC else{return}
                let nav = UINavigationController(rootViewController: vc)
                nav.modalPresentationStyle = .overFullScreen
                self?.present(nav, animated: true, completion: nil)
            }
            
            
            
        default:
            print("==> Default")
        }
        
    }
    
    
    
    
}
