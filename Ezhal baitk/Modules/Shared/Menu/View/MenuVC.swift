//
//  MenuVC.swift
//  Aman
//
//  Created by iMac on 6/27/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class MenuVC:BaseViewController {
    internal weak var delegate: TermsDelegate?
    @IBOutlet weak var tableView: UITableView!
    var presenter : MenuVCPresenter!
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .default
//    }
//View controller-based status bar appearance -> YES
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
        presenter = MenuVCPresenter(view: self)

        presenter?.viewDidLoad()
        setupTableView()

//        print("#jwt\(def.string(forKey: _Jwt) )")
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        table.allowsSelection = true
        setNeedsStatusBarAppearanceUpdate()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()


    }
    
    
    @IBAction func bygrandBtnPressed(_ sender: UIButton) {
        let vc = UIStoryboard(name: AppStoryboard.Shared.rawValue, bundle: nil).instantiateViewController(withIdentifier: "ByGrandVC") as! ByGrandVC
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        
        present(vc, animated: true, completion: nil)
    }
    

}
