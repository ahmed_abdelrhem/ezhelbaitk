//
//  MenuVC+Table.swift
//  Aman
//
//  Created by iMac on 7/1/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
extension MenuVC: UITableViewDelegate ,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let animation = AnimationFactory.makeFade(duration: 0.25, delayFactor: 0.025)
        let animator = Animator(animationCell: animation)
        animator.animate(cell: cell, at: indexPath, in: tableView)
    }
    func setupTableView() {
        tableView.register(UINib(nibName: "MenuCell", bundle: nil), forCellReuseIdentifier: "MenuCell")
        tableView.register(UINib(nibName: "MenuHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "MenuHeader")
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = 40
        tableView.separatorStyle = .none
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 160
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "MenuHeader") as! MenuHeader
        presenter.configureHeader(header: header, delegate: self as MenuHeaderDelegate)
        return header
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuCell
        presenter.configure(cell: cell, for: indexPath.row )
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRow(index: indexPath.row)
    }
    
}


extension MenuVC: MenuHeaderDelegate{
    
    func pushFrontViewController(_ storyboardIdentifier: String){
        let key = "storyboardIdentifier"
        let svc:SWRevealViewController = self.revealViewController() as SWRevealViewController
        if  let frontViewController =  svc.frontViewController as? UINavigationController, let id = frontViewController.viewControllers.first?.value(forKey: key) as? String, id == storyboardIdentifier {
            svc.pushFrontViewController(frontViewController, animated: true)
        }else{
            let storyboard = UIStoryboard(name: AppStoryboard.Shared.rawValue, bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: storyboardIdentifier)
            let nav = UINavigationController(rootViewController: vc)
            svc.pushFrontViewController(nav, animated: true)
        }
        
    }
    
    
    func presentProfile() {
        if DEF.isLogin {
            
            pushFrontViewController("ProfileVC")
        }else{
            alert_login("Please ,You should Login First.")
        }
        
    }
    
    
    
}


