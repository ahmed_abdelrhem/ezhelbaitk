//
//  MainInteractor.swift
//  Aman
//
//  Created by iMac on 7/1/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit
class MenuInteractor {
    private let apiManager = APIManager()
       private let decoder = JSONDecoder()
       
       typealias model = (User_Base)->()
    
    func update( didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorType) {
        
        let param = [
            "firebase_token": ""
            ] as [String : Any]
        print("#body ==>",param)
               print("#url ==>",MyURLs.update_profile.url)
        
        apiManager.contectToApiWith(url: MyURLs.update_profile.url,
                                    methodType: .post,
                                    params: param,
                                    success: { (json) in
                                        
                                        if let data = try? JSONSerialization.data(withJSONObject: json) {
                                            do {
                                                let result = try self.decoder.decode(User_Base.self, from: data)
                                                print("res",result)
                                                didDataReady(result)
                                            }catch{
                                                print("error\(error)")
                                            }
                                            
                                        }
        }) { (error) in
           print(error)
            errorCompletion(error)
        }
    }
    
    
    func getMenu() -> [MenuObject]  {
        let menuEn = [_Home.localized,_ActiveJobs.localized,_Subscription.localized,_SubscriptionStatus.localized,_History.localized,_Addresses.localized,_AboutUs.localized,_ContactUs.localized,_RateApp.localized,_Language.localized,_LogOut.localized]
   
        
        var menuobj: [MenuObject] = []
        for index in 0..<menuEn.count {
            menuobj.append(MenuObject(id: index, image: nil, titleEn: menuEn[index], show: true))
        }
        
       
     
        return menuobj
    }
    
    
}
