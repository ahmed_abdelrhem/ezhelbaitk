//
//  MenuPresenter.swift
//  Aman
//
//  Created by iMac on 7/1/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

protocol MenuVCView:LoaderDelegate {
    func PresentVC<T>(_ model:T)
}
class MenuVCPresenter {
    private weak var  view: MenuVCView?
    private let interactor = MenuInteractor()
    private var menuItems = [MenuObject]()
    init(view : MenuVCView) {
        self.view = view
    }
    func viewDidLoad() {
        getMenuItems()
    }
    func getMenuItems() {
        self.menuItems = interactor.getMenu()
        self.view?.onSuccess("")
    }
    func getCount() -> Int  {
        return menuItems.count
    }
    
    func configure(cell: MenuCellView, for index: Int) {
        let menuItem = menuItems[index]
        cell.setName(name: menuItem.titleEn ?? "")
        
        
        cell.setPhoto(with: menuItem.image)
    }
    func configureHeader(header: MenuHeaderView,delegate: MenuHeaderDelegate){
        header.setName(name: DEF.name)
        header.setPhoto(with: DEF.image)
        header.setDelegate(with: delegate)
    
    }
    
    func didSelectRow(index: Int) {
        let menuItem = menuItems[index]
                self.view?.PresentVC(menuItem.id ?? 0)
    }
    
    func updateFB() {
        DEF.logout()

              interactor.update(didDataReady: { [weak self](model) in
                   
                  if model.status == API_status.Success.rawValue{
                    
                  }else if model.status == API_status.JwtExpired.rawValue {
                      // must go to login
                      
                  }else{
                      self?.view?.onFailure(model.message  ?? "")
                  }
                  
              }) { [weak self](error) in
                   
                  self?.view?.onConnection()
            self?.view?.onFailure(error?.errorDescription)
              }
              
          }
    
}
