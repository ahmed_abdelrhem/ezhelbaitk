//
//  JobVC+Protocal.swift
//  Ezhal baitk
//
//  Created by a7med on 6/3/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

extension JobVC: LoaderJwtDelegate{
    func jwtExpired<T>(_ model: T) {
         alert_login(_Login)
    }
    func onConnection() {
        tableView.setConnectionView()
    }
    
    func onEmpty() {
        tableView.setEmptyView()
    }
    
    func onFailure<T>(_ msg: T) {
        Messages.instance.showMessage(title: "", body: msg as! String, state: .error, layout: .messageView)
        
    }
    
    func onSuccess<T>(_ msg: T) {
        tableView.backgroundView = nil
        tableView.reloadData()
    }
    
    
    func loader_start() {
        startAnimating(ponit: nil)
    }
    
    func loader_stop() {
        stopAnimating()
    }
    
    
    
    
}
