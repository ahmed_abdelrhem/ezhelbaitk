//
//  JobVC.swift
//  Ezhal baitk
//
//  Created by a7med on 6/3/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

class JobVC: BaseViewController {
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.tableFooterView = UIView(frame: .zero)
                   tableView.dataSource = self
                   tableView.delegate = self
                   tableView.separatorStyle = .none
                   tableView.rowHeight = UITableView.automaticDimension
                   tableView.register(UINib(nibName: "JobCell", bundle: nil), forCellReuseIdentifier: "JobCell")
        }
    }
    internal var presenter : JobVCPresenter!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter = JobVCPresenter(view: self)
        presenter.viewdidload()
        sideMenu()
        title = "Active Jobs".localized

    }
    

}

