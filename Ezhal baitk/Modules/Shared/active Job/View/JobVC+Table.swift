//
//  JobVC+Table.swift
//  Ezhal baitk
//
//  Created by a7med on 6/3/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

extension JobVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getlistCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "JobCell", for: indexPath)  as! JobCell
        presenter.configureCell(cell: cell, at: indexPath.row)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: AppStoryboard.Shared.rawValue, bundle: nil).instantiateViewController(withIdentifier: "StatusVC") as! StatusVC
        vc.order = presenter.diselect(indexPath.row)
        navigationController?.pushViewController(vc, animated: true)
    }
  
}
