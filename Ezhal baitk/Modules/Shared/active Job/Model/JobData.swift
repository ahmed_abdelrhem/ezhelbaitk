//
//  JobData.swift
//  Ezhal baitk
//
//  Created by a7med on 6/3/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
struct JobData : Codable {
    let area : String?
    let technician : String?
    let technician_image : String?
    
    let category : String?
    let subcategory : String?
    let currrency : String?
    let total : Int?
    let icon : String?
    
    
    
    let status : Int?
    
    let id : Int?
    let order_id : Int?
    
    
    
    
    enum CodingKeys: String, CodingKey {
        
        case area = "area"
        case technician = "technician"
        case technician_image = "technician_image"
        
        case category = "category"
        case subcategory = "subcategory"
        case currrency = "currrency"
        case total = "total"
        case icon = "icon"
        
        
        case status = "status"
        
        case id = "id"
        case order_id = "order_id"
        
        
        
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        area = try values.decodeIfPresent(String.self, forKey: .area)
        technician = try values.decodeIfPresent(String.self, forKey: .technician)
        technician_image = try values.decodeIfPresent(String.self, forKey: .technician_image)
        
        category = try values.decodeIfPresent(String.self, forKey: .category)
        subcategory = try values.decodeIfPresent(String.self, forKey: .subcategory)
        currrency = try values.decodeIfPresent(String.self, forKey: .currrency)
        total = try values.decodeIfPresent(Int.self, forKey: .total)
        
        status = try values.decodeIfPresent(Int.self, forKey: .status)
        icon = try values.decodeIfPresent(String.self, forKey: .icon)
        
        
        
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        order_id = try values.decodeIfPresent(Int.self, forKey: .order_id)
        
        
        
    }
    
}
