//
//  JobVCPresenter.swift
//  Ezhal baitk
//
//  Created by a7med on 6/3/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation

class JobVCPresenter {
    private let interactor = JobVCInteractor()
    private var list = [JobData]()
    weak var view: (LoaderJwtDelegate)?
    init(view:LoaderJwtDelegate) {
        self.view = view
    }
    private var status = ["submitted".localized,"assigned".localized,"on the road".localized," on Site".localized," in progress".localized,"completed".localized,"refused".localized]
    func viewdidload()  {
        view?.loader_start()
        interactor.getJobs(didDataReady: { [weak self](model) in
            guard self != nil else { return }
            if model.status == API_status.Success.rawValue{
                self?.list = model.data ?? []
                if self!.list.count == 0{
                    self?.view?.onEmpty()
                }else{
                    self!.view?.onSuccess("")
                }
            }else if model.status == API_status.JwtExpired.rawValue{
                self!.view?.jwtExpired(model.message ?? "")
            }else{
                
                self?.view?.onFailure(model.message ?? "")
            }
            self?.view?.loader_stop()
            
        }) { [weak self](error) in
            
            guard self != nil else { return }
            self?.view?.loader_stop()
            self?.view?.onConnection()
            self?.view?.onFailure(error?.errorDescription)
        }
    }
    func getlistCount() ->Int {
        return list.count
    }
    
    func configureCell(cell:JobCellView,at row: Int)  {
        cell.setPhoto(list[row].icon ?? "")
        cell.setArea(list[row].area ?? "")
        cell.setTech(list[row].technician ?? "")
        cell.setTotal("\(list[row].total ?? 0)")
        let i  = list[row].status ?? 0
        cell.setStatus("\(status[i])")
        cell.setCategory(list[row].category ?? "")
        
        
    }
    func diselect(_ row:Int) -> (status:Int,order_id: Int,name: String,cat: String,img:String) {
        return (list[row].status ?? 0,list[row].order_id ?? 0,list[row].technician ?? "",list[row].category ?? "",list[row].technician_image ?? "")
    }
    
    
    
    
}
