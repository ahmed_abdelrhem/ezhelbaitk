//
//  ProfileVCPresenter.swift
//  Moon Shop
//
//  Created by apple on 2/13/20.
//  Copyright © 2020 2grand. All rights reserved.
//

import Foundation

class ProfileVCPresenter {
    private let interactor = ProfileVCInteractor()

    
    weak var view: (ButtonDelegate&PresentDelegate&JwtDelegate)?
    init(view:(ButtonDelegate&PresentDelegate&JwtDelegate)) {
        self.view = view
    }
    
   
    
    
    func update(with image:(Data,String),name: String,email: String, phone: String,location: (String,String),address: String) {
        view?.button_start()
        interactor.update(with:image,name: name,email: email, phone: phone,location: location,address: address, didDataReady: { [weak self](model) in
             
            if model.status == API_status.Success.rawValue{
                DEF.saveUserDefault(model.data!)
                self!.view?.PresentVC(model.message  ?? "")

                
            }else if model.status == API_status.JwtExpired.rawValue {
                // must go to login
                self!.view?.jwtExpired(model.message  ?? "")
                
            }else{
                self?.view?.onFailure(model.message  ?? "")
            }
            self?.view?.button_stop()
            
        }) { [weak self](error) in
             
            self?.view?.button_stop()
            self?.view?.onConnection()
            self?.view?.onFailure(error?.errorDescription)
        }
        
    }
    
   
    func update(name: String,email: String, phone: String,location: (String,String),address: String) {
        view?.button_start()
        interactor.update(name: name,email: email, phone: phone,location: location,address: address, didDataReady: { [weak self](model) in
             
            if model.status == API_status.Success.rawValue{
                DEF.saveUserDefault(model.data!)

                self!.view?.PresentVC(model.message  ?? "")

                
            }else if model.status == API_status.JwtExpired.rawValue {
                // must go to login
                self!.view?.jwtExpired(model.message  ?? "")
                
            }else{
                self?.view?.onFailure(model.message  ?? "")
            }
            self?.view?.button_stop()
            
        }) { [weak self](error) in
             
            self?.view?.button_stop()
            self?.view?.onConnection()
            self?.view?.onFailure(error?.errorDescription)
        }
        
    }
    
}
