//
//  ProfileVC.swift
//  Moon Shop
//
//  Created by apple on 2/12/20.
//  Copyright © 2020 2grand. All rights reserved.
//

import UIKit

class ProfileVC: BaseViewController {
    
    
    internal var upload_data: Data?
    internal var data_ext: String = "png"
    internal var email = DEF.email
    internal var location = (DEF.lat,DEF.lng)
    internal var address = DEF.address
    internal var phone = DEF.phone
    internal var name = DEF.name
    internal var presenter : ProfileVCPresenter!
    @IBOutlet weak var saveBtn: TransitionButton!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setTable()
        sideMenu()
        presenter = ProfileVCPresenter(view: self)
        
        
    }
    
   
    
    
    @IBAction func saveBtnPressed(_ sender: TransitionButton) {
        if upload_data != nil{
            presenter.update(with: (upload_data!,data_ext), name: name, email: email, phone: phone, location: location, address: address)
        }else{
            presenter.update(name: name, email: email, phone: phone, location: location, address: address)

        }
    }
    
}
