//
//  ProfileVC+Table.swift
//  Moon Shop
//
//  Created by apple on 2/13/20.
//  Copyright © 2020 2grand. All rights reserved.
//

import Foundation

extension ProfileVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath)  as! TextFieldCell
            cell.delegate = self
            cell.settxt(txt: name, tag: indexPath.row,placeholder: _Name)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath)  as! TextFieldCell
            cell.delegate = self
            
            cell.settxt(txt: email, tag: indexPath.row,placeholder: _Email)
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath)  as! TextFieldCell
            cell.delegate = self
            cell.settxt(txt: phone, tag: indexPath.row,placeholder: _Phone)
            
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldCell", for: indexPath)  as! TextFieldCell
            cell.delegate = self
            cell.settxt(txt: address, tag: indexPath.row,placeholder: _Address)
            return cell
        default:
            return UITableViewCell()
        }
        
    }
  
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ProfileHeader") as! ProfileHeader
        header.setPhoto(with: DEF.image)
        header.setName(name)
        header.delegate = self
        return header
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 240
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func setTable()  {
        
        tableView.register(UINib(nibName: "ProfileHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "ProfileHeader")
        tableView.register(UINib(nibName: "TextFieldCell", bundle: nil), forCellReuseIdentifier: "TextFieldCell")
        tableView.register(UINib(nibName: "MenuCell", bundle: nil), forCellReuseIdentifier: "MenuCell")
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.dataSource = self
        tableView.delegate = self
        
        
        
        
    }
    
}

extension ProfileVC:LocationDelegate{
    
    func RetriveLocation(lat: Double, lng: Double, add: String,country: String){
        print("#MyAddress",add)
        self.location = ("\(lat)","\(lng)")
        self.address = add
        let indx = IndexPath(row: 3, section: 0)
        tableView.reloadRows(at: [indx], with: .none)
    }
    
    
}

extension ProfileVC: UITextFieldDelegate,ProfileHeaderDelegate{
    
    
    func photoTapped(_ sender: UIImageViewX) {
        AttachmentHandler.shared.showAttachmentActionSheetGalleryWithoutVedio(self, sender)
        AttachmentHandler.shared.singleImagePickedBlock = { [weak self] (url,image,data) in
            sender.image = image
            self!.upload_data = data as Data
            self!.data_ext = "jpeg"
            if url != nil{
                self!.data_ext = (url! as URL).pathExtension
                print("ex:\( (url! as URL).pathExtension)")
            }
            
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("#txt \(textField.tag)",textField.text ?? "")
        switch textField.tag {
        case 0:
            name = textField.text ?? "\(DEF.name)"
        case 1:
            email = textField.text ?? "\(DEF.email)"
        case 2:
            phone = textField.text ?? "\(DEF.phone)"
            print("ph==>",phone)
        default:
            print("end editing switch default case")
            
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
          if textField.tag == 3 {
                  let vc = MapVC()
                  vc.delegate = self
                  navigationController?.pushViewController(vc, animated: true)
            textField.endEditing(true)
                  
              }
    }
    
}
