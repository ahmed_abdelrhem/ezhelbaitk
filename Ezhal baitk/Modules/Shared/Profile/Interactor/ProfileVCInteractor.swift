//
//  ProfileVCInteractor.swift
//  Moon Shop
//
//  Created by apple on 2/13/20.
//  Copyright © 2020 2grand. All rights reserved.
//

import Foundation
import Alamofire
class ProfileVCInteractor {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    
    typealias model = (User_Base)->()
    
    func update(name: String,email: String, phone: String,location: (String,String),address: String, didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorType) {
        
        let param = [
            "name": name,
            "email": email,
            "phone": phone,
            "lat": location.0,
            "lng": location.1,
            "address": address
            ] as [String : Any]
        print("#body ==>",param)
               print("#url ==>",MyURLs.update_profile.url)
        
        apiManager.contectToApiWith(url: MyURLs.update_profile.url,
                                    methodType: .post,
                                    params: param,
                                    success: { (json) in
                                        
                                        if let data = try? JSONSerialization.data(withJSONObject: json) {
                                            do {
                                                let result = try self.decoder.decode(User_Base.self, from: data)
                                                print("res",result)
                                                didDataReady(result)
                                            }catch{
                                                print("error\(error)")
                                            }
                                            
                                        }
        }) { (error) in
           print(error)
            errorCompletion(error)
        }
    }
    
    
    func update(with image:(Data,String),name: String,email: String, phone: String,location: (String,String),address: String,  didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorType) {
        let param = [
                   "name": name,
                   "email": email,
                   "phone": phone,
                   "lat": location.0,
                   "lng": location.1,
                   "address": address
                   ] as [String : Any]
               
        print("#body ==>",param)
        print("#url ==>",MyURLs.update_profile.url)
        apiManager.requestWith(upload: image.0, to: MyURLs.update_profile.url, withkey: "image", andExtension: ("image",image.1), forbody: param, success: { (json) in
            print("jsn",json)
            if let data = try? JSONSerialization.data(withJSONObject: json) {
                
                do {
                    let result = try self.decoder.decode(User_Base.self, from: data)
                    didDataReady(result)
                    
                }catch{
                     print("##error\(error)")
                        let error = AFError.responseSerializationFailed(reason: .decodingFailed(error: error))
                        errorCompletion(error)
                    
                }
                
            }
        }) { (error) in
           print(error)
            errorCompletion(error)
        }
    }
    
    
    
}
