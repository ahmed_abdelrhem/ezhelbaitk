//
//  SubStatusVC.swift
//  Ezhal baitk
//
//  Created by a7med on 6/6/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit


class SubStatusVC:BaseViewController{
    var cat_id: Int?
    lazy var nextvc: NextVC = {
        var vc = NextVC()
        vc.cat_id = cat_id
        self.addViewControllerAsChild(childeViewController: vc)
        return vc
    }()
    lazy var progressvc: ProgressVC = {
        var vc = ProgressVC()
        vc.cat_id = cat_id

        self.addViewControllerAsChild(childeViewController: vc)
        return vc
    }()
    lazy var completedvc: CompletedVC = {
        var vc = CompletedVC()
        vc.cat_id = cat_id

        self.addViewControllerAsChild(childeViewController: vc)
        return vc
    }()
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var containerView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        sideMenu()
        guard let cat_id = cat_id else { return  }
        if cat_id == 4 {
        segmentControl.removeSegment(at: 1, animated: true)
        }
        SharedHandler.shared.setupSegment(view: view, segmentControl: segmentControl, numberOfSegments: CGFloat(segmentControl!.numberOfSegments))
        setupViews()
        title = "Subscription Status".localized
        
        _NC.addObserver(self, selector: #selector(onNotoification(_:)), name: .pushFromHomeVC, object: nil)

//        _NC.addObserver(self, selector: #selector(OnVist), name: .on_Vist , object: nil)

    }
    deinit {
        _NC.removeObserver(self)

    }
    
    
  
    
    
    @objc private func onNotoification(_ notification: Notification)
    {
        if let data = notification.userInfo as? [String: Any]
        {
            if let id = data["data"] as? Int {
             let vc = JobDetailsVC()
                vc.order_id = id
                vc.is_vist = cat_id != nil
                navigationController?.pushViewController(vc, animated: true)
                
            }
        }
        return
        
    }

    


    
    
    @IBAction func SegmentValueChanged(_ sender: UISegmentedControl) {
        setupViews()
    }
    
    
}


extension SubStatusVC{
    internal func setupViews() {
        print("selected segment index >>",segmentControl.selectedSegmentIndex)
        guard let cat_id = cat_id else { return  }
        nextvc.view.isHidden = !(segmentControl.selectedSegmentIndex == 0 )
        if cat_id == 4 {
            progressvc.view.isHidden = true
            completedvc.view.isHidden = !(segmentControl.selectedSegmentIndex == 1)
        }else{
            progressvc.view.isHidden = !(segmentControl.selectedSegmentIndex == 1 )
            completedvc.view.isHidden = !(segmentControl.selectedSegmentIndex == 2 )
        }
      
        
    }
    internal func addViewControllerAsChild(childeViewController: UIViewController) {
        childeViewController.view.frame = containerView.bounds
        childeViewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        containerView.addSubview(childeViewController.view)
    }
}
