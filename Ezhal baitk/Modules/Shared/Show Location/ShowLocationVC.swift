//
//  ShowLocationVC.swift
//  Tabadul
//
//  Created by Islamic on 3/5/19.
//  Copyright © 2019 2grand. All rights reserved.
//

import UIKit
import GoogleMaps

class ShowLocationVC:BaseViewController{

    //MARK:- Outelts
    @IBOutlet weak var myMapView: UIView!
    
    //MARK: Variables
    var mapView: GMSMapView!
    var lat: Double?
    var lng: Double?

    //MARK:- Storyboard
    override func viewDidLoad() {
        super.viewDidLoad()
        locationAuth()
    }

    
    //MARK:- Actions
    @IBAction func okBtnPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    


}


//MARK:- Maps Helper Funcs
extension ShowLocationVC: CLLocationManagerDelegate, GMSMapViewDelegate {
    
    func locationAuth(){
        mapView = GMSMapView()
        self.MapSetup(lat:"\(self.lat!)", long: "\(self.lng!)")
    }
    
    func MapSetup(lat: String , long: String) {
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(lat)!, longitude: CLLocationDegrees(long)!, zoom: 16)
        let f: CGRect = view.frame
        let mapFrame = CGRect(x: f.origin.x, y: 0, width: f.size.width, height: f.size.height)
        mapView = GMSMapView.map(withFrame: mapFrame, camera: camera)
        mapView.settings.compassButton = true
        mapView.delegate = self
        self.myMapView.addSubview(mapView)
        MapMarkerSetup()
    }
    
    func MapMarkerSetup() {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: self.lat!, longitude: self.lng!)
        marker.icon =  GMSMarker.markerImage(with: HexStringToUIColor.hexStringToUIColor(hex: "#FF7314"))
        marker.map = mapView
        mapView.selectedMarker = marker
    }
    
    
}
