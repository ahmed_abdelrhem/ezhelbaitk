//
//  statusVC+Protocals.swift
//  Ezhal baitk
//
//  Created by a7med on 6/8/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
extension StatusVC: LoaderJwtDelegate{
    func jwtExpired<T>(_ model: T) {
         alert_login(_Login)
    }
    func onConnection() {
        Messages.instance.showMessage(title: "", body: _ConnectionError, state: .error, layout: .messageView)
    }
    
    func onEmpty() {
        Messages.instance.showMessage(title: "", body: _EmptyData, state: .error, layout: .messageView)

    }
    
    func onFailure<T>(_ msg: T) {
        Messages.instance.showMessage(title: "", body: msg as! String, state: .error, layout: .messageView)
        
    }
    
    func onSuccess<T>(_ msg: T) {
          PresentHomeVC()

    }
    
    
    func loader_start() {
        startAnimating(ponit: nil)
    }
    
    func loader_stop() {
        stopAnimating()
    }
    
    
    
    
}
