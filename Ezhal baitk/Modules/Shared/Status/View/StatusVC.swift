//
//  StatusVC.swift
//  Ezhal baitk
//
//  Created by a7med on 6/3/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

class StatusVC:UIViewController{
    internal var presenter : StatusVCPresenter!
    var WithMenu = false

    var order:(status:Int,order_id: Int,name: String,cat: String,img:String)?
    private var list = [(title:"Job has been submitted".localized,details:"we are now matching you with a technician.".localized),(title:"Technician assigned".localized,details:"your request has been accepted.".localized),(title:"Technician on the road".localized,details:"Technician has been dispatched.".localized),(title:"Technician on Site".localized,details:"Technician has arrived to location".localized),(title:"Job in progress".localized,details:"Technician is working on your job".localized),(title:"Job completed".localized,details:"Complete the job by rating your service provider".localized),(title:"Job Refused".localized,details:"Technician refused job".localized)]
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.dataSource = self
            tableView.delegate = self
            tableView.separatorStyle = .singleLine
            tableView.rowHeight = 130

            let footer = (Bundle.main.loadNibNamed("StatusFooter", owner: self, options: nil)![0] as? StatusFooter)
            footer?.delegate = self
            if order?.status  ?? 0 >= 1 &&  order?.status  ?? 0 < 5  {
                footer!.hideBtns()
                footer!.setBtnTitle("View Job".localized, 0)
            }else if (order?.status  ?? 0) == 0 {
                footer!.setBtnTitle("View Job".localized, 0)
            }else if (order?.status  ?? 0 ) == 6 {
                footer!.setBtnTitle("View Job".localized, 0)
                footer!.hideBtns()
            }else{
                footer!.hideBtns()
                footer!.setBtnTitle("Rate Technician".localized, 1)
            }
            tableView.tableFooterView = footer
            tableView.tableFooterView?.height = 500


        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        _NC.addObserver(self, selector: #selector(onOrder), name: .on_Order , object: nil)

        presenter = StatusVCPresenter(view: self)
        if WithMenu {
            sideMenu()
        }
        
    }
    
    
      override func viewDidDisappear(_ animated: Bool) {
            super.viewDidDisappear(animated)
          print(">> BaseViewController >>> viewDidDisappear")
            _NC.removeObserver(self)

        }
    
      deinit {
          print(" >> BaseViewController >>> deinit")

          _NC.removeObserver(self)
      }
    
      
      @objc private func onOrder(_ notification: Notification)
      {
          print("#on_order")

          if let data = notification.userInfo as? [String: Any]
          {
           let  fb = (data["fb"] as? Firebase_Base)
              guard let order_idStr = fb?.order_id else {
                  return
              }
              guard let order_id = Int(order_idStr) else {
                  return
              }
              guard let statusStr = fb?.status else {
                  return
              }
              guard let status = Int(statusStr) else {
                  return
              }
              guard let tec = fb?.tech_name else {
                  return
              }
              guard let cat  = fb?.cat_name else {
                             return
                         }
              guard let img  = fb?.tech_image else {
                  return
              }
            order = (status:status,order_id: order_id,tec,cat,img)
            tableView.reloadData()
              
          }
          return
      }

}
extension StatusVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StatusCell") as! StatusCell
        switch indexPath.row  {
        case 0:
            if order?.status ?? 0 >= 0 &&  order?.status ?? 0 <= 5{
                cell.selectedStatus()
            }
            if order?.status ?? 0 > 0 &&  order?.status ?? 0 <= 5{
                cell.setBottomView()
            }
            
            cell.hideTopView()
        case 1:
            if order?.status ?? 0 >= 1  &&  order?.status ?? 0 <= 5{
                cell.selectedStatus()
            }
            if order?.status ?? 0 > 1 &&  order?.status ?? 0 <= 5{
                cell.setBottomView()
            }
        case 2:
            if order?.status ?? 0 >= 2 &&  order?.status ?? 0 <= 5{
                cell.selectedStatus()
            }
            if order?.status ?? 0 > 2 &&  order?.status ?? 0 <= 5{
                cell.setBottomView()
            }
        case 3:
            if order?.status ?? 0 >= 3 &&  order?.status ?? 0 <= 5 {
                cell.selectedStatus()
            }
            if order?.status ?? 0 > 3 &&  order?.status ?? 0 <= 5{
                cell.setBottomView()
            }
        case 4:
            if order?.status ?? 0 >= 4 &&  order?.status ?? 0 <= 5{
                cell.selectedStatus()
            }
            if order?.status ?? 0 > 4 &&  order?.status ?? 0 <= 5{
                cell.setBottomView()
            }
        case 5:
            if (order?.status ?? 0) == 5{
                cell.selectedStatus()
            }
          
            cell.hideBottomView()
        case 6:
            if (order?.status ?? 0) == 6{
                cell.selectedStatus()
            }
            cell.hideTopView()
            cell.hideBottomView()
        default:
            break
        }
        
        cell.setTitle(list[indexPath.row].title)
        cell.setDetails(list[indexPath.row].details)
        return cell
    }
    
}

extension StatusVC: StatusFooterDelegate{
    func reschaduleBtnPressed() {
        let vc = UIStoryboard(name: AppStoryboard.Business.rawValue, bundle: nil).instantiateViewController(withIdentifier: "DateVC") as! DateVC
        vc.pay4 = .reschadule
        vc.order_id = order?.order_id
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func viewJobBtnPressed(_ sender: UIButton) {
        if sender.tag == 0 {
            // view job
            let vc = JobDetailsVC()
            vc.order_id = order?.order_id
            navigationController?.pushViewController(vc, animated: true)
        }else{
            guard order != nil else {
                return
            }
            let vc  = UIStoryboard(name: AppStoryboard.Business.rawValue, bundle: nil).instantiateViewController(withIdentifier: "RateVC") as! RateVC
            guard order?.order_id != nil else {
                return
            }
            vc.order_id = order?.order_id
            vc.name = order?.name
            vc.cat = order?.cat
            vc.img = order?.img
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func cancelBtnPressed() {
        presenter.cancel(order?.order_id ?? 0)
        
    }
    
    
}
