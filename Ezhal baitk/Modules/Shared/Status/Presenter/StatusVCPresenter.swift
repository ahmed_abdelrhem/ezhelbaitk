//
//  StatusVCPresenter.swift
//  Ezhal baitk
//
//  Created by a7med on 6/8/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation


class StatusVCPresenter {
    private let interactor = StatusVCInteractor()
    weak var view: (LoaderJwtDelegate)?
    init(view:LoaderJwtDelegate) {
        self.view = view
    }
    
    func cancel(_ id: Int)  {
        view?.loader_start()
        interactor.cancel(id:id, didDataReady: { [weak self](model) in
            guard self != nil else { return }
            if model.status == API_status.Success.rawValue{
                self?.view?.onSuccess(model.message  ?? "")

            }else{
                
                self?.view?.onFailure(model.message  ?? "")
                self?.view?.loader_stop()

            }
            
        }) { [weak self](error) in
            
            guard self != nil else { return }
            self?.view?.loader_stop()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }
    
    
}
