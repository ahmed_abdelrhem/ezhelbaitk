//
//  PackageCategoryVC.swift
//  Ezhal baitk
//
//  Created by apple on 02/08/2021.
//  Copyright © 2021 a7med. All rights reserved.
//

import UIKit

class PackageCategoryVC: UIViewController {
    private let cats : [(id:Int,name:String,image:UIImage)] = [(1,"Air Conditioning".localized,.air_conditions),(4,"Cleaning".localized,.cleaning)]
    var is_menu = false

    @IBOutlet weak var tableView: UITableView!{
        didSet{
            let nib = UINib(nibName: "\(PackageCategoryCell.self)", bundle: .none)
            tableView.register(nib, forCellReuseIdentifier: "\(PackageCategoryCell.self)")
            tableView.separatorStyle = .none
            tableView.tableFooterView = UIView(frame: .zero)
            tableView.dataSource = self
            tableView.delegate = self
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        

        // Do any additional setup after loading the view.
        if is_menu {
            sideMenu()
        }
    }

}


extension PackageCategoryVC: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "\(PackageCategoryCell.self)", for: indexPath) as? PackageCategoryCell else { return UITableViewCell() }
        cell.configure(cats[indexPath.row].name, cats[indexPath.row].image)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if is_menu{
            let vc  = UIStoryboard(name: AppStoryboard.Shared.rawValue, bundle: nil).instantiateViewController(withIdentifier: "SubStatusVC") as! SubStatusVC
            vc.cat_id = cats[indexPath.row].id
            navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc  = UIStoryboard(name: AppStoryboard.Shared.rawValue, bundle: nil).instantiateViewController(withIdentifier: "PackageVC") as! PackageVC
            vc.cat_id = cats[indexPath.row].id
            navigationController?.pushViewController(vc, animated: true)
        }
   
    }
    
    
}
