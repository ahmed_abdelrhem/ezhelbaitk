//
//  FullImageVC.swift
//  ByDeals
//
//  Created by Grand iMac on 1/7/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import SDWebImage


class FullImageVC:BaseViewController{

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var displayImg: UIImageView!
    
    var url: String?
    var image: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if image != nil {
        displayImg.image = image!
        }
        if url != nil{
        displayImg.sd_imageIndicator = SDWebImageActivityIndicator.gray
        let imageURL = URL(string: url ?? "")
        displayImg.sd_setImage(with: imageURL, placeholderImage: placeHolder_image)
        }
         setScrollView()
    }
    
    
    func setScrollView() {
        scrollView.delegate = self
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 6.0
    }
    
    
    @IBAction func closeBtnClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
}

    
extension FullImageVC: UIScrollViewDelegate {
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return displayImg
    }


}
