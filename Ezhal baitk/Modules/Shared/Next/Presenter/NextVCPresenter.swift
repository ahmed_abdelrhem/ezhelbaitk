//
//  NextVCPresenter.swift
//  Ezhal baitk
//
//  Created by a7med on 6/6/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
class NextVCPresenter {
    private let interactor = NextVCInteractor()
    private var list = [SubscriptionData]()
    weak var view: (LoaderJwtDelegate)?
    init(view:LoaderJwtDelegate) {
        self.view = view
    }
    
    func viewdidload(cat_id:Int)  {
        view?.loader_start()
        interactor.getSubs(cat_id:cat_id,didDataReady: { [weak self](model) in
            guard self != nil else { return }
            if model.status == API_status.Success.rawValue{
                self?.list = model.data ?? []
                if self!.list.count == 0{
                    self?.view?.onEmpty()
                }else{
                    self!.view?.onSuccess("")
                }
            }else if model.status == API_status.JwtExpired.rawValue{
                self!.view?.jwtExpired(model.message ?? "")
            }else{
                
                self?.view?.onFailure(model.message ?? "")
            }
            self?.view?.loader_stop()
            
        }) { [weak self](error) in
            
            guard self != nil else { return }
            self?.view?.loader_stop()
            self?.view?.onConnection()
            self?.view?.onFailure(error?.errorDescription)
        }
    }
    func getlistCount() ->Int {
        return list.count
    }
    
    func configureCell(cell:PackageCellView,at row: Int)  {
        let item = list[row]
        cell.setDate(item.date ?? "")
        cell.setTime(item.time ?? "")
        cell.setId("\(item.id ?? 0)")
        cell.setSubPk(item.months ?? "")
     
 
        
        
    }
 
    
    func didselect(at row: Int)  {
       let id = list[row].id ?? 0
        print("tech>>",list[row].tech )
        if let tech = list[row].tech {
            _NC.post(name: .pushFromHomeVC, object: nil,userInfo: ["data":id])
        }

      }
    
    
}
