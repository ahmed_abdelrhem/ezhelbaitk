//
//  NextVC.swift
//  Ezhal baitk
//
//  Created by a7med on 6/6/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

class NextVC:BaseViewController{
    var cat_id: Int?

    internal var presenter : NextVCPresenter!
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.tableFooterView = UIView(frame: .zero)
                   tableView.dataSource = self
                   tableView.delegate = self
                   tableView.separatorStyle = .none
                   tableView.rowHeight = UITableView.automaticDimension
                   tableView.register(UINib(nibName: "PackageCell", bundle: nil), forCellReuseIdentifier: "PackageCell")
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        guard let cat_id = cat_id else { return  }
        presenter = NextVCPresenter(view: self)
        presenter.viewdidload(cat_id:cat_id)
        title = "Subscription Status".localized

    }


    
}
