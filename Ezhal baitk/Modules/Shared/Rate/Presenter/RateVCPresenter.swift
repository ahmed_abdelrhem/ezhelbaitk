//
//  RateVCPresenter.swift
//  Ezhal baitk
//
//  Created by a7med on 7/25/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
class RateVCPresenter {
    private let interactor = RateVCInteractor()
    weak var view: (LoginVCView)?
    init(view:LoginVCView) {
        self.view = view
    }
    
    func rate(rate: Double,review: String,order_id:Int)  {
        view?.loader_start()
        interactor.rate(rate: rate,review: review,order_id:order_id, didDataReady: { [weak self](model) in
            guard self != nil else { return }
            if model.status == API_status.Success.rawValue{
                self?.view?.PresentVC(model.message  ?? "")

            }
//            else if model.status == API_status.NotActive.rawValue{
//                self!.interactor.sendCode(key: key, didDataReady: {  [weak self](model) in
//                                      self?.view?.activatYourAcount(key)
//                              }) { [weak self](error) in
//                              guard self != nil else { return }
//                              self?.view?.loader_stop()
//                              }
//            }
            else{
                
                self?.view?.onFailure(model.message  ?? "")
                self?.view?.loader_stop()

            }
            
        }) { [weak self](error) in
            
            guard self != nil else { return }
            self?.view?.loader_stop()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }
    
}
