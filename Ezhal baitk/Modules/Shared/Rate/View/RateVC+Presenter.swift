//
//  RateVC+Presenter.swift
//  Ezhal baitk
//
//  Created by a7med on 7/25/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
extension RateVC:LoginVCView{
    func activatYourAcount(_ phone: String) {
          rateBtn.stopAnimation(animationStyle: .normal, revertAfterDelay: 0.5) { [weak self] in
                  let vc = UIStoryboard(name: AppStoryboard.Main.rawValue, bundle: .main).instantiateViewController(withIdentifier: "CodeVC") as! CodeVC
                  vc.is_activate = true
                  
                  self!.navigationController?.pushViewController(vc, animated: true)
                  
              }
    }
    
    func onConnection() {
        Messages.instance.showMessage(title: "", body: CONNECTION_ERROR, state: .error, layout: .messageView)
    }
    
 
    func PresentVC<T>(_ model: T) {
        rateBtn.stopAnimation(animationStyle: .normal, revertAfterDelay: 0.5) {[weak self] in
            self?.PresentHomeVC(msg: model as? String)
        }
    }
    
    func onFailure<T>(_ msg: T) {
        Messages.instance.showMessage(title: "", body: msg as! String, state: .error, layout: .messageView)
    }
    
    
    
    
    func loader_start() {
        rateBtn.startAnimation()
        
    }
    
    func loader_stop() {
        rateBtn.stopAnimation()
        
    }
    
    
    
}
