//
//  RateVC.swift
//  Ezhal baitk
//
//  Created by a7med on 6/3/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
import SDWebImage
class RateVC:BaseViewController{
    internal var presenter: RateVCPresenter!
    var order_id: Int!
    var name: String!
    var cat:String!
    var img : String!
    
    @IBOutlet weak var photo: UIImageViewX!
    @IBOutlet weak var catLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var reviewTxtF: UITextField!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var rateBtn: TransitionButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        nameLbl.text = name
        catLbl.text = cat
        setPhoto()

        // Do any additional setup after loading the view.
        presenter = RateVCPresenter(view: self)
    }
    
    
    @IBAction func rateBtnPressed(_ sender: TransitionButton) {
        presenter.rate(rate: rateView.rating, review: reviewTxtF.text ?? "", order_id: order_id)
    }
    
    func setPhoto() {
                let logo_url = URL(string: img)
                photo.sd_imageIndicator = SDWebImageActivityIndicator.gray
                photo.sd_setImage(with: logo_url, placeholderImage: placeHolder_image)
           
       }
}
