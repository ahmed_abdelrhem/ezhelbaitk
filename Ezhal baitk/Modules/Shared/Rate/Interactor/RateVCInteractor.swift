//
//  RateVCInteractor.swift
//  Ezhal baitk
//
//  Created by a7med on 6/8/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
import Alamofire
class RateVCInteractor {
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias user_model = (User_Base)->()

    func rate(rate: Double,review: String,order_id:Int, didDataReady: @escaping user_model, andErrorCompletion errorCompletion: @escaping errorType) {
        
        let param = [
            "rate": rate,
            "review": review,
            "order_id": order_id
            ] as [String : Any]
        print("param",param)
        apiManager.contectToApiWith(url: MyURLs.rate.url,
                                    methodType: .post,
                                    params: param,
                                    success: { (json) in
                                        print("==>jsn",json)
                                        if let data = try? JSONSerialization.data(withJSONObject: json) {
                                            
                                            do {
                                                let result = try self.decoder.decode(User_Base.self, from: data)
                                                didDataReady(result)

                                            }catch{
                                                 print("##error\(error)")
                        let error = AFError.responseSerializationFailed(reason: .decodingFailed(error: error))
                        errorCompletion(error)

                                            }
                                          
                                        }
        }) { (error) in
           print(error)
            errorCompletion(error)
        }
    }
 
}



