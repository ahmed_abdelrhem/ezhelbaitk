//
//  ChatVC.swift
//  Ezhal baitk
//
//  Created by apple on 05/01/2022.
//  Copyright © 2022 a7med. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class ChatVC: UIViewController {
    
    private (set) var presenter: ChatVCP!
    //MARK:- Outlets
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var sendBtn: UIButtonX!{
        didSet{
            if UIApplication.shared.isRTL {
                sendBtn.imageView?.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
            }
        }
    }
    @IBOutlet weak var messageTxt: IQTextView! {
        didSet {
            messageTxt.delegate = self
            messageTxt.placeholder = "Write your message...".localized
        }
    }
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            let nibA = UINib(nibName: "SenderChatCell", bundle: .main)
            tableView.register(nibA, forCellReuseIdentifier: "SenderChatCell")
            let nibB = UINib(nibName: "ReciverChatCell", bundle: .main)
            tableView.register(nibB, forCellReuseIdentifier: "ReciverChatCell")
            tableView.rowHeight = UITableView.automaticDimension
            tableView.tableFooterView = UIView(frame: .zero)
            tableView.separatorStyle = .none
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter.getChat()
    }

    
    init(id:Int,receiver_id:Int? = .none,is_vist:Bool) {
        super.init(nibName: "ChatVC", bundle: .main)
        presenter = ChatVCP(view: self, id: id,receiver_id:receiver_id, is_vist: is_vist)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal func scrollToBottom() {
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                let indexPath = IndexPath(row: self.presenter.messages.count-1, section: 0)
                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
            }
        }
    }
    
    
    @IBAction func sendBtnPressed(_ sender: UIButton) {
        presenter.sendBtnPressed(messageTxt.text)
    }



}


extension ChatVC: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        if textViewHeight.constant < 120 || self.messageTxt.contentSize.height < 120 {
            textViewHeight.constant = self.messageTxt.contentSize.height
        }
    }

}

