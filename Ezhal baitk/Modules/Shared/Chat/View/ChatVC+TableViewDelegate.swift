//
//  ChatVC+TableViewDelegate.swift
//  Ezhal baitk
//
//  Created by apple on 09/01/2022.
//  Copyright © 2022 a7med. All rights reserved.
//

import Foundation
import UIKit

extension ChatVC:UITableViewDelegate{
    
    
       
}


extension ChatVC:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let sender_id = presenter.messages[indexPath.row].sender_id else { return UITableViewCell() }
        if sender_id == DEF.userID{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SenderChatCell", for: indexPath) as? SenderChatCell else { return UITableViewCell() }
            presenter.Configure(cell: cell, at: indexPath.row)
            return cell
        }else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ReciverChatCell", for: indexPath) as? ReciverChatCell else { return UITableViewCell() }
            presenter.Configure(cell: cell, at: indexPath.row)
            return cell
        }
    }
    
       
}
