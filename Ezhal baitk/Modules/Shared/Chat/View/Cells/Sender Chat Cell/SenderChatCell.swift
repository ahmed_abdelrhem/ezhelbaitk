//
//  SenderChatCell.swift
//  Mo2tah
//
//  Created by Grand iMac on 9/23/20.
//  Copyright © 2020 Organization. All rights reserved.
//

import UIKit
import SDWebImage

class SenderChatCell: UITableViewCell, MessageCellView {

   static let identifier = "SenderChatCell"

    @IBOutlet weak var textView: UIView!
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var userImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        textView.layer.cornerRadius = 20
        textView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner]
    }
        
    func configure(text: String?, image: String?) {
       
            let messageURL = URL(string: image ?? "")
        userImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        userImage.sd_setImage(with: messageURL, placeholderImage: placeHolder_image)
        messageLbl.text = text
    }
    


    
}
