//
//  ChatVC+PresenterDelegate.swift
//  Ezhal baitk
//
//  Created by apple on 09/01/2022.
//  Copyright © 2022 a7med. All rights reserved.
//

import Foundation


extension ChatVC:ChatVCPDelegate{
    func inserAtLast() {
        let row = presenter.messages.count - 1
        self.tableView.insertRows(at: [IndexPath(row: row,
                                                    section: 0)],
                                     with: .automatic)

    }
    
    func setTableView() {
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func reload() {
        tableView.reloadData()
        if !presenter.messages.isEmpty {
            self.scrollToBottom()
        }
    }
    
    func onFailure<T>(_ msg: T) {
        guard let msg = msg as? String else {
            return
        }
        Messages.instance.showMessage(title: "", body: msg, state: .error, layout: .messageView)

    }
    
    func loader_start() {
        startAnimating(ponit: nil)

    }
    
    func loader_stop() {
        stopAnimating()
    }
    
    
    
}
