//
//  ChatVCI.swift
//  Ezhal baitk
//
//  Created by apple on 09/01/2022.
//  Copyright © 2022 a7med. All rights reserved.
//

import Foundation



class ChatVCI {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    
    typealias chatModel = (ChatModel)->()
    
    
    
    
    
    func getChat(id:Int,is_visit:Bool,didDataReady: @escaping chatModel, andErrorCompletion errorCompletion: @escaping errorType) {
        
        var url = MyURLs.messages.url + "order_id=\(id)&visit_id=\(id)"
        if is_visit {
           
             url = MyURLs.messages.url + "order_id=\(0)&visit_id=\(id)"


        }else{
           
            url = MyURLs.messages.url + "order_id=\(id)&visit_id=\(0)"

        }
        
        
        apiManager.contectToApiWith(url: url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
                                        
                                        if let data = try? JSONSerialization.data(withJSONObject: json) {
                                            do {
                                                let result = try JSONDecoder().decode(ChatModel.self, from: data)
                                                didDataReady(result)
                                            } catch let error {
                                                print("jsonError", error)
                                            }
                                        }
                                    }) { (error) in
           print(error)
            errorCompletion(error)
        }
    }
    
    func getChatList(order_id:Int,didDataReady: @escaping chatModel, andErrorCompletion errorCompletion: @escaping errorType) {
        
        
        
        
        apiManager.contectToApiWith(url: MyURLs.message_history.url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
                                        
                                        if let data = try? JSONSerialization.data(withJSONObject: json) {
                                            do {
                                                let result = try JSONDecoder().decode(ChatModel.self, from: data)
                                                didDataReady(result)
                                            } catch let error {
                                                print("jsonError", error)
                                            }
                                        }
                                    }) { (error) in
           print(error)
            errorCompletion(error)
        }
    }
    
    
    func sendTxt(id:Int,receiver_id:Int,text:String,is_visit:Bool, didDataReady: @escaping chatModel, andErrorCompletion errorCompletion: @escaping errorType) {
        
        var param = [
            "text":text,
            "receiver_id":receiver_id
        ] as [String : Any]
        if is_visit {
            param["visit_id"] = id
            param["order_id"] = 0

        }else{
            param["order_id"] = id
            param["visit_id"] = 0

        }
        
        apiManager.contectToApiWith(url: MyURLs.send_message.url,
                                    methodType: .post,
                                    params:  param,
                                    success: { (json) in
                                        
                                        if let data = try? JSONSerialization.data(withJSONObject: json) {
                                            do {
                                                let result = try JSONDecoder().decode(ChatModel.self, from: data)
                                                didDataReady(result)
                                            } catch let error {
                                                print("jsonError", error)
                                            }
                                        }
                                    }) { (error) in
           print(error)
            errorCompletion(error)
        }
        
    }
    
    
    
    
    
}
