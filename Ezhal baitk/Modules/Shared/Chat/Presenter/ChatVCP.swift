//
//  ChatVCP.swift
//  Ezhal baitk
//
//  Created by apple on 09/01/2022.
//  Copyright © 2022 a7med. All rights reserved.
//

import Foundation

protocol ChatVCPDelegate:LoaderView {
    func reload()
    func onFailure<T>(_ msg: T)
    func setTableView()
    func inserAtLast()
}

final class ChatVCP{
    
    
    private weak var view: ChatVCPDelegate?
    private let interactor = ChatVCI()
    private (set) var messages : [ChatObj] = []
    
    private var id:Int
    private var receiver_id:Int?
    private var is_vist = false


    
    init(view:ChatVCPDelegate,id:Int,receiver_id:Int? = .none,is_vist : Bool) {
        self.view = view
        self.id = id
        self.receiver_id = receiver_id
        self.is_vist = is_vist
        print("->","is_vist = \(is_vist)","->","id = \(id)","->","receiver_id = \(receiver_id)")
    }
    
    
    func getChat() {
        view?.loader_start()
        interactor.getChat(id: id,is_visit: is_vist) { [weak self] response in
            guard let self = self else{return}
            self.view?.loader_stop()
            switch response.status {
            case API_status.Success.rawValue:
                self.messages = response.data ?? []
                self.view?.setTableView()
                self.view?.reload()
                break
            default:
                self.view?.onFailure(response.message)
                break
            
            }
        } andErrorCompletion: { [weak self] (error)  in
            guard let self = self else{return}
            self.view?.loader_stop()
            self.view?.onFailure(error?.errorDescription)
        }

    }
    
    
    
    
    
    
    func Configure<T>(cell:T ,at row:Int)  {
        let msg = messages[row]
        if let cell = cell as? SenderChatCell {
            cell.configure(text: msg.text , image: msg.sender?.image)
            
        }
        
        if let cell = cell as? ReciverChatCell {
            cell.configure(text: msg.text , image: msg.sender?.image)

        }
        
        
    }
    
    
    func sendBtnPressed(_ text: String)  {

        let msg  = ChatObj(id: .none, receiver_id: receiver_id, order_id: id, text: text)
        messages.append(msg)
        self.view?.inserAtLast()
        sendText(text: text)
        
        
    }
    
    
    private func sendText(text: String){
        guard let receiver_id = receiver_id else {
            return
        }
        
        interactor.sendTxt(id: id, receiver_id: receiver_id, text: text, is_visit: is_vist) { [weak self] response in
            guard let self = self else{return}
            self.messages.removeLast()
            guard let single = response.single else{return}
            self.messages.append(single)
        } andErrorCompletion: {[weak self]  error in
            guard let self = self else{return}

        }

        
    }
    
    
    
    
}
