/* 
Copyright (c) 2022 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct ChatObj : Codable {
	let id : Int?
	let sender_id : Int?
	let receiver_id : Int?
	let order_id : Int?
	let visit_id : Int?
	let text : String?
	let type : Int?
	let created_at : String?
	let read_at : String?
	let media : [String]?
	let receiver : Receiver?
	var sender : Sender?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case sender_id = "sender_id"
		case receiver_id = "receiver_id"
		case order_id = "order_id"
		case visit_id = "visit_id"
		case text = "text"
		case type = "type"
		case created_at = "created_at"
		case read_at = "read_at"
		case media = "media"
		case receiver = "receiver"
		case sender = "sender"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		sender_id = try values.decodeIfPresent(Int.self, forKey: .sender_id)
		receiver_id = try values.decodeIfPresent(Int.self, forKey: .receiver_id)
		order_id = try values.decodeIfPresent(Int.self, forKey: .order_id)
		visit_id = try values.decodeIfPresent(Int.self, forKey: .visit_id)
		text = try values.decodeIfPresent(String.self, forKey: .text)
		type = try values.decodeIfPresent(Int.self, forKey: .type)
		created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
		read_at = try values.decodeIfPresent(String.self, forKey: .read_at)
		media = try values.decodeIfPresent([String].self, forKey: .media)
		receiver = try values.decodeIfPresent(Receiver.self, forKey: .receiver)
		sender = try values.decodeIfPresent(Sender.self, forKey: .sender)
	}
    
    init(id:Int?,receiver_id:Int?,order_id:Int?,text:String?) {

        self.id = id
        self.sender_id = DEF.userID
        self.receiver_id = receiver_id
        self.order_id = order_id
        self.visit_id = order_id
        self.text = text
        self.created_at = Date().dateString(with: "yyyy-MM-dd HH:mm:ss")
        
        sender = Sender()
        receiver = .none
        media = .none
        type = .none
        read_at = .none
    }

}
