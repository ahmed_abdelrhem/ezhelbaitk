//
//  NewSubscriptionVC.swift
//  Ezhal baitk
//
//  Created by a7med on 6/5/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

class NewSubscriptionVC:BaseViewController{
    var id:Int?
    var package : NewSubscriptionData?
    
    var cat_id:Int?

    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
            collectionView.delegate = self
            collectionView.dataSource = self
            collectionView.allowsMultipleSelection = false
            
        }
    }
    
    internal var presenter : NewSubscriptionVCPresenter!
    
    fileprivate var currentPage: Int = 0
    fileprivate var pageSize: CGSize {
        let layout = self.collectionView.collectionViewLayout as! UPCarouselFlowLayout
        var pageSize = layout.itemSize
        if layout.scrollDirection == .horizontal {
            pageSize.width += layout.minimumLineSpacing
        } else {
            pageSize.height += layout.minimumLineSpacing
        }
        return pageSize
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        guard let cat_id = cat_id else { return  }
        guard let id = id else { return  }

        presenter = NewSubscriptionVCPresenter(view: self)
        presenter.viewdidload(id: id, cat_id: cat_id)
        title = "Subscription".localized
        
    }
    
    @IBAction func continueBtnPressed(_ sender: UIButton) {
        guard let package = package else {
            Messages.instance.showMessage(title: "", body: "Please, Select a Package.".localized, state: .error, layout: .messageView)

            return
            
        }
        let vc  = UIStoryboard(name: AppStoryboard.Business.rawValue, bundle: nil).instantiateViewController(withIdentifier: "PayVC") as! PayVC
        vc.package =  package
        vc.pay4 = .package
        vc.cat_id = cat_id
        navigationController?.pushViewController(vc, animated: true)    }
    
    fileprivate func setupLayout() {
        let layout = self.collectionView.collectionViewLayout as! UPCarouselFlowLayout
        layout.spacingMode = UPCarouselFlowLayoutSpacingMode.overlap(visibleOffset: 15)
        layout.itemSize = collectionView.frame.size
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let layout = self.collectionView.collectionViewLayout as! UPCarouselFlowLayout
        let pageSide = (layout.scrollDirection == .horizontal) ? self.pageSize.width : self.pageSize.height
        let offset = (layout.scrollDirection == .horizontal) ? scrollView.contentOffset.x : scrollView.contentOffset.y
        currentPage = Int(floor((offset - pageSide / 2) / pageSide) + 1)
        print("current ==>",currentPage)
    }
}
