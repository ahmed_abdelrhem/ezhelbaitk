//
//  NewSubscriptionVC+Collection.swift
//  Ezhal baitk
//
//  Created by a7med on 6/5/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

extension NewSubscriptionVC:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.getlistCount()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubCell", for: indexPath) as! SubCell
        presenter.configureCell(cell: cell, at: indexPath.row)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! SubCell
        presenter.didUnselect(cell, indexPath.row)
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! SubCell
        package =  presenter.didselect(cell, indexPath.row)
    }
    
}
