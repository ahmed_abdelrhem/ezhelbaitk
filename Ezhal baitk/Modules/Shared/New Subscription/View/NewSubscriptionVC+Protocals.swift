//
//  NewSubscriptionVC+Protocals.swift
//  Ezhal baitk
//
//  Created by a7med on 6/5/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit


extension NewSubscriptionVC: LoaderJwtDelegate{
    func jwtExpired<T>(_ model: T) {
         alert_login(_Login)
    }
    func onConnection() {
        collectionView.setConnectionView()
    }
    
    func onEmpty() {
        collectionView.setEmptyView()
    }
    
    func onFailure<T>(_ msg: T) {
        Messages.instance.showMessage(title: "", body: msg as! String, state: .error, layout: .messageView)
        
    }
    
    func onSuccess<T>(_ msg: T) {
        
        collectionView.backgroundView = nil
        collectionView.reloadData()
        let indexPath = IndexPath(item: 0, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .centeredVertically, animated: false)
        
    }
    
    
    func loader_start() {
        startAnimating(ponit: nil)
    }
    
    func loader_stop() {
        stopAnimating()
    }
    
    
    
    
}
