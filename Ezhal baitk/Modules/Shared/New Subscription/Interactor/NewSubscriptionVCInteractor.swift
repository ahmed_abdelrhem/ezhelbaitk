//
//  NewSubscriptionVCInteractor.swift
//  Ezhal baitk
//
//  Created by a7med on 6/5/20.
//  Copyright © 2020 a7med. All rights reserved.
//
import Foundation

class NewSubscriptionVCInteractor{
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias model = (NewSubscription_Base)->()
    
    
    func getSubs(id:Int,cat_id:Int,didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorType) {
       
        apiManager.contectToApiWith(url: MyURLs.sub.url + "\(id)&category_id=\(cat_id)",
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
                                        print("##",json)
                                        if let data = try? JSONSerialization.data(withJSONObject: json) {
                                            
                                            do {
                                                let result = try self.decoder.decode(NewSubscription_Base.self, from: data)
                                                didDataReady(result)

                                            }catch{
                                                print("model parse error\(error)")

                                            }
                                          
                                        }
        }) { (error) in
           print(error)
            errorCompletion(error)
        }
    }

}
