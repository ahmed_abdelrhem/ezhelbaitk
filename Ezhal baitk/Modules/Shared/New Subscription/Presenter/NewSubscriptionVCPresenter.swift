//
//  NewSubscriptionVCPresenter.swift
//  Ezhal baitk
//
//  Created by a7med on 6/5/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
class NewSubscriptionVCPresenter {
    private let interactor = NewSubscriptionVCInteractor()
    private var list = [NewSubscriptionData]()
    weak var view: (LoaderJwtDelegate)?
    init(view:LoaderJwtDelegate) {
        self.view = view
    }
    
    func viewdidload(id:Int,cat_id:Int)  {
        view?.loader_start()
        interactor.getSubs(id: id, cat_id: cat_id, didDataReady: { [weak self](model) in
            guard self != nil else { return }
            if model.status == API_status.Success.rawValue{
                self?.list = model.data ?? []
                if self!.list.count == 0{
                    self?.view?.onEmpty()
                }else{
                    self!.view?.onSuccess("")
                }
            }else if model.status == API_status.JwtExpired.rawValue{
                self!.view?.jwtExpired(model.message ?? "")
            }else{
                
                self?.view?.onFailure(model.message ?? "")
            }
            self?.view?.loader_stop()
            
        }) { [weak self](error) in
            
            guard self != nil else { return }
            self?.view?.loader_stop()
            self?.view?.onConnection()
            self?.view?.onFailure(error?.errorDescription)
        }
    }
    func getlistCount() ->Int {
        return list.count
    }
    
    func configureCell(cell:SubCellView,at row: Int)  {
        let item = list[row]
        cell.setMonth("\(item.num_of_months ?? 0)")
        cell.setPrice(item.price ?? "", item.currency ?? "QR".localized)
        if (item.category_id ?? 0) == 1{
            cell.setCategory("air condition".localized)
        }else if (item.category_id ?? 0) == 4{
            cell.setCategory("cleaning".localized)

        }else{
            cell.setCategory("")
        }
        

      
    }
    func didselect(_ cell:SubCellView,_ row:Int) -> NewSubscriptionData{
        let item = list[row]
        cell.didSelect()
        return item
    }
    func didUnselect(_ cell:SubCellView,_ row:Int)  {
        let item = list[row]
         cell.didUnselect()
    }
    
    
    
    
}
