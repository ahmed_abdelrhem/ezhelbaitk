//
//  NotificationsVC.swift
//  Jaz-User
//
//  Created by iMac on 10/29/19.
//  Copyright © 2019 Grand. All rights reserved.
//

import UIKit

class NotificationsVC: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    internal var presenter : NotificationsVCPresenter!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setTable()
        presenter = NotificationsVCPresenter(view: self)
        presenter.viewdidload()
//        sideMenu() 
        title = "Notifications".localized

    }
    
    
    

}
