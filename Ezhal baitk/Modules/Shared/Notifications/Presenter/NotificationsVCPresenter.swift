//
//  DepartmentsVCPresenter.swift
//  Jaz-User
//
//  Created by iMac on 10/27/19.
//  Copyright © 2019 Grand. All rights reserved.
//

import Foundation
class NotificationsVCPresenter {
    private let interactor = NotificationsVCAPI()
    private var list = [NotificationData]()
    weak var view: (LoaderJwtDelegate)?
    init(view:LoaderJwtDelegate) {
        self.view = view
    }
    
    func viewdidload()  {
        view?.loader_start()
        interactor.getNotifications(didDataReady: { [weak self](model) in
            guard self != nil else { return }
            if model.status == API_status.Success.rawValue{
                self?.list = model.data ?? []
                if self!.list.count == 0{
                    self?.view?.onEmpty()
                }else{
                    self!.view?.onSuccess("")
                }
            }else if model.status == API_status.JwtExpired.rawValue{
                self!.view?.jwtExpired(model.message ?? "")
            }else{
                
                self?.view?.onFailure(model.message ?? "")
            }
            self?.view?.loader_stop()
            
        }) { [weak self](error) in
            
            guard self != nil else { return }
            self?.view?.loader_stop()
            self?.view?.onConnection()
            self?.view?.onFailure(error?.errorDescription)
        }
    }
    func getlistCount() ->Int {
        return list.count
    }
    
    func configureCell(cell:NotificationCellDelegate,at row: Int)  {
        cell.setBody(body: list[row].message ?? "")
        let dateString = list[row].created_at ?? ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateObj = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = "MMM d, yyyy"
        let date = dateObj?.dateString() ?? ""
        cell.setTime(time: " ")
        cell.setTitle(title: date ?? "")
        
    }
    
    
    
    
}
