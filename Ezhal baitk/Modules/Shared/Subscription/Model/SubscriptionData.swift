/* 
 Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 
 For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar
 
 */

import Foundation
import  GoogleMaps



struct SubscriptionData : Codable {
    let id : Int?
    let name : String?
    let price : Int?
    let category_id : Int?
    let num_of_months : Int?
    let category : Categories?
    let user : UserData?
    let tech : UserData?
    
    //    let location : AddressInfo?
    
    let rest_of_months : Int?
    let time_of_visit : String?
    let currency : String?
    
    
    let order_id : Int?
    let date : String?
    let time : String?
    let subscripton_price : String?
    let months : String?
    let subscripton_number : String?
    let lat : String?
    let lng : String?
    let total_price : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case name = "name"
        case price = "price"
        case category_id = "category_id"
        case num_of_months = "num_of_months"
        case category = "category"
        case user = "user"
        case rest_of_months = "rest_of_months"
        case time_of_visit = "time_of_visit"
        case currency = "currency"
        case tech = "tech"
        
        
        case order_id = "order_id"
        case date = "date"
        case time = "time"
        case subscripton_price = "subscripton_price"
        case months = "months"
        case subscripton_number = "subscripton_number"
        case total_price = "total_price"
        
        case lat = "lat"
        case lng = "lng"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        do {
            price = try values.decodeIfPresent(Int.self, forKey: .price)
        } catch  {
            let p = try values.decodeIfPresent(String.self, forKey: .price)
            price = Int(p ?? "0") 
        }
        
        category_id = try values.decodeIfPresent(Int.self, forKey: .category_id)
        num_of_months = try values.decodeIfPresent(Int.self, forKey: .num_of_months)
        category = try values.decodeIfPresent(Categories.self, forKey: .category)
        user = try values.decodeIfPresent(UserData.self, forKey: .user)
        rest_of_months = try values.decodeIfPresent(Int.self, forKey: .rest_of_months)
        time_of_visit = try values.decodeIfPresent(String.self, forKey: .time_of_visit)
        currency = try values.decodeIfPresent(String.self, forKey: .currency)
        order_id = try values.decodeIfPresent(Int.self, forKey: .order_id)
        date = try values.decodeIfPresent(String.self, forKey: .date)
        time = try values.decodeIfPresent(String.self, forKey: .time)
        subscripton_price = try values.decodeIfPresent(String.self, forKey: .subscripton_price)
        do {
            months = try values.decodeIfPresent(String.self, forKey: .months)
        } catch  {
            let m = try values.decodeIfPresent(Int.self, forKey: .months)
            months = "\(m ?? 0)"
        }
        subscripton_number = try values.decodeIfPresent(String.self, forKey: .subscripton_number)
        total_price = try values.decodeIfPresent(String.self, forKey: .total_price)
        tech = try values.decodeIfPresent(UserData.self, forKey: .tech)
        lat = try values.decodeIfPresent(String.self, forKey: .lat)
        lng = try values.decodeIfPresent(String.self, forKey: .lng)
     
        
        
        
    }
    
}

final class LocationConverter {
    private var geocoder : GMSGeocoder?
    private var lat : String?
    private var lng : String?

    init(lat:String?,lng:String?) {
        self.lat = lat
        self.lng = lng
        geocoder = GMSGeocoder()
    }

     func convertToAddress(completionHandler: @escaping (String?)
                            -> Void) {
        guard let geocoder = geocoder else{return }

        guard  let lat = Double(lat ?? "") else{return }
        guard let lng = Double(lng ?? "") else{return }
        
        let position = CLLocationCoordinate2DMake(lat, lng)
        geocoder.reverseGeocodeCoordinate(position) { response , error in
            if error != nil {
                print("Error: \(String(describing: error?.localizedDescription))")
                completionHandler(.none)

            } else {
                guard let response = response else { return  }
                print("#google location",response.results())
                let result = response.results()?.first
                let location = result?.lines?.reduce("") { $0 == "" ? $1 : $0 + ", " + $1 }
//                let country = result?.country ?? ""
                completionHandler(location)

            }
        }


        
    }
    
}




