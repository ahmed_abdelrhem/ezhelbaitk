//
//  SubscriptionVCInteractor.swift
//  Ezhal baitk
//
//  Created by a7med on 6/5/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation

class SubscriptionVCInteractor{
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias model = (Subscription_Base)->()
    typealias make_model = (Make_Subscription_Base)->()

    
    func getSubs(didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorType) {
       
        apiManager.contectToApiWith(url: MyURLs.old_sub.url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
                                        print(json)
                                        if let data = try? JSONSerialization.data(withJSONObject: json) {
                                            
                                            do {
                                                let result = try self.decoder.decode(Subscription_Base.self, from: data)
                                                didDataReady(result)

                                            }catch{
                                                print("model parse error\(error)")

                                            }
                                          
                                        }
        }) { (error) in
           print(error)
            errorCompletion(error)
        }
    }
    
    func make_subscription(parameters:[String:Any],didDataReady: @escaping make_model, andErrorCompletion errorCompletion: @escaping errorType) {
        apiManager.contectToApiWith(url: MyURLs.make_subscription.url,
                                    methodType: .post,
                                    params: parameters,
                                    success: { (json) in
                                        print(json)
                                        if let data = try? JSONSerialization.data(withJSONObject: json) {
                                            
                                            do {
                                                let result = try self.decoder.decode(Make_Subscription_Base.self, from: data)
                                                didDataReady(result)

                                            }catch{
                                                print("model parse error\(error)")

                                            }
                                          
                                        }
        }) { (error) in
           print(error)
            errorCompletion(error)
        }
    }

}
