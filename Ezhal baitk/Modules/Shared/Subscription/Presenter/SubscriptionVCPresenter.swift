//
//  SubscriptionVCPresenter.swift
//  Ezhal baitk
//
//  Created by a7med on 6/5/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation

protocol SubscriptionVCD:LoaderJwtDelegate {
    func onSuccessList(msg:String)
}

class SubscriptionVCPresenter {
    private let interactor = SubscriptionVCInteractor()
    private var list = [SubscriptionData]()
    weak var view: (SubscriptionVCD)?
    init(view:SubscriptionVCD) {
        self.view = view
    }
    
    func viewdidload()  {
        view?.loader_start()
        interactor.getSubs(didDataReady: { [weak self](model) in
            guard self != nil else { return }
            if model.status == API_status.Success.rawValue{
                self?.list = model.data ?? []
                if self!.list.count == 0{
                    self?.view?.onEmpty()
                }else{
                    self!.view?.onSuccessList(msg: "")
                }
            }else if model.status == API_status.JwtExpired.rawValue{
                self!.view?.jwtExpired(model.message ?? "")
            }else{
                
                self?.view?.onFailure(model.message ?? "")
            }
            self?.view?.loader_stop()
            
        }) { [weak self](error) in
            
            guard self != nil else { return }
            self?.view?.loader_stop()
            self?.view?.onConnection()
            self?.view?.onFailure(error?.errorDescription)
        }
    }
    
    func make_subscriptions(parameters:[String:Any])  {
        view?.loader_start()
        interactor.make_subscription(parameters:parameters,didDataReady: { [weak self](model) in
            guard let self = self else { return }
            print(">>",model)
            if model.status == API_status.Success.rawValue{
                if let subscription = model.data {
                    var suscrippedBefore = false
                    for subsc in self.list {
                        if subsc.id == subscription.id{
                            suscrippedBefore = true
                            break
                        }
                    }
                    if !suscrippedBefore{
                    self.list.append(subscription)
                    }
                }
                    self.view?.onSuccess("")
            }else if model.status == API_status.JwtExpired.rawValue{
                self.view?.jwtExpired(model.message ?? "")
            }else{
                
                self.view?.onFailure(model.message ?? "")
            }
            self.view?.loader_stop()
            
        }) { [weak self](error) in
            
            guard self != nil else { return }
            self?.view?.loader_stop()
            self?.view?.onConnection()
            self?.view?.onFailure(error?.errorDescription)
        }
    }
    func getlistCount() ->Int {
        return list.count
    }
    
    func configureCell(cell:PackageDetailsCellView,at row: Int)  {
        let item = list[row]
        let converter = LocationConverter(lat: item.lat, lng: item.lng)
        converter.convertToAddress { (address) in
            cell.setAddress( address ?? "")

        }
        cell.setDate("\(item.num_of_months ?? 0)")
        cell.setRest("\(item.rest_of_months ?? 0)")
        cell.setName(item.user?.name ?? "")
        cell.setTime(item.time_of_visit ?? "")
        cell.setEmail(item.user?.email ?? "")
        cell.setPhone(item.user?.phone ?? "")
        let p = "\(item.price ?? 0) \((item.currency ?? "").localized)"
        cell.setPrice(p)
        cell.setFacility(item.category?.name ?? "")
 
        

    }
    func update(_ row:Int) ->  Int {
        return list[row].id ?? 0
    }
    
    
    
    
}
