//
//  SubscriptionVC+table.swift
//  Ezhal baitk
//
//  Created by a7med on 6/5/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
extension SubscriptionVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getlistCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PackageDetailsCell", for: indexPath)  as! PackageDetailsCell
        presenter.configureCell(cell: cell, at: indexPath.row)
        cell.delegate = self
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
    }
  
}

extension SubscriptionVC:PackageDetailsCellDelegate{
    func updateBtnPressed(_ cell: UITableViewCell) {
        let ind = tableView.indexPath(for: cell)!
       let id =  presenter.update(ind.row)
        /*
        let vc  = UIStoryboard(name: AppStoryboard.Shared.rawValue, bundle: nil).instantiateViewController(withIdentifier: "PackageVC") as! PackageVC
        vc.sub_id = id
        navigationController?.pushViewController(vc, animated: true)
        */
        let vc  = PackageCategoryVC()
        navigationController?.pushViewController(vc, animated: true)

    }
    
    
    
}
