//
//  SubscriptionVC.swift
//  Ezhal baitk
//
//  Created by a7med on 6/5/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

class SubscriptionVC:BaseViewController{

    @IBOutlet weak var addBtn: UIButtonX!
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.tableFooterView = UIView(frame: .zero)
                   tableView.dataSource = self
                   tableView.delegate = self
                   tableView.separatorStyle = .none
                   tableView.rowHeight = UITableView.automaticDimension
                   tableView.register(UINib(nibName: "PackageDetailsCell", bundle: nil), forCellReuseIdentifier: "PackageDetailsCell") 
        }
    }
  
    internal var presenter : SubscriptionVCPresenter!

     
     override func viewDidLoad() {
         super.viewDidLoad()
        _NC.addObserver(self, selector: #selector(callPaymentApi), name: .make_subscription, object: .none)

         // Do any additional setup after loading the view.
         presenter = SubscriptionVCPresenter(view: self)
         presenter.viewdidload()
         sideMenu()
        title = "Subscription".localized

     }
    
    
    deinit {
        print("deinit home")
        _NC.removeObserver(self)
    }
    
    @objc private func callPaymentApi(_ notification:Notification){
        print("callPaymentApi")
        guard let userInfo = notification.userInfo as? [String:Any]? else { return  }
//        print(">> call api",userInfo)
      
        if let userInfo = userInfo {
            
            presenter.make_subscriptions(parameters: userInfo)
        }else{
            let alert = UIAlertController(title: "Sorry!".localized, message: "Payment Failed, Please Try Again.".localized, preferredStyle: .alert)
            let action = UIAlertAction(title: "Done".localized, style: .default, handler: .none)
            alert.addAction(action)
            present(alert, animated: true, completion: .none)

        }
        
        
    }
    
    @IBAction func addBtnPressed(_ sender: UIButton) {
        let vc  = PackageCategoryVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    

}
