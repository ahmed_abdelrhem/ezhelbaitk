//
//  SubscriptionVC+Protocals.swift
//  Ezhal baitk
//
//  Created by a7med on 6/5/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
extension SubscriptionVC: SubscriptionVCD{
    func onSuccessList(msg: String) {
        self.tableView.backgroundView = nil
        self.tableView.reloadData()
        
    }
    
    func jwtExpired<T>(_ model: T) {
         alert_login(_Login)
    }
    func onConnection() {
        tableView.setConnectionView()
    }
    
    func onEmpty() {
        tableView.setNoSub()
    }
    
    func onFailure<T>(_ msg: T) {
        Messages.instance.showMessage(title: "", body: msg as! String, state: .error, layout: .messageView)
        
    }
    
    func onSuccess<T>(_ msg: T) {
        let alert = UIAlertController(title: "Congratulations!".localized, message: "Package subscribed successfully".localized, preferredStyle: .alert)
        let action = UIAlertAction(title: "Done".localized, style: .default){ [weak self] (_) in
            guard let self = self else {return}
            self.tableView.backgroundView = nil
            self.tableView.reloadData()
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(action)
        present(alert, animated: true, completion: .none)
    
    }
    
    
    func loader_start() {
        startAnimating(ponit: nil)
    }
    
    func loader_stop() {
        stopAnimating()
    }
    
    
    
    
}
