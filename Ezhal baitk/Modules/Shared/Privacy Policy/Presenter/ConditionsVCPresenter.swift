//
//  ConditionsVCPresenter.swift
//  Aman
//
//  Created by iMac on 7/3/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit

class ConditionsVCPresenter {
    private weak var view: (LoaderDelegate)?
    private var interactor = ConditionsVCAPI()
    init(view: LoaderDelegate) {
        self.view = view
    }
    
    func viewDidLoad()  {
        view?.loader_start()
        interactor.getTerms( didDataReady: { [weak self](model) in
             
            if model.status == API_status.Success.rawValue{
                if model.data == nil{
                    self?.view?.onEmpty()
                }else{
                    self!.view?.onSuccess(model.data)
                }
            }else{
                
                self?.view?.onFailure(model.message ?? "")
            }
            self?.view?.loader_stop()
            
        }) { [weak self](error) in
            
             
            self?.view?.loader_stop()
            self?.view?.onConnection()
            self?.view?.onFailure(error?.errorDescription)
        }
    }
    
    
    
    
}
