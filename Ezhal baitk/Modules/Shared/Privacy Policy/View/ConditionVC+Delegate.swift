//
//  ConditionVC+Delegate.swift
//  Jaz-User
//
//  Created by iMac on 10/30/19.
//  Copyright © 2019 Grand. All rights reserved.
//

import UIKit
extension ConditionsVC:LoaderDelegate{
    func onFailure<T>(_ msg: T) {
        Messages.instance.showMessage(title: "", body: msg as! String, state: .error, layout: .messageView)

    }
    
    func onEmpty() {
        setEmptyView()
    }
    
    func onConnection() {
        setConnectionView()
    }
    
    func onSuccess<T>(_ msg: T) {
        let dt = msg as! AboutData
        termsLbl.text = "\(dt.text ?? "") "
        facebook = dt.facebook ?? ""
        google = dt.google ?? ""
        
    }
    
    func loader_start() {
        startAnimating(ponit: nil)
    }
    
    func loader_stop() {
        stopAnimating()
    }
    
 
    
    
    
}
