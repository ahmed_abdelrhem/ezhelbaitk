//
//  ConditionsVC.swift
//  Aman
//
//  Created by iMac on 4/15/19.
//  Copyright © 2019 apple. All rights reserved.
//

import UIKit


class ConditionsVC: BaseViewController {
    private var presenter : ConditionsVCPresenter!
    internal var facebook = "https://www.facebook.com/"
    internal var google = "https://www.google.com/"
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var termsLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        hideBackButtonTitle()
        presenter = ConditionsVCPresenter(view: self)
        presenter.viewDidLoad()
        title = "About App".localized
        sideMenu()
      
    }
    
    @IBAction func facebookBtnPressed(_ sender: UIButton) {
        let link = URL(string: facebook)!
        UIApplication.shared.open( link, options: [:], completionHandler: nil)
    }
    
    
    @IBAction func googleBtnPressed(_ sender: UIButton) {
        let link = URL(string: google)!
               UIApplication.shared.open( link, options: [:], completionHandler: nil)
    }
}
