//
//  ConditionsVCAPI.swift
//  Jaz-User
//
//  Created by iMac on 10/30/19.
//  Copyright © 2019 Grand. All rights reserved.
//

import UIKit
class ConditionsVCAPI {
    
    
    
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias model = (About_Base)->()
    
    
    func getTerms (didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorType) {
        apiManager.contectToApiWith(url: MyURLs.about_us.url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
                                        print(json)
                                        if let data = try? JSONSerialization.data(withJSONObject: json) {
                                            
                                            do {
                                                let result = try self.decoder.decode(About_Base.self, from: data)
                                                didDataReady(result)
                                                
                                            }catch{
                                                print("model parse error\(error)")
                                                
                                            }
                                            
                                        }
        }) { (error) in
           print(error)
            errorCompletion(error)
        }
    }
    
    
    
    
}
