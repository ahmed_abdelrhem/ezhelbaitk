//
//  MapVC.swift
//  Rahty
//
//  Created by Islamic on 3/24/19.
//  Copyright © 2019 2grand. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit


class MapVC:BaseViewController{

    //MARK:- Outelts
    @IBOutlet weak var myMapView: UIView!
    
    //MARK: Variables
    let locationManager = CLLocationManager()
    var lat: Double?
    var lng: Double?
    var address = ""
    var country = ""
    
    var mapView: GMSMapView!
    var centerMapCoordinate:CLLocationCoordinate2D!
    var marker: GMSMarker!
    
    weak var delegate: LocationDelegate?
    
    
    //MARK:- Storyboard
    override func viewDidLoad() {
        super.viewDidLoad()
        locationAuth()
        transparent_nav()
        
    }
    
    //MARK:- Actions
    @IBAction func doneBtnPressed(_ sender: UIButton) {
        self.delegate?.RetriveLocation(lat: self.lat ?? 0.0, lng: self.lng ?? 0.0, add: self.address, country: self.country)
        print("lat: ",self.lat ?? 0.0, "lng: ", self.lng ?? 0.0, "add:", self.address)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cancelBtnPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
}

//MARK:- Maps Helper Funcs
extension MapVC: CLLocationManagerDelegate, GMSMapViewDelegate {
    
    private func locationAuth(){
        mapView = GMSMapView()
        marker = GMSMarker()
        
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        self.lat = locValue.latitude
        self.lng = locValue.longitude
        
        self.MapSetup(lat:"\((locValue.latitude))", long: "\((locValue.longitude))")
        
        locationManager.stopUpdatingLocation()
        
    }
    
    
    private func MapSetup(lat: String , long: String) {
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(lat)!, longitude: CLLocationDegrees(long)!, zoom: 15.0)
        
        let f: CGRect = view.frame
        let mapFrame = CGRect(x: f.origin.x, y: 0, width: f.size.width, height: f.size.height)
        mapView = GMSMapView.map(withFrame: mapFrame, camera: camera)
        mapView.settings.myLocationButton = true
        mapView.settings.compassButton = true
        mapView.isMyLocationEnabled = true
        mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 80, right: 0)
        self.myMapView.addSubview(mapView)
        mapView.delegate = self
    }
    
    
    private func MapMarkerSetup() {
        let marker1 = GMSMarker()
        marker1.position = CLLocationCoordinate2D(latitude: self.lat!, longitude: self.lng!)
        marker1.icon = GMSMarker.markerImage(with: HexStringToUIColor.hexStringToUIColor(hex: "#FF7314"))
        marker1.map = mapView
    }
    
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
        let latitude = mapView.camera.target.latitude
        let longitude = mapView.camera.target.longitude
        
        centerMapCoordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        self.placeMarkerOnCenter(centerMapCoordinate:centerMapCoordinate)
    }
    
    private func placeMarkerOnCenter(centerMapCoordinate:CLLocationCoordinate2D) {
        marker.position = centerMapCoordinate
        marker.map = self.mapView
        marker.icon = GMSMarker.markerImage(with: HexStringToUIColor.hexStringToUIColor(hex: "#FF7314"))
    }
    
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        returnPostionOfMapView(mapView: mapView)
    }
    
    private func returnPostionOfMapView(mapView:GMSMapView) {
        let geocoder = GMSGeocoder()
        let latitute = mapView.camera.target.latitude
        let longitude = mapView.camera.target.longitude
        
        self.lat = Double(latitute)
        self.lng = Double(longitude)
        
        let position = CLLocationCoordinate2DMake(latitute, longitude)
        geocoder.reverseGeocodeCoordinate(position) { response , error in
            if error != nil {
                print("Error: \(String(describing: error?.localizedDescription))")
            } else {
                print("#google location",response!.results()!)
                let result = response?.results()?.first
                let location = result?.lines?.reduce("") { $0 == "" ? $1 : $0 + ", " + $1 }
                self.country = result?.country ?? ""
                self.address = location ?? ""
            }
        }
    }
    
    


}

