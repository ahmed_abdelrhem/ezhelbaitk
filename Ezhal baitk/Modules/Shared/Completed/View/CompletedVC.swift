//
//  CompletedVC.swift
//  Ezhal baitk
//
//  Created by a7med on 6/6/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

class CompletedVC:BaseViewController{
    var cat_id: Int?

    internal var presenter : CompletedVCPresenter!
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.tableFooterView = UIView(frame: .zero)
                   tableView.dataSource = self
                   tableView.delegate = self
                   tableView.separatorStyle = .none
                   tableView.rowHeight = UITableView.automaticDimension
                   tableView.register(UINib(nibName: "CompletedCell", bundle: nil), forCellReuseIdentifier: "CompletedCell")
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Subscription Status".localized
        guard let cat_id = cat_id else { return  }
        
        // Do any additional setup after loading the view.
        presenter = CompletedVCPresenter(view: self)
        presenter.viewdidload(cat_id:cat_id)
    }
}
