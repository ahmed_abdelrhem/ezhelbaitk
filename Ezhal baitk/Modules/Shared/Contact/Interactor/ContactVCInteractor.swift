//
//  ContactVCInteractor.swift
//  Ezhal baitk
//
//  Created by a7med on 6/2/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
class ContactVCInteractor {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    
    typealias model = (User_Base)->()
    
    func save(username: String,phone: String,problem: String, didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorType) {
        
        let param = [
            "username": username,
            "phone": phone,
            "problem": problem
            ] as [String : Any]
        print("#body ==>",param)
               print("#url ==>",MyURLs.contacts.url)
        
        apiManager.contectToApiWith(url: MyURLs.contacts.url,
                                    methodType: .post,
                                    params: param,
                                    success: { (json) in
                                        
                                        if let data = try? JSONSerialization.data(withJSONObject: json) {
                                            do {
                                                let result = try self.decoder.decode(User_Base.self, from: data)
                                                print("res",result)
                                                didDataReady(result)
                                            }catch{
                                                print("error\(error)")
                                            }
                                            
                                        }
        }) { (error) in
           print(error)
            errorCompletion(error)
        }
    }
    
    
}
