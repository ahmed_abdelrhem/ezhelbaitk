//
//  ContactVCPresenter.swift
//  Ezhal baitk
//
//  Created by a7med on 6/2/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
class ContactVCPresenter {
    private let interactor = ContactVCInteractor()

    
    weak var view: (ButtonDelegate&PresentDelegate&JwtDelegate)?
    init(view:(ButtonDelegate&PresentDelegate&JwtDelegate)) {
        self.view = view
    }
    
   
    
  
    func save(username: String,phone: String,problem: String) {
        view?.button_start()
        interactor.save(username: username,phone: phone,problem: problem, didDataReady: { [weak self](model) in
             
            if model.status == API_status.Success.rawValue{

                self!.view?.PresentVC(model.message  ?? "")

                
            }else if model.status == API_status.JwtExpired.rawValue {
                // must go to login
                self!.view?.jwtExpired(model.message  ?? "")
                
            }else{
                self?.view?.onFailure(model.message  ?? "")
            }
            self?.view?.button_stop()
            
        }) { [weak self](error) in
             
            self?.view?.button_stop()
            self?.view?.onConnection()
            self?.view?.onFailure(error?.errorDescription)
        }
        
    }
    
}
