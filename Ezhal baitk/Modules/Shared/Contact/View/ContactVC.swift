//
//  ContactVC.swift
//  Ezhal baitk
//
//  Created by a7med on 6/2/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

class ContactVC:BaseViewController{
    internal var presenter: ContactVCPresenter!

    @IBOutlet weak var problem: UITextViewX!
    @IBOutlet weak var phone: UITextFieldX!
    @IBOutlet weak var name: UITextFieldX!
    @IBOutlet weak var saveBtn: TransitionButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        sideMenu()
        name.forceSwitchingRegardlessOfTag = true
        phone.forceSwitchingRegardlessOfTag = true
        problem.forceSwitchingRegardlessOfTag = true

               // Do any additional setup after loading the view.
               presenter = ContactVCPresenter(view: self)
    }
    
    @IBAction func saveBtnPressed(_ sender: TransitionButton) {
           guard  name.text != "" else {
               Messages.instance.showMessage(title: _Info, body: "\(_Please) \(_Name).", state: .info, layout: .messageView)
               return
           }
           
           guard  phone.text != "" else {
               Messages.instance.showMessage(title: _Info, body: "\(_Please) \(_Phone).", state: .info, layout: .messageView)
               return
           }
        guard  problem.text != "" else {
                   Messages.instance.showMessage(title: _Info, body: "\(_Please) \(_Problem).", state: .info, layout: .messageView)
                   return
               }
        
           
        presenter.save(username: name.text ?? "", phone: phone.text ?? "", problem: problem.text ?? "")

    }
    

}
