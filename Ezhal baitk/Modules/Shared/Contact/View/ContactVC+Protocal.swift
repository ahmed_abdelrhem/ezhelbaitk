//
//  ContactVC+Protocal.swift
//  Ezhal baitk
//
//  Created by a7med on 6/2/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
extension ContactVC:ButtonDelegate,PresentDelegate,JwtDelegate{
func onConnection() {
      Messages.instance.showMessage(title: "", body: CONNECTION_ERROR , state: .error, layout: .messageView)
}

func onFailure<T>(_ msg: T) {
    Messages.instance.showMessage(title: "", body: msg as! String, state: .error, layout: .messageView)

}

func PresentVC<T>(_ model: T) {
    saveBtn.stopAnimation(animationStyle: .normal, revertAfterDelay: 0.5) {[weak self] in
        self?.PresentHomeVC(msg: model as? String)
    }
    
}





func jwtExpired<T>(_ model: T) {
     alert_login(_Login)
}

func button_start() {
    saveBtn.startAnimation()

}

func button_stop() {
    saveBtn.stopAnimation()

    
}
}
