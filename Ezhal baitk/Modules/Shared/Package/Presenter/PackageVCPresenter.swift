//
//  PackageVCPresenter.swift
//  Ezhal baitk
//
//  Created by a7med on 6/5/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
class PackageVCPresenter {
    private let interactor = PackageVCInteractor()
    private var list = [PackageData]()
    weak var view: (LoaderJwtDelegate)?
    init(view:LoaderJwtDelegate) {
        self.view = view
    }
    
    func viewdidload()  {
        view?.loader_start()
        interactor.getJobs(didDataReady: { [weak self](model) in
            guard self != nil else { return }
            if model.status == API_status.Success.rawValue{
                self?.list = model.data ?? []
                if self!.list.count == 0{
                    self?.view?.onEmpty()
                }else{
                    self!.view?.onSuccess("")
                }
            }else if model.status == API_status.JwtExpired.rawValue{
                self!.view?.jwtExpired(model.message ?? "")
            }else{
                
                self?.view?.onFailure(model.message ?? "")
            }
            self?.view?.loader_stop()
            
        }) { [weak self](error) in
            
            guard self != nil else { return }
            self?.view?.loader_stop()
            self?.view?.onConnection()
            self?.view?.onFailure(error?.errorDescription)
        }
    }
    func getlistCount() ->Int {
        return list.count
    }
    
    func configureCell(cell:OfferCellView,at row: Int)  {
        let item = list[row]
        cell.setTitle(item.name ?? "")
        cell.setBtn("btn\(row)")
    }
    
    
    func diselect(_ row:Int) -> Int {
        return list[row].id ?? 0
    }
    
    
    
    
}
