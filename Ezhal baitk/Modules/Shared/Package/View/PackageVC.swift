//
//  PackageVC.swift
//  Ezhal baitk
//
//  Created by a7med on 6/5/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

class PackageVC:BaseViewController{
    var sub_id:Int?
    var cat_id:Int?
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.tableFooterView = UIView(frame: .zero)
                   tableView.dataSource = self
                   tableView.delegate = self
                   tableView.separatorStyle = .none
                   tableView.rowHeight = UITableView.automaticDimension
                   tableView.register(UINib(nibName: "OfferCell", bundle: nil), forCellReuseIdentifier: "OfferCell")
        }
    }
    internal var presenter : PackageVCPresenter!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter = PackageVCPresenter(view: self)
        presenter.viewdidload()
        title = "Offers & Packages".localized

    }
    


}
