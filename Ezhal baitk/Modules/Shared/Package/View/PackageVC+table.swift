//
//  PackageVC+table.swift
//  Ezhal baitk
//
//  Created by a7med on 6/5/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
extension PackageVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getlistCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OfferCell", for: indexPath)  as! OfferCell
        presenter.configureCell(cell: cell, at: indexPath.row)
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc  = UIStoryboard(name: AppStoryboard.Shared.rawValue, bundle: nil).instantiateViewController(withIdentifier: "NewSubscriptionVC") as! NewSubscriptionVC
        vc.id =   presenter.diselect(indexPath.row)
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

extension PackageVC:OfferCellDelegate{
    func offerBtnPressed(_ sender: UITableViewCell) {
        let ind = tableView.indexPath(for: sender)
        let vc  = UIStoryboard(name: AppStoryboard.Shared.rawValue, bundle: nil).instantiateViewController(withIdentifier: "NewSubscriptionVC") as! NewSubscriptionVC
        vc.id =   presenter.diselect(ind!.row)
        vc.cat_id = cat_id
        navigationController?.pushViewController(vc, animated: true)    }
    
    
    
}
