//
//  ByGrandVC.swift
//  tofa7a
//
//  Created by OSX on 6/25/18.
//  Copyright © 2018 OSX. All rights reserved.
//

import UIKit

class ByGrandVC:BaseViewController{

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        showAnimate()
        
    }
    
    
    @IBAction func closeButton(_ sender: Any) {
        removeAnimation()
        
    }

    @IBAction func openSite(_ sender: Any) {
        if let url = URL(string: "http://2grand.net") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    @IBAction func call(_ sender: Any) {
        let url:NSURL = NSURL(string: "tel://00201157771069")!
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
    }

    @IBAction func whatsApp(_ sender: Any) {
        
                let msg = "Hello"
        let whatsappURL = URL(string: "https://api.whatsapp.com/send?phone=+201157771069&text=\(msg)")
        if UIApplication.shared.canOpenURL(whatsappURL!) {
            UIApplication.shared.open(whatsappURL!, options: [:], completionHandler: nil)
        }else{
            Messages.instance.showMessage(title: "Info", body: "Please Install WhatsApp", state: .info, layout: .cardView)
        }



    }
    
    
}
