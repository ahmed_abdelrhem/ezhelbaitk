//
//  SplashVC.swift
//  Ezhal baitk
//
//  Created by a7med on 5/9/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

class SplashVC:BaseViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if DEF.isLogin{
            perform(#selector(segueToHomeVC), with: nil, afterDelay: 0.5)
            
        }else{
            if DEF.isFristRun {
                perform(#selector(segueToLanguageVC), with: nil, afterDelay: 1.0)
            }else{
                perform(#selector(segueMainNavVC), with: nil, afterDelay: 1.0)
                
            }
        }
    }
    
    @objc func segueToLanguageVC() {
        let vc = UIStoryboard(name: AppStoryboard.Main.rawValue, bundle: .main).instantiateViewController(withIdentifier: "LanguageVC") as! LanguageVC
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: true, completion: nil)
        
    }
    @objc func segueToHomeVC() {
        let menu =  MenuVC()
        let tabs =  TabBarVC()
        let swRevealVC =  SWRevealViewController(rearViewController: menu, frontViewController: tabs)
        swRevealVC?.rightViewController = menu
        UIApplication.shared.keyWindow?.rootViewController = swRevealVC
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
        
        
        
    }
    @objc func segueMainNavVC() {
        guard let vc = UIStoryboard(name: AppStoryboard.Main.rawValue, bundle: .main).instantiateViewController(withIdentifier: "LoginVC") as? LoginVC else{return}
        let nav = UINavigationController(rootViewController: vc)
        
        UIApplication.shared.keyWindow?.rootViewController = nav
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
    }
    
    
    
}
