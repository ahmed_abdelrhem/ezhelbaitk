//
//  NewPasswordVCPresenter.swift
//  jaz-worker
//
//  Created by iMac on 11/27/19.
//  Copyright © 2019 Grand. All rights reserved.
//

import Foundation

import Foundation
class NewPasswordVCPresenter{
    private let interactor = AuthAPI()
       weak var view: (LoaderDelegate)?
       init(view:LoaderDelegate) {
           self.view = view
       }
    func updatePassword(_ phone:String,_ pw:String) {
        view?.loader_start()
        interactor.renew_password( newpassword: pw, didDataReady: { [weak self](model) in
            guard self != nil else { return }
                       if model.status == 200{
                        self?.view?.onSuccess(model.message  ?? "")
                       }else{

                           self?.view?.onFailure(model.message  ?? "")
                       }
                       self?.view?.loader_stop()
        }) { [weak self](error) in
            guard self != nil else { return }
                      self?.view?.loader_stop()
                      self?.view?.onFailure(CONNECTION_ERROR)
            
        }
    }
    
}
