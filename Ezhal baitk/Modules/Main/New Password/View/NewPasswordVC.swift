//
//  NewPasswordVC.swift
//  jaz-worker
//
//  Created by iMac on 11/27/19.
//  Copyright © 2019 Grand. All rights reserved.
//

import UIKit

class NewPasswordVC:BaseViewController{
    internal var presenter: NewPasswordVCPresenter!
    var phone = ""
    @IBOutlet weak var conpw: UITextFieldX!
    @IBOutlet weak var pw: UITextFieldX!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        presenter =  NewPasswordVCPresenter(view: self)
    }
    
    @IBAction func saveBtn(_ sender: UIButton) {
        guard  pw.text == conpw.text else {
                  Messages.instance.showMessage(title: "Info", body: "please, your confirm password must be match your password .", state: .info, layout: .messageView)
                  return
              }
        guard  pw.text != "" else {
            Messages.instance.showMessage(title: "Info", body: "please, your password must not be empty .", state: .info, layout: .messageView)
            return
        }
        presenter.updatePassword(phone, pw.text ?? "")
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
 */
     @IBAction func showPw(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.tag == 0 {
               pw.isSecureTextEntry  = !pw.isSecureTextEntry
        }else{
            conpw.isSecureTextEntry  = !conpw.isSecureTextEntry

        }
     }
    
    
}
