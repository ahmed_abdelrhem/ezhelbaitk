//
//  NewPasswordVC+Delegate.swift
//  jaz-worker
//
//  Created by iMac on 11/27/19.
//  Copyright © 2019 Grand. All rights reserved.
//

import Foundation
extension NewPasswordVC:LoaderDelegate{
    func onFailure<T>(_ msg: T) {
        Messages.instance.showMessage(title: "", body: msg as! String, state: .error, layout: .messageView)
        
    }
    
    func onSuccess<T>(_ msg: T) {
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC")
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .overFullScreen
        present(nav, animated: true) {
            DEF.logout()
            Messages.instance.showMessage(title:"", body: (msg as? String) ?? "", state: .success
                                          , layout: .messageView)
        }
    }
    
    func onEmpty() {
        return
    }
    
    func onConnection() {
        return
    }
    
    func loader_start() {
        startAnimating(ponit: nil)
    }
    
    func loader_stop() {
        stopAnimating()
    }
    
    func onFailure(_ msg: String) {
    }
    
    
    
    
}
