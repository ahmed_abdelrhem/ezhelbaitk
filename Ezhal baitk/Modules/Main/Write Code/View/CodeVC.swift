//
//  CodeVC.swift
//  Jaz-User
//
//  Created by a7med on 11/4/19.
//  Copyright © 2019 Grand. All rights reserved.
//

import UIKit

class CodeVC:BaseViewController{
    var phone = ""
    var is_activate = false
    internal var presenter: CodeVCPresenter!
    @IBOutlet weak var codetxtf: CBPinEntryView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        presenter = CodeVCPresenter(view: self)
    }
    
    @IBAction func submitBtnPressed(_ sender: UIButton) {
        guard  codetxtf.getPinAsString() != "" else {
            Messages.instance.showMessage(title: "Info", body: "please, you should enter code.", state: .info, layout: .messageView)
            return
        }
        presenter.check_code(codetxtf.getPinAsString())
        }
    }
    
