
//
//  CodeVCPresenter.swift
//  Jaz-User
//
//  Created by a7med on 11/5/19.
//  Copyright © 2019 Grand. All rights reserved.
//

import Foundation
class CodeVCPresenter {
    weak var view: (LoaderDelegate)?
       var interactor = AuthAPI()
       init(view: (LoaderDelegate)) {
           self.view = view
       }
    func check_code(_ code: String)  {
        interactor.check_code(code: code, didDataReady: { [weak self] (model) in
                       guard self != nil else { return }
                       if model.status == 200{
                        DEF.saveUserDefault(model.data!)
                        self!.view?.onSuccess(model.message)
                       }else{
                           
                           self?.view?.onFailure(model.message ?? "")
                       }
                       self?.view?.loader_stop()
                   }) { [weak self] (error) in
                       guard self != nil else { return }
                       self?.view?.loader_stop()
                       self?.view?.onFailure(CONNECTION_ERROR)
                   }
    }
    

}
