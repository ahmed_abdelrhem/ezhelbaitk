//
//  ForgetVC.swift
//  Jaz-User
//
//  Created by a7med on 11/4/19.
//  Copyright © 2019 Grand. All rights reserved.
//

import UIKit

class ForgetVC:BaseViewController{
    var presenter: ForgetVCPresenter!
    private let country_code = "+974"

    @IBOutlet weak var phonetxtf: UITextFieldX!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        presenter = ForgetVCPresenter(view: self)
        
    }
    
    
    @IBAction func submitBtnPressed(_ sender: UIButton) {
        guard  phonetxtf.text != "" else {
            Messages.instance.showMessage(title: _Info, body: "\(_Please) \(_Phone) \(_And) \(_Email).", state: .info, layout: .messageView)
            return
        }
       let key = (phonetxtf.text ?? "").isNum() ? country_code + (phonetxtf.text ?? "")  :  phonetxtf.text ?? ""
            presenter.send_code(key)
            
    }
    
}
