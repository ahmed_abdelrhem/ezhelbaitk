//
//  CodeVC+Delegate.swift
//  Jaz-User
//
//  Created by a7med on 11/5/19.
//  Copyright © 2019 Grand. All rights reserved.
//

import Foundation
extension ForgetVC:LoaderDelegate{
    func onFailure<T>(_ msg: T) {
          Messages.instance.showMessage(title: "", body: msg as! String, state: .error , layout: .messageView )
    }
    
    func onEmpty() {
        return
    }
    
    func onConnection() {
        return
    }
    
    func loader_start() {
        startAnimating(ponit: nil)
    }
    
    func loader_stop() {
        stopAnimating()
    }
    
 
    
    func onSuccess<T>(_ msg: T) {
        
            let vc = UIStoryboard(name: AppStoryboard.Main.rawValue, bundle: nil).instantiateViewController(withIdentifier: "CodeVC") as! CodeVC
            vc.phone = phonetxtf.text ?? ""
        vc.is_activate = false
            navigationController?.pushViewController(vc, animated: true)
            
        
    }
    
    
    
}
