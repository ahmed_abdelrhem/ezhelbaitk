

import Foundation

class ForgetVCPresenter {
    weak var view: (LoaderDelegate)?
       var interactor = AuthAPI()
    private (set) var code : Int?
       init(view: (LoaderDelegate)) {
           self.view = view
       }

    
    
    func send_code(_ phone: String)  {
             self.view?.loader_start()
             interactor.sendCode(phone: phone, didDataReady: { [weak self] (model) in
                            guard self != nil else { return }
                            if model.status == 200{
                                self!.view?.onSuccess(model)
                            }else{
                                
                                self?.view?.onFailure(model.message  ?? "")
                            }
                            self?.view?.loader_stop()
                        }) { [weak self] (error) in
                            guard self != nil else { return }
                            self?.view?.loader_stop()
                            self?.view?.onFailure(CONNECTION_ERROR)
                        }
         }
    
}
