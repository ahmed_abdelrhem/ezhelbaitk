

import UIKit

class PagesVC: UIPageViewController {
    
    private(set) lazy var orderedViewControllers: [UIViewController] = {
        return [self.newViewController("FirstViewController"),
                self.newViewController("SecondViewController"),
                self.newViewController("ThirdViewController"),
                self.newViewController("LoginVC",hasNavigation: true)]

    }()

    private func newViewController(_ serial: String,hasNavigation:Bool = false) -> UIViewController{
        if hasNavigation{
            let vc =  UIStoryboard(name: AppStoryboard.Main.rawValue , bundle: nil) .
                instantiateViewController(withIdentifier: "\(serial)")
            let nav = UINavigationController(rootViewController: vc)
            return nav
        }
        return UIStoryboard(name: AppStoryboard.Main.rawValue , bundle: nil) .
            instantiateViewController(withIdentifier: "\(serial)")
    }

  
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        
                
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        }

    }
    
    
    
}


extension PagesVC: UIPageViewControllerDataSource {

    private func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = UIColor.gray
        appearance.currentPageIndicatorTintColor = UIColor.white
        appearance.backgroundColor = UIColor.darkGray
    }

    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        setupPageControl()
        return orderedViewControllers.count
    }

    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
            guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
                return nil
            }
            let previousIndex = viewControllerIndex - 1
            guard previousIndex >= 0 else {
                return nil
            }
            guard orderedViewControllers.count > previousIndex else {
                return nil
            }
            return orderedViewControllers[previousIndex]
    }

    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
            guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
                return nil
            }
            let nextIndex = viewControllerIndex + 1
            let orderedViewControllersCount = orderedViewControllers.count
            guard orderedViewControllersCount != nextIndex else {
                return nil
            }
            guard orderedViewControllersCount > nextIndex else {
                return nil
            }
            return orderedViewControllers[nextIndex]
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        guard let firstViewController = viewControllers?.first, let firstViewControllerIndex = orderedViewControllers.firstIndex(of: firstViewController) else {
                return 0
        }
        return firstViewControllerIndex
    }

    
}
