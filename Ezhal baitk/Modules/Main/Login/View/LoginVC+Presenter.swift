//
//  LoginVC+Delegate.swift
//  Jaz-User
//
//  Created by iMac on 10/27/19.
//  Copyright © 2019 Grand. All rights reserved.
//

import UIKit
extension LoginVC:LoginVCView{
    func activatYourAcount(_ phone: String) {
          singBtn.stopAnimation(animationStyle: .normal, revertAfterDelay: 0.5) { [weak self] in
                  let vc = UIStoryboard(name: AppStoryboard.Main.rawValue, bundle: .main).instantiateViewController(withIdentifier: "CodeVC") as! CodeVC
                  vc.is_activate = true
            vc.phone = phone
                  self!.navigationController?.pushViewController(vc, animated: true)
                  
              }
    }
    
    func onConnection() {
        Messages.instance.showMessage(title: "", body: CONNECTION_ERROR, state: .error, layout: .messageView)
    }
    
 
    func PresentVC<T>(_ model: T) {
        singBtn.stopAnimation(animationStyle: .normal, revertAfterDelay: 0.5) {[weak self] in
            self?.PresentHomeVC(msg: model as? String)
        }
    }
    
    func onFailure<T>(_ msg: T) {
        Messages.instance.showMessage(title: "", body: msg as! String, state: .error, layout: .messageView)
    }
    
    
    
    
    func loader_start() {
        singBtn.startAnimation()
        
    }
    
    func loader_stop() {
        singBtn.stopAnimation()
        
    }
    
    
    
}
