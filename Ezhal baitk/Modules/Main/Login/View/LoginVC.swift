//
//  LoginVC.swift
//  Jaz-User
//
//  Created by iMac on 10/27/19.
//  Copyright © 2019 Grand. All rights reserved.
//

import UIKit


class LoginVC:BaseViewController{
    internal var presenter: LoginVCPresenter!
    private let country_code = "+974"
    @IBOutlet weak var phonetxtf: UITextFieldX!
    @IBOutlet weak var forgetBtn: UIButton!
    @IBOutlet weak var newAccountBtn: UIButton!
    @IBOutlet weak var passwordtxtf: UITextFieldX!
    @IBOutlet weak var singBtn: TransitionButton!
    @IBOutlet weak var showBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        transparent_nav()
        passwordtxtf.forceSwitchingRegardlessOfTag = true
        phonetxtf.forceSwitchingRegardlessOfTag = true
        // Do any additional setup after loading the view.
        presenter = LoginVCPresenter(view: self)
        //        navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func signBtnPressed(_ sender: TransitionButton) {
        guard  phonetxtf.text != "" else {
            Messages.instance.showMessage(title: _Info, body: "\(_Please) \(_Phone) \(_And) \(_Email).", state: .info, layout: .messageView)
            return
        }
        
        guard  passwordtxtf.text != "" else {
            Messages.instance.showMessage(title: _Info, body: "\(_Please) \(_Password).", state: .info, layout: .messageView)
            return
        }
     
        let key = (phonetxtf.text ?? "").isNum() ? country_code + (phonetxtf.text ?? "")  :  phonetxtf.text ?? ""
        presenter.login(key, passwordtxtf.text ?? "")
    }
    @IBAction func newAccountBtnPressed(_ sender: UIButton) {
        let vc = UIStoryboard(name: AppStoryboard.Main.rawValue, bundle: nil).instantiateViewController(withIdentifier: "RegisterVC") 
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func forgetBtnPressed(_ sender: UIButton) {
        let vc = UIStoryboard(name: AppStoryboard.Main.rawValue, bundle: nil).instantiateViewController(withIdentifier: "ForgetVC") as! ForgetVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
   
    @IBAction func showPwBtnPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        passwordtxtf.isSecureTextEntry  = !passwordtxtf.isSecureTextEntry
    }
    
    @IBAction func skipBtnPressed(_ sender: UIButton) {
        PresentHomeVC()

    }
    
}

