//
//  LoginPresenter.swift
//  Jaz-User
//
//  Created by iMac on 10/27/19.
//  Copyright © 2019 Grand. All rights reserved.
//

import Foundation
protocol LoginVCView: PresentDelegate {
    func loader_start()
    func loader_stop()
    func activatYourAcount(_ phone:String) 
    
}

class LoginVCPresenter {
    private let interactor = LoginVCInteractor()
    weak var view: (LoginVCView)?
    init(view:LoginVCView) {
        self.view = view
    }
    
    func login(_ key: String,_ password: String)  {
        view?.loader_start()
        interactor.login(key: key, password: password, didDataReady: { [weak self](model) in
            guard self != nil else { return }
            if model.status == API_status.Success.rawValue{
                DEF.saveUserDefault(model.data!)
                self?.view?.PresentVC(model.message  ?? "")
                
            }
            else if model.status == API_status.NotActive.rawValue{
                self!.interactor.sendCode(phone: key, didDataReady: {  [weak self](model) in
                    self?.view?.activatYourAcount(key)
                }) { [weak self](error) in
                    guard self != nil else { return }
                    self?.view?.loader_stop()
                }
            }
            else{
                
                self?.view?.onFailure(model.message  ?? "")
                self?.view?.loader_stop()
                
            }
            
        }) { [weak self](error) in
            
            guard self != nil else { return }
            self?.view?.loader_stop()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }
    
}
