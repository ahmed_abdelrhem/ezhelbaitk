/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct UserData : Codable {
	let id : Int?
	let name : String?
	let email : String?
	let phone : String?
	let image : String?
	let password : String?
	let jwt_token : String?
	let verify_code : String?
	let firebase_token : String?
	let address : String?
	let lat : String?
	let lng : String?
	let user_subscriptions : [User_subscriptions]?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
		case email = "email"
		case phone = "phone"
		case image = "image"
		case password = "password"
		case jwt_token = "jwt_token"
		case verify_code = "verify_code"
		case firebase_token = "firebase_token"
		case address = "address"
		case lat = "lat"
		case lng = "lng"
		case user_subscriptions = "user_subscriptions"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		email = try values.decodeIfPresent(String.self, forKey: .email)
		phone = try values.decodeIfPresent(String.self, forKey: .phone)
		image = try values.decodeIfPresent(String.self, forKey: .image)
		password = try values.decodeIfPresent(String.self, forKey: .password)
		jwt_token = try values.decodeIfPresent(String.self, forKey: .jwt_token)
		verify_code = try values.decodeIfPresent(String.self, forKey: .verify_code)
		firebase_token = try values.decodeIfPresent(String.self, forKey: .firebase_token)
		address = try values.decodeIfPresent(String.self, forKey: .address)
		lat = try values.decodeIfPresent(String.self, forKey: .lat)
		lng = try values.decodeIfPresent(String.self, forKey: .lng)
		user_subscriptions = try values.decodeIfPresent([User_subscriptions].self, forKey: .user_subscriptions)
	}

}
