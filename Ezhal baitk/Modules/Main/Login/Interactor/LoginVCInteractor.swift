//
//  LoginVCInteractor.swift
//  Moon Shop
//
//  Created by apple on 1/22/20.
//  Copyright © 2020 2grand. All rights reserved.
//

import Foundation
import Alamofire
class LoginVCInteractor {
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias user_model = (User_Base)->()

    func login(key: String,password: String, didDataReady: @escaping user_model, andErrorCompletion errorCompletion: @escaping errorType) {
        
        let param = [
            "key": key,
            "password": password,
            "firebase_token": DEF.FireBaseToken
            ] as [String : Any]
        print("param",param)
        apiManager.contectToApiWith(url: MyURLs.Login.url,
                                    methodType: .post,
                                    params: param,
                                    success: { (json) in
                                        print("==>jsn",json)
                                        if let data = try? JSONSerialization.data(withJSONObject: json) {
                                            
                                            do {
                                                let result = try self.decoder.decode(User_Base.self, from: data)
                                                didDataReady(result)

                                            }catch{
                                                 print("##error\(error)")
                        let error = AFError.responseSerializationFailed(reason: .decodingFailed(error: error))
                        errorCompletion(error)

                                            }
                                          
                                        }
        }) { (error) in
           print(error)
            errorCompletion(error)
        }
    }
    
    func sendCode(phone:String, didDataReady: @escaping user_model,andErrorCompletion errorCompletion: @escaping errorType)  {
            // for register
               let param = [
                   "key":phone
          ]
               apiManager.contectToApiWith(url: MyURLs.forget_password.url,
                                           methodType: .post,
                                           params: param,
                                           success: { (json) in
                                               print(json)
                                               if let data = try? JSONSerialization.data(withJSONObject: json) {
                                                   
                                                   do {
                                                       let result = try self.decoder.decode(User_Base.self, from: data)
                                                       didDataReady(result)
                                                       
                                                   }catch{
                                                       print("model parse error\(error)")
                                                       
                                                   }
                                                   
                                               }
               }) { (error) in
                  print(error)
                   errorCompletion(error)
               }
               
           }
 
}












