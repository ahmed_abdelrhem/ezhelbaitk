
//

import UIKit
extension RegisterVC:LoginVCView{
    func activatYourAcount(_ phone: String) {
        print("activatYourAcount")
        let vc = UIStoryboard(name: AppStoryboard.Main.rawValue, bundle: nil).instantiateViewController(withIdentifier: "CodeVC") as! CodeVC
            vc.phone = phone
              vc.is_activate = true
                  navigationController?.pushViewController(vc, animated: true)
    }
    
    func onConnection() {
        Messages.instance.showMessage(title: "", body: CONNECTION_ERROR, state: .error, layout: .messageView)
    }
    
 
    func PresentVC<T>(_ model: T) {
        signupBtn.stopAnimation(animationStyle: .normal, revertAfterDelay: 0.5) {[weak self] in
            self?.PresentHomeVC(msg: model as? String)
        }
    }
    
    func onFailure<T>(_ msg: T) {
        Messages.instance.showMessage(title: "", body: msg as! String, state: .error, layout: .messageView)
    }
    
    
    
    
    func loader_start() {
        signupBtn.startAnimation()
        
    }
    
    func loader_stop() {
        signupBtn.stopAnimation()
        
    }
    
    
    
}
