//
//  RegisterVC.swift
//  Ezhal baitk
//
//  Created by a7med on 6/2/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

class RegisterVC:BaseViewController{
    internal var presenter: RegisterVCPresenter!
    private let country_code = "+974"
    
    @IBOutlet weak var countLbl: UILabel!
    private var location : (lat:String,lng:String)?
    @IBOutlet weak var address: UITextFieldX!
    @IBOutlet weak var password: UITextFieldX!
    @IBOutlet weak var email: UITextFieldX!
    @IBOutlet weak var mobile: UITextFieldX!
    @IBOutlet weak var name: UITextFieldX!
    @IBOutlet weak var signupBtn: TransitionButton!
    
    @IBOutlet weak var agreeBtn: UIButtonX!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        transparent_nav()
        password.forceSwitchingRegardlessOfTag = true
        email.forceSwitchingRegardlessOfTag = true
        mobile.forceSwitchingRegardlessOfTag = true
        name.forceSwitchingRegardlessOfTag = true
        address.forceSwitchingRegardlessOfTag = true

        presenter = RegisterVCPresenter(view: self)

        
        
    }
    
    @IBAction func changeEditing(_ sender: UITextField) {
        let txt  = sender.text ?? ""

        if txt.count > 8 {
            sender.text?.removeLast(1)
            sender.endEditing(true)
        }else{
            countLbl.text = "\(txt.count)/8"
        }
    }
    
    @IBAction func beginEditing(_ sender: UITextField) {
        let txt  = sender.text ?? ""
        if txt.count > 8 {
                  sender.text?.removeLast(1)
                  sender.endEditing(true)
              }
    }
    
    @IBAction func agreeBtnPressed(_ sender: UIButtonX) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func TermsBtnPressed(_ sender: UIButton) {
        let vc = TermsVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func signinBtnPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func showBtnPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
              password.isSecureTextEntry  = !password.isSecureTextEntry
    }
    
    @IBAction func addressBtnPressed(_ sender: UIButton) {
        let vc = MapVC()
              vc.delegate = self
              navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func signupBtnPressed(_ sender: TransitionButton) {
        if !agreeBtn.isSelected {
                    Messages.instance.showMessage(title: _Info, body: "\(_AcceptTerms) .", state: .info, layout: .messageView)
            return
        }
           guard  mobile.text != "" else {
               Messages.instance.showMessage(title: _Info, body: "\(_Please) \(_Mobile).", state: .info, layout: .messageView)
               return
           }
           
           guard  password.text != "" else {
               Messages.instance.showMessage(title: _Info, body: "\(_Please) \(_Password).", state: .info, layout: .messageView)
               return
           }
        guard  name.text != "" else {
               Messages.instance.showMessage(title: _Info, body: "\(_Please) \(_Name).", state: .info, layout: .messageView)
               return
           }
           
           guard  email.text != "" else {
               Messages.instance.showMessage(title: _Info, body: "\(_Please) \(_Email).", state: .info, layout: .messageView)
               return
           }
        guard  address.text != "" else {
                      Messages.instance.showMessage(title: _Info, body: "\(_Please) \(_Address).", state: .info, layout: .messageView)
                      return
                  }
        
        
           
        presenter.signUp(name: name.text ?? "", email: email.text ?? "", phone: country_code + (mobile.text ?? ""), address: address.text ?? "", password: password.text ?? "", location: location ?? ("0","0"))

    }
}
extension RegisterVC:LocationDelegate{
  
    func RetriveLocation(lat: Double, lng: Double, add: String,country: String){
        print("#MyAddress",add)
        self.location = ("\(lat)","\(lng)")
        address.text = add
    }
    
    
}
