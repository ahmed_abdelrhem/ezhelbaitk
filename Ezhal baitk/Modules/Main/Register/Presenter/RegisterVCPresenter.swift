//
//  RegisterVCPresenter.swift
//  Ezhal baitk
//
//  Created by a7med on 6/2/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
class RegisterVCPresenter {
    private let interactor = RegisterVCInteractor()
    weak var view: (LoginVCView)?
    init(view:LoginVCView) {
        self.view = view
    }
    
    func signUp(name: String,email: String,phone: String,address: String,password: String,location:(lat:String,lng:String))  {
        view?.loader_start()
        interactor.signUp(name: name,email: email,phone: phone,address: address,password: password,location:location, didDataReady: { [weak self](model) in
            guard self != nil else { return }
            if model.status == API_status.Success.rawValue{
//                DEF.saveUserDefault(model.data!)
                self?.view?.activatYourAcount(phone)
                                             
            }
            else{
                
                self?.view?.onFailure(model.message  ?? "")
                self?.view?.loader_stop()

            }
            
        }) { [weak self](error) in
            
            guard self != nil else { return }
            self?.view?.loader_stop()
            self?.view?.onFailure(CONNECTION_ERROR)
        }
    }
    
}
