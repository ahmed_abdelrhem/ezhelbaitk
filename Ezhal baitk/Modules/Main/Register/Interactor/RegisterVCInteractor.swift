//
//  RegisterVCInteractor.swift
//  Ezhal baitk
//
//  Created by a7med on 6/2/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
import Alamofire
class RegisterVCInteractor {
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias user_model = (User_Base)->()
    
    
     func sendCode(phone:String, didDataReady: @escaping user_model,andErrorCompletion errorCompletion: @escaping errorType)  {
           // for register
              let param = [
                  "key":phone
         ]
              print("code_send ==>\(param)")
           print("url",MyURLs.forget_password.url)
              apiManager.contectToApiWith(url: MyURLs.forget_password.url,
                                          methodType: .post,
                                          params: param,
                                          success: { (json) in
                                              print(json)
                                              if let data = try? JSONSerialization.data(withJSONObject: json) {
                                                  
                                                  do {
                                                      let result = try self.decoder.decode(User_Base.self, from: data)
                                                      didDataReady(result)
                                                      
                                                  }catch{
                                                      print("model parse error\(error)")
                                                      
                                                  }
                                                  
                                              }
              }) { (error) in
                 print(error)
                  errorCompletion(error)
              }
              
          }

    func signUp(name: String,email: String,phone: String,address: String,password: String,location:(lat:String,lng:String), didDataReady: @escaping user_model, andErrorCompletion errorCompletion: @escaping errorType) {
        
        let param = [
            "name": name,
            "email": email,
            "phone": phone,
            "address": address,
            "lat": location.lat,
            "lng": location.lng,
            "password": password,
            "social_type": 0,
            "social_token": "social_token",
            "firebase_token": DEF.FireBaseToken
            ] as [String : Any]
        print("param",param)
        apiManager.contectToApiWith(url: MyURLs.Register.url,
                                    methodType: .post,
                                    params: param,
                                    success: { (json) in
                                        print("==>jsn",json)
                                        if let data = try? JSONSerialization.data(withJSONObject: json) {
                                            
                                            do {
                                                let result = try self.decoder.decode(User_Base.self, from: data)
                                                didDataReady(result)

                                            }catch{
                                                 print("##error\(error)")
                        let error = AFError.responseSerializationFailed(reason: .decodingFailed(error: error))
                        errorCompletion(error)

                                            }
                                          
                                        }
        }) { (error) in
           print(error)
            errorCompletion(error)
        }
    }
 
}
