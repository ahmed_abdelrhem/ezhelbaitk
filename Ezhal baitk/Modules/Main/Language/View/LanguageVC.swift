//
//  LanguageVC.swift
//  a3ln
//
//  Created by a7med on 3/23/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit
import MOLH
class LanguageVC:BaseViewController{
    var WithMenu = false
    @IBOutlet weak var arBtn: UIButtonX!
    @IBOutlet weak var engBtn: UIButtonX!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if WithMenu {
                   sideMenu()
            transparent_nav()

               }
        
    }
    
    
    
    @IBAction func engBtnPressed(_ sender: UIButton) {
            if MOLHLanguage.currentAppleLanguage() == DEF.language && DEF.isFristRun {
                DEF.isFristRun = false
                let vc  = UIStoryboard(name: AppStoryboard.Main.rawValue, bundle: .main).instantiateViewController(withIdentifier: "PagesVC") as! PagesVC
                vc.modalPresentationStyle = .overFullScreen
                present(vc, animated: true, completion: nil)
            }else{
                  Messages.instance.demoCentered(title: "", body: _RestLanguage.localized, buttonTitle: _Ok.localized) {[weak self] (success) in
                          if success {
                            DEF.language = "en"
                            MOLH.setLanguageTo(DEF.language)
                              MOLH.reset()
                            exit(0)

                          }
                      }
                
        }
    }
    
    
    @IBAction func arBtn(_ sender: UIButton) {
        
            if MOLHLanguage.currentAppleLanguage() == DEF.language && DEF.isFristRun {
                DEF.isFristRun = false
                let vc  = UIStoryboard(name: AppStoryboard.Main.rawValue, bundle: .main).instantiateViewController(withIdentifier: "PagesVC")  as! PagesVC
                vc.modalPresentationStyle = .overFullScreen
                present(vc, animated: true, completion: nil)
            }else{
                  Messages.instance.demoCentered(title: "", body: _RestLanguage.localized, buttonTitle: _Ok.localized) {[weak self] (success) in
                                          if success {
                                            DEF.language = "ar"
                                            MOLH.setLanguageTo(DEF.language)
                                              MOLH.reset()
                                            exit(0)
                                          }
                                      }
                
        }
        
        
    }
    
   
}
