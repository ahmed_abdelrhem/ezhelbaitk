//
//  TermsVC+Presenter.swift
//  Ezhal baitk
//
//  Created by iMac on 10/19/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
extension TermsVC:LoaderDelegate{
    func onFailure<T>(_ msg: T) {
        Messages.instance.showMessage(title: "", body: msg as! String, state: .error, layout: .messageView)

    }
    
    func onEmpty() {
        setEmptyView()
    }
    
    func onConnection() {
        setConnectionView()
    }
    
    func onSuccess<T>(_ msg: T) {
        guard let dt = msg as? String else{return}
        termsLbl.text = "\(dt ) "

    }
    
    func loader_start() {
        startAnimating(ponit: nil)
        termsLbl.isHidden = true

    }
    
    func loader_stop() {
        stopAnimating()
        termsLbl.isHidden = false
    }
    
 
    
    
    
}
