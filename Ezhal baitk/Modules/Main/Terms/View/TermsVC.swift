//
//  TermsVC.swift
//  Ezhal baitk
//
//  Created by iMac on 10/19/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import UIKit

class TermsVC: UIViewController {

    private (set) var presenter: TermsVCPresenter!
    
    @IBOutlet weak var termsLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter = TermsVCPresenter(view: self)
        presenter.viewDidLoad()
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
