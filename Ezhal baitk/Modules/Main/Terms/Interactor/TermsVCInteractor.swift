//
//  TermsVCInteractor.swift
//  Ezhal baitk
//
//  Created by iMac on 10/19/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
class TermsVCInteractor {
    
    
    
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias model = (Terms_Base)->()
    
    
    func getTerms (didDataReady: @escaping model, andErrorCompletion errorCompletion: @escaping errorType) {
        apiManager.contectToApiWith(url: MyURLs.terms.url,
                                    methodType: .get,
                                    params: nil,
                                    success: { (json) in
                                        print(json)
                                        if let data = try? JSONSerialization.data(withJSONObject: json) {
                                            
                                            do {
                                                let result = try self.decoder.decode(Terms_Base.self, from: data)
                                                didDataReady(result)
                                                
                                            }catch{
                                                print("model parse error\(error)")
                                                
                                            }
                                            
                                        }
        }) { (error) in
           print(error)
            errorCompletion(error)
        }
    }
    
    
    
    
}
