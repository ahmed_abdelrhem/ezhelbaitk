//
//  TermsVCPresenter.swift
//  Ezhal baitk
//
//  Created by iMac on 10/19/20.
//  Copyright © 2020 a7med. All rights reserved.
//

import Foundation
class TermsVCPresenter {
    private weak var view: (LoaderDelegate)?
    private var interactor = TermsVCInteractor()
    init(view: LoaderDelegate) {
        self.view = view
    }
    
    func viewDidLoad()  {
        view?.loader_start()
        interactor.getTerms( didDataReady: { [weak self](model) in
             
            if model.status == API_status.Success.rawValue{
                if model.data == nil{
                    self?.view?.onEmpty()
                }else{
                    self!.view?.onSuccess(model.data)
                }
            }else{
                
                self?.view?.onFailure(model.message ?? "")
            }
            self?.view?.loader_stop()
            
        }) { [weak self](error) in
            
             
            self?.view?.loader_stop()
            self?.view?.onFailure(error?.errorDescription)
        }
    }
    
    
    
    
}
