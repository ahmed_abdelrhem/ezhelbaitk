//
//  LoginVCAPI.swift
//  Jaz-User
//
//  Created by iMac on 10/27/19.
//  Copyright © 2019 Grand. All rights reserved.
//

import Foundation
import  Alamofire
class AuthAPI {
    
    private let apiManager = APIManager()
    private let decoder = JSONDecoder()
    typealias model = (User_Base)->()
    
    func renew_password(newpassword:String, didDataReady: @escaping model,andErrorCompletion errorCompletion: @escaping errorType)  {
        let param = [
            "password":newpassword,
            "password_confirmation": newpassword
            ] as [String : Any]
        
        print("code_send ==>\(param)")
        apiManager.contectToApiWith(url: MyURLs.change_password.url,
                                    methodType: .post,
                                    params: param,
                                    success: { (json) in
                                        print(json)
                                        if let data = try? JSONSerialization.data(withJSONObject: json) {
                                            
                                            do {
                                                let result = try self.decoder.decode(User_Base.self, from: data)
                                                didDataReady(result)
                                                
                                            }catch{
                                                print("model parse error\(error)")
                                                
                                            }
                                            
                                        }
        }) { (error) in
           print(error)
            errorCompletion(error)
        }
        
    }
    
    
    func sendCode(phone:String, didDataReady: @escaping model,andErrorCompletion errorCompletion: @escaping errorType)  {
          // for register
             let param = [
                 "key":phone
        ]
             print("code_send ==>\(param)")
          print("url",MyURLs.forget_password.url)
             apiManager.contectToApiWith(url: MyURLs.forget_password.url,
                                         methodType: .post,
                                         params: param,
                                         success: { (json) in
                                             print(json)
                                             if let data = try? JSONSerialization.data(withJSONObject: json) {
                                                 
                                                 do {
                                                     let result = try self.decoder.decode(User_Base.self, from: data)
                                                     didDataReady(result)
                                                     
                                                 }catch{
                                                     print("model parse error\(error)")
                                                     
                                                 }
                                                 
                                             }
             }) { (error) in
                print(error)
                 errorCompletion(error)
             }
             
         }

    
    
   
    
    
    
    func check_code(code:String, didDataReady: @escaping model,andErrorCompletion errorCompletion: @escaping errorType)  {
        let param = [
        "verify_code":code
        ]
        
        print("code_send ==>\(param)")
        apiManager.contectToApiWith(url: MyURLs.code_check.url,
                                       methodType: .post,
                                       params: param,
                                       success: { (json) in
                                           print(json)
                                           if let data = try? JSONSerialization.data(withJSONObject: json) {
                                               
                                               do {
                                                   let result = try self.decoder.decode(User_Base.self, from: data)
                                                   didDataReady(result)

                                               }catch{
                                                   print("model parse error\(error)")

                                               }
                                             
                                           }
           }) { (error) in
              print(error)
            errorCompletion(error)
           }
        
    }


    
    
  
    
    func update_password(oldpassword:String,newpassword:String, didDataReady: @escaping model,andErrorCompletion errorCompletion: @escaping errorType)  {
           let param = [
           "old_password":oldpassword,
           "new_password":newpassword
            ] as [String : Any]
           
           apiManager.contectToApiWith(url: MyURLs.change_password.url,
                                          methodType: .post,
                                          params: param,
                                          success: { (json) in
                                              print(json)
                                              if let data = try? JSONSerialization.data(withJSONObject: json) {
                                                  
                                                  do {
                                                      let result = try self.decoder.decode(User_Base.self, from: data)
                                                      didDataReady(result)

                                                  }catch{
                                                      print("model parse error\(error)")

                                                  }
                                                
                                              }
              }) { (error) in
                 print(error)
               errorCompletion(error)
              }
           
       }
    
    
    
}
