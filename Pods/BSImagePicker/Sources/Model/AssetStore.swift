// The MIT License (MIT)
//
// Copyright (c) 2019 Joakim Gyllström
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import Foundation
import Photos

@objcMembers public class AssetStore : NSObject {
    public private(set) var assets: [Asset]
    
    public init(assets: [Asset] = []) {
        self.assets = assets
    }
    
    public var count: Int {
        return assets.count
    }
    
    func contains(_ asset: Asset) -> Bool {
        return assets.contains(asset)
    }
    
    func append(_ asset: Asset) {
        guard contains(asset) == false else { return }
        assets.append(asset)
    }
    
    func remove(_ asset: Asset) {
        guard let index = assets.firstIndex(of: asset) else { return }
        assets.remove(at: index)
    }
    
    func removeFirst() -> Asset? {
        return assets.removeFirst()
    }
    
    func index(of asset: Asset) -> Int? {
        return assets.firstIndex(of: asset)
    }
}



@objcMembers public class Asset :Equatable {
    
    
    public private(set) var asset: PHAsset
    public private(set) var image: UIImage? = .none
    public private(set) var url: URL? = .none
    public private(set) var data: Data? = .none

    
    public init(asset: PHAsset) {
        self.asset = asset
        PHImageManager.default().requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: nil) { [weak self] (image, info) in
            // Do something with image
            guard let self = self else{return}
            self.image = image
            self.data = image?.jpegData(compressionQuality: 1)
        }
        
        asset.requestContentEditingInput(with: PHContentEditingInputRequestOptions()) { [weak self] (eidtingInput, info) in
            guard let self = self else{return}
            if let input = eidtingInput, let photoUrl = input.fullSizeImageURL {
                self.url = photoUrl
            }
        }
        
    }
    
    public static func == (lhs: Asset, rhs: Asset) -> Bool {
        return lhs.asset == rhs.asset
    }
    
}
